package io.piqued.database.event

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface EventDao {
    @Query("SELECT * FROM eventTable")
    fun getAll(): List<Event>

    @Query("SELECT * FROM eventTable")
    fun getAllAsLiveData(): LiveData<List<Event>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(events: List<Event>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllTarget(eventRecord: List<EventTargetRecord>)

    @Delete
    fun delete(event: Event)

    @Query("DELETE FROM eventTargetTable WHERE _event_target_id=:postId ")
    fun deletePost(postId: Long)

    @Query("DELETE FROM eventTable")
    fun resetEventTable()

    @Query("DELETE FROM eventTargetTable")
    fun resetEventTargetTable()

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertUnreadNotification(events: List<EventNotificationRecord>)

    @Query("SELECT * FROM eventNotificationTable")
    fun getAllUnreadRecord(): List<EventNotificationRecord>

    @Query("SELECT * FROM eventTable ORDER BY _created_at DESC LIMIT 1")
    fun getLatestEvent(): List<Event>

    @Query("SELECT * FROM eventTable e INNER JOIN eventNotificationTable n WHERE e._id=n._event_id ORDER BY _created_at DESC")
    fun getAllUnReadEvents(): List<Event>

    @Query("SELECT EXISTS(SELECT * FROM eventTable WHERE _id=:eventId)")
    fun eventExist(eventId: Int): Boolean

    @Query("DELETE FROM eventNotificationTable")
    fun resetEventNotificationTable()
}