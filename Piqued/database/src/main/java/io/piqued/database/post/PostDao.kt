package io.piqued.database.post

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 *
 * Created by Kenny M. Liou on 7/28/18.
 * Piqued Inc.
 *
 */
@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPost(post: CloudPost)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPosts(posts: List<CloudPost>)

    @Query("SELECT * FROM postTable")
    fun getAllPosts(): LiveData<List<CloudPost>>

    @Query("SELECT * FROM postTable WHERE postId = :postId")
    fun getPostAsLiveData(postId: Long): LiveData<CloudPost>

    @Query("SELECT * FROM postTable WHERE postId = :postId")
    fun getPost(postId: Long): CloudPost

    @Query("SELECT * FROM postTable WHERE userId = :userId ORDER BY created_at DESC")
    fun getPostMadeByUser(userId: Long): LiveData<List<CloudPost>>

    @Delete
    fun deletePost(post: CloudPost)

    @Query("DELETE FROM postTable")
    fun reset()
}