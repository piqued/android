package io.piqued.database.post

import com.google.gson.annotations.SerializedName

class GetPostsResponse(
        @SerializedName("posts")
        val posts: List<CloudPost>
)