package io.piqued.database.event

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

/**
 *
 * Created by Kenny M. Liou on 11/11/18.
 * Piqued Inc.
 *
 */
@Database(entities = [Event::class, EventTargetRecord::class, EventNotificationRecord::class], version = 1)
@TypeConverters(EventTargetConverter::class, ActorConverter::class)
abstract class EventDatabase: RoomDatabase() {
    internal abstract fun eventDao(): EventDao

    companion object {

        fun getInstance(context: Context): EventDatabase {
            return Room.databaseBuilder(context, EventDatabase::class.java, "event_database").build()
        }
    }

    fun reset() {
        eventDao().resetEventTable()
        eventDao().resetEventTargetTable()
        eventDao().resetEventNotificationTable()
    }

    /**
     * Return a list of new events based on events
     */
    fun getNewEvents(events: List<Event>): List<Event> {
        val result = ArrayList<Event>()
        val latest = eventDao().getLatestEvent()[0]

        for (event in events) {
            if (event.createdAt > latest.createdAt) {
                result.add(event)
            }
        }
        return result
    }

    fun insertEventsForNotification(events: List<Event>) {
        val list = ArrayList<EventNotificationRecord>()
        for (event in events) {
            list.add(EventNotificationRecord(event.id))
        }

        eventDao().insertUnreadNotification(list)
        insertEvents(events)
    }

    fun getUnreadEvents(): List<Event> {
        return eventDao().getAllUnReadEvents()
    }

    fun insertEvents(events: List<Event>) {
        val list = ArrayList<EventTargetRecord>()
        for(event in events) {
            list.add(EventTargetRecord(event.id, event.eventTarget.id))
        }

        eventDao().insertAllTarget(list)
        eventDao().insertAll(events)
    }

    fun postDeleted(postId: Long) {
        eventDao().deletePost(postId)
    }

    fun getEvents(): LiveData<List<Event>> {
        return eventDao().getAllAsLiveData()
    }

    fun clearNotification() {
        eventDao().resetEventNotificationTable()
    }
}