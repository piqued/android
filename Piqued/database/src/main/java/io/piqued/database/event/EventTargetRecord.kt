package io.piqued.database.event

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "eventTargetTable")
class EventTargetRecord(
        @PrimaryKey
        @ColumnInfo(name = "_event_id")
        val id: Int,
        @ColumnInfo(name = "_event_target_id")
        val targetId: Long
)