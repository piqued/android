package io.piqued.database.util

import android.content.Context
import android.util.Log
import io.piqued.database.BuildConfig
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.logging.FileHandler
import java.util.logging.Level
import java.util.logging.Logger
import java.util.logging.SimpleFormatter


/**
 * Created by kennymliou on 11/16/16.
 * Piqued Inc.
 */

class PiquedLogger {

    interface Callback {
        fun onLogFileReady(file: File)
    }

    companion object {

        private const val LOG_FILE_PREFERENCE = "log_file_preference"
        private const val LOG_FILE_INDEX = "log_file_index"
        private const val LOG_FILE_NAME = "piqued_logfile_"
        private const val MAX_FILE_INDEX = 10

        private val logger = Logger.getLogger("Piqued Logger")
        private lateinit var filePattern: String
        private lateinit var fileHandler: FileHandler

        fun initialize(context: Context) {

            fileHandler = FileHandler(getFilePattern(context), 1024 * 1024, 10, true) // 1MB

            fileHandler.formatter = SimpleFormatter()
            fileHandler.level = Level.FINE

            logger.addHandler(fileHandler)

            filePattern = getFilePattern(context)

            logger.level = Level.FINE
        }

        fun onTerminate() {
            fileHandler.close()
            logger.removeHandler(fileHandler)
        }

        fun getLogFileList(context: Context): ArrayList<File> {

            val fileList = ArrayList<File>()

            for (i in 0..MAX_FILE_INDEX) {
                val file = getFile(context, i)

                if (file.exists()) {
                    fileList.add(file)
                }
            }

            return fileList
        }

        fun getCurrentLogFile(context: Context): File {

            val preference = context.getSharedPreferences(LOG_FILE_PREFERENCE, Context.MODE_PRIVATE);
            val fileIndex = preference.getInt(LOG_FILE_INDEX, 0)

            return getFile(context, fileIndex)
        }

        private fun getFile(context: Context, index: Int): File {
            return File(context.filesDir.absolutePath + File.separator + LOG_FILE_NAME + index + ".log")
        }

        private fun getFilePattern(context: Context): String {
            return context.filesDir.absolutePath + File.separator + LOG_FILE_NAME + "%g.log"
        }

        fun prepareLogFiles(context: Context, callback: Callback) {

            Thread(Runnable {

                val file = getAllLogFiles(context)

                callback.onLogFileReady(file)

            }).start()
        }

        private fun getAllLogFiles(context: Context): File {

            val tempFile = File.createTempFile("piqued_logfile_", ".txt", context.externalCacheDir)
            val outputStream = FileOutputStream(tempFile)

            for (index in 0 until MAX_FILE_INDEX) {
                val file = getFile(context, index)

                if (file.exists()) {
                    val fileInputStream = FileInputStream(file)
                    var count: Int
                    val temp = ByteArray(4096)
                    do {
                        count = fileInputStream.read(temp)
                        if (count > -1) {
                            outputStream.write(temp, 0, count)
                        }
                    } while (count != -1)
                    fileInputStream.close()
                }
            }

            outputStream.close()

            return tempFile
        }

        private fun writeMessageToLog(level: Level, message: String) {

            val stackTraceElement = Thread.currentThread().stackTrace[4]
            logger.log(level, "\n${stackTraceElement.className}: $message")

            fileHandler.flush()
        }

        @JvmStatic
        fun i(tag: String, message: String) {
            writeMessageToLog(Level.INFO, "$tag $message")
        }

        @JvmStatic
        fun d(tag: String, message: String) {

            if (BuildConfig.DEBUG) {
                Log.d(tag, message)
                writeMessageToLog(Level.FINE, "$tag $message")
            }
        }

        @JvmStatic
        fun w(tag: String, message: String) {
            if (BuildConfig.DEBUG) {
                Log.w(tag, message)
            }
            writeMessageToLog(Level.WARNING, "$tag $message")
        }

        @JvmStatic
        fun e(tag: String, message: String, errorCode: PiquedErrorCode?, payload: String?) {
            e(tag, errorCode, "$message: $payload")
        }

        @JvmStatic
        fun e(tag: String, errorCode: PiquedErrorCode?, message: String?) {

            if (errorCode != null) {
                if (BuildConfig.DEBUG) {
                    Log.e(tag, message)
                }
                writeMessageToLog(Level.SEVERE, "$tag\n${errorCode.name} $message")
            } else {
                if (BuildConfig.DEBUG) {
                    Log.e(tag, message)
                }
                writeMessageToLog(Level.SEVERE, "$tag\n$message")
            }
        }

        @JvmStatic
        fun e(tag: String, message: String) {
            e(tag, null, message)
        }

        @JvmStatic
        fun e(throwable: Throwable?, message: String?) {
            if (BuildConfig.DEBUG) {
                Log.e("", message, throwable)
            }
            writeMessageToLog(Level.SEVERE, "$throwable\n$message")
        }

        @JvmStatic
        fun e(tag: String, e: Exception?) {
            if (e != null) {
                if (BuildConfig.DEBUG) {
                    Log.e(tag, "", e)
                }
                writeMessageToLog(Level.SEVERE, "$tag\n${e.stackTrace}")
            }
        }
    }

}
