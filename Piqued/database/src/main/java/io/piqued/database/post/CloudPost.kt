package io.piqued.database.post

import android.os.Parcel
import android.os.Parcelable
import androidx.room.*
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "postTable")
@TypeConverters(PiquedImageTypeConverters::class, ReactionsTypeConverters::class)
class CloudPost(
        @PrimaryKey
        @SerializedName("id") @ColumnInfo(name = "postId") val postId: Long,
        @SerializedName("latitude") @ColumnInfo(name = "latitude") protected val latitude: Double,
        @SerializedName("longitude") @ColumnInfo(name = "longitude") protected val longitude: Double,
        @SerializedName("locationName") @ColumnInfo(name = "locationName") val locationName: String?,
        @SerializedName("address") @ColumnInfo(name = "address") val address: String?,
        @SerializedName("userId") @ColumnInfo(name = "userId") val userId: Long,
        @SerializedName("userName") @ColumnInfo(name = "userName") val userName: String,
        @SerializedName("userImage") @ColumnInfo(name = "userImage") protected val userImage: String?,
        @SerializedName("postTitle") @ColumnInfo(name = "title") val title: String,
        @SerializedName("postDescription") @ColumnInfo(name = "description") val description: String,
        @SerializedName("foursquareVenueID") @ColumnInfo(name = "foursquareVenueID") val fourSquareId: String?,
        @SerializedName("foursquare_cc") @ColumnInfo(name = "foursquareCountryCode") val countryCode: String?,
        @SerializedName("foursquare_state") @ColumnInfo(name = "foursquareStateName") val stateName: String?,
        @SerializedName("foursquare_city") @ColumnInfo(name = "foursquareCityName") val cityName: String?,
        @SerializedName("yelp_id") @ColumnInfo(name = "yelpId") val yelpId: String?,
        @SerializedName("createdAt") @ColumnInfo(name = "created_at") val createdAt: Long,
        @SerializedName("primaryColor") @ColumnInfo(name = "primaryColor") val primaryColor: String?,
        @SerializedName("bookmarked") @ColumnInfo(name = "bookmarked") var bookmarked: Boolean,
        @SerializedName("myReaction") @ColumnInfo(name = "myReaction") var myReaction: Int,
        @SerializedName("images") @ColumnInfo(name = "images") val images: List<PiquedImage>,
        @SerializedName("sharePath") @ColumnInfo(name = "sharePath") val sharePath: String?,
        @SerializedName("totalBookmarks") @ColumnInfo(name = "totalBookmarks") var totalBookmarks: Int,
        @SerializedName("reactions") @ColumnInfo(name = "reactions") var reactions: Reactions?
        ): Parcelable {

    @Ignore
    private var latLng: LatLng? = null

    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readString(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readString() ?: "",
            parcel.readString(),
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readInt(),
            Arrays.asList(*parcel.readParcelableArray(PiquedImage::class.java.classLoader)) as List<PiquedImage>,
            parcel.readString(),
            parcel.readInt(),
            parcel.readParcelable(Reactions::class.java.classLoader)) {
        latLng = parcel.readParcelable(LatLng::class.java.classLoader)
    }

    fun getLatLng(): LatLng {
        if (latLng == null) {
            latLng = LatLng(latitude, longitude)
        }
        return latLng!!
    }

    fun getImageUrl(): String {
        return images[0].largeImageUrl
    }

    fun getSmallImageUrl(): String {
        return images[0].smallImageUrl
    }

    fun getUserImageUrl(): String {
        return userImage ?: ""
    }

    fun liked() : Boolean {
        return Reaction.LIKE.ordinal == myReaction
    }

    fun getTotalLikes(): Int {
        return reactions?.totalLikes ?: 0
    }

    fun setTotalLikes(likes: Int) {
        reactions = Reactions(likes)
    }

    fun mergeNewData(newPostData: CloudPost?) {

        if (newPostData == null) return

        myReaction = newPostData.myReaction
        reactions = newPostData.reactions
        totalBookmarks = newPostData.totalBookmarks
        bookmarked = newPostData.bookmarked
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CloudPost

        if (postId != other.postId) return false

        return true
    }

    override fun hashCode(): Int {
        return postId.hashCode()
    }

    enum class Reaction {
        NONE,
        LIKE;

        companion object {
            private val values = Reaction.values()

            fun fromInt(index: Int): Reaction {
                if (index > -1 && index < values.size) {
                    return values[index]
                }

                throw IllegalArgumentException("Illegal index value for enum " + Reaction::class.java.simpleName + ": " + index)
            }
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(postId)
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeString(locationName)
        parcel.writeString(address)
        parcel.writeLong(userId)
        parcel.writeString(userName)
        parcel.writeString(userImage)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(fourSquareId)
        parcel.writeString(countryCode)
        parcel.writeString(stateName)
        parcel.writeString(cityName)
        parcel.writeString(yelpId)
        parcel.writeLong(createdAt)
        parcel.writeString(primaryColor)
        parcel.writeByte(if (bookmarked) 1 else 0)
        parcel.writeInt(myReaction)
        parcel.writeParcelable(latLng, flags)
        parcel.writeParcelableArray(images.toTypedArray(), flags)
        parcel.writeString(sharePath ?: "")
        parcel.writeInt(totalBookmarks)
        parcel.writeParcelable(reactions, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CloudPost> {
        override fun createFromParcel(parcel: Parcel): CloudPost {
            return CloudPost(parcel)
        }

        override fun newArray(size: Int): Array<CloudPost?> {
            return arrayOfNulls(size)
        }
    }


}
