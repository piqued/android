package io.piqued.database.event

import android.text.TextUtils
import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity(tableName = "eventTable",
        foreignKeys = [
                ForeignKey(
                        entity = EventTargetRecord::class,
                        parentColumns = ["_event_id"],
                        childColumns = ["_id"],
                        onDelete = ForeignKey.CASCADE)
])
class Event (
        @PrimaryKey()
        @SerializedName("id") @ColumnInfo(name = "_id")
        val id: Int,
        @SerializedName("created_at") @ColumnInfo(name = "_created_at")
        val createdAt: Long,
        @SerializedName("eventTarget") @ColumnInfo(name = "_eventTarget")
        val eventTarget: EventTarget,
        @SerializedName("actor") @ColumnInfo(name = "_actor")
        val actor: Actor,
        @SerializedName("eventType") @ColumnInfo(name = "_eventType")
        val eventType: String,
        @SerializedName("message") @ColumnInfo(name = "_message")
        val message: String
) {
        @Ignore
        private var mType: EventType? = null

        val type: EventType?
                get() {
                        if (mType == null && !TextUtils.isEmpty(eventType)) {
                                mType = EventType.fromString(eventType)
                        }
                        return mType
                }

        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as Event

                if (id != other.id) return false

                return true
        }

        override fun hashCode(): Int {
                return id
        }
}