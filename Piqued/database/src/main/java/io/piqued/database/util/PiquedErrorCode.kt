package io.piqued.database.util

/**
 * Piqued
 *
 *
 * Created by Kenny M. Liou on 11/22/16.
 *
 *
 * Piqued Inc.
 */
enum class PiquedErrorCode {
    DEVICE_INIT_ERROR,
    JSON,
    NETWORK,
    NETWORK_404,
    NETWORK_403_FORBIDDEN,
    POST_DELETED,
    INVALID_ARGUMENT,
    PERMISSION_DENIED,
    POST_NOT_VALID,
    USER_AUTH_FAILED,
    USER_LOGIN_REQUIRED,
    UNKNOWN
}