package io.piqued.database.user

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 *
 * Created by Kenny M. Liou on 11/11/18.
 * Piqued Inc.
 *
 */
@Dao
interface FollowingUserDao {

    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    fun insertUserRecord(user: FollowingUser)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserRecords(users: List<FollowingUser>)

    @Query("SELECT * FROM userTable INNER JOIN followingUserTable WHERE _userId=_followingUserId")
    fun getFollowingUsers(): LiveData<List<User>>

    @Query("DELETE FROM followingUserTable")
    fun reset()
}