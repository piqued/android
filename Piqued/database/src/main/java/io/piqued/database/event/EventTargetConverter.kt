package io.piqued.database.event

import androidx.room.TypeConverter
import com.google.gson.Gson

class EventTargetConverter {
    @TypeConverter
    fun toString(target: EventTarget): String {
        return Gson().toJson(target)
    }
    @TypeConverter
    fun getEventTarget(eventTargetString: String): EventTarget {
        return Gson().fromJson(eventTargetString, EventTarget::class.java)
    }
}