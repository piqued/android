package io.piqued.database

import android.provider.MediaStore

/**
 *
 * Created by Kenny M. Liou on 8/18/18.
 * Piqued Inc.
 *
 */
class Constants {
    companion object {
        val ImageDataProjection = arrayOf(MediaStore.Images.ImageColumns._ID, MediaStore.Images.ImageColumns.DATA, MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, MediaStore.Images.ImageColumns.DATE_TAKEN, MediaStore.Images.ImageColumns.MIME_TYPE)
    }
}