package io.piqued.database.event

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "eventNotificationTable")
class EventNotificationRecord(
        @PrimaryKey
        @ColumnInfo(name = "_event_id")
        val id: Int
)