package io.piqued.database.post

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface HomeListPostDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPostRecord(post: HomeListPost)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPostRecords(posts: List<HomeListPost>)

    @Query("SELECT * FROM postTable INNER JOIN homeListPostTable ON postTable.postId=homeListPostTable.postId WHERE homeListPostTable.latitude BETWEEN :latMin AND :latMax AND homeListPostTable.longitude BETWEEN :lngMin AND :lngMax")
    fun getAllPosts(latMin: Double, latMax: Double,lngMin: Double, lngMax: Double): LiveData<List<CloudPost>>

    @Query("DELETE FROM homeListPostTable")
    fun reset()

    @Query("DELETE FROM homeListPostTable WHERE homeListPostTable.postId=:postId")
    fun deletePost(postId: Long)
}