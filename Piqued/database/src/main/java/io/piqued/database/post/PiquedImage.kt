package io.piqued.database.post

import androidx.room.TypeConverter
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import java.io.Serializable
import java.util.*

/**
 *
 * Created by Kenny M. Liou on 7/28/18.
 * Piqued Inc.
 *
 */

class PiquedImageTypeConverters: Serializable {

    @TypeConverter
    fun stringToPiquedImageList(data: String?): List<PiquedImage> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<PiquedImage>>() {
        }.type

        return Gson().fromJson(data, listType)
    }

    @TypeConverter
    fun piquedImageListToString(piquedImages : List<PiquedImage>): String {
        val listType = object : TypeToken<List<PiquedImage>>() {
        }.type

        return Gson().toJson(piquedImages, listType)
    }
}

class PiquedImage(
        @SerializedName("sm") val smallImageUrl: String,
        @SerializedName("lg") val largeImageUrl: String
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString() ?: "",
            parcel.readString() ?: "") {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(smallImageUrl)
        parcel.writeString(largeImageUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PiquedImage> {
        override fun createFromParcel(parcel: Parcel): PiquedImage {
            return PiquedImage(parcel)
        }

        override fun newArray(size: Int): Array<PiquedImage?> {
            return arrayOfNulls(size)
        }
    }
}