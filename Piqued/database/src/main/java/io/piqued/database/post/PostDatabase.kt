package io.piqued.database.post

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import io.piqued.database.UserId
import io.piqued.database.bookmark.Bookmark
import io.piqued.database.bookmark.BookmarkDao

/**
 *
 * Created by Kenny M. Liou on 7/28/18.
 * Piqued Inc.
 *
 */
@Database(entities = [CloudPost::class, HomeListPost::class, Bookmark::class], version = 6)
abstract class PostDatabase: RoomDatabase() {

    protected abstract val postDao: PostDao
    abstract val homeListPostDao: HomeListPostDao
    abstract val bookmarkDao: BookmarkDao

    companion object {
        fun getInstance(context: Context): PostDatabase {
            return Room.databaseBuilder(context, PostDatabase::class.java, "post_database")
                    .addMigrations(CloudPostMigration.MIGRATION_4_5)
                    .addMigrations(CloudPostMigration.MIGRATION_5_6)
                    .build()
        }
    }

    fun getAllPosts(): LiveData<List<CloudPost>> {
        return postDao.getAllPosts()
    }

    fun getPostAsLiveData(postId: Long): LiveData<CloudPost> {
        return postDao.getPostAsLiveData(postId)
    }

    fun getPost(postId: Long): CloudPost {
        return postDao.getPost(postId)
    }

    fun getUserPostHistory(userId: UserId): LiveData<List<CloudPost>> {
        return postDao.getPostMadeByUser(userId.userId)
    }

    fun addPostsToCache(posts: List<CloudPost>) {
        postDao.insertPosts(posts)
    }

    fun addPostToCache(post: CloudPost) {
        postDao.insertPost(post)
    }

    fun deletePost(post: CloudPost) {
        homeListPostDao.deletePost(post.postId)
        bookmarkDao.deletePost(post.postId)
        postDao.deletePost(post)
    }

    fun reset() {
        homeListPostDao.reset()
        bookmarkDao.reset()
        postDao.reset()
    }
}