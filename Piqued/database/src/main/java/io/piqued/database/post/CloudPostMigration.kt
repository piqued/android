package io.piqued.database.post

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

class CloudPostMigration {

    companion object {
        val MIGRATION_4_5 = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {

                database.execSQL("ALTER TABLE postTable "
                + "ADD COLUMN sharePath TEXT")
            }
        }

        val MIGRATION_5_6 = object : Migration(5, 6) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE postTable "
                        + "ADD COLUMN totalBookmarks INTEGER NOT NULL DEFAULT (0)")
                database.execSQL("ALTER TABLE postTable "
                + "ADD COLUMN reactions TEXT")
            }

        }
    }
}