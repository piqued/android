package io.piqued.database.event

import androidx.room.TypeConverter
import com.google.gson.Gson

class ActorConverter {
    @TypeConverter
    fun toString(actor: Actor): String {
        return Gson().toJson(actor)
    }
    @TypeConverter
    fun getActor(actorString: String): Actor {
        return Gson().fromJson(actorString, Actor::class.java)
    }
}