package io.piqued.database.event

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

class Actor(
        @SerializedName("id")
        val id: Long,
        @SerializedName("name") @ColumnInfo(name = "_name")
        val name: String,
        @SerializedName("total_posts") @ColumnInfo(name = "_total_posts")
        val totalPostCount: Int,
        @SerializedName("followers") @ColumnInfo(name = "_followers")
        val followerCount: Int,
        @SerializedName("following") @ColumnInfo(name = "_following")
        val followingCount: Int,
        @SerializedName("followed") @ColumnInfo(name = "_followed")
        val followed: Boolean,
        @SerializedName("profileImage") @ColumnInfo(name = "_profileImage")
        val profileImage: String?,
        @SerializedName("createdAt") @ColumnInfo(name = "_createdAt")
        val createdAt: Long
)