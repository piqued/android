package io.piqued.database.event

enum class EventType {
    Bookmark, Comment, Reaction, Relationship;

    companion object {
        private val values: Array<EventType> = EventType.values()
        fun fromString(string: String): EventType {
            for (i in values.indices) {
                val type = values[i]
                if (type.name == string) {
                    return type
                }
            }
            throw IllegalArgumentException("Cannot convert string to EventType: $string")
        }
    }
}