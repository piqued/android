package io.piqued.database.bookmark

import androidx.room.Entity
import androidx.room.ForeignKey
import io.piqued.database.post.CloudPost

/**
 *
 * Created by Kenny M. Liou on 7/21/18.
 * Piqued Inc.
 *
 */
@Entity(tableName = "bookmarkDatabase",
        primaryKeys = ["postId"],
        foreignKeys = [ForeignKey(
                entity = CloudPost::class,
                parentColumns = ["postId"],
                childColumns = ["postId"]
        )])
class Bookmark(
        val postId: Long
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Bookmark

        if (postId != other.postId) return false

        return true
    }

    override fun hashCode(): Int {
        return postId.hashCode()
    }
}