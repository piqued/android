package io.piqued.database.user

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Piqued
 *
 *
 * Created by Kenny M. Liou on 11/19/16.
 *
 *
 * Piqued Inc.
 */

@Entity(tableName = "userTable")
class User(
        @PrimaryKey
        @SerializedName("id") @ColumnInfo(name = "_userId")
        val userId: Long,
        @SerializedName("name") @ColumnInfo(name = "_userName")
        val name: String,
        @SerializedName("total_posts") @ColumnInfo(name = "_totalPostCount")
        val totalPostCount: Int,
        @SerializedName("followers") @ColumnInfo(name = "_followerCount")
        val followerCount: Int,
        @SerializedName("following") @ColumnInfo(name = "_followingCount")
        val followingCount: Int,
        @SerializedName("followed") @ColumnInfo(name = "_isFollowed")
        var isFollowed: Boolean,
        @SerializedName("totalBookmarks") @ColumnInfo(name = "_bookmarkCount")
        val bookmarkCount: Int,
        @SerializedName("totalReactions") @ColumnInfo(name = "_likeCount")
        val likeCount: Int,
        @SerializedName("totalComments") @ColumnInfo(name = "_commentCount")
        val commentCount: Int,
        @SerializedName("profileImage") @ColumnInfo(name = "_profileImageUrl")
        val profileImageUrl: String?,
        @SerializedName("createdAt") @ColumnInfo(name = "_userCreatedAt")
        val createdAt: Long,
        @SerializedName("email") @ColumnInfo(name = "_userEmail")
        val email: String = ""
)