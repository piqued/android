package io.piqued.database.post

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/15/17.
 * Piqued Inc.
 */
class PostId(
        val postId: Long
) {
    override fun equals(other: Any?): Boolean {

        return if (other != null && other is PostId) {
            postId == other.postId
        } else false
    }

    override fun hashCode(): Int {
        return postId.hashCode()
    }
}
