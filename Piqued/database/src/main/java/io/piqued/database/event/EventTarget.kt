package io.piqued.database.event

import com.google.gson.annotations.SerializedName
import io.piqued.database.post.PiquedImage

class EventTarget (
        @SerializedName("id")
        val id: Long,
        @SerializedName("latitude")
        val latitude: Double,
        @SerializedName("longitude")
        val longitude: Double,
        @SerializedName("locationName")
        val locationName: String,
        @SerializedName("address")
        val address: String,
        @SerializedName("userId")
        val userId: Long,
        @SerializedName("userName")
        val userName: String,
        @SerializedName("userImage")
        val userImage: String,
        @SerializedName("postTitle")
        val postTitle: String,
        @SerializedName("postDescription")
        val postDescription: String,
        @SerializedName("foursquareVenueID")
        val foursquareVenueID: String,
        @SerializedName("createdAt")
        val createdAt: Long,
        @SerializedName("bookmarked")
        val bookmarked: Boolean,
        @SerializedName("myReaction")
        val myReaction: Int,
        @SerializedName("images")
        private val mImages: Array<PiquedImage>? = null
) {
        val smallImages: String
                get() = if (mImages != null && mImages.isNotEmpty()) {
                        mImages[0].smallImageUrl
                } else ""
}