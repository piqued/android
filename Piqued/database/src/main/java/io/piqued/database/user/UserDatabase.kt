package io.piqued.database.user

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 *
 * Created by Kenny M. Liou on 11/11/18.
 * Piqued Inc.
 *
 */
@Database(entities = [User::class, FollowingUser::class], version = 1)
abstract class UserDatabase: RoomDatabase() {

    internal abstract val userDao: UserDao
    internal abstract val followingUserDao: FollowingUserDao

    companion object {
        fun getInstance(context: Context): UserDatabase {
            return Room.databaseBuilder(context, UserDatabase::class.java, "user_database").build()
        }
    }

    fun updateUserFollowing(users: List<User>) {
        userDao.insertUsers(users)

        val followingUsers = ArrayList<FollowingUser>()

        for (user in users) {
            val followingUser = FollowingUser(user.userId, user.name, user.profileImageUrl ?: "")
            followingUsers.add(followingUser)
        }

        followingUserDao.reset()
        followingUserDao.insertUserRecords(followingUsers)
    }

    fun getFollowingUsers(): LiveData<List<User>> {

        return followingUserDao.getFollowingUsers()
    }

    fun updateUser(user: User) {
        userDao.insertUser(user)
    }

    fun getUser(userId: Long): LiveData<User> {
        return userDao.getUserAsLiveData(userId)
    }

    fun reset() {
        userDao.reset()
        followingUserDao.reset()
    }
}