package io.piqued.database

/**
 *
 * Created by Kenny M. Liou on 9/7/18.
 * Piqued Inc.
 *
 */
data class UserId(
        val userId: Long
)