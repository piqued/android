package io.piqued.database.album

import android.content.Context
import android.database.Cursor
import android.provider.MediaStore
import android.text.TextUtils
import io.piqued.database.Constants

/**
 *
 * Created by Kenny M. Liou on 8/18/18.
 * Piqued Inc.
 *
 */
data class ImageAlbum(
        val mediaId: String,
        val path: String,
        val bucketName: String,
        val bucketId: String) : Comparable<ImageAlbum> {

    override fun toString(): String {
        return bucketName
    }

    fun createCursor(context: Context): Cursor {

        return context.contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                Constants.ImageDataProjection,
                MediaStore.Images.Media.BUCKET_ID + "=?",
                arrayOf(bucketId),
                MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC"
                )!!
    }

    companion object {
        @JvmStatic
        fun createAllImageCursor(context: Context): Cursor {
            return context.contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    Constants.ImageDataProjection,
                    null,
                    null,
                    MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC"
            )!!
        }
    }

    override fun equals(other: Any?): Boolean {
        return (other is ImageAlbum) and ((other as ImageAlbum).bucketName == bucketName) and (other.path == path)
    }

    override fun compareTo(other: ImageAlbum): Int {
        if (TextUtils.isEmpty(other.bucketName) && TextUtils.isEmpty(this.bucketName)) {
            return 0
        }

        if (TextUtils.isEmpty(other.bucketName)) {
            return 1
        }

        if (TextUtils.isEmpty(this.bucketName)) {
            return -1
        }

        return this.bucketName.compareTo(other.bucketName)
    }
}