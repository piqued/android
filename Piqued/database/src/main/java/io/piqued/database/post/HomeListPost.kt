package io.piqued.database.post

import androidx.room.*

@Entity(tableName = "homeListPostTable",
        primaryKeys = ["postId"],
        foreignKeys = [
                (ForeignKey(entity = CloudPost::class,
                        parentColumns = ["postId"],
                        childColumns = ["postId"]))
        ]
)
class HomeListPost(
        @ColumnInfo(name = "postId") val postId: Long,
        @ColumnInfo(name = "latitude") val latitude: Double,
        @ColumnInfo(name = "longitude") val longitude: Double
)