package io.piqued.database.post

import android.os.Parcel
import android.os.Parcelable
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import java.io.Serializable

class ReactionsTypeConverters: Serializable {
    @TypeConverter
    fun stringToReactions(data: String?): Reactions {
        if (data.isNullOrEmpty()) {
            return Reactions(0)
        }

        val reactionsType = object : TypeToken<Reactions>() {
        }.type

        return Gson().fromJson(data, reactionsType)
    }

    @TypeConverter
    fun reactionsToString(reactions: Reactions?): String {

        if (reactions == null) return ""

        val reactionsType = object : TypeToken<Reactions>() {
        }.type

        return Gson().toJson(reactions, reactionsType)
    }
}

class Reactions(
        @SerializedName("totalLikes") val totalLikes: Int
): Parcelable {

    constructor(parcel: Parcel) : this(parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(totalLikes)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Reactions> {
        override fun createFromParcel(parcel: Parcel): Reactions {
            return Reactions(parcel)
        }

        override fun newArray(size: Int): Array<Reactions?> {
            return arrayOfNulls(size)
        }
    }
}