package io.piqued.database.bookmark

import androidx.lifecycle.LiveData
import androidx.room.*
import io.piqued.database.post.CloudPost


/**
 *
 * Created by Kenny M. Liou on 7/21/18.
 * Piqued Inc.
 *
 */
@Dao
interface BookmarkDao {

    @Query("SELECT * FROM bookmarkDatabase")
    fun getAllBookmarks(): LiveData<List<Bookmark>>


    @Query("SELECT * FROM postTable INNER JOIN bookmarkDatabase ON postTable.postId=bookmarkDatabase.postId")
    fun getAllBookmarksAsPost(): LiveData<List<CloudPost>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertBookmark(vararg bookmarks: Bookmark)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertBookmarks(bookmarks: List<Bookmark>)

    @Delete
    fun delete(bookmark: Bookmark)

    @Query("DELETE FROM bookmarkDatabase")
    fun reset()

    @Query("DELETE FROM bookmarkDatabase WHERE bookmarkDatabase.postId=:postId")
    fun deletePost(postId: Long)
}