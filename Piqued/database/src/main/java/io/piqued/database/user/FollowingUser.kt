package io.piqued.database.user

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey

/**
 *
 * Created by Kenny M. Liou on 11/11/18.
 * Piqued Inc.
 *
 */
@Entity(tableName = "followingUserTable",
        primaryKeys = ["_followingUserId"],
        foreignKeys = [
            (ForeignKey(entity = User::class,
                    parentColumns = ["_userId"],
                    childColumns = ["_followingUserId"]))
        ]
)
class FollowingUser(
        @ColumnInfo(name = "_followingUserId") val userId: Long,
        @ColumnInfo(name = "_followingUserName") val userName: String,
        @ColumnInfo(name = "_followingUserImageUrl") val userImageUrl: String
)