package io.piqued.database.user

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 *
 * Created by Kenny M. Liou on 11/11/18.
 * Piqued Inc.
 *
 */
@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsers(users: List<User>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(users: User)

    @Query("SELECT * FROM userTable WHERE _userId = :userId")
    fun getUser(userId: Long): User

    @Query("SELECT * FROM userTable WHERE _userId = :userId")
    fun getUserAsLiveData(userId: Long): LiveData<User>

    @Query("DELETE FROM userTable")
    fun reset()
}