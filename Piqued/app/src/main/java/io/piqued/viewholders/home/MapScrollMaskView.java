package io.piqued.viewholders.home;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 7/1/17.
 * Piqued Inc.
 */

public class MapScrollMaskView extends RelativeLayout {

    public MapScrollMaskView(Context context) {
        super(context);
    }

    public MapScrollMaskView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MapScrollMaskView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
