package io.piqued.viewholders.settings;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import io.piqued.views.settings.Settings;
import io.piqued.views.settings.SettingsDelegate;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/11/17.
 * Piqued Inc.
 */

public abstract class SettingsBaseViewHolder extends RecyclerView.ViewHolder{

    protected Settings mType;
    protected SettingsDelegate mDelegate;

    public SettingsBaseViewHolder(View itemView) {
        super(itemView);
    }

    public void setSettingType(Settings type) {
        mType = type;
    }

    public void setDelegate(SettingsDelegate delegate) {
        mDelegate = delegate;
    }

    public abstract void onBindingData();
}
