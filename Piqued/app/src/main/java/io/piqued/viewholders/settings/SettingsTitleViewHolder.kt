package io.piqued.viewholders.settings

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout

import java.security.InvalidParameterException

import androidx.annotation.StringRes
import io.piqued.R
import io.piqued.views.settings.Settings
import io.piqued.views.settings.SettingsCategory
import kotlinx.android.synthetic.main.view_holder_settings_title.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/11/17.
 * Piqued Inc.
 */

class SettingsTitleViewHolder(parent: ViewGroup)
    : SettingsBaseViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_settings_title, parent, false)
) {

    fun setCategory(group: SettingsCategory) {

        @StringRes val titleRes: Int = when (group) {
            SettingsCategory.FOLLOWING -> R.string.settings_title_following
            SettingsCategory.SOCIAL_NETWORK -> R.string.settings_title_social_network
            SettingsCategory.MY_ACCOUNT -> R.string.settings_title_my_account
            SettingsCategory.NOTIFICATIONS -> R.string.settings_title_notification_settings
            SettingsCategory.SECURITY -> R.string.settings_title_security
            SettingsCategory.DEVELOPER -> R.string.settings_title_developer
            else -> throw IllegalArgumentException("Illegal category " + group.name + ", is this a new enum value?")
        }

        if (titleRes != 0) {
            itemView.title_text.setText(titleRes)
        } else {
            throw InvalidParameterException("Invalid string res value: " + titleRes + " for enum" + group.name)
        }
    }

    override fun onBindingData() {

        val padding: Int

        if (mType === Settings.FOLLOWING) {
            padding = 0
        } else {
            padding = itemView.context.resources.getDimension(R.dimen.settings_section_item_padding).toInt()
        }

        val params = itemView.title_text.layoutParams as LinearLayout.LayoutParams
        params.setMargins(0, padding, 0, 0)
    }
}
