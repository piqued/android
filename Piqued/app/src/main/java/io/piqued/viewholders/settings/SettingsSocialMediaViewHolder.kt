package io.piqued.viewholders.settings

import android.content.Context
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.ViewGroup

import io.piqued.R
import io.piqued.views.settings.Settings
import kotlinx.android.synthetic.main.view_holder_settings_social_media.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/11/17.
 * Piqued Inc.
 */

class SettingsSocialMediaViewHolder(parent: ViewGroup)
    : SettingsBaseViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_settings_social_media, parent, false)
) {
    private fun setName(connect: Boolean, @StringRes name: Int) {

        val sb = StringBuilder()
        sb.append(itemView.context.getString(if (connect) R.string.settings_disconnect else R.string.settings_connect))
        sb.append(" ")
        sb.append(itemView.context.getString(name))

        itemView.title.text = sb
    }

    private fun setImage(@DrawableRes drawableRes: Int) {

        itemView.icon.setImageDrawable(ContextCompat.getDrawable(itemView.context, drawableRes))
    }

    override fun onBindingData() {

        val isOn = false

        when (mType) {
            Settings.SOCIAL_NETWORK_FB -> {
                setName(isOn, R.string.settings_facebook)
                setImage(if (isOn) R.drawable.facebook_on else R.drawable.facebook_off)
            }
            Settings.SOCIAL_NETWORK_TW -> {
                setName(isOn, R.string.settings_twitter)
                setImage(if (isOn) R.drawable.twitter_on else R.drawable.twitter_off)
            }
            Settings.SOCIAL_NETWORK_IN -> {
                setName(isOn, R.string.settings_instagram)
                setImage(if (isOn) R.drawable.instagram_on else R.drawable.instagram_off)
            }
            else -> throw IllegalArgumentException("Illegal type for view holder: " + SettingsSocialMediaViewHolder::class.java.name + ", type: " + mType.name)
        }


    }
}
