package io.piqued.viewholders.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.R
import kotlinx.android.synthetic.main.view_holder_map_pull_handle.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 7/1/17.
 * Piqued Inc.
 */

class MapPullHandleViewHolder(parent: ViewGroup)
    : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_map_pull_handle, parent, false)
) {
    fun setIconVisible(visible: Boolean) {

        itemView.pullHandle.visibility = if (visible) View.VISIBLE else View.INVISIBLE
    }
}
