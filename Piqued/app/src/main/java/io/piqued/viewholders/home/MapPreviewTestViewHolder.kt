package io.piqued.viewholders.home

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R
import kotlinx.android.synthetic.main.view_holder_map_preview_test.view.*

/**
 *
 * Created by Kenny M. Liou on 2/24/18.
 * Piqued Inc.
 *
 */
class MapPreviewTestViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.view_holder_map_preview_test, parent, false)
        ) {

    fun setTextView(position: Int) {

        itemView.text_view.text = position.toString()
    }

}