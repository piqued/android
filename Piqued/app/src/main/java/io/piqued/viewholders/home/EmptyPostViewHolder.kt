package io.piqued.viewholders.home

import android.graphics.Outline
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import androidx.recyclerview.widget.RecyclerView
import io.piqued.R
import kotlinx.android.synthetic.main.view_holder_empty_post.view.*

/**
 *
 * Created by Kenny M. Liou on 1/27/18.
 * Piqued Inc.
 *
 */
class EmptyPostViewHolder(parent: ViewGroup) :
    RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_empty_post, parent, false)
    ) {
    init {

        val cornerRadius = itemView.context.resources.getDimensionPixelSize(R.dimen.space_08).toFloat()

        itemView.emptyViewImageView.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                outline?.setRoundRect(0, 0, view!!.width, (view.height + cornerRadius).toInt(), cornerRadius)
            }
        }
        itemView.emptyViewImageView.clipToOutline = true
    }
}