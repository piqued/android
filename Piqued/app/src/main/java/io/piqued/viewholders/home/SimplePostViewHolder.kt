package io.piqued.viewholders.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.kingfisher.easyviewindicator.RecyclerViewIndicator
import io.piqued.R
import io.piqued.utils.customview.CheckableImageButton
import io.piqued.views.widget.BasePostViewHolder
import kotlinx.android.synthetic.main.view_holder_simple_post.view.*

/**
 *
 * Created by Kenny M. Liou on 11/3/18.
 * Piqued Inc.
 *
 */
class SimplePostViewHolder(parent: ViewGroup)
    : BasePostViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.view_holder_simple_post, parent, false)
) {
    init {
        initializeView()
    }

    override val mapButton: ImageButton?
        get() = itemView.simplePostMapButton

    override val userImage: ImageView
        get() = itemView.simplePostUserImage
    override val postRecyclerView: RecyclerView
        get() = itemView.simplePostRecyclerView
    override val postImageCount: TextView
        get() = itemView.simplePostImageCount
    override val postImageIndicator: RecyclerViewIndicator
        get() = itemView.simplePostImageIndicator
    override val distanceImage: ImageView
        get() = itemView.simplePostDistanceImage
    override val infoBubbleView: View
        get() = itemView.simplePostInfoBubble
    override val postTime: TextView
        get() = itemView.simplePostPostTime
    override val userNameView: TextView
        get() = itemView.simplePostUserName
    override val venueNameView: TextView
        get() = itemView.simplePostVenueName
    override val descriptionView: TextView
        get() = itemView.simplePostPostDescription
    override val distanceTextView: TextView
        get() = itemView.simplePostDistanceText
    override val likeButton: CheckableImageButton
        get() = itemView.simplePostLikeButton
    override val bookmarkButton: CheckableImageButton
        get() = itemView.simplePostBookmarkButton
    override val followButton: ToggleButton
        get() = itemView.simplePostFollowButton
    override val youButton: Button
        get() = itemView.simplePostYouButton
    override val animationView: LottieAnimationView
        get() = itemView.simplePostAnimationView
}