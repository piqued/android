package io.piqued.viewholders.settings

import android.content.Context
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.StringRes
import io.piqued.R
import io.piqued.views.settings.Settings
import kotlinx.android.synthetic.main.view_holder_settings_button.view.*

/**
 *
 * Created by Kenny M. Liou on 10/27/18.
 * Piqued Inc.
 *
 */
class SettingsButtonViewHolder(parent: ViewGroup)
    : SettingsBaseViewHolder(LayoutInflater.from(parent.context)
        .inflate(R.layout.view_holder_settings_button, parent, false))
{

    init {
        itemView.settingsButton.setOnClickListener {
            onButtonClicked()
        }
    }

    private fun setButtonText(@StringRes titleRes: Int)
    {
        itemView.settingsButton.setText(titleRes)
    }

    private fun setButtonText(context: Context,
                              @StringRes normalTitleRes: Int,
                              @StringRes boldTitleRes: Int)
    {
        val ssb = SpannableStringBuilder()
        val normalTitle = context.getString(normalTitleRes)
        val boldTitle = context.getString(boldTitleRes)

        ssb.append(normalTitle)
        ssb.append(" ")
        ssb.append(boldTitle)

        ssb.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), normalTitle.length + 1, ssb.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        itemView.settingsButton.text = ssb
    }

    override fun onBindingData() {

        when (mType) {
            Settings.FOLLOWING_FB -> setButtonText(itemView.context, R.string.settings_find_friends, R.string.settings_facebook)
            Settings.FOLLOWING_CONTACTS -> setButtonText(itemView.context, R.string.settings_find_friends, R.string.settings_contacts)
            Settings.MY_ACCOUNT_EDIT -> setButtonText(R.string.settings_edit_profile)
            Settings.MY_ACCOUNT_PASSWORD -> setButtonText(R.string.settings_change_password)
            Settings.MY_ACCOUNT_MAP_P -> setButtonText(R.string.settings_map_preference)
            Settings.SECURITY_TAS -> setButtonText(R.string.settings_terms)
            Settings.SECURITY_REPORT -> setButtonText(R.string.settings_report)
            Settings.DEVELOPER_SEND_LOG -> setButtonText(R.string.settings_send_log)
            Settings.DEVELOPER_READ_ALL_LOGS -> setButtonText(R.string.settings_read_all_logs)
            else -> throw IllegalArgumentException("Illegal type for view holder: " + SettingsButtonViewHolder::class.java.name + ", " + mType.name)
        }
    }

    private fun onButtonClicked()
    {
        mDelegate.onButtonPressed(mType)
    }
}