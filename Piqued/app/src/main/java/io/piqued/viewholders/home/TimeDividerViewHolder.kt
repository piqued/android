package io.piqued.viewholders.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.R
import io.piqued.views.home.list.HomeDateDivider
import kotlinx.android.synthetic.main.view_holder_time_divider.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 6/6/17.
 * Piqued Inc.
 */

class TimeDividerViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_time_divider, parent, false)) {

    fun setTimeString(dividerType: HomeDateDivider) {

        itemView.timeDividerTimeString.setBackgroundResource(dividerType.backgroundRes)
        itemView.timeDividerTimeString.text = dividerType.getTimeString(itemView.context)
    }
}
