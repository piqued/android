package io.piqued.viewholders.settings

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.piqued.R
import kotlinx.android.synthetic.main.view_holder_settings_logout.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/11/17.
 * Piqued Inc.
 */

class SettingsLogoutViewHolder(parent: ViewGroup)
    : SettingsBaseViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_settings_logout, parent, false)
) {

    private val mOnLogoutClickedListener = View.OnClickListener { mDelegate.onLogout() }

    override fun onBindingData() {
        itemView.button.setOnClickListener(mOnLogoutClickedListener)
    }
}
