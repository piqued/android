package io.piqued.viewholders.home

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R

/**
 *
 * Created by Kenny M. Liou on 2/17/18.
 * Piqued Inc.
 *
 */
class HomeListLastItemLoading(parent: ViewGroup):
        RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_holder_home_list_last_item_loading, parent, false)
        )