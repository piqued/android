package io.piqued.viewholders.settings

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.core.app.NotificationManagerCompat
import io.piqued.R
import io.piqued.views.settings.Settings
import io.piqued.views.settings.Settings.NOTIFICATION_OVERALL
import kotlinx.android.synthetic.main.view_holder_settings_switch.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/11/17.
 * Piqued Inc.
 */

class SettingsSwitchViewHolder(parent: ViewGroup)
    : SettingsBaseViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_settings_switch, parent, false)
) {

    private val mWrapperClickedListener = View.OnClickListener {
        if (mType === NOTIFICATION_OVERALL) {
            openNotificationSetting()
        }
    }

    init {
        itemView.wrapper.setOnClickListener(mWrapperClickedListener)
    }

    override fun onBindingData() {

        val enabled = NotificationManagerCompat
                .from(itemView.context)
                .areNotificationsEnabled()
        val actualState = false

        @StringRes val titleRes: Int

        when (mType) {
            Settings.MY_ACCOUNT_PRIVATE -> titleRes = R.string.settings_make_account_private
            NOTIFICATION_OVERALL -> {
                titleRes = R.string.settings_notification_all
                itemView.switchView.isClickable = false
                setSwitchState(enabled)
            }
            Settings.NOTIFICATION_SOUND -> {
                titleRes = R.string.settings_notification_sound
                setSwitchState(enabled && actualState)
            }
            Settings.NOTIFICATION_VIBRATE -> {
                titleRes = R.string.settings_notification_vibrate
                setSwitchState(enabled && actualState)
            }
            Settings.NOTIFICATION_COMMENTS -> {
                titleRes = R.string.settings_notification_comments
                setSwitchState(enabled && actualState)
            }
            else -> throw IllegalArgumentException("Illegal type for view holder: " + SettingsSwitchViewHolder::class.java.name + ", type: " + mType.name)
        }

        if (titleRes != 0) {
            itemView.title.setText(titleRes)
        }
    }

    private fun setSwitchState(isOn: Boolean) {

        itemView.switchView.isChecked = isOn
    }

    private fun openNotificationSetting() {

        mDelegate.onSwitchClicked(mType)
    }
}
