package io.piqued.viewholders.settings

import android.view.LayoutInflater
import android.view.ViewGroup

import io.piqued.R
import io.piqued.views.settings.Settings
import kotlinx.android.synthetic.main.view_holder_settings_toggle.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/11/17.
 * Piqued Inc.
 */

class SettingsToggleViewHolder(parent: ViewGroup)
    : SettingsBaseViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_settings_toggle, parent, false)
) {

    override fun onBindingData() {

        when (mType) {
            Settings.MY_ACCOUNT_DISTANCE -> itemView.title.setText(R.string.settings_distance_unit)
            else -> {
                throw IllegalArgumentException("Illegal type for view holder " + SettingsToggleViewHolder::class.java.name + ", " + mType.name)
            }
        }
    }
}
