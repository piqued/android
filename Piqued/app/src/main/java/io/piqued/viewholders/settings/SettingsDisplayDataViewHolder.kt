package io.piqued.viewholders.settings

import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R
import io.piqued.views.settings.Settings
import kotlinx.android.synthetic.main.view_holder_settings_display_data.view.*

/**
 *
 * Created by Kenny M. Liou on 1/13/18.
 * Piqued Inc.
 *
 */
class SettingsDisplayDataViewHolder(parent: ViewGroup) :
        SettingsBaseViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.view_holder_settings_display_data, parent, false)) {

    override fun onBindingData() {

        when (mType) {

            Settings.APP_VERSION -> {
                val context = itemView.context
                val info = context.packageManager.getPackageInfo(context.packageName, 0)

                itemView.dataName.setText(R.string.settings_app_version)
                itemView.dataValue.text = info.versionName
            }
            else -> {
                // do nothing
            }
        }
    }


}