package io.piqued.viewholders.home

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R
import kotlinx.android.synthetic.main.view_holder_footer.view.*

/**
 *
 * Created by Kenny M. Liou on 2/24/18.
 * Piqued Inc.
 *
 */
class FooterViewHolder(parent: ViewGroup):
        RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.view_holder_footer, parent, false)
        ){

    fun onBindView(height: Int) {
        val param = itemView.root_view.layoutParams
        param.height = height
        itemView.root_view.layoutParams = param
    }
}