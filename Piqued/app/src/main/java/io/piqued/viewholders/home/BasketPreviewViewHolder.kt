package io.piqued.viewholders.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.ToggleButton
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.kingfisher.easyviewindicator.RecyclerViewIndicator
import io.piqued.R
import io.piqued.utils.customview.CheckableImageButton
import io.piqued.views.widget.BasePostViewHolder
import kotlinx.android.synthetic.main.view_holder_basket_preview.view.*

/**
 *
 * Created by Kenny M. Liou on 11/3/18.
 * Piqued Inc.
 *
 */
class BasketPreviewViewHolder(parent: ViewGroup)
    : BasePostViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_preview, parent, false)) {

    init {
        initializeView()
    }

    override val userImage: ImageView
        get() = itemView.basketPreviewUserImage
    override val postRecyclerView: RecyclerView
        get() = itemView.basketPreviewRecyclerView
    override val postImageCount: TextView
        get() = itemView.basketPreviewImageCount
    override val postImageIndicator: RecyclerViewIndicator
        get() = itemView.basketPreviewImageIndicator
    override val distanceImage: ImageView
        get() = itemView.basketPreviewDistanceImage
    override val infoBubbleView: View
        get() = itemView.basketPreviewInfoBubble
    override val postTime: TextView
        get() = itemView.basketPreviewPostTime
    override val userNameView: TextView
        get() = itemView.basketPreviewUserName
    override val venueNameView: TextView
        get() = itemView.basketPreviewVenueName
    override val descriptionView: TextView
        get() = itemView.basketPreviewPostDescription
    override val distanceTextView: TextView
        get() = itemView.basketPreviewDistanceText
    override val likeButton: CheckableImageButton
        get() = itemView.basketPreviewLikeButton
    override val bookmarkButton: CheckableImageButton
        get() = itemView.basketPreviewBookmarkButton
    override val followButton: ToggleButton
        get() = itemView.basketPreviewFollowButton
    override val youButton: Button
        get() = itemView.basketPreviewYouButton
    override val animationView: LottieAnimationView
        get() = itemView.basketPreviewAnimationView

}