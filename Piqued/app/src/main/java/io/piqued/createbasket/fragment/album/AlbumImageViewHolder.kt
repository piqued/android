package io.piqued.createbasket.fragment.album

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import io.piqued.R
import io.piqued.module.GlideApp
import kotlinx.android.synthetic.main.view_holder_album_image.view.*

/**
 *
 * Created by Kenny M. Liou on 11/3/18.
 * Piqued Inc.
 *
 */
class AlbumImageViewHolder(
        parent: ViewGroup,
        val provider: Provider)
    : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_album_image, parent, false)
) {

    private var imageUri: Uri? = null
    private var imageSelected = false

    init {
        itemView.albumImageSelectButton.setOnClickListener {
            if (imageUri != null) {
                if (imageSelected) {
                    provider.onRemoveImage(imageUri!!, adapterPosition)
                } else {
                    if (provider.canAddMoreImage()) {
                        provider.onSelectImage(imageUri!!, adapterPosition)
                    }
                }
            }
        }
    }

    interface Provider {
        fun getSelectionIndex(imageUri: Uri): Int
        fun canAddMoreImage(): Boolean
        fun onSelectImage(imageUri: Uri, viewHolderPosition: Int)
        fun onRemoveImage(imageUri: Uri,  viewHolderPosition: Int)
    }

    fun onBind(imageUri: Uri) {

        val index = provider.getSelectionIndex(imageUri)

        imageSelected = index > -1

        if (imageSelected) {
            itemView.albumImageIndexText.text = (index + 1).toString()
            itemView.albumImageIndexText.visibility = View.VISIBLE
        } else {
            itemView.albumImageIndexText.visibility = View.GONE
        }

        if (imageUri != this.imageUri) {
            this.imageUri = imageUri

            GlideApp.with(itemView.context)
                    .load(imageUri)
                    .centerCrop()
                    .placeholder(R.drawable.explorer_blank)
                    .into(itemView.albumImage)
        }
    }
}