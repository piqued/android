package io.piqued.createbasket.fragment.addcomment

import android.net.Uri
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import io.piqued.common.SimpleImageFragment
import io.piqued.database.post.PiquedImage

class ImageViewPagerAdapter(fragmentManager: FragmentManager): FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val images = ArrayList<Uri>()

    override fun getItem(position: Int): Fragment {

        return SimpleImageFragment.newInstance(images[position])
    }

    override fun getCount(): Int {
        return images.size
    }

    fun updateImages(newImages: List<Uri>) {
        images.clear()
        images.addAll(newImages)
    }

    fun updatePiquedImages(newImages: List<PiquedImage>) {
        images.clear()

        for (image: PiquedImage in newImages) {
            images.add(Uri.parse(image.largeImageUrl))
        }
    }
}