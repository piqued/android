package io.piqued.createbasket.fragment

import android.location.Location
import android.net.Uri
import android.view.MenuItem
import androidx.navigation.fragment.NavHostFragment
import com.google.android.gms.maps.model.LatLng
import io.piqued.PiquedFragment
import io.piqued.R
import io.piqued.api.PiquedApiCallback
import io.piqued.cloud.model.piqued.Venue
import io.piqued.createbasket.NewCreateBasketActivity
import io.piqued.createbasket.fragment.album.NewAlbumFragment

/**
 *
 * Created by Kenny M. Liou on 8/11/18.
 * Piqued Inc.
 *
 */
abstract class NewCreateBasketBaseFragment: PiquedFragment() {

    private fun getCreateBasketActivity(): NewCreateBasketActivity {
        return piquedActivity as NewCreateBasketActivity
    }

    override fun onResume() {
        super.onResume()

        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if ((this is NewAlbumFragment) or (this is NewCameraFragment)) {
                    piquedActivity.finish()
                } else {
                    NavHostFragment.findNavController(this).navigateUp()
                }
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    protected fun clearImageUri() {
        getCreateBasketActivity().clearImageUri()
    }

    protected fun addImageUri(imageUri: Uri, viewHolderPosition: Int) {
        getCreateBasketActivity().addImageUri(imageUri, viewHolderPosition)
    }

    protected fun removeImageUri(imageUri: Uri) {
        getCreateBasketActivity().removeImageUri(imageUri)
    }

    protected fun canAddImages(): Boolean {
        return getCreateBasketActivity().canAddMoreImage()
    }

    protected fun getSelectedImageUIPosition(): List<Int> {
        return getCreateBasketActivity().getAllSelectedImagePosition()
    }

    protected fun getAllSelectedImages(): List<Uri> {
        return getCreateBasketActivity().getAllSelectedImages()
    }

    protected fun getImageIndex(imageUri: Uri): Int {
        return getCreateBasketActivity().getImageIndex(imageUri)
    }

    protected fun setVenue(venue: Venue?) {
        getCreateBasketActivity().setVenue(venue)
    }

    protected fun getVenue(): Venue? {
        return getCreateBasketActivity().getVenue()
    }

    protected fun setComment(comment: String?) {
        getCreateBasketActivity().setComment(comment)
    }

    protected fun getPhoneLocation(): Location? {
        return getCreateBasketActivity().getCurrentLocation()
    }

    private fun setStatusBarHidden(hidden: Boolean) {
        getCreateBasketActivity().setStatusBarHidden(hidden)
    }

    protected fun goToAddComment() {
        setVenue(null)
        NavHostFragment.findNavController(this).navigate(R.id.goToAddComment)
    }

    protected fun fetchVenueBasedOnLocation(latlng: LatLng?, callback: PiquedApiCallback<Venue>) {
        getCreateBasketActivity().fetchVenueBasedOnLocation(latlng, callback)
    }

    protected fun createNewPost() {
        getCreateBasketActivity().createNewPost()
    }

    abstract fun statusBarIsHidden(): Boolean
}