package io.piqued.createbasket.fragment.addcomment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import io.piqued.R
import io.piqued.api.PiquedApiCallback
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.model.piqued.Venue
import io.piqued.cloud.model.twitter.TwitterHashTag
import io.piqued.cloud.repository.TwitterRepository
import io.piqued.createbasket.fragment.NewCreateBasketBaseFragment
import io.piqued.createbasket.fragment.addcomment.viewholder.HashTagOptionViewHolder
import io.piqued.utils.ImageDownloadUtil
import io.piqued.utils.ImageUtil.getImageLatLng
import io.piqued.database.util.PiquedErrorCode
import io.piqued.utils.modelmanager.PiquedUserManager
import io.piqued.utils.postmanager.PostManager
import kotlinx.android.synthetic.main.fragment_new_add_comment.*
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 8/25/18.
 * Piqued Inc.
 *
 */
class NewAddCommentFragment: NewCreateBasketBaseFragment() {

    @Inject
    internal lateinit var userManager: PiquedUserManager

    @Inject
    internal lateinit var postManager: PostManager

    @Inject
    internal lateinit var twitterRepository: TwitterRepository

    private lateinit var imageViewPagerAdapter: ImageViewPagerAdapter
    private lateinit var postButton: MenuItem

    private val hashTagSelectionCallback = object : HashTagOptionViewHolder.OnHashTagClickedListener {
        override fun onHashTagClicked(hashTag: String) {

            userComment.removeTextChangedListener(textWatcher)
            replaceLastHashTagWithUserSelection(hashTag) // append space behind
            userComment.addTextChangedListener(textWatcher)

            hashTagSuggestion.visibility = View.GONE
        }
    }

    private val hashTagAdapter = HashTagAdapter(hashTagSelectionCallback)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)
    }

    override fun statusBarIsHidden(): Boolean {
        return false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_new_add_comment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageViewPagerAdapter = ImageViewPagerAdapter(childFragmentManager)
        imageViewPager.adapter = imageViewPagerAdapter
        imageIndicator.setupWithViewPager(imageViewPager, true)

        if (userManager.myUserRecord != null) {
            ImageDownloadUtil.getDownloadUtilInstance(view.context)
                    .loadUserImage(userManager.myUserRecord!!, userImage)
        }

        addCommentNavigationBar.setOnApplyWindowInsetsListener { navigationView, windowInsets ->
            navigationView.setPadding(navigationView.paddingLeft, windowInsets.systemWindowInsetTop, navigationView.paddingRight, navigationView.paddingBottom)
            windowInsets
        }

        locationInputView.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.goToSelectLocation)
        }

        hashTagSuggestionList.adapter = hashTagAdapter
        hashTagSuggestionList.layoutManager = LinearLayoutManager(view.context)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.add_comment_menu, menu)

        postButton = menu.findItem(R.id.create_post)

        updatePostButton()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.create_post -> {
                hideSoftKeyboard()
                createNewPost()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }

        }
    }

    override fun shouldTrackKeyboardVisible(): Boolean {
        return true
    }
    override fun onKeyboardHidden(isHidden: Boolean, keyboardHeight: Int) {

        if (isHidden) {
            updateGuideLine(keyboardLocationGuideline, 0)
        } else {
            updateGuideLine(keyboardLocationGuideline, keyboardHeight)
        }
    }

    override fun onResume() {
        super.onResume()

        updateActionBar(toolbar, R.string.add_comment_title)

        imageViewPagerAdapter.updateImages(getAllSelectedImages())

        imageViewPagerAdapter.notifyDataSetChanged()

        imageIndicator.visibility = if (getAllSelectedImages().size > 1) View.VISIBLE else View.GONE

        if (getVenue() == null) {
            fetchVenueBasedOnLocation(getImageLatLng(getAllSelectedImages()[0]), object : PiquedApiCallback<Venue> {
                override fun onSuccess(data: Venue?) {

                    if (isUIActive && context != null) {
                        locationInputView.setVenue(data)
                    }

                    if (data != null) {
                        setVenue(data)
                    }
                }

                override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                    // :(
                }
            })
        } else {
            locationInputView.setVenue(getVenue())
        }

        userComment.addTextChangedListener(textWatcher)
    }

    override fun onPause() {

        userComment.removeTextChangedListener(textWatcher)

        super.onPause()
    }

    private fun updatePostButton() {
        if (isUIActive && context != null) {
            postButton.isEnabled = ((userComment.text.length > 3) and (getVenue() != null))
        }
    }

    private fun fetchHashTagSuggestions(comment: String) {

        if (!comment.isEmpty() && !comment.endsWith(" ")) {
            val splitComment = comment.split(" ")

            if (splitComment.isNotEmpty()) {
                val lastWord = splitComment[splitComment.size - 1]

                if (lastWord.startsWith("#") && lastWord.length > 2) {

                    twitterRepository.getTwitterTypeAHeadSuggestions(lastWord.substring(1, lastWord.length), object : PiquedCloudCallback<List<TwitterHashTag>> {
                        override fun onSuccess(result: List<TwitterHashTag>?) {
                            if (result != null) {
                                setHashTagSuggestionVisible(lastWord, result)
                            }
                        }

                        override fun onFailure(error: Throwable?, message: String?) {
                            // TODO notify user
                        }

                    })

                    return
                }
            }
        }

        hashTagSuggestion.visibility = View.GONE
    }

    private fun setHashTagSuggestionVisible(userInput: String, suggestions: List<TwitterHashTag>) {
        hashTagSuggestion.visibility = View.VISIBLE

        hashTagInput.text = userInput

        hashTagAdapter.updateSuggestion(suggestions)
        hashTagAdapter.notifyDataSetChanged()
    }

    private fun replaceLastHashTagWithUserSelection(newHashTag: String) {

        val cleanUpHashTag = newHashTag.replace("#", "") + " "

        val comment = userComment.text.toString()

        userComment.setText(comment.replaceAfterLast("#", cleanUpHashTag))

        userComment.setSelection(userComment.text.length)
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {

            val comment = userComment.text.toString()

            setComment(comment)

            fetchHashTagSuggestions(comment)

            updatePostButton()
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

    }
}