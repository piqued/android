package io.piqued.createbasket.fragment.album

import android.net.Uri
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/29/17.
 * Piqued Inc.
 */

class AlbumImageAdapter(
    private val adapterOwner: AdapterOwner,
    private val viewHolderProvider: AlbumImageViewHolder.Provider
) : RecyclerView.Adapter<AlbumImageViewHolder>() {

    interface AdapterOwner {
        fun getImageUriAtPosition(position: Int): Uri
        fun getImageCount(): Int
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumImageViewHolder {
        return AlbumImageViewHolder(parent, viewHolderProvider)
    }

    override fun onBindViewHolder(holder: AlbumImageViewHolder, position: Int) {

        holder.onBind(adapterOwner.getImageUriAtPosition(position))
    }

    override fun getItemCount(): Int {

        return adapterOwner.getImageCount()
    }
}
