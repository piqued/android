package io.piqued.createbasket.fragment.addcomment

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.createbasket.fragment.addcomment.viewholder.HashTagOptionViewHolder
import io.piqued.cloud.model.twitter.TwitterHashTag

/**
 *
 * Created by Kenny M. Liou on 8/26/18.
 * Piqued Inc.
 *
 */
class HashTagAdapter(val callback: HashTagOptionViewHolder.OnHashTagClickedListener): RecyclerView.Adapter<HashTagOptionViewHolder>() {

    private val suggestions = ArrayList<TwitterHashTag>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HashTagOptionViewHolder {
        return HashTagOptionViewHolder(parent)
    }

    override fun getItemCount(): Int {

        return suggestions.size
    }

    override fun onBindViewHolder(holder: HashTagOptionViewHolder, position: Int) {

        holder.onBind(suggestions[position].hashTag, callback)
    }

    fun updateSuggestion(newSuggestions: List<TwitterHashTag>) {
        suggestions.clear()
        suggestions.addAll(newSuggestions)
    }
}