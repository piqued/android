package io.piqued.createbasket.fragment.addcomment.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.R
import kotlinx.android.synthetic.main.view_holder_hash_tag_suggestion.view.*

/**
 *
 * Created by Kenny M. Liou on 8/26/18.
 * Piqued Inc.
 *
 */

class HashTagOptionViewHolder(parent: ViewGroup)
    : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.view_holder_hash_tag_suggestion, parent, false)
) {

    interface OnHashTagClickedListener {
        fun onHashTagClicked(hashTag: String)
    }

    fun onBind(hashTag: String, callback: OnHashTagClickedListener) {

        itemView.hashTagSuggestion.text = hashTag

        itemView.hashTagSuggestion.setOnClickListener {
            callback.onHashTagClicked(hashTag)
        }
    }
}