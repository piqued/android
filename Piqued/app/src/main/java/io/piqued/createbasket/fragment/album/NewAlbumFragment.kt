package io.piqued.createbasket.fragment.album

import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import io.piqued.R
import io.piqued.createbasket.fragment.NewCreateBasketBaseFragment
import io.piqued.database.album.ImageAlbum
import io.piqued.utils.Constants
import io.piqued.views.profile.ProfileItemDecoration
import kotlinx.android.synthetic.main.fragment_new_album.*
import kotlinx.android.synthetic.main.toolbar_new_album.*
import kotlinx.android.synthetic.main.toolbar_new_album.view.*
import java.io.File
import java.util.*

/**
 *
 * Created by Kenny M. Liou on 8/11/18.
 * Piqued Inc.
 *
 */
class NewAlbumFragment: NewCreateBasketBaseFragment(), AlbumImageAdapter.AdapterOwner, AlbumImageViewHolder.Provider {

    override fun statusBarIsHidden(): Boolean {
        return true
    }

    companion object {
        private val TAG = NewAlbumFragment::class.java.simpleName
    }

    private lateinit var albumImageAdapter: AlbumImageAdapter
    private val albumList = ArrayList<ImageAlbum>()
    private var selectedCursor: Cursor? = null

    override fun getTagName(): String {
        return TAG
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_new_album, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val param = album_status_bar_space.layoutParams as ConstraintLayout.LayoutParams
        param.height = metric.statusBarHeight
        album_status_bar_space.layoutParams = param

        albumToolbar.cameraButton.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.goToCamera)
        }

        albumToolbar.nextButton.setOnClickListener {
            goToAddComment()
        }

        albumImageAdapter = AlbumImageAdapter(this, this)

        val gridLayoutManager = GridLayoutManager(view.context, 4)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return 1
            }
        }

        recyclerView.layoutManager = gridLayoutManager
        recyclerView.adapter = albumImageAdapter
        recyclerView.addItemDecoration(ProfileItemDecoration(0, view.context))
        (recyclerView.itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
    }

    override fun onResume() {
        super.onResume()

        fetchAlbumList()

        updateActionBar(albumToolbar)

        updateButton()
    }

    private fun fetchAlbumList() {

        val cursor = context!!.contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.PhotoGalleryProjection, null, null, null)

        if (cursor!!.moveToFirst()) {

            var bucketName: String
            var bucketId: String
            var mediaId: String
            var dataUri: String

            val mediaIdColumn = cursor.getColumnIndex(MediaStore.Images.Media._ID)
            val bucketColumn = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
            val bucketIdColumn = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_ID)
            val bucketData = cursor.getColumnIndex(MediaStore.Images.Media.DATA)

            do {
                bucketName = cursor.getString(bucketColumn)
                bucketId = cursor.getString(bucketIdColumn)
                mediaId = cursor.getString(mediaIdColumn)
                dataUri = cursor.getString(bucketData)

                val path = dataUri.substring(IntRange(0, dataUri.lastIndexOf(bucketName) - 1))

                if (!TextUtils.isEmpty(bucketName) && !TextUtils.isEmpty(bucketId)) {
                    val bucket = ImageAlbum(mediaId, path, bucketName, bucketId)

                    if (!albumList.contains(bucket)) {
                        albumList.add(bucket)
                    }
                }

            } while (cursor.moveToNext())
        }

        cursor.close()

        // sort albumList
        albumList.sort()

        val arrayAdapter = ArrayAdapter(context!!, R.layout.spinner_album, albumList)
        arrayAdapter.setDropDownViewResource(R.layout.spinner_album_dropdown)

        dropDownMenu.adapter = arrayAdapter
        dropDownMenu.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                val currentContext = context
                if (currentContext != null) {
                    selectedCursor = albumList[0].createCursor(currentContext)
                    albumImageAdapter.notifyDataSetChanged()
                }
            }

            override fun onItemSelected(adapterView: AdapterView<*>?, selectedItemView: View?, position: Int, itemId: Long) {
                val currentContext = context
                if (currentContext != null) {
                    selectedCursor = albumList[position].createCursor(currentContext)
                    albumImageAdapter.notifyDataSetChanged()
                }
            }
        }
        // try to select "camera"
        var index = 0
        for ((i, album) in albumList.withIndex()) {
            println(album.bucketId)
            if ("camera".equals(album.bucketName, true)) {
                index = i
                break
            }
        }
        dropDownMenu.setSelection(index)
    }

    private fun updateButton() {
        if (getAllSelectedImages().isEmpty()) {
            albumToolbar.cameraButton.visibility = View.VISIBLE
            albumToolbar.nextButton.visibility = View.GONE
        } else {
            albumToolbar.cameraButton.visibility = View.GONE
            albumToolbar.nextButton.visibility = View.VISIBLE
        }
    }

    /**
     * Adapter Owner
     */
    override fun getImageUriAtPosition(position: Int): Uri {

        val cursor = selectedCursor
        if (cursor != null) {
            cursor.moveToPosition(position)
            val filePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA))
            return Uri.fromFile(File(filePath))
        }

        throw IllegalStateException("calling get image uri at position when cursor is not ready")
    }

    override fun getImageCount(): Int {

        return selectedCursor?.count ?: 0
    }

    /**
     * AlbumImageViewHolder provider
     */

    override fun getSelectionIndex(imageUri: Uri): Int {
        return getImageIndex(imageUri)
    }

    override fun canAddMoreImage(): Boolean {
        return canAddImages()
    }

    override fun onSelectImage(imageUri: Uri, viewHolderPosition: Int) {
        addImageUri(imageUri, viewHolderPosition)

        albumImageAdapter.notifyItemChanged(viewHolderPosition)

        updateButton()
    }

    override fun onRemoveImage(imageUri: Uri, viewHolderPosition: Int) {
        removeImageUri(imageUri)

        val effectedPosition = getSelectedImageUIPosition()

        albumImageAdapter.notifyItemChanged(viewHolderPosition)

        effectedPosition.forEach { position ->

            albumImageAdapter.notifyItemChanged(position)
        }

        updateButton()
    }
}