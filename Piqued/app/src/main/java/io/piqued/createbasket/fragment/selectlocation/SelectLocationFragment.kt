package io.piqued.createbasket.fragment.selectlocation

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import io.piqued.R
import io.piqued.api.PiquedApiCallback
import io.piqued.cloud.model.piqued.Venue
import io.piqued.createbasket.fragment.NewCreateBasketBaseFragment
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.utils.postmanager.PostManager
import kotlinx.android.synthetic.main.fragment_select_location.*
import kotlinx.android.synthetic.main.toolbar_select_location.*
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 8/25/18.
 * Piqued Inc.
 *
 */

class SelectLocationFragment: NewCreateBasketBaseFragment() {

    @Inject
    internal lateinit var postManager: PostManager

    private lateinit var mapView: MapView
    private lateinit var googleMap: GoogleMap
    private var mapLocation: LatLng? = null

    private val listAdapter = LocationSuggestionAdapter(object : LocationSuggestionViewHolder.OnItemClickedCallback {
        override fun onClicked(venue: Venue) {
            setVenue(venue)

            goBack()
        }
    })

    companion object {
        private const val DEFAULT_ZOOM_LEVEL = 15f
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)
    }

    override fun statusBarIsHidden(): Boolean {
        return false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_select_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        selectLocationNavigationBar.setOnApplyWindowInsetsListener { navigationView, windowInsets ->
            navigationView.setPadding(navigationView.paddingLeft, windowInsets.systemWindowInsetTop, navigationView.paddingRight, navigationView.paddingBottom)
            windowInsets
        }

        locationSuggestionList.adapter = listAdapter
        locationSuggestionList.layoutManager = LinearLayoutManager(view.context)

        selectLocationMap.onCreate(savedInstanceState)

        try {
            MapsInitializer.initialize(view.context)
        } catch (e: Exception) {
            PiquedLogger.d(tagName, "Failed to initialize Map")
        }

        mapView = selectLocationMap

        mapView.getMapAsync { map ->
            if (isUIActive && context != null) {
                googleMap = map

                if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    googleMap.isMyLocationEnabled = true
                    googleMap.uiSettings.isMapToolbarEnabled = false
                }


                if (getCurrentLatLng() != null) {
                    val newPosition = CameraPosition.builder()
                            .target(getCurrentLatLng())
                            .zoom(DEFAULT_ZOOM_LEVEL)
                            .build()

                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(newPosition))
                    googleMap.setOnCameraIdleListener {
                        mapLocation = googleMap.cameraPosition.target
                        fetchLocationSuggestion()
                    }
                }

            }
        }

        updateActionBar(selectLocationToolbar)
    }

    override fun onResume() {
        super.onResume()

        searchInput.addTextChangedListener(textWatcher)

        mapView.onResume()

        fetchLocationSuggestion()
    }

    override fun onPause() {

        hideSoftKeyboard()

        searchInput.removeTextChangedListener(textWatcher)

        mapView.onPause()

        super.onPause()
    }

    override fun onDestroy() {

        mapView.onDestroy()
        super.onDestroy()
    }

    override fun goBack() {

        hideSoftKeyboard()

        super.goBack()
    }

    private fun fetchLocationSuggestion() {

        val latLng = getCurrentLatLng()

        if (latLng != null) {
            fetchVenueWithLocation(latLng)
        } else {
            // TODO: well, I don't know yet, maybe display error message?
        }
    }

    private fun getCurrentLatLng() : LatLng? {

        if (mapLocation != null) {
            return mapLocation
        } else if (getVenue() != null) {
            return getVenue()!!.location
        } else if (getPhoneLocation() != null) {
            val location = getPhoneLocation()!!
            return LatLng(location.latitude, location.longitude)
        } else {
            // TODO: well, I don't know yet, maybe display error message?
            return null
        }
    }

    private fun fetchVenueWithLocation(latLng: LatLng) {
        postManager.searchVenueNearLatLng(latLng, object : PiquedApiCallback<List<Venue>> {
            override fun onSuccess(data: List<Venue>?) {

                if (data != null) {
                    listAdapter.setAutoSuggestedItems(data)
                }

                listAdapter.notifyDataSetChanged()
            }

            override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                // TODO: display error message
                PiquedLogger.e(SelectLocationFragment::class.java.simpleName, errorCode, payload)
            }
        })
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {}

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            if (getCurrentLatLng() != null) {
                postManager.searchVenueWithQuery(searchInput.text.toString(), getCurrentLatLng()!!, object : PiquedApiCallback<List<Venue>> {
                    override fun onSuccess(data: List<Venue>?) {

                        if (data != null) {
                            listAdapter.setSearchResult(data)
                        }

                        listAdapter.notifyDataSetChanged()
                    }

                    override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                        // TODO: display error message
                        PiquedLogger.e(SelectLocationFragment::class.java.simpleName, errorCode, payload)
                    }
                })
            }
        }

    }
}