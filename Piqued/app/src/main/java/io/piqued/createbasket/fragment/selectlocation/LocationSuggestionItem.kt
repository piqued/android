package io.piqued.createbasket.fragment.selectlocation

import io.piqued.cloud.model.piqued.Venue

/**
 *
 * Created by Kenny M. Liou on 8/25/18.
 * Piqued Inc.
 *
 */

class LocationSuggestionItem(val type: Type, val venue: Venue?) {
    enum class Type {
        SUGGESTION_TITLE,
        SEARCH_VENUE_SUGGESTION;

        companion object {
            private val values = Type.values()

            fun fromInt(index: Int): Type {
                if (index > -1 && index < values.size) {
                    return values[index]
                }

                throw IllegalArgumentException("Illegal index value: " + index + " for enum class " + Type::class.java.simpleName)
            }
        }
    }
}