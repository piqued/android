package io.piqued.createbasket.fragment.selectlocation

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.piqued.cloud.model.piqued.Venue

/**
 *
 * Created by Kenny M. Liou on 8/25/18.
 * Piqued Inc.
 *
 */

abstract class LocationSuggestionViewHolder(view: View): RecyclerView.ViewHolder(view) {

    interface OnItemClickedCallback {
        fun onClicked(venue: Venue)
    }

    abstract fun onBind(item: LocationSuggestionItem, callback: OnItemClickedCallback)
}