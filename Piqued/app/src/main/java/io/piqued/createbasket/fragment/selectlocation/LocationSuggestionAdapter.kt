package io.piqued.createbasket.fragment.selectlocation

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.cloud.model.piqued.Venue
import io.piqued.createbasket.fragment.selectlocation.viewholder.SuggestionTitleViewHolder
import io.piqued.createbasket.fragment.selectlocation.viewholder.SuggestionVenueViewHolder

/**
 *
 * Created by Kenny M. Liou on 8/25/18.
 * Piqued Inc.
 *
 */

class LocationSuggestionAdapter(
        val callback: LocationSuggestionViewHolder.OnItemClickedCallback
)
    : RecyclerView.Adapter<LocationSuggestionViewHolder>() {



    private val itemList = ArrayList<LocationSuggestionItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationSuggestionViewHolder {
        val type = LocationSuggestionItem.Type.fromInt(viewType)

        return when (type) {

            LocationSuggestionItem.Type.SUGGESTION_TITLE -> SuggestionTitleViewHolder(parent)
            LocationSuggestionItem.Type.SEARCH_VENUE_SUGGESTION -> SuggestionVenueViewHolder(parent)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return itemList[position].type.ordinal
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: LocationSuggestionViewHolder, position: Int) {

        holder.onBind(itemList[position], callback)
    }

    fun setAutoSuggestedItems(items: List<Venue>) {

        itemList.clear()

        itemList.add(LocationSuggestionItem(LocationSuggestionItem.Type.SUGGESTION_TITLE, null))

        for ((index, venue) in items.withIndex()) {
            if (index > 2) {
                break
            } else {
                itemList.add(LocationSuggestionItem(LocationSuggestionItem.Type.SEARCH_VENUE_SUGGESTION, venue))
            }
        }
    }

    fun setSearchResult(items: List<Venue>) {
        itemList.clear()

        for (venue in items) {
            itemList.add(LocationSuggestionItem(LocationSuggestionItem.Type.SEARCH_VENUE_SUGGESTION, venue))
        }
    }
}