package io.piqued.createbasket.fragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.NavHostFragment
import io.piqued.R
import io.piqued.database.util.PiquedLogger
import io.piqued.module.GlideApp
import io.piqued.utils.PiquedOnBackHandler
import kotlinx.android.synthetic.main.fragment_new_camera.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * Created by Kenny M. Liou on 8/11/18.
 * Piqued Inc.
 *
 */
class NewCameraFragment: NewCreateBasketBaseFragment() {
    override fun statusBarIsHidden(): Boolean {
        return true
    }

    companion object {
        val TAG = NewCameraFragment::class.java.simpleName
        const val BROADCAST_ACTION = "save_image_completed"
        const val BROADCAST_KEY = "saved_image_url"
        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
    }

    private val projection = arrayOf(
            MediaStore.Images.ImageColumns._ID,
            MediaStore.Images.ImageColumns.DATA,
            MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
            MediaStore.Images.ImageColumns.DATE_TAKEN,
            MediaStore.Images.ImageColumns.MIME_TYPE)
    private var cameraFacing = CameraSelector.LENS_FACING_FRONT // so we start with "back"
    private var preview: Preview? = null
    private var camera: Camera? = null
    private var imageCapture: ImageCapture? = null

    override fun getTagName(): String {
        return TAG
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_new_camera, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        album_button.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }

        cameraFacingButton.setOnClickListener {v ->
            changeCameraFacing(v.context)
        }

        camera_button.setOnClickListener {v ->
            takePhoto(v.context)
        }

        val params = cameraStatusBarSpace.layoutParams as ConstraintLayout.LayoutParams
        params.height = metric.statusBarHeight
        cameraStatusBarSpace.layoutParams = params

        startCamera(piquedActivity)
    }

    override fun onResume() {
        super.onResume()

        fetchLastPictureTaken()
        LocalBroadcastManager.getInstance(context!!).registerReceiver(saveImageReceiver, IntentFilter(BROADCAST_ACTION))
        updateActionBar(cameraToolbar)
        piquedActivity.setBackHandler(onBackPressedHandler)
    }

    override fun onPause() {

        piquedActivity.setBackHandler(null)
        LocalBroadcastManager.getInstance(context!!).unregisterReceiver(saveImageReceiver)
        super.onPause()
    }

    private fun startCamera(c: Context) {
        val cameraProviderFeature = ProcessCameraProvider.getInstance(c)

        cameraProviderFeature.addListener(Runnable {
           changeCameraFacing(c)
        }, ContextCompat.getMainExecutor(c))
    }

    private fun takePhoto(c: Context) {
        // Get a stable reference of the modifiable image capture use case
        val imageCapture = imageCapture ?: return

        // create timestamped output file to hold the image
        val photoFile = File(
                c.cacheDir,
                String.format("Piqued_%s.jpg",
                        SimpleDateFormat(FILENAME_FORMAT, Locale.US)
                        .format(System.currentTimeMillis()))
        )

        // create output options object which contains file + metadata
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

        imageCapture.takePicture(outputOptions, ContextCompat.getMainExecutor(c), object: ImageCapture.OnImageSavedCallback {
            override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                val intent = Intent(BROADCAST_ACTION)
                intent.putExtra(BROADCAST_KEY, photoFile.absolutePath)

                LocalBroadcastManager.getInstance(context!!).sendBroadcast(intent)
            }

            override fun onError(exc: ImageCaptureException) {
                PiquedLogger.e(exc, String.format("Photo capture failed: ${exc.message}"))
            }
        })

    }

    private fun changeCameraFacing(c: Context) {
        val cameraProvider: ProcessCameraProvider = ProcessCameraProvider.getInstance(c).get()

        preview = Preview.Builder().build()
        imageCapture = ImageCapture.Builder()
                .build()

        cameraFacing = if (cameraFacing == CameraSelector.LENS_FACING_BACK) {
            CameraSelector.LENS_FACING_FRONT
        } else {
            CameraSelector.LENS_FACING_BACK
        }

        // select camera
        val cameraSelector = CameraSelector.Builder().requireLensFacing(cameraFacing).build()
        cameraProvider.unbindAll()

        camera = cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)
        preview?.setSurfaceProvider(viewFinder.createSurfaceProvider())
    }

    private fun fetchLastPictureTaken() {
        val cursor = context?.contentResolver?.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC")

        if (cursor?.moveToFirst() == true) {
            val imageLocation = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA))

            GlideApp.with(this)
                    .load(imageLocation)
                    .centerCrop()
                    .into(album_button)
        }

        cursor?.close()
    }

    private val saveImageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val imageUrl = intent.getStringExtra(BROADCAST_KEY)!!
            clearImageUri()
            addImageUri(Uri.fromFile(File(imageUrl)), -1)

            goToAddComment()
        }
    }

    private val onBackPressedHandler = PiquedOnBackHandler {
        piquedActivity.finish()
        true
    }
}