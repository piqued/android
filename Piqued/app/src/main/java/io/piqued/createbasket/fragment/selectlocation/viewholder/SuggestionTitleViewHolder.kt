package io.piqued.createbasket.fragment.selectlocation.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R
import io.piqued.createbasket.fragment.selectlocation.LocationSuggestionItem
import io.piqued.createbasket.fragment.selectlocation.LocationSuggestionViewHolder

/**
 *
 * Created by Kenny M. Liou on 8/25/18.
 * Piqued Inc.
 *
 */

class SuggestionTitleViewHolder(parent: ViewGroup)
    : LocationSuggestionViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_suggestion_title, parent, false)
) {

    override fun onBind(item: LocationSuggestionItem, callback: OnItemClickedCallback) {
        // do nothing
    }

}