package io.piqued.createbasket

import android.location.Location
import android.net.Uri
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import io.piqued.cloud.model.piqued.BasketData
import io.piqued.cloud.model.piqued.Venue
import io.piqued.cloud.model.yelp.YelpMatchBusiness

/**
 *
 * Created by Kenny M. Liou on 8/11/18.
 * Piqued Inc.
 *
 */
class NewBasketModel: ViewModel() {

    val imageUris = ArrayList<Uri>()
    var fourSquareVenue: Venue? = null
    var yelpVenue: YelpMatchBusiness? = null
    var userComment: String? = null

    fun isValidBasket(deviceLocation: Location?): Boolean {

        return imageUris.isNotEmpty() and ((fourSquareVenue != null) or (deviceLocation != null)) and !userComment.isNullOrEmpty()
    }

    fun toBasketData(deviceLocation: Location?): BasketData? {

        if (isValidBasket(deviceLocation)) {
            val venue = fourSquareVenue
            val location = if (venue != null) {
                venue.location
            } else if (deviceLocation != null){
                LatLng(deviceLocation.latitude, deviceLocation.longitude)
            } else {
                LatLng(-1.0, -1.0)
            }

            return BasketData(
                    imageUris,
                    location.latitude,
                    location.longitude,
                    venue?.venueName ?: "",
                    venue?.venueName ?: "",
                    userComment ?: "",
                    venue?.venueId ?: "",
                    venue?.countryCode ?: "",
                    venue?.state ?: "",
                    venue?.city ?: "",
                    venue?.fullAddress ?: "",
                    yelpVenue?.yelpId ?: "",
                    ""
                    )
        }

        throw IllegalStateException("Basket is not ready to be uploaded")
    }

    fun canAddMoreImage(): Boolean {
        return imageUris.size < 9
    }

    fun getImageIndex(imageUri: Uri): Int {
        return imageUris.indexOf(imageUri)
    }
}