package io.piqued.createbasket

import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.model.LatLng
import io.piqued.R
import io.piqued.activities.PiquedActivity
import io.piqued.api.PiquedApiCallback
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.model.piqued.Venue
import io.piqued.cloud.model.yelp.YelpMatchBusiness
import io.piqued.cloud.repository.YelpRepository
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.model.MetricValues
import io.piqued.service.imageupload.CreateMultiImageBasketService
import io.piqued.utils.LocationCallback
import io.piqued.utils.PiquedLocationManager
import io.piqued.utils.postmanager.PostManager
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 8/11/18.
 * Piqued Inc.
 *
 */
class NewCreateBasketActivity: PiquedActivity() {

    companion object {
        private val TAG = NewCreateBasketActivity::class.java.simpleName
    }

    @Inject
    internal lateinit var postManager: PostManager
    @Inject
    internal lateinit var locationManager: PiquedLocationManager
    @Inject
    internal lateinit var yelpRepository: YelpRepository

    private lateinit var newBasketModel: NewBasketModel

    private val imageUriAndPositionMap = HashMap<Uri, Int>()

    private var currentLocation: Location? = null

    override fun getMetric(): MetricValues {
        return mMetric
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        newBasketModel = ViewModelProvider(this).get(NewBasketModel::class.java)

        setContentView(R.layout.activity_new_create_basket)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        locationManager.getCurrentLocation(object : LocationCallback() {
            override fun onLocationUpdated(location: Location) {
                currentLocation = location
            }

            override fun onceOnly(): Boolean {
                return true
            }
        })
    }

    override fun getRootView(): View {
        return findViewById(R.id.activity_root_view)
    }

    fun setStatusBarHidden(hidden: Boolean) {

        if (hidden) {
            window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
    }

    fun clearImageUri() {
        imageUriAndPositionMap.clear()
        newBasketModel.imageUris.clear()
    }

    fun addImageUri(imageUri: Uri, viewHolderPosition: Int) {

        imageUriAndPositionMap[imageUri] = viewHolderPosition

        newBasketModel.imageUris.add(imageUri)
    }

    fun removeImageUri(imageUri: Uri) {
        imageUriAndPositionMap.remove(imageUri)

        newBasketModel.imageUris.remove(imageUri)
    }

    fun getAllSelectedImagePosition(): List<Int> {

        val result = ArrayList<Int>()

        for((_, value) in imageUriAndPositionMap) {
            result.add(value)
        }

        return result
    }

    fun canAddMoreImage(): Boolean {
        return newBasketModel.canAddMoreImage()
    }

    fun getImageIndex(imageUri: Uri): Int {
        return newBasketModel.getImageIndex(imageUri)
    }

    fun getAllSelectedImages(): ArrayList<Uri> {
        return newBasketModel.imageUris
    }

    fun getVenue(): Venue? {
        return newBasketModel.fourSquareVenue
    }

    fun setVenue(venue: Venue?) {
        newBasketModel.fourSquareVenue = venue

        if (venue != null && venue.venueName.isNotEmpty() && venue.venueName.isNotBlank()) {
            searchYelpVenue(venue)
        } else {
            newBasketModel.yelpVenue = null
        }
    }

    fun setComment(comment: String?) {

        newBasketModel.userComment = comment
    }

    fun fetchVenueBasedOnLocation(latLng: LatLng?, callback: PiquedApiCallback<Venue>) {

        if (latLng != null) {
            searchVenue(latLng, callback)
        } else if (currentLocation != null) {
            searchVenue(LatLng(currentLocation!!.latitude, currentLocation!!.longitude), callback)
        } else {
            locationManager.getCurrentLocation(object : LocationCallback() {
                override fun onLocationUpdated(location: Location) {
                    currentLocation = location
                    searchVenue(LatLng(location.latitude, location.longitude), callback)
                }

                override fun onceOnly(): Boolean {
                    return true
                }
            })
        }
    }

    fun getCurrentLocation(): Location? {
        return currentLocation
    }

    private fun searchVenue(latLng: LatLng, callback: PiquedApiCallback<Venue>) {
        if (isUiActive) {
            postManager.searchVenueNearLatLng(latLng, object : PiquedApiCallback<List<Venue>> {
                override fun onSuccess(data: List<Venue>?) {
                    if (isUiActive && data != null) {
                        if (data.isNotEmpty()) {
                            val venue = data[0]

                            callback.onSuccess(venue)
                        }
                    }
                }

                override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                    // :(
                }
            })
        }
    }

    private fun searchYelpVenue(venue: Venue) {

        yelpRepository.matchYelpBusiness(venue, object : PiquedCloudCallback<YelpMatchBusiness?> {
            override fun onSuccess(result: YelpMatchBusiness?) {
                newBasketModel.yelpVenue = result
            }

            override fun onFailure(error: Throwable?, message: String?) {
                PiquedLogger.e(error, message)
            }
        })
    }

    fun createNewPost() {

        if (newBasketModel.isValidBasket(currentLocation)) {

            val basketData = newBasketModel.toBasketData(currentLocation)

            if (basketData != null) {
                CreateMultiImageBasketService.uploadImage(this, basketData)
                //CreateBasketService.uploadImage(this, basketData)
                finish()
            } else {
                PiquedLogger.e(TAG, "failed to create basket due to lack of location info")
            }
        }
    }
}