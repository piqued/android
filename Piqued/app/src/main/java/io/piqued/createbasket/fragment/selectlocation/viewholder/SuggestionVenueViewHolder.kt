package io.piqued.createbasket.fragment.selectlocation.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R
import io.piqued.createbasket.fragment.selectlocation.LocationSuggestionItem
import io.piqued.createbasket.fragment.selectlocation.LocationSuggestionViewHolder
import kotlinx.android.synthetic.main.view_holder_suggestion_venue.view.*

/**
 *
 * Created by Kenny M. Liou on 8/25/18.
 * Piqued Inc.
 *
 */

class SuggestionVenueViewHolder(parent: ViewGroup)
    : LocationSuggestionViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_suggestion_venue, parent, false)
) {

    override fun onBind(item: LocationSuggestionItem, callback: OnItemClickedCallback) {

        val venue = item.venue!!

        itemView.venueTitle.text = venue.venueName
        itemView.venueAddress.text = venue.fullAddress

        itemView.setOnClickListener {
            callback.onClicked(venue)
        }
    }

}