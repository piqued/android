package io.piqued.api.square;

import io.piqued.cloud.model.foursquare.FourSquareVenue;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/6/17.
 * Piqued Inc.
 */

class FourSquareResponse {

    class Response {
        FourSquareVenue venue;
    }

    Response response;
}
