package io.piqued.api;

import android.content.Context;
import android.os.Handler;

import org.json.JSONObject;

import java.io.IOException;

import androidx.annotation.NonNull;
import io.piqued.cloud.model.piqued.ImageContainer;
import io.piqued.cloud.util.CloudPreference;
import io.piqued.cloud.util.ImageUploadProgressListener;
import io.piqued.cloud.util.RequestBodyWithProgress;
import io.piqued.model.imageupload.PreSignObject;
import io.piqued.database.util.PiquedErrorCode;
import io.piqued.database.util.PiquedLogger;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/4/17.
 * Piqued Inc.
 */

public abstract class PiquedNetworkApi {

    private static final String MULTI_PART_CONTENT = "Content-Disposition";
    private static final String MULTI_PART_KEY = "key";
    private static final String MULTI_PART_POLICY = "policy";
    private static final String MULTI_PART_CREDENTIAL = "x-amz-credential";
    private static final String MULTI_PART_ALGORITHM = "x-amz-algorithm";
    private static final String MULTI_PART_DATE = "x-amz-date";
    private static final String MULTI_PART_SIGNATURE = "x-amz-signature";
    private static final String MULTI_PART_IMAGE_FORM = "form-data; name=\"file\"";
    private static final String MULTI_PART_IMAGE_ENCODING = "base64";

    private static final String TAG = PiquedNetworkApi.class.getSimpleName();

    public enum NetworkAction {
        POST,
        PUT,
        PATCH,
        GET,
        DELETE
    }

    protected abstract class JSONCallback<T> {

        private final PiquedApiCallback<T> mCallback;

        protected JSONCallback(PiquedApiCallback<T> callback) {
            mCallback = callback;
        }

        private boolean mReceivedResultSuccess = true;
        private PiquedErrorCode mErrorCode;
        private String mErrorPayload;

        void onCallback(String responseString) {
            T response = onReceiveResult(responseString);
            if (mReceivedResultSuccess) {
                callOnSuccess(mCallback, response);
            } else {
                onFailure(mErrorCode, mErrorPayload);
            }
        }

        public void onReceivedFailed(PiquedErrorCode errorCode, String payload) {
            mReceivedResultSuccess = false;
            mErrorCode = errorCode;
            mErrorPayload = payload;
        }

        public void onFailure(PiquedErrorCode errorCode, String payload) {
            mCallback.onFailure(errorCode, payload);
        }

        protected abstract T onReceiveResult(String responseString);
    }

    private static final String JSON_MEDIA_TYPE_STRING = "application/json; charset=utf-8";
    private static final MediaType JSON = MediaType.parse(JSON_MEDIA_TYPE_STRING);
    private final OkHttpClient client = new OkHttpClient();
    private final Context mContext;
    private final Handler mMainLoopHandler;
    private final CloudPreference cloudPreference;

    protected PiquedNetworkApi(@NonNull Context context, @NonNull CloudPreference cloudPreference) {
        mContext = context;
        mMainLoopHandler = new Handler(mContext.getMainLooper());
        this.cloudPreference = cloudPreference;
    }

    protected Context getContext() {
        return mContext;
    }

    protected void networkHelper(PiquedNetworkRequest networkRequest, JSONObject payload, JSONCallback<?> callback) {

        // set up requests based on action type
        final Request request;
        final String url = networkRequest.getUrl(cloudPreference.getHostUrl());
        final NetworkAction action = networkRequest.getNetworkAction();

        if (action == NetworkAction.POST) {

            RequestBody body = RequestBody.create(JSON, payload.toString());

            request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
        } else if (action == NetworkAction.GET) {
            request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();
        } else if (action == NetworkAction.DELETE) {
            request = new Request.Builder()
                    .url(url)
                    .delete()
                    .build();
        } else if (action == NetworkAction.PATCH) {

            RequestBody body = RequestBody.create(JSON, payload.toString());

            request = new Request.Builder()
                    .url(url)
                    .patch(body)
                    .build();
        } else if (action == NetworkAction.PUT) {

            RequestBody body = RequestBody.create(JSON, payload.toString());

            request = new Request.Builder()
                    .addHeader("Accept", JSON_MEDIA_TYPE_STRING)
                    .url(url)
                    .put(body)
                    .build();
        } else {
            throw new IllegalArgumentException("Action: " + action + " not supported yet.");
        }

        if (request == null) {
            throw new IllegalArgumentException("Request is null, are we handling this case properly? " + action.name());
        }

        sendRequest(request, callback);
    }

    protected void networkHelper(PreSignObject preSignObject, String originalImageUrl, ImageContainer base64Image, ImageUploadProgressListener progressListener, JSONCallback<?> callback) {

        MultipartBody.Builder multiRequestBodyBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);

        for (String key : preSignObject.fields.keySet()) {
            String value = preSignObject.fields.get(key);

            if (value != null) {
                multiRequestBodyBuilder.addFormDataPart(key, value);
            }
        }

        multiRequestBodyBuilder
                .addFormDataPart("file", "filename.jpg", RequestBody.create(MediaType.parse("image/jpeg"), base64Image.getImage()));

        RequestBody multiRequestBody = multiRequestBodyBuilder.build();

        Request request;

        if (progressListener != null) {
            RequestBodyWithProgress monitoredBody = new RequestBodyWithProgress(multiRequestBody, originalImageUrl, progressListener);

            request = new Request.Builder()
                    .url(preSignObject.url)
                    .post(monitoredBody)
                    .build();
        } else {
            request = new Request.Builder()
                    .url(preSignObject.url)
                    .post(multiRequestBody)
                    .build();
        }

        sendRequest(request, callback);
    }

    private void sendRequest(Request request, final JSONCallback<?> callback) {
        // send request to network
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                callOnFailure(callback, PiquedErrorCode.NETWORK, e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {

                    callback.onCallback(response.body().string());

                } else {

                    if (response.code() == 404) {
                        callOnFailure(callback, PiquedErrorCode.NETWORK_404, "Respond received code: " + response.code());
                    } else if (response.code() == 403) {
                        callOnFailure(callback, PiquedErrorCode.NETWORK_403_FORBIDDEN, "403 Forbidden");
                    } else {
                        PiquedLogger.e(TAG, PiquedErrorCode.NETWORK, "Received error message" + response.body().string());
                        callOnFailure(callback, PiquedErrorCode.NETWORK, "Respond received code: " + response.code());
                    }
                }
            }
        });
    }

    protected void callOnFailure(final PiquedApiCallback<?> callback, final PiquedErrorCode code, final String errorMessage) {

        getNetworkHandler().post(new Runnable() {
            @Override
            public void run() {
                callback.onFailure(code, errorMessage);
            }
        });
    }

    private void callOnFailure(final JSONCallback<?> callback, final PiquedErrorCode code, final String errorMessage) {

        getNetworkHandler().post(new Runnable() {
            @Override
            public void run() {
                callback.onFailure(code, errorMessage);
            }
        });
    }


    private <T> void callOnSuccess(final PiquedApiCallback<T> callback, final T obj) {

        getNetworkHandler().post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(obj);
            }
        });
    }

    private Handler getNetworkHandler() {
        return mMainLoopHandler;
    }
}
