package io.piqued.api.piqued;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.piqued.api.PiquedApiCallback;
import io.piqued.api.PiquedNetworkApi;
import io.piqued.api.PiquedRequest;
import io.piqued.api.square.FourSquareApi;
import io.piqued.api.square.FourSquareApiImpl;
import io.piqued.cloud.model.piqued.BasketData;
import io.piqued.cloud.model.piqued.ImageContainer;
import io.piqued.cloud.model.piqued.PiquedBookmarkResponse;
import io.piqued.cloud.model.piqued.UserToken;
import io.piqued.cloud.model.piqued.Venue;
import io.piqued.cloud.util.CloudPreference;
import io.piqued.cloud.util.ImageUploadProgressListener;
import io.piqued.cloud.util.RegionBounds;
import io.piqued.database.post.CloudPost;
import io.piqued.database.user.User;
import io.piqued.database.util.PiquedErrorCode;
import io.piqued.model.Comment;
import io.piqued.model.imageupload.ImageUploadResult;
import io.piqued.model.imageupload.PreSignObject;
import io.piqued.service.imageupload.ImageUploadType;
import io.piqued.utils.upload.ImageUploadUtil;

import static io.piqued.api.piqued.PiquedApiImpl.Action.BOOKMARKS_GET;
import static io.piqued.api.piqued.PiquedApiImpl.Action.COMMENT_CREATE;
import static io.piqued.api.piqued.PiquedApiImpl.Action.COMMENT_GET;
import static io.piqued.api.piqued.PiquedApiImpl.Action.POST_BOOKMARK;
import static io.piqued.api.piqued.PiquedApiImpl.Action.POST_CREATE;
import static io.piqued.api.piqued.PiquedApiImpl.Action.POST_DELETE;
import static io.piqued.api.piqued.PiquedApiImpl.Action.POST_GET;
import static io.piqued.api.piqued.PiquedApiImpl.Action.POST_HISTORY;
import static io.piqued.api.piqued.PiquedApiImpl.Action.POST_HISTORY_OTHERS;
import static io.piqued.api.piqued.PiquedApiImpl.Action.POST_REACT;
import static io.piqued.api.piqued.PiquedApiImpl.Action.POST_REGION;
import static io.piqued.api.piqued.PiquedApiImpl.Action.PROFILE_EDIT;
import static io.piqued.api.piqued.PiquedApiImpl.Action.PUSH_NOTIFICATION_DELETE;
import static io.piqued.api.piqued.PiquedApiImpl.Action.PUSH_NOTIFICATION_PUT;
import static io.piqued.api.piqued.PiquedApiImpl.Action.USERS_CREATE;
import static io.piqued.api.piqued.PiquedApiImpl.Action.USERS_GET;
import static io.piqued.api.piqued.PiquedApiImpl.Action.USER_FOLLOW;
import static io.piqued.api.piqued.PiquedApiImpl.Action.USER_LOGIN;
import static io.piqued.api.piqued.PiquedApiImpl.Action.USER_LOGIN_OAUTH;

/**
 * Piqued
 * <p>
 * Created by Kenny M. Liou on 11/16/16.
 * <p>
 * Piqued Inc.
 */

public class PiquedApiImpl extends PiquedNetworkApi implements PiquedApi {

    enum Action {
        USERS_CREATE("/users.json", NetworkAction.POST),
        USERS_GET("/users/%d.json", NetworkAction.GET),
        USER_FOLLOW("/users/%d/follow.json", NetworkAction.POST),
        USER_LOGIN("/users/login.json", NetworkAction.POST),
        USER_LOGIN_OAUTH("/users/oauth.json", NetworkAction.POST),
        FOLLOWING("/users/%d/followings.json", NetworkAction.GET),
        FOLLOWERS("/users/%d/followers.json", NetworkAction.GET),
        PROFILE("/profile.json", NetworkAction.GET),
        PROFILE_EDIT("/profile.json", NetworkAction.PATCH),
        EVENT_GET("/events.json", NetworkAction.GET),
        POST_HISTORY("/posts.json", NetworkAction.GET),
        POST_HISTORY_OTHERS("/users/%d/posts.json", NetworkAction.GET),
        POST_CREATE("/posts.json", NetworkAction.POST),
        POST_DELETE("/posts/%d.json", NetworkAction.DELETE),
        POST_GET("/posts/%d.json", NetworkAction.GET),
        POST_BOOKMARK("/posts/%d/bookmark.json", NetworkAction.POST),
        BOOKMARKS_GET("/bookmarks.json", NetworkAction.GET),
        POST_REGION("/posts/region.json", NetworkAction.GET),
        POST_IMPORT("/posts/import.json", NetworkAction.GET),
        POST_REACT("/posts/%d/reactions.json", NetworkAction.POST),
        POST_REACT_GET("/posts/%d/reactions.json", NetworkAction.GET),
        POSTS_GET("/posts/%ds.json", NetworkAction.GET), //TODO: need special care cause it takes more than one post ID
        COMMENT_CREATE("/posts/%d/comments.json", NetworkAction.POST),
        COMMENT_GET("/posts/%d/comments.json", NetworkAction.GET),
        PUSH_NOTIFICATION_PUT("/users/%d/devices/%s", NetworkAction.PUT),
        PUSH_NOTIFICATION_DELETE("/users/%d/devices/%s", NetworkAction.DELETE);

        private final String mPath;
        private final NetworkAction mAction;
        Action(String path, NetworkAction action) {
            mPath = path;
            mAction = action;
        }

        String getPath() {
            return mPath;
        }

        NetworkAction getAction() {
            return mAction;
        }
    }

    private final FourSquareApi mFourSquareApi;
    private final ImageUploadUtil mImageUploadUtil;

    public PiquedApiImpl(@NonNull Context context, @NonNull CloudPreference cloudPreference) {
        super(context, cloudPreference);
        mFourSquareApi = new FourSquareApiImpl(context, cloudPreference);
        mImageUploadUtil = new ImageUploadUtil(context, cloudPreference);
    }

    private GsonBuilder builder = new GsonBuilder();

    private void networkHelper(String path, NetworkAction action, JSONObject payload, JSONCallback<?> callback) {
        super.networkHelper(new PiquedRequest(path, action), payload, callback);
    }

    @Override
    public void createAccount(@NotNull String email,
                              @NotNull String password,
                              @NotNull String userName,
                              @NonNull final PiquedApiCallback<UserToken> callback) {
        try {
            JSONObject userObj = PiquedApiHelper.createNewUserObject(email, password, userName);

            networkHelper(
                    USERS_CREATE.getPath(),
                    USERS_CREATE.getAction(),
                    userObj,
                    new JSONCallback<UserToken>(callback) {
                        @Override
                        protected UserToken onReceiveResult(String responseString) {

                            Gson gson = builder.create();
                            return gson.fromJson(responseString, UserToken.class);
                        }
                    }
            );

        } catch (JSONException e) {
            callOnFailure(callback, PiquedErrorCode.JSON, "Failed to create new user account object");
        }
    }

    @Override
    public void loginWithSiteToken(@NonNull String siteName,
                                   @NonNull String token,
                                   @NonNull final PiquedApiCallback<UserToken> callback) {

        try {
            JSONObject login = PiquedApiHelper.createLoginSiteObject(siteName, token);

            networkHelper(
                    USER_LOGIN_OAUTH.getPath(),
                    USER_LOGIN_OAUTH.getAction(),
                    login,
                    new JSONCallback<UserToken>(callback) {
                        @Override
                        protected UserToken onReceiveResult(String responseString) {

                            Gson gson = builder.create();

                            return gson.fromJson(responseString, UserToken.class);
                        }
                    });
        } catch (JSONException e) {
            callOnFailure(callback, PiquedErrorCode.JSON, "Failed to create json object");
        }
    }

    @Override
    public void loginToUser(@NonNull String email, @NonNull String password, final PiquedApiCallback<UserToken> callback) {

        try {

            JSONObject login = PiquedApiHelper.createLoginObject(email, password);

            networkHelper(USER_LOGIN.getPath(), USER_LOGIN.getAction(), login, new JSONCallback<UserToken>(callback) {
                @Override
                protected UserToken onReceiveResult(String responseString) {

                    Gson gson = builder.create();

                    return gson.fromJson(responseString, UserToken.class);
                }
            });

        } catch (JSONException e) {

            callOnFailure(callback, PiquedErrorCode.JSON, "Failed to create json object");
        }
    }

    @Override
    public void fetchUserInfo(@Nullable UserToken token, long userId, @NonNull PiquedApiCallback<User> callback) {
        final String urlWithData = String.format(USERS_GET.getPath(), (int) userId);
        final String urlWithCred = PiquedApiHelper.createURLWithCredential(urlWithData, token);

        networkHelper(urlWithCred, USERS_GET.getAction(), null, new JSONCallback<User>(callback) {
            @Override
            protected User onReceiveResult(String responseString) {

                Gson gson = builder.create();

                return gson.fromJson(responseString, User.class);
            }
        });
    }

    @Override
    public void retrievePostsForRegion(final UserToken token,
                                       final int page,
                                       final int size,
                                       @NonNull final RegionBounds outerBound,
                                       @Nullable final RegionBounds innerBound,
                                       final PiquedApiCallback<List<CloudPost>> callback) {

        final String url = PiquedApiHelper.createURLForGetPostWithRegion(POST_REGION.getPath(),
                token,
                page,
                size,
                outerBound,
                innerBound);

        networkHelper(url, POST_REGION.getAction(), null, new JSONCallback<List<CloudPost>>(callback) {
            @Override
            protected List<CloudPost> onReceiveResult(String responseString) {

                ArrayList<CloudPost> posts = new ArrayList<>();
                Gson gson = builder.create();

                try {

                    JSONObject postObject = new JSONObject(responseString);
                    JSONArray postArray = postObject.getJSONArray("posts");

                    for (int i = 0; i < postArray.length(); i++) {
                        JSONObject postObj = postArray.getJSONObject(i);
                        posts.add(getPost(gson, postObj.toString()));
                    }

                } catch (JSONException e) {
                    callback.onFailure(PiquedErrorCode.JSON, "Failed to parse json when calling retrievePostsForRegion");
                }

                return posts;
            }
        });
    }

    @Override
    public void getBookmarks(int page, int size, @NonNull UserToken token, @NonNull final PiquedApiCallback<List<CloudPost>> callback) {

        final String urlWithCred = PiquedApiHelper.createPostHistoryUrlForUser(BOOKMARKS_GET.getPath(), page, size, token);
        networkHelper(urlWithCred, BOOKMARKS_GET.getAction(), null, new JSONCallback<List<CloudPost>>(callback) {
            @Override
            protected List<CloudPost> onReceiveResult(String responseString) {

                ArrayList<CloudPost> bookmarks = new ArrayList<>();
                Gson gson = builder.create();

                try {
                    JSONArray postArray = new JSONArray(responseString);

                    for (int i = 0; i < postArray.length(); i++) {
                        JSONObject postJson = (JSONObject) postArray.get(i);

                        bookmarks.add(getPost(gson, postJson.toString()));
                    }
                } catch (JSONException e) {
                    callback.onFailure(PiquedErrorCode.JSON, "Failed to parse json when calling getBookmarkPostsAsLiveData");
                }

                return bookmarks;
            }
        });
    }

    @Override
    public void getVenuesFromPost(@NotNull CloudPost post, @NotNull PiquedApiCallback<Venue> callback) {
        mFourSquareApi.getVenuesFromPost(post, callback);
    }

    @Override
    public void getPostById(long postId, @Nullable UserToken token, PiquedApiCallback<CloudPost> callback) {

        Action action = POST_GET;

        final String urlWithCred = PiquedApiHelper.createURLForGetPost(action.getPath(), postId, token);

        networkHelper(urlWithCred, action.getAction(), null, new JSONCallback<CloudPost>(callback) {
            @Override
            protected CloudPost onReceiveResult(String responseString) {

                Gson gson = builder.create();

                return getPost(gson, responseString);
            }
        });
    }

    @Override
    public void getComments(CloudPost post, @Nullable UserToken token, @NonNull final PiquedApiCallback<List<Comment>> callback) {
        final String urlWithCred = PiquedApiHelper.createURLWithCredential(String.format(COMMENT_GET.getPath(), post.getPostId()), token);

        networkHelper(urlWithCred, COMMENT_GET.getAction(), null, new JSONCallback<List<Comment>>(callback) {

            @Override
            protected List<Comment> onReceiveResult(String responseString) {

                ArrayList<Comment> comments = new ArrayList<>();
                Gson gson = builder.create();

                try {
                    JSONArray commentArray = new JSONArray(responseString);

                    for (int i = 0; i < commentArray.length(); i++) {

                        JSONObject postJson = (JSONObject) commentArray.get(i);

                        Comment comment = gson.fromJson(postJson.toString(), Comment.class);

                        comments.add(comment);
                    }
                } catch (JSONException e) {
                    callback.onFailure(PiquedErrorCode.JSON, "Failed to parse json when calling getEvents");
                }

                return comments;
            }
        });

    }

    @Override
    public void postComment(CloudPost post, UserToken token, String message, PiquedApiCallback<Comment> callback) {
        final String path = String.format(COMMENT_GET.getPath(), post.getPostId());

        try {
            networkHelper(path,
                    COMMENT_CREATE.getAction(),
                    PiquedApiHelper.createNewCommentObject(message, token),
                    new JSONCallback<Comment>(callback) {
                @Override
                protected Comment onReceiveResult(String responseString) {

                    Gson gson = builder.create();

                    Comment response = gson.fromJson(responseString, Comment.class);
                    return response;
                }
            });

        } catch (JSONException e) {

            callOnFailure(callback, PiquedErrorCode.JSON, "Failed to create json object");
        }
    }

    @Override
    public void reactToPost(@Nullable CloudPost post, int reaction, @Nullable UserToken token, @Nullable PiquedApiCallback<Void> callback) {

        final String path = String.format(POST_REACT.getPath(), post.getPostId());

        try {
            networkHelper(path,
                    POST_REACT.getAction(),
                    PiquedApiHelper.createReactToPostObject(reaction, token),
                    new JSONCallback<Void>(callback) {
                @Override
                protected Void onReceiveResult(String responseString) {

                    return null;
                }
            });

        } catch (JSONException e) {

            callOnFailure(callback, PiquedErrorCode.JSON, "Failed to create json object");
        }
    }

    @Override
    public void bookmarkPost(@NonNull CloudPost post, final boolean bookmarked, @NonNull UserToken token, @NonNull final PiquedApiCallback<PiquedBookmarkResponse> callback) {

        Action action = POST_BOOKMARK;

        final String path = String.format(action.getPath(), post.getPostId());

        try {
            networkHelper(path,
                    action.getAction(),
                    PiquedApiHelper.createBookmarkObject(bookmarked, token),
                    new JSONCallback<PiquedBookmarkResponse>(callback) {
                @Override
                protected PiquedBookmarkResponse onReceiveResult(String responseString) {

                    Gson gson = builder.create();
                    return gson.fromJson(responseString, PiquedBookmarkResponse.class);
                }
            });
        } catch (JSONException e) {
            callOnFailure(callback, PiquedErrorCode.JSON, "Failed to create json object: " + e.getMessage());
        }
    }

    @Override
    public void deletePost(@NonNull CloudPost post, @NonNull UserToken userToken, @Nullable PiquedApiCallback<Void> callback) {

        Action action = POST_DELETE;

        final String path = PiquedApiHelper.createURLWithCredential(String.format(action.getPath(), post.getPostId()), userToken);

        networkHelper(path,
                action.getAction(),
                null,
                new JSONCallback<Void>(callback) {
            @Override
            protected Void onReceiveResult(String responseString) {

                return null;
            }
        });
    }

    @Override
    public void getUserPostHistory(long userId, int page, int size, UserToken token, final PiquedApiCallback<List<CloudPost>> callback) {

        Action action = POST_HISTORY_OTHERS;

        final String path = PiquedApiHelper.createPostHistoryUrlForUser(String.format(action.getPath(), userId), page, size, token);

        networkHelper(path,
                action.getAction(),
                null,
                new JSONCallback<List<CloudPost>>(callback) {

            @Override
            protected List<CloudPost> onReceiveResult(String responseString) {

                try {
                    return getListFromResponse(responseString);
                } catch (JSONException e) {

                    callOnFailure(callback, PiquedErrorCode.JSON, e.getMessage());
                    return null;
                }
            }
        });
    }

    @Override
    public void getOwnerPostHistory(final int page, int size, UserToken token, final PiquedApiCallback<List<CloudPost>> callback) {

        Action action = POST_HISTORY;

        final String path = PiquedApiHelper.createUrlWithPageAndSize(
                PiquedApiHelper.createURLWithCredential(action.getPath(), token),
                page,
                size);

        networkHelper(path,
                action.getAction(),
                null,
                new JSONCallback<List<CloudPost>>(callback) {
            @Override
            protected List<CloudPost> onReceiveResult(String responseString) {

                try {
                    return getListFromResponse(responseString);

                } catch (JSONException e) {

                    callOnFailure(callback, PiquedErrorCode.JSON, e.getMessage());
                    return null;
                }
            }
        });
    }

    private List<CloudPost> getListFromResponse(String responseString) throws JSONException {
        ArrayList<CloudPost> posts = new ArrayList<CloudPost>();

        JSONObject object = new JSONObject(responseString);
        JSONArray postsArray = object.getJSONArray("posts");

        Gson gson = builder.create();

        for (int i = 0; i < postsArray.length(); i++) {

            JSONObject postJson = postsArray.getJSONObject(i);

            posts.add(getPost(gson, postJson.toString()));
        }

        return posts;
    }

    @Override
    public void searchVenueNearLatLng(LatLng latLng, PiquedApiCallback<List<Venue>> callback) {

        mFourSquareApi.searchVenue(latLng, callback);
    }

    @Override
    public void searchVenueLatLngWithQuery(@Nullable String query, @NonNull LatLng latLng, @NonNull PiquedApiCallback<List<Venue>> callback) {

        if (TextUtils.isEmpty(query)) {
            mFourSquareApi.searchVenue(latLng, callback);
        } else {
            mFourSquareApi.searchVenueWithQuery(query, latLng, callback);
        }
    }

    @Override
    public void followUser(Long userId, boolean followed, @NonNull UserToken userToken, PiquedApiCallback<Void> callback) {

        Action action = USER_FOLLOW;

        final String path = String.format(action.getPath(), userId);

        try {
            networkHelper(path,
                    action.getAction(),
                    PiquedApiHelper.createFollowObject(followed, userToken),
                    new JSONCallback<Void>(callback) {
                @Override
                protected Void onReceiveResult(String responseString) {

                    return null;
                }
            });

        } catch (JSONException e) {
            callOnFailure(callback, PiquedErrorCode.JSON, "Failed to create json object: " + e.getMessage());
        }
    }

    @Override
    public void updateUserData(String userName, String email, String password, ImageUploadResult uploadResult, @NonNull UserToken userToken, @NonNull PiquedApiCallback<User> callback) {

        Action action = PROFILE_EDIT;

        try {
            final JSONObject payload =
                    PiquedApiHelper.createEditUserData(
                            userName,
                            email,
                            password,
                            uploadResult,
                            userToken
                    );

            networkHelper(action.getPath(),
                    action.getAction(),
                    payload,
                    new JSONCallback<User>(callback) {
                @Override
                protected User onReceiveResult(String responseString) {

                    Gson gson = builder.create();

                    return gson.fromJson(responseString, User.class);
                }
            });
        } catch (JSONException e) {
            callOnFailure(callback, PiquedErrorCode.JSON, "Failed to create json object: " + e.getMessage());
        }
    }

    @Override
    public void getPreSign(@NonNull PiquedApiCallback<PreSignObject> callback) {
        mImageUploadUtil.fetchPreSign(callback);
    }

    @Override
    public void uploadImageToPreSign(@NonNull ImageUploadType type,
                                     @NonNull PreSignObject preSign,
                                     @NotNull String imageUrl,
                                     @NonNull ImageContainer imageBase64,
                                     @Nullable ImageUploadProgressListener listener,
                                     @NonNull PiquedApiCallback<ImageUploadResult> callback) {
        uploadImageToPreSign(type.ordinal(), preSign, imageUrl, imageBase64, listener, callback);
    }

    @Override
    public void uploadImageToPreSign(int index, @NotNull PreSignObject preSign, @NotNull String imageUrl, @NotNull ImageContainer imageBase64, @org.jetbrains.annotations.Nullable ImageUploadProgressListener listener, @NotNull PiquedApiCallback<ImageUploadResult> callback) {
        mImageUploadUtil.uploadImageToCache(index, preSign, imageUrl, imageBase64, listener, callback);
    }

    @Override
    public void createBasket(@NonNull ImageUploadResult imageUrlLarge,
                             @NonNull ImageUploadResult imageUrlSmall,
                             @NonNull BasketData newBasket,
                             @NonNull UserToken userToken,
                             @NonNull PiquedApiCallback<Void> callback) {

        Action action = POST_CREATE;

        try {
            JSONObject payload = PiquedApiHelper.createNewBasketObject(imageUrlLarge, imageUrlSmall, newBasket, userToken);
            networkHelper(action.getPath(), action.getAction(), payload, new JSONCallback<Void>(callback) {
                @Override
                protected Void onReceiveResult(String responseString) {

                    // TODO: 4/29/17 report success
                    return null;
                }
            });

        } catch (JSONException e) {
            callOnFailure(callback, PiquedErrorCode.JSON, "Failed to create json object: " + e.getMessage());
        }
    }

    @Override
    public void registerForPushNotification(@NonNull final String deviceToken,
                                            @NonNull UserToken userToken,
                                            @NonNull PiquedApiCallback<Boolean> callback) {

        Action action = PUSH_NOTIFICATION_PUT;

        try {

            JSONObject payload = PiquedApiHelper.createPushNotificationDeviceObject(userToken);

            final String path = String.format(action.getPath(), userToken.getUserId(), deviceToken);

            networkHelper(path, action.getAction(), payload, new JSONCallback<Boolean>(callback) {
                @Override
                protected Boolean onReceiveResult(String responseString) {
                    return true;
                }
            });

        } catch (JSONException e) {
            callOnFailure(callback, PiquedErrorCode.JSON, "Failed to create device token json object" + e.getMessage());
        }
    }

    @Override
    public void unregisterForPushNotification(@NonNull final String deviceToken,
                                              @NonNull UserToken userToken,
                                              @NonNull PiquedApiCallback<Boolean> callback) {

        Action action = PUSH_NOTIFICATION_DELETE;

        final String path = PiquedApiHelper.createURLWithCredential(String.format(action.getPath(), userToken.getUserId(), deviceToken), userToken);

        networkHelper(path, action.getAction(), null, new JSONCallback<Boolean>(callback) {
            @Override
            protected Boolean onReceiveResult(String responseString) {
                return true;
            }
        });

    }

    private CloudPost getPost(final Gson gson, final String jsonString) {
        return gson.fromJson(jsonString, CloudPost.class);
    }
}
