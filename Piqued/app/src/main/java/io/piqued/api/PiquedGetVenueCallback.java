package io.piqued.api;

import io.piqued.cloud.model.piqued.Venue;
import io.piqued.database.util.PiquedErrorCode;

/**
 * Created by Kenny M. Liou on 10/21/17.
 * Piqued Inc.
 */

public interface PiquedGetVenueCallback {

    void onSuccess(long postId, Venue venue);

    void onFailure(PiquedErrorCode errorCode, String payload);
}
