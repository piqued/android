package io.piqued.api

/**
 *
 * Created by Kenny M. Liou on 12/29/17.
 * Piqued Inc.
 *
 */
interface PiquedNetworkRequest {

    fun getUrl(piquedHostUrl: String) : String
    fun getNetworkAction() : PiquedNetworkApi.NetworkAction
}

class PiquedRequest(val path: String, private val action: PiquedNetworkApi.NetworkAction) : PiquedNetworkRequest {

    override fun getUrl(piquedHostUrl: String): String {
        return piquedHostUrl + path
    }

    override fun getNetworkAction(): PiquedNetworkApi.NetworkAction {
        return action
    }
}

class FourSquareRequest(private val fullUrl: String, private val action: PiquedNetworkApi.NetworkAction) : PiquedNetworkRequest {

    override fun getUrl(piquedHostUrl: String): String {
        return fullUrl
    }

    override fun getNetworkAction(): PiquedNetworkApi.NetworkAction {
        return action
    }
}
