package io.piqued.api.square;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import io.piqued.api.PiquedApiCallback;
import io.piqued.database.post.CloudPost;
import io.piqued.cloud.model.piqued.Venue;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/3/17.
 * Piqued Inc.
 */

public interface FourSquareApi {

    /**
     * Fetch venue info from cache, if not exists, fetch it from network
     * @param post post that has venue info
     * @param callback callback
     */
    void getVenuesFromPost(CloudPost post, PiquedApiCallback<Venue> callback);

    void searchVenue(LatLng latLng, PiquedApiCallback<List<Venue>> callback);

    void searchVenueWithQuery(@NonNull String query, @NonNull LatLng latLng, PiquedApiCallback<List<Venue>> callback);
}
