package io.piqued.api.piqued

import android.os.Build
import android.text.TextUtils
import io.piqued.BuildConfig
import io.piqued.cloud.model.piqued.BasketData
import io.piqued.cloud.model.piqued.UserToken
import io.piqued.cloud.util.RegionBounds
import io.piqued.model.imageupload.ImageUploadResult
import io.piqued.utils.Constants
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 *
 * Created by Kenny M. Liou on 11/18/17.
 * Piqued Inc.
 *
 */
class PiquedApiHelper {

    companion object {

        @JvmStatic
        @Throws(JSONException::class)
        fun createNewUserObject(email: String,
                                password: String,
                                userName: String) :JSONObject {

            val userObject = JSONObject()
            val userDetail = JSONObject()

            userDetail.put(Constants.FIELD_USER_NAME, userName)
            userDetail.put(Constants.FIELD_USER_EMAIL, email)
            userDetail.put(Constants.FIELD_USER_PASSWORD, password)
            userDetail.put(Constants.FIELD_USER_PASSWORD_CONFIRMATION, password)

            userObject.put(Constants.FIELD_USER, userDetail)

            return userObject
        }

        @JvmStatic
        @Throws(JSONException::class)
        fun createLoginSiteObject(siteName: String,
                                  token: String) : JSONObject {

            val loginInfo = JSONObject()

            loginInfo.put("site_name", siteName)
            loginInfo.put("site_token", token)

            val wrapper = JSONObject()

            wrapper.put("user", loginInfo)

            return wrapper
        }

        @JvmStatic
        @Throws(JSONException::class)
        fun createLoginObject(email: String, password: String): JSONObject {

            val loginInfo = JSONObject()

            loginInfo.put("email", email)
            loginInfo.put("password", password)

            val wrapper = JSONObject()

            wrapper.put("user", loginInfo)

            return wrapper
        }

        @JvmStatic
        @Throws(JSONException::class)
        fun createEditUserData(newName: String?,
                               email: String?,
                               password: String?,
                               uploadResult: ImageUploadResult?,
                               token: UserToken): JSONObject {

            val newUserData = JSONObject()
            val userDataObj = JSONObject()

            newUserData.put(Constants.FIELD_USER, userDataObj)

            if (!TextUtils.isEmpty(newName)) {
                userDataObj.put(Constants.FIELD_USER_NAME, newName)
            }
            if (!TextUtils.isEmpty(email)) {
                userDataObj.put(Constants.FIELD_USER_EMAIL, email)
            }
            if (!TextUtils.isEmpty(password)) {
                userDataObj.put(Constants.FIELD_USER_PASSWORD, password)
                userDataObj.put(Constants.FIELD_USER_PASSWORD_CONFIRMATION, password)
            }
            if (uploadResult != null) {
                val avatar = JSONObject()
                avatar.put(Constants.FIELD_AVATAR_ID, uploadResult.id)
                avatar.put(Constants.FIELD_AVATAR_STORAGE, uploadResult.storage)
                avatar.put(Constants.FIELD_AVATAR_METADATA, uploadResult.metadata)
                userDataObj.put(Constants.FIELD_AVATAR, avatar)
            }

            addCredObject(newUserData, token)

            return newUserData
        }

        @JvmStatic
        @Throws(JSONException::class)
        fun createFollowObject(followed: Boolean,
                                       token: UserToken): JSONObject {

            val newFollowObj = JSONObject()
            val follow = JSONObject()

            newFollowObj.put(Constants.FIELD_FOLLOW, follow)
            follow.put(Constants.FIELD_FOLLOWED, if (followed) "true" else "false")

            addCredObject(newFollowObj, token)

            return newFollowObj
        }

        @JvmStatic
        @Throws(JSONException::class)
        fun createNewBasketObject(
                imageUrlLarge: ImageUploadResult,
                imageUrlSmall: ImageUploadResult,
                newBasket: BasketData,
                userToken: UserToken): JSONObject {

            val newBasketObject = JSONObject()
            addCredObject(newBasketObject, userToken)

            // create images object
            val images = JSONArray()
            val image = JSONObject()
            image.put("lg", createImageObject("large", imageUrlLarge))
            image.put("sm", createImageObject("small", imageUrlSmall))
            images.put(image)

            // create basket object
            val basketObject = JSONObject()
            basketObject.put(Constants.FIELD_LOCATION_LAT, newBasket.latitude)
            basketObject.put(Constants.FIELD_LOCATION_LNG, newBasket.longitude)
            basketObject.put(Constants.FIELD_TITLE, newBasket.title)
            basketObject.put(Constants.FIELD_LOCATION_NAME, newBasket.title)
            basketObject.put(Constants.FIELD_DESCRIPTION, newBasket.description)
            basketObject.put(Constants.FIELD_ADDRESS, newBasket.fullAddress)
            basketObject.put(Constants.FIELD_FOURSQUARE_VENUE_ID, newBasket.fourSquareVenueId)
            basketObject.put(Constants.FIELD_FOURSQUARE_CC, newBasket.countryCode)
            basketObject.put(Constants.FIELD_FOURSQUARE_CITY, newBasket.city)
            basketObject.put(Constants.FIELD_FOURSQUARE_STATE, newBasket.state)
            basketObject.put(Constants.FIELD_YELP_ID, newBasket.yelpId)
            basketObject.put(Constants.FIELD_IMAGES, images)
            basketObject.put(Constants.FIELD_PRIMARY_COLOR, newBasket.primaryColor)

            newBasketObject.put(Constants.FIELD_POST, basketObject)

            return newBasketObject
        }

        @JvmStatic
        @Throws(JSONException::class)
        fun createImageObject(prefix: String, result: ImageUploadResult): JSONObject {

            val imageDetails = JSONObject()
            val metadata = JSONObject()

            // setup metadata
            metadata.put("filename", prefix + "_image.jpg")
            metadata.put("mime_type", "image/jpeg")
            metadata.put("size", result.sizeInBytes)
            imageDetails.put("id", result.id)
            imageDetails.put("storage", result.storage)
            imageDetails.put("metadata", metadata)

            return imageDetails
        }

        @JvmStatic
        @Throws(JSONException::class)
        fun createBookmarkObject(bookmarked: Boolean, userToken: UserToken): JSONObject {
            val newBookmarkObject = JSONObject()
            val bookmarkObject = JSONObject()
            bookmarkObject.put("bookmarked", if (bookmarked) "true" else "false")

            newBookmarkObject.put("bookmark", bookmarkObject)
            addCredObject(newBookmarkObject, userToken)

            return newBookmarkObject
        }

        @JvmStatic
        @Throws(JSONException::class)
        fun createReactToPostObject(reaction: Int?, userToken: UserToken): JSONObject {
            val newReactionPayload = JSONObject()
            val reactionObject = JSONObject()
            reactionObject.put("reaction", reaction)

            addCredObject(newReactionPayload, userToken)
            newReactionPayload.put("reaction", reactionObject)

            return newReactionPayload
        }

        @JvmStatic
        @Throws(JSONException::class)
        fun createNewCommentObject(message: String, userToken: UserToken): JSONObject {

            val newCommentObject = JSONObject()
            val messageObject = JSONObject()
            messageObject.put("message", message)

            newCommentObject.put("comment", messageObject)

            addCredObject(newCommentObject, userToken)

            return newCommentObject
        }

        @JvmStatic
        @Throws(JSONException::class)
        fun addCredObject(wrapperObject: JSONObject, userToken: UserToken) {
            val credObject = JSONObject()
            credObject.put(Constants.FIELD_TOKEN_ID, userToken.token)
            credObject.put(Constants.FIELD_SECRET, userToken.secret)

            wrapperObject.put(Constants.FIELD_CREDS, credObject)
        }

        @JvmStatic
        @Throws(JSONException::class)
        fun createPushNotificationDeviceObject(userToken: UserToken) : JSONObject {
            val wrapper = JSONObject()
            addCredObject(wrapper, userToken)

            val device = JSONObject()

            device.put("udid", UUID.randomUUID().toString())
            device.put("platform", "android")
            device.put("os_version", Build.VERSION.RELEASE)
            device.put("environment", BuildConfig.PIQUED_BUILD_ENV)

            wrapper.put("device", device)

            return wrapper;
        }

        @JvmStatic
        fun createPostHistoryUrlForUser(url: String, page: Int, size: Int, token: UserToken): String {

            return createUrlWithPageAndSize(createURLWithCredential(url, token), page, size)
        }

        @JvmStatic
        fun createUrlWithPageAndSize(url: String, page: Int, size: Int): String {

            val sb = StringBuilder()

            sb.append(url)

            sb.append("&page=")
            sb.append(page)

            sb.append("&size")
            sb.append(size)

            return sb.toString()
        }

        @JvmStatic
        fun createURLWithCredential(url: String, token: UserToken?): String {

            val sb = StringBuilder()

            sb.append(url)

            sb.append("?creds[token]=")

            sb.append(token?.token ?: "") // token

            sb.append("&creds[secret]=")

            sb.append(token?.secret ?: "")

            return sb.toString()
        }

        @JvmStatic
        fun createURLForGetPost(path: String,
                                postId: Long,
                                token: UserToken?): String {

            return createURLWithCredential(String.format(path, postId), token)
        }

        @JvmStatic
        fun createURLForGetPostWithRegion(path: String,
                                          token: UserToken,
                                          page: Int,
                                          size: Int,
                                          outerBound: RegionBounds,
                                          innerBound: RegionBounds?): String {

            val stringBuilder = StringBuilder()

            stringBuilder.append(createURLWithCredential(path, token))

            appendKeyAndValue(stringBuilder, "page", page)
            appendKeyAndValue(stringBuilder, "size", size)

            // outer bounds
            appendKeyAndValue(stringBuilder, "region[outer_bounds][lat_min]", outerBound.latMin)
            appendKeyAndValue(stringBuilder, "region[outer_bounds][lat_max]", outerBound.latMax)
            appendKeyAndValue(stringBuilder, "region[outer_bounds][lng_min]", outerBound.lngMin)
            appendKeyAndValue(stringBuilder, "region[outer_bounds][lng_max]", outerBound.lngMax)

            if (innerBound != null) {
                // inner bounds
                appendKeyAndValue(stringBuilder, "region[inner_bounds][lat_min]", outerBound.latMin)
                appendKeyAndValue(stringBuilder, "region[inner_bounds][lat_max]", outerBound.latMax)
                appendKeyAndValue(stringBuilder, "region[inner_bounds][lng_min]", outerBound.lngMin)
                appendKeyAndValue(stringBuilder, "region[inner_bounds][lng_max]", outerBound.lngMax)
            }

            return stringBuilder.toString()
        }

        private fun appendKeyAndValue(builder: StringBuilder, key: String, value: Int) {
            appendKeyAndValue(builder, key, Integer.toString(value))
        }

        private fun appendKeyAndValue(builder: StringBuilder, key: String, value: Double) {
            appendKeyAndValue(builder, key, java.lang.Double.toString(value))
        }

        private fun appendKeyAndValue(builder: StringBuilder, key: String, value: String) {
            builder.append("&")
            builder.append(key)
            builder.append("=")
            builder.append(value)
        }
    }
}