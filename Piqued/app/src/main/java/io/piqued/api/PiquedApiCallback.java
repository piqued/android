package io.piqued.api;

import io.piqued.database.util.PiquedErrorCode;

/**
 * Piqued
 * Created by Kenny M. Liou on 11/22/16.
 * Piqued Inc.
 */

public interface PiquedApiCallback<T> {

    void onSuccess(T data);

    void onFailure(PiquedErrorCode errorCode, String payload);
}
