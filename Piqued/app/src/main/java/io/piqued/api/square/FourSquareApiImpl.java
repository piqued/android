package io.piqued.api.square;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import io.piqued.R;
import io.piqued.api.FourSquareRequest;
import io.piqued.api.PiquedApiCallback;
import io.piqued.api.PiquedNetworkApi;
import io.piqued.cloud.model.foursquare.FourSquareVenue;
import io.piqued.cloud.util.CloudPreference;
import io.piqued.database.post.CloudPost;
import io.piqued.cloud.model.piqued.Venue;
import io.piqued.database.util.PiquedErrorCode;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/3/17.
 * Piqued Inc.
 */

public class FourSquareApiImpl extends PiquedNetworkApi implements FourSquareApi {

    private static final String CLIENT_ID = "%s&client_id=%s";
    private static final String CLIENT_SECRET = "%s&client_secret=%s";
    private static final String VERSION = "%s&v=20170729"; // 2017, JUL 29
    private static final String EXPLORE_LL = "ll=%f,%f";
    private static final String QUERY = "query=%s";
    private static final String EN_DATA = "&locale=en";

    private enum RequestType {
        EXPLORE("https://api.foursquare.com/v2/venues/explore?%s"),
        SEARCH("https://api.foursquare.com/v2/venues/search?%s"),
        VENUES("https://api.foursquare.com/v2/venues/%s?");

        private final String mPath;
        RequestType(String path) {
            mPath = path;
        }

        public String getPath() {
            return mPath;
        }
    }

    private final GsonBuilder builder = new GsonBuilder();
    public FourSquareApiImpl(@NonNull Context context, @NonNull CloudPreference preferences) {
        super(context, preferences);
    }

    private void networkHelper(String fullUrl, NetworkAction action, JSONCallback<?> callback) {
        super.networkHelper(new FourSquareRequest(fullUrl, action), null, callback);
    }

    @Override
    public void getVenuesFromPost(CloudPost post, PiquedApiCallback<Venue> callback) {
        final String venueId = post.getFourSquareId();

        if (venueId == null || venueId.length() < 1) {
            callback.onFailure(PiquedErrorCode.INVALID_ARGUMENT, "Invalid venueId, venueID is null");
        }

        final String urlString = addCredential(createGetVenueString(RequestType.VENUES, venueId)) + EN_DATA; // hack for now

        networkHelper(urlString, NetworkAction.GET, new JSONCallback<Venue>(callback) {
            @Override
            protected Venue onReceiveResult(String responseString) {

                Gson gson = builder.create();

                FourSquareResponse response = gson.fromJson(responseString, FourSquareResponse.class);
                return Venue.fromFourSquare(getContext(), response.response.venue);
            }
        });
    }

    @Override
    public void searchVenue(LatLng latLng, PiquedApiCallback<List<Venue>> callback) {

        final String urlString = addCredential(createExploreString(latLng));

        networkHelper(urlString, NetworkAction.GET, new JSONCallback<List<Venue>>(callback) {
            @Override
            protected List<Venue> onReceiveResult(String responseString) {
                Gson gson = builder.create();
                ArrayList<Venue> result = new ArrayList<>();

                JsonParser parser = new JsonParser();
                JsonObject serverResponse = (JsonObject) parser.parse(responseString);
                JsonObject response = serverResponse.getAsJsonObject("response");
                JsonArray groups = response.getAsJsonArray("groups");
                JsonArray items = groups.get(0).getAsJsonObject().getAsJsonArray("items");

                for (int i = 0; i < items.size(); i++) {
                    JsonObject item = items.get(i).getAsJsonObject();
                    JsonObject venueObject = item.getAsJsonObject("venue");
                    FourSquareVenue fsVenue = gson.fromJson(venueObject, FourSquareVenue.class);
                    result.add(Venue.fromFourSquare(getContext(), fsVenue));
                }

                return result;
            }
        });
    }

    @Override
    public void searchVenueWithQuery(@NonNull String query, @Nullable LatLng latLng, PiquedApiCallback<List<Venue>> callback) {

        final String urlString = addCredential(createSearchString(query, latLng));

        networkHelper(urlString, NetworkAction.GET, new JSONCallback<List<Venue>>(callback) {
            @Override
            protected List<Venue> onReceiveResult(String responseString) {
                Gson gson = builder.create();
                ArrayList<Venue> result = new ArrayList<>();

                JsonParser parser = new JsonParser();
                JsonObject serverResponse = (JsonObject) parser.parse(responseString);
                JsonObject response = serverResponse.getAsJsonObject("response");
                JsonArray venues = response.getAsJsonArray("venues");

                for (int i = 0; i < venues.size(); i++) {
                    JsonObject venueObject = venues.get(i).getAsJsonObject();
                    FourSquareVenue fsVenue = gson.fromJson(venueObject, FourSquareVenue.class);
                    result.add(Venue.fromFourSquare(getContext(), fsVenue));
                }

                return result;
            }
        });
    }

    private String createGetVenueString(RequestType type, String venueId) {

        final String result = String.format(type.getPath(), venueId);

        return addVersionParams(result);
    }

    private String createExploreString(LatLng latLng) {

        final String result = String.format(RequestType.EXPLORE.getPath(), String.format(EXPLORE_LL, latLng.latitude, latLng.longitude));

        return addVersionParams(result);
    }

    private String createSearchString(String query, LatLng latLng) {

        final StringBuilder sb = new StringBuilder();

        sb.append(String.format(QUERY, query));
        sb.append("&");
        sb.append(String.format(EXPLORE_LL, latLng.latitude, latLng.longitude));

        final String result = String.format(RequestType.SEARCH.getPath(), sb.toString());

        return addVersionParams(result);
    }

    private String addVersionParams(final String path) {

        return String.format(VERSION, path);
    }

    private String addCredential(final String pathWithVersion) {

        return String.format(CLIENT_SECRET, String.format(CLIENT_ID, pathWithVersion, getContext().getString(R.string.four_square_client_id)), getContext().getString(R.string.four_square_client_secret));
    }


}
