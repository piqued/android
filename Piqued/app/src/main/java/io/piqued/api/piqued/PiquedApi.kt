package io.piqued.api.piqued

import com.google.android.gms.maps.model.LatLng
import io.piqued.api.PiquedApiCallback
import io.piqued.cloud.model.piqued.*
import io.piqued.cloud.util.ImageUploadProgressListener
import io.piqued.cloud.util.RegionBounds
import io.piqued.database.post.CloudPost
import io.piqued.database.user.User
import io.piqued.model.Comment
import io.piqued.model.imageupload.ImageUploadResult
import io.piqued.model.imageupload.PreSignObject
import io.piqued.service.imageupload.ImageUploadType

/**
 * Piqued
 *
 * Created by Kenny M. Liou on 11/16/16.
 *
 * Piqued Inc.
 */

interface PiquedApi {

    fun createAccount(email: String,
                      password: String,
                      userName: String,
                      callback: PiquedApiCallback<UserToken>)

    fun loginWithSiteToken(siteName: String,
                           token: String,
                           callback: PiquedApiCallback<UserToken>)

    fun loginToUser(email: String,
                    password: String,
                    callback: PiquedApiCallback<UserToken>)

    fun fetchUserInfo(token: UserToken?,
                      userId: Long,
                      callback: PiquedApiCallback<User>)

    fun retrievePostsForRegion(token: UserToken,
                               page: Int,
                               size: Int,
                               outerBound: RegionBounds,
                               innerBound: RegionBounds?,
                               callback: PiquedApiCallback<List<CloudPost>>)

    fun getBookmarks(page: Int, size: Int, token: UserToken, callback: PiquedApiCallback<List<CloudPost>>)

    fun getVenuesFromPost(post: CloudPost, callback: PiquedApiCallback<Venue>)

    fun getPostById(postId: Long, token: UserToken?, callback: PiquedApiCallback<CloudPost>)

    fun getComments(post: CloudPost, token: UserToken?, callback: PiquedApiCallback<List<Comment>>)

    fun postComment(post: CloudPost,
                    token: UserToken,
                    message: String,
                    callback: PiquedApiCallback<Comment>)

    fun reactToPost(post: CloudPost, reaction: Int, token: UserToken, callback: PiquedApiCallback<Void?>)

    fun bookmarkPost(post: CloudPost, bookmarked: Boolean, userToken: UserToken, callback: PiquedApiCallback<PiquedBookmarkResponse>)

    fun deletePost(post: CloudPost, userToken: UserToken, callback: PiquedApiCallback<Void?>)

    fun getUserPostHistory(userId: Long, page: Int, size: Int, token: UserToken, callback: PiquedApiCallback<List<CloudPost>>)

    fun getOwnerPostHistory(page: Int, size: Int, token: UserToken, callback: PiquedApiCallback<List<CloudPost>>)

    fun searchVenueNearLatLng(latLng: LatLng, callback: PiquedApiCallback<List<Venue>>)

    fun searchVenueLatLngWithQuery(query: String?, latLng: LatLng, callback: PiquedApiCallback<List<Venue>>)

    fun followUser(userId: Long?, following: Boolean, userToken: UserToken, callback: PiquedApiCallback<Void?>)

    fun updateUserData(userName: String?, email: String?, password: String?, imageUploadResult: ImageUploadResult?, userToken: UserToken, callback: PiquedApiCallback<User>)

    /**
     * NOTE: these method run on current thread, and this method is used by CreatePostService ONLY!
     */
    fun getPreSign(callback: PiquedApiCallback<PreSignObject>)

    fun uploadImageToPreSign(type: ImageUploadType,
                             preSign: PreSignObject,
                             imageUrl: String,
                             imageBase64: ImageContainer,
                             listener: ImageUploadProgressListener?,
                             callback: PiquedApiCallback<ImageUploadResult>)

    fun uploadImageToPreSign(index: Int,
                             preSign: PreSignObject,
                             imageUrl: String,
                             imageBase64: ImageContainer,
                             listener: ImageUploadProgressListener?,
                             callback: PiquedApiCallback<ImageUploadResult>)

    fun createBasket(imageUrlLarge: ImageUploadResult,
                     imageUrlSmall: ImageUploadResult,
                     newBasket: BasketData,
                     userToken: UserToken,
                     callback: PiquedApiCallback<Void?>)

    fun registerForPushNotification(deviceToken: String,
                                    userToken: UserToken,
                                    callback: PiquedApiCallback<Boolean>)

    fun unregisterForPushNotification(deviceToken: String,
                                      userToken: UserToken,
                                      callback: PiquedApiCallback<Boolean>)
}
