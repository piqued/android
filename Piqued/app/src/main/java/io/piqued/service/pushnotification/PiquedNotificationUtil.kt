package io.piqued.service.pushnotification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import androidx.core.app.NotificationCompat
import io.piqued.R
import io.piqued.activities.PiquedActivity
import io.piqued.database.event.Event

abstract class PiquedNotificationUtil {
    companion object {
        private const val defaultChannel = "PiquedDefault"

        fun createNotificationChannel(c: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val manager = c.getSystemService(
                        PiquedActivity.NOTIFICATION_SERVICE) as NotificationManager
                val channel = NotificationChannel("PiquedDefault", "Piqued Notification",
                        NotificationManager.IMPORTANCE_DEFAULT)
                manager.createNotificationChannel(channel)
            }
        }

        fun createNotification(c: Context, event: Event, image: Bitmap): Notification {
            return NotificationCompat.Builder(c, defaultChannel)
                    .setSmallIcon(R.drawable.ic_stat_onesignal_default)
                    .setLargeIcon(image)
                    .setContentTitle(event.actor.name)
                    .setContentText(event.message)
                    .build()
        }
    }
}