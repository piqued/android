package io.piqued.service.pushnotification

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import io.piqued.utils.Preferences

class PiquedFMSImpl: FirebaseMessagingService() {
    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Preferences(this).setFCMToken(token)
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        println("Piqued FCM received message!!")
    }
}