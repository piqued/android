package io.piqued.service.pushnotification

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import androidx.core.app.NotificationManagerCompat
import io.piqued.Piqued
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.repository.ActivityRepository
import io.piqued.database.event.Event
import io.piqued.database.util.PiquedLogger
import io.piqued.utils.ImageDownloadUtil
import io.piqued.utils.NotificationImageTarget
import io.piqued.utils.PiquedLatch
import org.jetbrains.anko.runOnUiThread
import javax.inject.Inject

class PiquedNotificationService : IntentService(PiquedNotificationService::class.java.simpleName) {
    companion object {
        fun createNotification(c: Context) {
            c.startService(Intent(c, PiquedNotificationService::class.java))
        }
    }

    @Inject
    lateinit var repository: ActivityRepository

    private var latch: PiquedLatch? = null
    private val hashMap = HashMap<Int, Bitmap>()

    override fun onCreate() {
        super.onCreate()
        (application as Piqued).getComponent().inject(this)
    }

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            val c = this
            repository.getNewActivitiesForPushNotification(object :
                    PiquedCloudCallback<List<Event>> {
                override fun onSuccess(result: List<Event>?) {
                    if (result != null) {
                        runOnUiThread {
                            latch = PiquedLatch(result.size)
                            latch?.start()
                            latch?.setCallback(object : PiquedLatch.PiquedLatchCallback {
                                override fun onCountDown() {
                                    for (i in result.size downTo 1) {
                                        createNotification(c, result[i-1])
                                    }
                                }
                            })

                            // fetch for all images
                            for (event in result) {
                                ImageDownloadUtil.getDownloadUtilInstance(c).loadActorImageWithCallback(c, event, object : NotificationImageTarget() {
                                    override fun onImageReady(image: Bitmap) {
                                        hashMap[event.id] = image
                                        latch?.countDown()
                                    }
                                })
                            }
                        }
                        repository.clearNotification()
                    }
                }

                override fun onFailure(error: Throwable?, message: String?) {
                    PiquedLogger.e(error, message)
                }
            })
        }
    }

    private fun createNotification(c: Context, event: Event) {
        if (hashMap.containsKey(event.id)) {
            val notification =
                    PiquedNotificationUtil.createNotification(c, event, hashMap[event.id]!!)
            with(NotificationManagerCompat.from(c)) {
                notify(event.id, notification)
            }
        }
    }
}