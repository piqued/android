package io.piqued.service.imageupload

/**
 *
 * Created by Kenny M. Liou on 8/28/18.
 * Piqued Inc.
 *
 */

enum class ImageUploadType(
        val imageSize: Int,
        val trackProgress: Boolean
) {
    SMALL(480, false),
    LARGE(1080, true);

    companion object {
        private val values = ImageUploadType.values()

        fun fromIndex(index: Int): ImageUploadType {
            if (index > -1 && index < values.size) {
                return values[index]
            }

            throw IllegalArgumentException("Illegal index value: " + index + " for enum " + ImageUploadType::class.java.simpleName )
        }
    }
}