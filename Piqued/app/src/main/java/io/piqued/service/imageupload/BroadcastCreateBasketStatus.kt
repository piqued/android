package io.piqued.service.imageupload

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.localbroadcastmanager.content.LocalBroadcastManager

/**
 *
 * Created by Kenny M. Liou on 8/28/18.
 * Piqued Inc.
 *
 */

class BroadcastCreateBasketStatus {

    companion object {

        private const val ACTION_IMAGE_UPLOAD_PROGRESS = "upload_progress"
        private const val KEY_UPLOAD_STATUS = "status"
        private const val KEY_IMAGE_URL = "image_url"
        private const val KEY_BYTE_SENT = "byte_sent"
        private const val KEY_CONTENT_LENGTH = "length"
        private const val KEY_UPLOADED_IMAGE_COUNT = "uploaded_image_count"
        private const val KEY_TOTAL_IMAGE_COUNT = "total_image_count"

        @JvmStatic
        fun registerBroadcast(context: Context, receiver: BroadcastReceiver) {

            val intentFilter = IntentFilter(ACTION_IMAGE_UPLOAD_PROGRESS)

            LocalBroadcastManager.getInstance(context).registerReceiver(receiver, intentFilter)
        }

        @JvmStatic
        fun unregisterBroadcast(context: Context, receiver: BroadcastReceiver) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver)
        }

        fun sendBroadcastProgress(
                context: Context,
                status: ImageUploadStatus,
                imageUrl: String,
                byteSent: Long,
                contentLength: Long) {

            val intent = Intent(ACTION_IMAGE_UPLOAD_PROGRESS)
            intent.putExtra(KEY_UPLOAD_STATUS, status)
            intent.putExtra(KEY_IMAGE_URL, imageUrl)
            intent.putExtra(KEY_BYTE_SENT, byteSent)
            intent.putExtra(KEY_CONTENT_LENGTH, contentLength)

            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }

        fun sendBroadcastProgress(
                context: Context,
                status: ImageUploadStatus,
                imageUrl: String,
                imageSent: Int,
                totalImageCount: Int
        ) {
            val intent = Intent(ACTION_IMAGE_UPLOAD_PROGRESS)
            intent.putExtra(KEY_UPLOAD_STATUS, status)
            intent.putExtra(KEY_IMAGE_URL, imageUrl)
            intent.putExtra(KEY_UPLOADED_IMAGE_COUNT, imageSent)
            intent.putExtra(KEY_TOTAL_IMAGE_COUNT, totalImageCount)

            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }

        @JvmStatic
        fun getUploadStatus(intent: Intent): ImageUploadStatus {
            return intent.getSerializableExtra(KEY_UPLOAD_STATUS) as ImageUploadStatus
        }

        @JvmStatic
        fun getBytesSent(intent: Intent): Long {
            return intent.getLongExtra(KEY_BYTE_SENT, 0)
        }

        @JvmStatic
        fun getContentLength(intent: Intent): Long {
            return intent.getLongExtra(KEY_CONTENT_LENGTH, 0)
        }

        @JvmStatic
        fun getImageUrl(intent: Intent): String {
            return intent.getStringExtra(KEY_IMAGE_URL)
        }

        @JvmStatic
        fun getImageSent(intent: Intent): Int {
            return intent.getIntExtra(KEY_UPLOADED_IMAGE_COUNT, 0)
        }

        @JvmStatic
        fun getTotalImageCount(intent: Intent): Int {
            return intent.getIntExtra(KEY_TOTAL_IMAGE_COUNT, 0)
        }
    }
}