package io.piqued.service.imageupload

/**
 *
 * Created by Kenny M. Liou on 8/28/18.
 * Piqued Inc.
 *
 */

enum class ImageUploadStatus {
    STARTING,
    UPLOADING,
    FINISHING,
    FINISHED,
    ERROR
}