package io.piqued.service.imageupload

import io.piqued.cloud.model.piqued.ImageContainer

/**
 *
 * Created by Kenny M. Liou on 8/28/18.
 * Piqued Inc.
 *
 */
data class ImageTempFile(
        val type: ImageUploadType,
        val imageContainer: ImageContainer
)