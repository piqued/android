package io.piqued.service.imageupload

import android.app.IntentService
import android.content.Intent
import android.net.Uri
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import io.piqued.Piqued
import io.piqued.activities.PiquedActivity
import io.piqued.cloud.PiquedCloudImpl
import io.piqued.cloud.model.piqued.BasketData
import io.piqued.cloud.model.piqued.CloudImageUploadContainer
import io.piqued.cloud.model.piqued.CloudImageUploadResult
import io.piqued.cloud.util.ImageUploadProgressListener
import io.piqued.database.util.PiquedLogger
import io.piqued.utils.Constants
import io.piqued.utils.Preferences
import io.piqued.utils.upload.ImageUploadUtil
import javax.inject.Inject

class CreateMultiImageBasketService: IntentService(CreateMultiImageBasketService::class.java.simpleName) {

    companion object {
        private const val KEY_NEW_BASKET = "new_basket"
        private val TAG = CreateBasketService::class.java.simpleName

        fun uploadImage(activity: PiquedActivity,
                        basketData: BasketData) {
            val serviceIntent = Intent(activity, CreateMultiImageBasketService::class.java)
            serviceIntent.putExtra(KEY_NEW_BASKET, basketData)

            activity.startService(serviceIntent)
        }
    }

    @Inject
    internal lateinit var piquedCloud: PiquedCloudImpl
    @Inject
    internal lateinit var preferences: Preferences

    private lateinit var basketData: BasketData

    override fun onCreate() {
        super.onCreate()

        (application as Piqued).getComponent().inject(this)
    }

    private fun sendStatus(status: ImageUploadStatus,
                           imageUrl: String,
                           imageSent: Int,
                           totalImageCount: Int) {
        BroadcastCreateBasketStatus.sendBroadcastProgress(
                this,
                status,
                imageUrl,
                imageSent,
                totalImageCount)

        if (status == ImageUploadStatus.FINISHED) {
            LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(Constants.ACTION_POST_UPLOADED))
        }
    }

    override fun onHandleIntent(intent: Intent?) {

        val userToken = preferences.userToken

        if (userToken != null) {
            basketData = intent?.getParcelableExtra(KEY_NEW_BASKET)!!

            val allUris = ArrayList<CloudImageUploadContainer>()
            val totalImageCount = basketData.imageUris.size
            var imageSent = 0

            sendStatus(ImageUploadStatus.STARTING, "", imageSent, totalImageCount)

            for (imageUri: Uri in basketData.imageUris) {

                val imageUploadUriResult = CloudImageUploadContainer()

                for (type: ImageUploadType in ImageUploadType.values()) {
                    val imageUriString = imageUri.path!!

                    // step 1: create image

                    val imageToUpload = createImageWithType(imageUriString, type)

                    // step 2: upload small image
                    val imageUploadResult = uploadImagesAsync(imageUriString, imageToUpload)

                    // step 3: notify UI to change
                    if (imageUploadResult != null) {

                        when(type) {
                            ImageUploadType.SMALL -> imageUploadUriResult.smallImageUrl.setImageUploadResult(imageUploadResult)
                            ImageUploadType.LARGE -> imageUploadUriResult.largeImageUrl.setImageUploadResult(imageUploadResult)
                        }
                    } else {
                        PiquedLogger.e(TAG, "Failed to upload image")
                        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(Constants.ACTION_POST_UPLOAD_FAILED))
                        sendStatus(ImageUploadStatus.ERROR, "", 1, 1)
                        return
                    }
                }
                allUris.add(imageUploadUriResult)

                imageSent++

                sendStatus(ImageUploadStatus.UPLOADING, imageUri.toString(), imageSent, totalImageCount)
            }

            // step 4: wrap up everything and notify server

            sendStatus(ImageUploadStatus.FINISHING, "", imageSent, totalImageCount)

            if (piquedCloud.createNewBasket(allUris, basketData, userToken)) {
                // notify UI that upload completed
                sendStatus(ImageUploadStatus.FINISHED, "", imageSent, totalImageCount)
                LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(Constants.ACTION_POST_UPLOADED))
            } else {
                sendStatus(ImageUploadStatus.ERROR, "", imageSent, totalImageCount)
                LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(Constants.ACTION_POST_UPLOAD_FAILED))
            }

            return
        }

        throw java.lang.IllegalStateException("User token does not exist, cannot upload image")
    }

    private fun createImageWithType(imageUri: String, type: ImageUploadType): ImageTempFile {

        val imageSize = ImageUploadUtil.getImageSize(imageUri, type.imageSize)

        return ImageTempFile(type, ImageUploadUtil.createBase64ImagesWithSize(this, imageUri, imageSize))
    }

    private fun uploadImagesAsync(originalImageUrl: String, tempFile: ImageTempFile): CloudImageUploadResult? {

        val preSign = piquedCloud.getPreSign()

        return piquedCloud.uploadImageToPreSign(
                preSign,
                originalImageUrl,
                tempFile.imageContainer,
                if (tempFile.type.trackProgress) imageUploadProgressListener else null)
    }

    private val imageUploadProgressListener = object: ImageUploadProgressListener {
        override fun onRequestProgress(imageUrl: String, bytesWritten: Long, contentLength: Long) {
            // sendStatus(ImageUploadStatus.UPLOADING, imageUrl, bytesWritten, contentLength)
        }
    }
}