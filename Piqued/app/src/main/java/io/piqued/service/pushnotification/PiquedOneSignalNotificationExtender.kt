package io.piqued.service.pushnotification

import com.onesignal.NotificationExtenderService
import com.onesignal.OSNotificationReceivedResult

/**
 * Notification handler for One Signal, this class forward notification to PiquedNotificationService
 */
class PiquedOneSignalNotificationExtender: NotificationExtenderService() {
    override fun onNotificationProcessing(notification: OSNotificationReceivedResult?): Boolean {
        // we have received notification via OneSignal, notify
        PiquedNotificationService.createNotification(applicationContext)
        return true
    }
}