package io.piqued.service.imageupload

import android.app.IntentService
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import io.piqued.Piqued
import io.piqued.activities.PiquedActivity
import io.piqued.api.PiquedApiCallback
import io.piqued.api.piqued.PiquedApi
import io.piqued.cloud.model.piqued.BasketData
import io.piqued.cloud.util.ImageUploadProgressListener
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.model.imageupload.ImageUploadResult
import io.piqued.model.imageupload.PreSignObject
import io.piqued.utils.Constants
import io.piqued.utils.PiquedLatch
import io.piqued.utils.Preferences
import io.piqued.utils.upload.ImageUploadUtil
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 8/28/18.
 * Piqued Inc.
 *
 */
class CreateBasketService: IntentService(CreateBasketService::class.java.simpleName) {

    companion object {
        private const val KEY_NEW_BASKET = "new_basket"
        private val TAG = CreateBasketService::class.java.simpleName

        fun uploadImage(activity: PiquedActivity,
                        basketData: BasketData) {

            val serviceIntent = Intent(activity, CreateBasketService::class.java)
            serviceIntent.putExtra(KEY_NEW_BASKET, basketData)

            activity.startService(serviceIntent)
        }
    }

    @Inject
    internal lateinit var api: PiquedApi
    @Inject
    internal lateinit var preferences: Preferences

    private var basketData: BasketData? = null

    private var imageUploadLatch = PiquedLatch(2)

    private var largeImageUploadResult: ImageUploadResult? = null
    private var smallImageUploadResult: ImageUploadResult? = null

    override fun onCreate() {
        super.onCreate()

        (application as Piqued).getComponent().inject(this)
    }

    private fun sendStatus(status: ImageUploadStatus,
                           imageUrl: String,
                           bytesSent: Long,
                           contentLength: Long) {
        if (status == ImageUploadStatus.ERROR) {
            imageUploadLatch.cancel()
        }
        BroadcastCreateBasketStatus.sendBroadcastProgress(
                this,
                status,
                imageUrl,
                bytesSent,
                contentLength)

        if (status == ImageUploadStatus.FINISHED) {
            LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(Constants.ACTION_POST_UPLOADED))
        }
    }

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {

            basketData = intent.getParcelableExtra(KEY_NEW_BASKET) as BasketData
            if (basketData != null) {

                imageUploadLatch.setCallback(uploadCompleteCallback)
                imageUploadLatch.start()

                val imageUrl = basketData?.imageUris?.get(0)?.path

                // step 1: create images
                val imagesToUpload = createImages(imageUrl ?: "")

                // step 2: upload images
                uploadImages(imageUrl ?: "", imagesToUpload)
            }
        }
    }

    private fun createImages(imageUrl: String): List<ImageTempFile> {

        val result = ArrayList<ImageTempFile>()

        if (imageUrl.isNotEmpty()) {
            for (type: ImageUploadType in ImageUploadType.values()) {

                val imageSize = ImageUploadUtil.getImageSize(imageUrl, type.imageSize);

                val imageContainer = ImageUploadUtil.createBase64ImagesWithSize(this, imageUrl, imageSize)

                result.add(ImageTempFile(type, imageContainer))
            }
        } else {
            sendStatus(ImageUploadStatus.ERROR, imageUrl, 0, 0)
        }

        return result
    }

    private fun uploadImages(originalImageUrl: String, tempFileList: List<ImageTempFile>) {

        if (tempFileList.isNotEmpty()) {
            sendStatus(ImageUploadStatus.STARTING, originalImageUrl, 0, 0)
        }

        for (tempFile: ImageTempFile in tempFileList) {
            api.getPreSign(object : PiquedApiCallback<PreSignObject> {
                override fun onSuccess(preSign: PreSignObject?) {
                    if (preSign != null) {
                        api.uploadImageToPreSign(
                                tempFile.type,
                                preSign,
                                originalImageUrl,
                                tempFile.imageContainer,
                                if (tempFile.type.trackProgress) imageUploadProgressListener else null,
                                imageUploadCallback)
                    } else {
                        sendStatus(ImageUploadStatus.ERROR, originalImageUrl, 0, 0)
                    }
                }

                override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                    PiquedLogger.e(TAG, errorCode, payload)
                    sendStatus(ImageUploadStatus.ERROR, originalImageUrl, 0, 0)
                }

            })
        }
    }

    private val uploadCompleteCallback = object : PiquedLatch.PiquedLatchCallback {
        override fun onCountDown() {
            storeNewBasketInfo()
        }
    }

    private fun storeNewBasketInfo() {

        if (smallImageUploadResult != null && largeImageUploadResult != null && basketData != null) {
            api.createBasket(largeImageUploadResult!!, smallImageUploadResult!!, basketData!!, preferences.userToken!!, object : PiquedApiCallback<Void?> {
                override fun onSuccess(data: Void?) {
                    PiquedLogger.i(TAG, "create post successful")
                    sendStatus(ImageUploadStatus.FINISHED,
                            basketData?.imageUris?.get(0).toString(),
                            0,
                            0)
                }

                override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                    PiquedLogger.e(TAG, errorCode, payload)
                    sendStatus(ImageUploadStatus.ERROR, "", 0, 0)
                }

            })
        }
    }

    private val imageUploadCallback = object : PiquedApiCallback<ImageUploadResult> {
        override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
            PiquedLogger.e(TAG, errorCode, payload)
            sendStatus(ImageUploadStatus.ERROR, "", 0, 0)
        }

        override fun onSuccess(data: ImageUploadResult?) {

            if (data != null) {

                val type = ImageUploadType.fromIndex(data.originalIndex)

                when (type) {
                    ImageUploadType.SMALL -> smallImageUploadResult = data
                    ImageUploadType.LARGE -> largeImageUploadResult = data
                }
            }

            imageUploadLatch.countDown()
        }
    }

    private val imageUploadProgressListener = object: ImageUploadProgressListener {
        override fun onRequestProgress(imageUrl: String, bytesWritten: Long, contentLength: Long) {
            sendStatus(ImageUploadStatus.UPLOADING,  imageUrl, bytesWritten, contentLength)
        }

    }
}
