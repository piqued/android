package io.piqued.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.CameraPosition
import io.piqued.cloud.util.PostsCallback
import io.piqued.cloud.util.RegionBounds
import io.piqued.database.post.CloudPost
import io.piqued.database.post.PostId
import io.piqued.utils.postmanager.PostManager

/**
 *
 * Created by Kenny M. Liou on 9/5/18.
 * Piqued Inc.
 *
 */

class HomeMapViewModel: ViewModel() {
    private val postsLiveData = MutableLiveData<LinkedHashSet<CloudPost>>()

    var cameraPosition: CameraPosition? = null
    var lastLoadTime = -1L
    var miniBasketVisible = false
    val miniBasketList = ArrayList<PostId>()


    fun getPosts(): LiveData<LinkedHashSet<CloudPost>> {
        return postsLiveData
    }

    fun updateMiniBasketList(posts: List<CloudPost>) {

        miniBasketList.clear()

        for (post : CloudPost in posts) {
            miniBasketList.add(PostId(post.postId))
        }
    }

    fun loadPostBasedOnLocation(
            postManager: PostManager,
            regionBounds: RegionBounds) {

        postManager.getPostsBasedOnLocationOnMap(regionBounds, PostsCallback { _, posts, _ ->

            val hashSet = postsLiveData.value ?: LinkedHashSet<CloudPost>()

            hashSet.addAll(posts)

            postsLiveData.value = hashSet
        })
    }

    fun getCurrentData(): LinkedHashSet<CloudPost>? {
        return postsLiveData.value
    }

    fun validateAllPosts(postManager: PostManager) {

        val hashSet = postsLiveData.value

        if (hashSet != null && hashSet.isNotEmpty()) {

            val newPostList = ArrayList<CloudPost>()

            for (post: CloudPost in hashSet) {
                val validatedPost = postManager.getCloudPost(post.postId)

                if (validatedPost != null) {
                    newPostList.add(validatedPost)
                }
            }

            hashSet.clear()

            hashSet.addAll(newPostList)

            postsLiveData.value = hashSet
        }
    }
}

