package io.piqued.logviewer

import android.os.Bundle
import io.piqued.R
import io.piqued.activities.PiquedActivity
import io.piqued.model.MetricValues

/**
 *
 * Created by Kenny M. Liou on 1/12/19.
 * Piqued Inc.
 *
 */
class LogViewerActivity: PiquedActivity() {
    override fun getMetric(): MetricValues {
        return mMetric
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_log_viewer)
    }

}