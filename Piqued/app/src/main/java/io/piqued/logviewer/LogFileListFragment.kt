package io.piqued.logviewer

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.piqued.PiquedFragment
import io.piqued.R
import io.piqued.database.util.PiquedLogger
import io.piqued.utils.ViewUtil
import kotlinx.android.synthetic.main.fragment_log_file_list.*
import kotlinx.android.synthetic.main.toolbar.*
import java.io.File
import java.text.SimpleDateFormat

/**
 *
 * Created by Kenny M. Liou on 1/12/19.
 * Piqued Inc.
 *
 */

class LogFileListFragment: PiquedFragment()
{

    private val fileArray = ArrayList<File>()
    private val modifiedTimeFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")

    companion object {

        private const val ACTION_OPEN_FILE_INDEX = "file_index"
        private const val KEY_INDEX_VALUE = "index"

        fun openFileAtIndex(context: Context, index: Int) {

            val intent = Intent(ACTION_OPEN_FILE_INDEX)
            intent.putExtra(KEY_INDEX_VALUE, index)

            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_log_file_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        logFileList.layoutManager = LinearLayoutManager(context)
        logFileList.adapter = LogFileListAdapter()

        ViewUtil.initializeSwipeRefreshLayout(logFileListSwipeLayout) {
            fetchAllFileInfos(view.context)
        }
    }

    override fun onResume() {
        super.onResume()

        LocalBroadcastManager.getInstance(context!!).registerReceiver(broadcastReceiver, IntentFilter(ACTION_OPEN_FILE_INDEX))

        setSupportActionBar(toolbar, R.string.log_viewer_title)

        fetchAllFileInfos(context!!)
    }

    override fun onPause() {

        LocalBroadcastManager.getInstance(context!!).unregisterReceiver(broadcastReceiver)

        super.onPause()
    }

    private fun fetchAllFileInfos(context: Context)
    {
        fileArray.clear()

        fileArray.addAll(PiquedLogger.getLogFileList(context))

        LogFileListAdapter().notifyDataSetChanged()

        logFileListSwipeLayout.isRefreshing = false
    }

    private fun showLogContent(filePath: String) {

        val directions = LogFileListFragmentDirections.showLogFileContent(filePath)

        findNavController().navigate(directions)
    }

    private val broadcastReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null) {
                val file = fileArray[intent.getIntExtra(KEY_INDEX_VALUE, 0)]
                showLogContent(file.absolutePath)
            }
        }
    }

    private inner class LogFileListAdapter: RecyclerView.Adapter<LogFileInfoViewHolder>()
    {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LogFileInfoViewHolder {
            return LogFileInfoViewHolder(parent)
        }

        override fun getItemCount(): Int {
            return fileArray.size
        }

        override fun onBindViewHolder(holder: LogFileInfoViewHolder, position: Int) {
            val file = fileArray[position]

            holder.onBind(file.name, "last modified at: " + modifiedTimeFormat.format(file.lastModified()))
        }

    }
}