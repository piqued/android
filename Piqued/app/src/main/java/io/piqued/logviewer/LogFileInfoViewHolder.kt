package io.piqued.logviewer

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.R
import kotlinx.android.synthetic.main.view_holder_log_file_info.view.*

/**
 *
 * Created by Kenny M. Liou on 1/12/19.
 * Piqued Inc.
 *
 */

class LogFileInfoViewHolder(parent: ViewGroup)
    : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_log_file_info, parent, false))
{
    init {
        itemView.viewLogFile.setOnClickListener {
            LogFileListFragment.openFileAtIndex(itemView.context, adapterPosition)
        }
    }

    fun onBind(fileName: String, lastModifiedTime: String) {

        itemView.fileName.text = fileName
        itemView.lastModifyTime.text = lastModifiedTime
    }
}