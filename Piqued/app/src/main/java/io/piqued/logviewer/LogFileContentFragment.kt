package io.piqued.logviewer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.piqued.PiquedFragment
import io.piqued.R
import io.piqued.utils.ViewUtil
import kotlinx.android.synthetic.main.fragment_log_file_content.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.BufferedReader
import java.io.File
import java.io.FileReader

/**
 *
 * Created by Kenny M. Liou on 1/12/19.
 * Piqued Inc.
 *
 */

class LogFileContentFragment: PiquedFragment()
{

    private lateinit var file: File

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        file = File(LogFileContentFragmentArgs.fromBundle(arguments!!).filePath)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_log_file_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewUtil.initializeSwipeRefreshLayout(logContentSwipeRefreshLayout) { reloadFile() }
    }

    override fun onResume() {
        super.onResume()

        // load file

        setSupportActionBar(toolbar, file.name)

        logContentSwipeRefreshLayout.isRefreshing = true
        
        reloadFile()
    }

    private fun reloadFile() {

        if (file.exists()) {
            doAsync {

                val reader = BufferedReader(FileReader(file))
                val stringBuilder = StringBuilder()

                file.forEachLine {
                    stringBuilder.append(it)
                    stringBuilder.append("\n")
                }

                reader.close()

                uiThread {
                    logFileContent.text = stringBuilder.toString()

                    if (logFileContent.text.isBlank()) {
                        logFileContent.setText(R.string.log_file_no_content)
                    }

                    logContentSwipeRefreshLayout.isRefreshing = false
                }
            }
        } else {

            logFileContent.setText(R.string.log_file_no_content)
            logContentSwipeRefreshLayout.isRefreshing = false
        }
    }
}