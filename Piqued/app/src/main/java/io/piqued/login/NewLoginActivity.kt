package io.piqued.login

import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.annotation.ColorInt
import androidx.appcompat.widget.Toolbar
import androidx.navigation.findNavController
import io.piqued.R
import io.piqued.activities.FBLoginHandlerActivity
import io.piqued.login.utils.LoginActivityHandler
import io.piqued.login.utils.LoginSignUpSupport
import io.piqued.model.MetricValues
import io.piqued.utils.AccountManager
import io.piqued.utils.Preferences
import io.piqued.utils.modelmanager.PiquedUserManager
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class NewLoginActivity: FBLoginHandlerActivity() {

    @Inject
    lateinit var metricValues: MetricValues
    @Inject
    lateinit var preferences: Preferences
    @Inject
    lateinit var userManager: PiquedUserManager
    @Inject
    lateinit var accountManager: AccountManager

    lateinit var loginSignUpSupport: LoginSignUpSupport
    var loginActivityHandler: LoginActivityHandler? = null
    private var isFirstFragment = false

    override fun getMetric(): MetricValues {
        return metricValues
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        loginSignUpSupport = LoginSignUpSupport(this)

        setContentView(R.layout.activity_login)

        setupNavigation()
    }

    private fun setupNavigation() {
        val navController = findNavController(R.id.login_host_fragment)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            val graph = navController.graph
            isFirstFragment = destination.id == graph.startDestination
        }
    }

    private fun updateActionBar() {
        supportActionBar?.setDisplayShowHomeEnabled(isFirstFragment)
        supportActionBar?.setDisplayHomeAsUpEnabled(!isFirstFragment)
    }

    override fun setSupportActionBar(toolbar: Toolbar?) {
        super.setSupportActionBar(toolbar)

        updateActionBar()
    }

    override val facebookLoginResult: Callback
        get() = object: Callback {
            override fun onLoginSuccess(token: String) {
                if (token.isNotEmpty()) {
                    accountManager.loginWithOauth(
                            AccountManager.SupportedSite.FACEBOOK,
                            token,
                            loginSignUpSupport.loginCallback)
                }
            }

            override fun onCancelled() {
                loginSignUpSupport.dismissLoading()
            }
        }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        setStatusBarImmersiveMode(Color.TRANSPARENT)
    }

    override fun onBackPressed() {
        if (loginActivityHandler?.onBackPressed() != true) {
            if (!findNavController(R.id.login_host_fragment).navigateUp()) {
                super.onBackPressed()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    fun showForgetPassword() {
        findNavController(R.id.login_host_fragment).navigate(R.id.showForgetPassword)
    }

    override fun onKeyboardSizeChanged(isHidden: Boolean, keyboardHeight: Int) {
        loginActivityHandler?.animateLogo(!isHidden)
        loginActivityHandler?.animateBottomButton(keyboardHeight)
    }

    private fun setStatusBarImmersiveMode(@ColorInt color: Int) {
        window.attributes.systemUiVisibility =
        (window.attributes.systemUiVisibility
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = color
    }

    override fun getRootView(): View {
        return loginActivityRootView
    }
}