package io.piqued.login.utils

/**
 *
 * Created by Kenny M. Liou on 6/30/18.
 * Piqued Inc.
 *
 */
interface LoginActivityHandler {
    fun onBackPressed(): Boolean
    fun animateLogo(isSmall: Boolean)
    fun animateBottomButton(keyboardHeight: Int)
}