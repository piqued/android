package io.piqued.login.utils

import android.app.AlertDialog
import android.text.TextUtils
import io.piqued.PiquedFragment
import io.piqued.R
import io.piqued.activities.WelcomeActivity
import io.piqued.database.user.User
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.login.LoginStartFragment
import io.piqued.login.NewLoginActivity
import io.piqued.utils.LoginCallback
import io.piqued.utils.PasswordUtil
import io.piqued.utils.UIHelper
import io.piqued.views.login.*

/**
 *
 * Created by Kenny M. Liou on 6/30/18.
 * Piqued Inc.
 *
 */
class LoginSignUpSupport(private val activity: NewLoginActivity): LoginSignUpHandler {

    companion object {
        private val TAG = LoginSignUpSupport::class.java.simpleName

        fun isEmailValid(email: String): Boolean {
            return !TextUtils.isEmpty(email)
                    && email.contains("@")
                    && email.length > 4
                    && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }
    }


    private val loadingDialog = LoginLoadingDialog(activity)

    fun dismissLoading() {
        loadingDialog.dismiss()
    }

    override fun createAccountWithEmail(sender: PiquedFragment, email: String) {
        (sender.parentFragment as? LoginStartFragment)?.pushSubFragment(NewAccountNamePasswordSubFragment.newInstance(this, email), false, true)
    }

    override fun createAccount(email: String, password: String, userName: String) {
        activity.hideKeyboard()
        loadingDialog.show()
        activity.accountManager.createAccount(email, password, userName, loginCallback)
    }

    override fun isNameValid(userName: String): Boolean {
        return userName.length >= 2
    }

    override fun isPasswordValid(password: String): Boolean {
        return PasswordUtil.isPasswordValid(password)
    }

    override fun isEmailValid(email: String): Boolean {
        return LoginSignUpSupport.isEmailValid(email)
    }

    override fun configureBottom(sender: PiquedFragment, buttonOption: BottomButtonOption, bottomText: BottomOption) {
        (sender.parentFragment as? LoginStartFragment)?.configureBottom(buttonOption, bottomText)
    }

    override fun performPiquedLogin(userName: String, password: String) {
        loadingDialog.show()
        activity.accountManager.login(userName, password, loginCallback)
    }

    override fun performFacebookLogin() {
        loadingDialog.show()
        activity.performManualLogin()
    }

    override fun startPasswordResetFlow() {
        activity.showForgetPassword()
    }

    override fun goToFragment(sender: PiquedFragment?, fragment: LoginSignUpBaseFragment) {
        (sender?.parentFragment as? LoginStartFragment)?.pushSubFragment(fragment, false, true)
    }

    val loginCallback = object : LoginCallback {
        override fun onLoginSuccess() {
            dismissLoading()
            UIHelper.startActivity(activity, WelcomeActivity::class.java)
        }

        override fun onLoginFailed(errorCode: PiquedErrorCode?, payload: String?) {
            PiquedLogger.e(TAG, "Failed to login", errorCode, payload)
            loadingDialog.dismiss()

            val builder = AlertDialog.Builder(activity)
            builder.setMessage(R.string.login_failed)
            builder.setPositiveButton(R.string.ok_button) {
                _, _ ->
                dismissLoading()
            }
            builder.setCancelable(false)
            builder.create().show()
        }

        override fun onFetchingUserInfo() {}

        override fun onFetchingUserInfoSuccess(user: User) {
            activity.userManager.myUserRecord = user
        }

        override fun onFetchingUserInfoFailed(errorCode: PiquedErrorCode?, payload: String?) {}
    }

}