package io.piqued.login.utils

import android.app.Dialog
import android.content.Context


/**
 *
 * Created by Kenny M. Liou on 6/30/18.
 * Piqued Inc.
 *
 */
class LoginLoadingDialog(context: Context) {

    private val dialog: Dialog = Dialog(context)

    init {
        dialog.setContentView(io.piqued.R.layout.dialog_login_loading)
    }

    fun show() {
        dialog.show()
    }

    fun dismiss() {
        dialog.dismiss()
    }
}