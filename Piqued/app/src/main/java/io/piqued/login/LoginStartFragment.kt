package io.piqued.login

import android.os.Bundle
import android.transition.AutoTransition
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.navigation.fragment.NavHostFragment
import io.piqued.BuildConfig
import io.piqued.R
import io.piqued.login.utils.LoginActivityHandler
import io.piqued.views.login.*
import kotlinx.android.synthetic.main.fragment_login_start.*

class LoginStartFragment: NewLoginSubFragment(), LoginActivityHandler {

    companion object {
        private const val ANIMATION_DURATION = 500L
    }

    private var mBottomButtonGoBack = false

    override fun getTagName(): String {
        return LoginStartFragment::class.java.simpleName
    }

    override fun onBackPressed(): Boolean {
        if (childFragmentManager.backStackEntryCount > 0) {
            childFragmentManager.popBackStack()
            return true
        }

        return false
    }

    override fun animateLogo(isSmall: Boolean) {
        animateLogo(isSmall, true)
    }

    private fun animateLogo(isSmall: Boolean, animate: Boolean) {
        val constraint = ConstraintSet()
        val transition = AutoTransition()

        transition.duration = if (animate) ANIMATION_DURATION else 0

        constraint.clone(startLoginFragmentRootView)
        TransitionManager.beginDelayedTransition(startLoginFragmentRootView, transition)
        constraint.setGuidelineBegin(R.id.logoTopGuideline, (if (isSmall) resources.getDimensionPixelSize(R.dimen.space_14) else resources.getDimensionPixelSize(R.dimen.space_88)) + getLoginActivity().metricValues.statusBarHeight)
        constraint.setGuidelineBegin(R.id.logoBottomGuideline, (if (isSmall) resources.getDimensionPixelSize(R.dimen.space_72) else resources.getDimensionPixelSize(R.dimen.space_168)) + getLoginActivity().metricValues.statusBarHeight)

        constraint.applyTo(startLoginFragmentRootView)
    }

    override fun animateBottomButton(keyboardHeight: Int) {
        val constraint = ConstraintSet()
        val transition = AutoTransition()

        transition.duration = ANIMATION_DURATION

        constraint.clone(startLoginFragmentRootView as ConstraintLayout)
        TransitionManager.beginDelayedTransition(startLoginFragmentRootView as ConstraintLayout, transition)
        constraint.setGuidelineEnd(R.id.bottomButtonGuideline, keyboardHeight)

        constraint.applyTo(startLoginFragmentRootView)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login_start, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pushSubFragment(LoginChoiceSubFragment.newInstance(getLoginActivity().loginSignUpSupport), true, false)

        bottomButton.setOnClickListener {

            if (mBottomButtonGoBack) {
                getLoginActivity().hideKeyboard()
                onBackPressed()
            } else {
                pushSubFragment(NewAccountEmailSubFragment.newInstance(getLoginActivity().loginSignUpSupport), false, true)
            }
        }

        animateLogo(false, false)

        if ("sandbox" == BuildConfig.PIQUED_BUILD_ENV) {
            // we are not in production, make dev configure button visible
            devConfigureButton.visibility = View.VISIBLE
            devConfigureButton.isEnabled = true
            devConfigureButton.setOnClickListener {
                NavHostFragment.findNavController(this).navigate(R.id.showServerConfig)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        getLoginActivity().loginActivityHandler = this
    }

    override fun onPause() {
        getLoginActivity().loginActivityHandler = null
        super.onPause()
    }

    fun configureBottom(buttonOption: BottomButtonOption, bottomText: BottomOption) {
        when(bottomText) {
            BottomOption.EMPTY -> {
//                disclaimerTextView.visibility = View.GONE
//                forgetPasswordButton.visibility = View.GONE
            }
            BottomOption.DISCLAIMER -> {
//                disclaimerTextView.visibility = View.VISIBLE
//                forgetPasswordButton.visibility = View.GONE
            }
            BottomOption.FORGOT_PASSWORD -> {
//                disclaimerTextView.visibility = View.GONE
                // forgetPasswordButton.visibility = View.VISIBLE
            }
        }

        when(buttonOption) {

            BottomButtonOption.NONE -> {
                mBottomButtonGoBack = false
                bottomButton.visibility = View.GONE
            }
            BottomButtonOption.SIGN_UP -> {
                mBottomButtonGoBack = false
                bottomButton.visibility = View.VISIBLE
                bottomButton.text = getText(R.string.sign_up)
            }
            BottomButtonOption.SIGN_IN -> {
                mBottomButtonGoBack = true
                bottomButton.visibility = View.VISIBLE
                bottomButton.text = getText(R.string.sign_in)
            }
        }
    }

    fun pushSubFragment(fragment: LoginSignUpBaseFragment, asRoot: Boolean, shouldAnimate: Boolean) {

        val transation = childFragmentManager.beginTransaction()

        if (shouldAnimate) {
            transation.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
        }

        transation.replace(R.id.subFragmentContainer, fragment)
        if (!asRoot) {
            transation.addToBackStack(tagName)
        }
        transation.commit()
    }
}