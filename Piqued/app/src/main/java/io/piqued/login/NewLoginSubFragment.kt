package io.piqued.login

import io.piqued.PiquedFragment

abstract class NewLoginSubFragment: PiquedFragment() {

    fun getLoginActivity() : NewLoginActivity {
        return activity as NewLoginActivity
    }
}