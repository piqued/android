package io.piqued;

import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.BindingAdapter;
import androidx.fragment.app.Fragment;

import io.piqued.activities.FloatingActivity;
import io.piqued.activities.PiquedActivity;
import io.piqued.model.MetricValues;
import io.piqued.utils.IntentUtil;
import io.piqued.utils.KeyboardEventListener;
import io.piqued.utils.LoginRequiredExecution;
import io.piqued.utils.PiquedDialogFragment;
import io.piqued.utils.PiquedOnBackHandler;
import io.piqued.utils.Preferences;

/**
 * Piqued
 * <p>
 * Created by Kenny M. Liou on 11/19/16.
 * <p>
 * Piqued Inc.
 */

public abstract class PiquedFragment extends Fragment {

    public PiquedActivity getPiquedActivity() {
        return (PiquedActivity) getActivity();
    }

    public Piqued getApplication() {
        return ((Piqued) getActivity().getApplication());
    }

    public void startActivity(Class<? extends PiquedActivity> clazz) {
        Intent intent = new Intent(getContext(), clazz);

        startActivity(intent);
    }
    public void setOnKeyListener(PiquedOnBackHandler handler) {
        getPiquedActivity().setBackHandler(handler);
    }

    public void setSupportActionBar(Toolbar toolbar, @StringRes int titleRes) {

        setSupportActionBar(toolbar, titleRes, false);
    }

    public void setSupportActionBar(Toolbar toolbar, @StringRes int titleRes, boolean shouldShowBackButton) {
        getPiquedActivity().setSupportActionBar(toolbar);

        if (getPiquedActivity().getSupportActionBar() != null) {
            getPiquedActivity().getSupportActionBar().setTitle(titleRes);

            getPiquedActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(shouldShowBackButton);
        }
    }

    public void setSupportActionBar(Toolbar toolbar, String title) {
        setSupportActionBar(toolbar, title, false);
    }

    private void setSupportActionBar(Toolbar toolbar, String title, boolean shouldShowBackButton) {
        getPiquedActivity().setSupportActionBar(toolbar);

        if (getPiquedActivity().getSupportActionBar() != null) {
            getPiquedActivity().getSupportActionBar().setTitle(title);

            getPiquedActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(shouldShowBackButton);
        }
    }

    protected AppComponent getComponent() {
        return getApplication().getComponent();
    }

    protected MetricValues getMetric() {
        return getPiquedActivity().getMetric();
    }

    public void pushFragment(PiquedDialogFragment fragment) {
        getPiquedActivity().pushFragment(fragment);
    }

    public void pushFragment(PiquedFragment fragment) {
        getPiquedActivity().pushFragment(fragment, true, true);
    }

    public void pushFragmentOtherSide(PiquedFragment fragment) {
        getPiquedActivity().pushFragment(fragment, true, true, false);
    }

    public void goBack() {
        getPiquedActivity().onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (shouldTrackKeyboardVisible()) {
            getPiquedActivity().setKeyboardEventListener(mKeyboardEventListener);
        }
    }

    @Override
    public void onPause() {
        if (shouldTrackKeyboardVisible()) {
            getPiquedActivity().setKeyboardEventListener(null);
        }
        super.onPause();
    }

    protected void updateActionBar(Toolbar toolbar) {
        updateActionBar(toolbar, R.string.empty);
    }

    protected void updateActionBar(Toolbar toolbar, @StringRes int titleRes) {
        updateActionBar(toolbar, titleRes, true);
    }

    protected void updateActionBar(Toolbar toolbar, @StringRes int titleRes, boolean displayHomeAsUp) {
        int titleResToUse = R.string.empty;

        if (titleRes != 0) {
            titleResToUse = titleRes;
        }

        getPiquedActivity().setSupportActionBar(toolbar);
        getPiquedActivity().setTitle(titleResToUse);

        ActionBar actionBar = getPiquedActivity().getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(displayHomeAsUp);
        }
    }

    public void showSoftKeyboard(@NonNull EditText editText) {
        if (isUIActive()) {
            editText.requestFocus();
            InputMethodManager imm = (InputMethodManager) getPiquedActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            }
        }
    }

    public void hideSoftKeyboard() {
        // Check if no view has focus:
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public int getSystemScreenHeight() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getPiquedActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public boolean isUIActive() {
        return isAdded() && !isDetached() && !isRemoving() && getContext() != null && getView() != null;
    }

    protected void updateGuideLine(Guideline guideline) {

        if (guideline != null && isUIActive()) {
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) guideline.getLayoutParams();

            params.guideEnd = getPiquedActivity().getBothNavigationHeight();

            guideline.setLayoutParams(params);
        }
    }

    protected void updateGuideLine(Guideline guideline, int distanceFromBottom) {
        if (guideline != null && isUIActive()) {

            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) guideline.getLayoutParams();

            params.guideEnd = distanceFromBottom;

            guideline.setLayoutParams(params);
        }
    }

    protected int getNavigationBarHeight() {
        return getPiquedActivity().getNavigationBarHeight();
    }

    protected int getPiquedNavigationBarHeight() {
        return getPiquedActivity().getBothNavigationHeight();
    }

    protected int getSoftKeyboardHeight() {
        return 0;
    }

    protected void onKeyboardHidden(boolean isHidden, int keyboardHeight) {
        // override me
    }

    private final KeyboardEventListener mKeyboardEventListener = (isHidden, keyboardHeight) -> {
        if (isUIActive()) {
            PiquedFragment.this.onKeyboardHidden(isHidden, keyboardHeight);
        }
    };

    public String getTagName() {
        return this.getClass().getSimpleName();
    }

    protected boolean shouldTrackKeyboardVisible() {
        return false;
    }

    public void runIfLoggedIn(Preferences preferences, LoginRequiredExecution exec) {
        IntentUtil.runIfUserLoggedIn(getContext(), preferences.userLoggedIn(), exec);
    }
    /**
     *
     * Databinding related code
     *
     */
    @BindingAdapter("android:layout_marginTop")
    public static void setLayoutMarginTop(View view, float marginTop) {

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();

        layoutParams.setMargins(layoutParams.leftMargin, (int) marginTop, layoutParams.rightMargin, layoutParams.bottomMargin);

        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter("android:layout_marginBottom")
    public static void setLayoutMarginBottom(View view, float marginBottom) {

        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();

            layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.rightMargin, (int) marginBottom);

            view.setLayoutParams(layoutParams);
        } else {

            throw new IllegalArgumentException("Unsupported layout param type: " + view.getLayoutParams().getClass().getName());
        }
    }

    @BindingAdapter("android:layout_height")
    public static void setLayoutHeight(View view, float height) {

        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();

        layoutParams.height = (int) height;

        view.setLayoutParams(layoutParams);
    }
}
