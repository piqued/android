package io.piqued.passwordrecovery

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Keep
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import io.piqued.PiquedFragment
import io.piqued.R
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.repository.UserRepository
import io.piqued.cloud.util.ResetPasswordState
import io.piqued.login.utils.LoginSignUpSupport
import io.piqued.database.util.PiquedLogger
import io.piqued.utils.ToastHelper
import kotlinx.android.synthetic.main.fragment_forget_password_enter_email.*
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 1/4/19.
 * Piqued Inc.
 *
 */

@Keep
class PasswordRecoveryEnterEmailFragment: PiquedFragment() {

    @Inject
    lateinit var userRepository: UserRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_forget_password_enter_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        closeButton.setOnClickListener {
            findNavController().navigateUp()
        }

        passwordRecoveryEnterEmailRootView.setOnApplyWindowInsetsListener { _, windowInsets ->
            val params = statusBarBottomGuideLine.layoutParams as ConstraintLayout.LayoutParams
            params.guideBegin = windowInsets.systemWindowInsetTop
            statusBarBottomGuideLine.layoutParams = params
            windowInsets.consumeSystemWindowInsets()
        }

        emailInput.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                sendPasswordButton.isEnabled = LoginSignUpSupport.isEmailValid(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        sendPasswordButton.setOnClickListener {

            userRepository.resetPassword(emailInput.text.toString(), object: PiquedCloudCallback<ResetPasswordState> {
                override fun onSuccess(result: ResetPasswordState?) {

                    if (result != null) {
                        showMessage(result)
                    } else {
                        showMessage(ResetPasswordState.ERROR)
                    }
                }

                override fun onFailure(error: Throwable?, message: String?) {
                    PiquedLogger.e(error, message)
                    showMessage(ResetPasswordState.ERROR)
                }

            })
        }
    }

    private fun showMessage(state: ResetPasswordState) {

        val currentContext = context
        if (currentContext != null) {
            ToastHelper.showPiquedToast(currentContext, state.stringRes)

            if (state == ResetPasswordState.EMAIL_SENT) {
                NavHostFragment.findNavController(this).popBackStack()
            }
        }
    }
}