package io.piqued.common

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

import io.piqued.R
import io.piqued.module.GlideApp
import kotlinx.android.synthetic.main.fragment_simple_image.*

class SimpleImageFragment: Fragment()
{
    companion object {

        private const val KEY_IMAGE_URI = "image_uri"

        fun newInstance(imageUri: Uri): SimpleImageFragment {
            val args = Bundle()
            args.putParcelable(KEY_IMAGE_URI, imageUri)

            val result = SimpleImageFragment()

            result.arguments = args

            return result
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_simple_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val imageUri = arguments?.getParcelable<Uri>(KEY_IMAGE_URI)

        if (imageUri != null) {
            GlideApp.with(view.context)
                    .load(imageUri)
                    .centerCrop()
                    .into(simpleImageMainImage)
        }
    }
}