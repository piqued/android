package io.piqued.config

import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.Toast
import io.piqued.PiquedFragment
import io.piqued.R
import io.piqued.cloud.BuildConfig
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.PiquedCloudImpl
import io.piqued.cloud.model.foursquare.FourSquareVenue
import io.piqued.cloud.model.piqued.Echo
import io.piqued.cloud.util.CloudPreference
import io.piqued.logviewer.LogViewerActivity
import io.piqued.utils.IntentUtil
import kotlinx.android.synthetic.main.fragment_server_config.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class ServerConfigFragment: PiquedFragment() {

    companion object {
        fun newInstance(): ServerConfigFragment {
            return ServerConfigFragment()
        }
    }

    @Inject
    lateinit var cloudPreference: CloudPreference
    @Inject
    lateinit var piquedCloud: PiquedCloudImpl

    private var oldHostUrl: String = ""

    override fun getTagName(): String {
        return ServerConfigFragment::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_server_config, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.server_config_menu, menu)

        testButton.setOnClickListener {
            performHealthCheck()
        }

        useStagingButton.setOnClickListener {
            currentServerUrl.setText(BuildConfig.PIQUED_STAGING_URL)
            currentShareUrl.setText(BuildConfig.PIQUED_STAGING_SHARE_HOST_URL)
        }

        useProductionButton.setOnClickListener {
            currentServerUrl.setText(BuildConfig.PIQUED_PROD_URL)
            currentShareUrl.setText(BuildConfig.Piqued_PROD_SHARE_HOST_URL)
        }

        fourSquareTestButton.setOnClickListener {
            performFourSquareHealthCheck()
        }

        fourSquareHostResetButton.setOnClickListener {
            currentFourSquareServerUrl.setText(BuildConfig.DEFAULT_FOURSQUARE_HOST_URL)
        }

        sendLogButton.setOnClickListener {

            val currentContext = context

            if (currentContext != null) {
                IntentUtil.sendLogToPiqued(currentContext)
            }
        }

        viewAllLogs.setOnClickListener {
            startActivity(LogViewerActivity::class.java)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        currentServerUrl.setText(cloudPreference.getHostUrl())
        currentFourSquareServerUrl.setText(cloudPreference.getFourSquareHostUrl())
        currentShareUrl.setText(cloudPreference.getShareHostUrl())

        setSupportActionBar(toolbar, R.string.configure_server_title)
    }

    override fun onResume() {
        super.onResume()

        oldHostUrl = cloudPreference.getHostUrl()
        updateVersionInfo()
    }

    override fun onPause() {
        if (!oldHostUrl.isEmpty()) {
            saveNewHostUrl(oldHostUrl)
        }

        super.onPause()
    }

    private fun updateVersionInfo() {

        val fields = Build.VERSION_CODES::class.java.fields
        val osName = fields[Build.VERSION.SDK_INT].name
        val osVersion = Build.VERSION.SDK_INT

        androidVersion.text = getString(R.string.android_version, osName, osVersion.toString())
        appVersionNumber.text = getString(R.string.app_version, io.piqued.BuildConfig.VERSION_NAME)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.configure_save_button -> {
                oldHostUrl = ""
                saveNewHostUrl(currentServerUrl.text.toString().trim())
                saveNewShareHostUrl(currentShareUrl.text.toString().trim())
                saveNewFourSquareUrl(currentFourSquareServerUrl.text.toString().trim())
                goBack()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun saveNewHostUrl(newUrl: String) {
        if (cloudPreference.getHostUrl() != newUrl) {
            cloudPreference.setHostUrl(newUrl)
            piquedCloud.updateHostUrl(newUrl)
        }
    }

    private fun saveNewShareHostUrl(newShareHostUrl: String) {
        if (cloudPreference.getShareHostUrl() != newShareHostUrl) {
            cloudPreference.setShareHostUrl(newShareHostUrl)
        }
    }

    private fun saveNewFourSquareUrl(newHostUrl: String) {
        if (cloudPreference.getFourSquareHostUrl() != newHostUrl) {
            cloudPreference.setFourSquareHostUrl(newHostUrl)
            piquedCloud.updateHostUrl(newHostUrl)
        }
    }

    private fun performHealthCheck() {

        piquedCloud.updateHostUrl(currentServerUrl.text.toString().trim())

        piquedCloud.performHealthCheck(object : PiquedCloudCallback<Echo?> {
            override fun onFailure(error: Throwable?, message: String?) {
                Toast.makeText(context, "Cannot reach server :(", Toast.LENGTH_LONG).show()
            }

            override fun onSuccess(result: Echo?) {
                Toast.makeText(context, "Looks good to me!", Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun performFourSquareHealthCheck() {

        piquedCloud.updateFourSquareHostUrl(currentFourSquareServerUrl.text.toString().trim())

        piquedCloud.searchFourSquareVenue(37.422439, -22.084041, object : PiquedCloudCallback<FourSquareVenue> {
            override fun onSuccess(result: FourSquareVenue?) {
                Toast.makeText(context, "Looks good to me!", Toast.LENGTH_LONG).show()
            }

            override fun onFailure(error: Throwable?, message: String?) {
                Toast.makeText(context, "Cannot reach four square server :(", Toast.LENGTH_LONG).show()
            }

        })
    }
}