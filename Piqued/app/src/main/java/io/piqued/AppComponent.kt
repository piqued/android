package io.piqued

import dagger.Component
import io.piqued.activities.*
import io.piqued.basket.comment.BasketCommentFragment
import io.piqued.config.ServerConfigFragment
import io.piqued.createbasket.NewCreateBasketActivity
import io.piqued.createbasket.fragment.addcomment.NewAddCommentFragment
import io.piqued.createbasket.fragment.selectlocation.SelectLocationFragment
import io.piqued.login.NewLoginActivity
import io.piqued.module.AppModule
import io.piqued.passwordrecovery.PasswordRecoveryEnterEmailFragment
import io.piqued.service.imageupload.CreateBasketService
import io.piqued.service.imageupload.CreateMultiImageBasketService
import io.piqued.service.pushnotification.PiquedNotificationService
import io.piqued.views.activities.ActivitiesFragment
import io.piqued.views.activities.YourActivityFragment
import io.piqued.views.bookmark.BookmarkFragment
import io.piqued.views.home.HomeFragment
import io.piqued.views.home.list.HomeListFragment
import io.piqued.views.home.map.HomeMapFragment
import io.piqued.views.home.map.HomeMapboxFragment
import io.piqued.views.home.map.NewHomeMapFragment
import io.piqued.views.login.LoginChoiceSubFragment
import io.piqued.views.profile.ProfileFragment
import io.piqued.views.profile.edit.EditProfileFragment
import io.piqued.views.profile.edit.important.EditImportantInfoFragment
import io.piqued.views.settings.SettingsFragment
import io.piqued.views.welcome.WelcomeFragment
import javax.inject.Singleton

/**
 * Created by kennymliou on 11/16/16.
 * Piqued Inc.
 */

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    // Services
    fun inject(service: CreateBasketService)

    fun inject(service: CreateMultiImageBasketService)

    fun inject(service: PiquedNotificationService)

    // Activities

    fun inject(activity: NewLoginActivity)

    fun inject(activity: FloatingActivity)

    fun inject(activity: SplashActivity)

    fun inject(activity: HomeActivity)

    fun inject(activity: SettingsActivity)

    fun inject(activity: EditProfileActivity)

    // Fragments

    fun inject(fragment: LoginChoiceSubFragment)

    fun inject(fragment: ServerConfigFragment)

    fun inject(fragment: HomeFragment)

    fun inject(fragment: HomeMapFragment)

    fun inject(fragment: HomeListFragment)

    fun inject(fragment: WelcomeFragment)

    fun inject(fragment: BookmarkFragment)

    fun inject(fragment: YourActivityFragment)

    fun inject(fragment: ActivitiesFragment)

    fun inject(fragment: SettingsFragment)

    fun inject(fragment: BasketCommentFragment)

    fun inject(fragment: EditImportantInfoFragment)

    // Home
    fun inject(fragment: HomeMapboxFragment)

    fun inject(fragment: NewHomeMapFragment)

    fun inject(fragment: io.piqued.basket.BasketFragment)

    // Profile Fragments
    fun inject(fragment: ProfileFragment)

    fun inject(fragment: EditProfileFragment)

    // Create Basket

    fun inject(activity: NewCreateBasketActivity)

    fun inject(fragment: NewAddCommentFragment)

    fun inject(fragment: SelectLocationFragment)

    // Password recovery

    fun inject(fragment: PasswordRecoveryEnterEmailFragment)
}
