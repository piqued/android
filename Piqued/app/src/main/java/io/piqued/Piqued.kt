package io.piqued

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import com.google.firebase.FirebaseApp
import com.onesignal.OneSignal
import io.piqued.database.util.PiquedLogger
import io.piqued.module.AppModule
import io.piqued.service.imageupload.BroadcastCreateBasketStatus

/**
 *
 * Created by Kenny M. Liou on 11/11/17.
 * Piqued Inc.
 *
 */
class Piqued : Application() {

    private lateinit var mComponent : AppComponent

    val uploadStatusData = MutableLiveData<Intent>()

    private val onUploadStatusChangedReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null) {
                uploadStatusData.value = intent
            }
        }
    }

    override fun onCreate() {
        super.onCreate()

        PiquedLogger.initialize(this)

        mComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
        BroadcastCreateBasketStatus.registerBroadcast(this, onUploadStatusChangedReceiver)

        // Setup Firebase Cloud Message
        FirebaseApp.initializeApp(this)

        // Logging set to help debug issues, remove before releasing your app.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init()

    }

    override fun onTerminate() {
        BroadcastCreateBasketStatus.unregisterBroadcast(this, onUploadStatusChangedReceiver)

        PiquedLogger.onTerminate()

        super.onTerminate()
    }

    fun getComponent() : AppComponent {
        return mComponent
    }
}