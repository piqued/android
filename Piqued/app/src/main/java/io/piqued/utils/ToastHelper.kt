package io.piqued.utils

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes



/**
 *
 * Created by Kenny M. Liou on 2019-01-12.
 * Piqued Inc.
 *
 */
class ToastHelper {

    companion object {

        fun showPiquedToast(context: Context, @StringRes stringRes: Int) {

            Toast.makeText(context, stringRes, Toast.LENGTH_LONG).show()
        }
    }
}