package io.piqued.utils;

import java.util.List;

import io.piqued.database.post.PostId;

/**
 * Created by Kenny M. Liou on 8/6/17.
 * Piqued Inc.
 */

public interface GetAllPostsCallback {

    void onPosts(int currentPage, List<PostId> posts);
    void onFetchingComplete(List<PostId> allPosts);
}
