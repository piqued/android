package io.piqued.utils.customview

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.recyclerview.widget.RecyclerView
import io.piqued.views.home.map.PreviewListCallback

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 6/24/17.
 * Piqued Inc.
 */

class PostPreviewRecyclerView
    @JvmOverloads constructor(context: Context,
                              attrs: AttributeSet? = null,
                              defStyle: Int = 0)
    : RecyclerView(context, attrs, defStyle) {

    private var mCallback: PreviewListCallback? = null

    override fun onInterceptTouchEvent(e: MotionEvent): Boolean {

        println("onInterceptTouchEvent: " + e.action)

        return if (e.action == MotionEvent.ACTION_MOVE || e.y + computeVerticalScrollOffset() > mCallback!!.fullListHeight) {
            super.onInterceptTouchEvent(e)
        } else false
    }

    override fun onTouchEvent(e: MotionEvent): Boolean {

        println("onTouchEvent: " + e.action )
        return if (e.action == MotionEvent.ACTION_MOVE || e.y + computeVerticalScrollOffset() > mCallback!!.fullListHeight) {
            super.onTouchEvent(e)
        } else false
    }

    fun setCallback(callback: PreviewListCallback) {
        mCallback = callback
    }
}
