package io.piqued.utils

interface LoginRequiredExecution {
    fun runIfLoggedIn()
}