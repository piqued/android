package io.piqued.utils

import android.content.Context
import com.google.gson.GsonBuilder
import io.piqued.cloud.model.piqued.UserToken

/**
 * Created by kennymliou on 11/16/16.
 * Piqued Inc.
 */
class Preferences(context: Context) {
    companion object {
        private const val PREFERENCES = "Piqued Preferences"
        private const val USER_TOKEN = "Piqued token"
        private const val WELCOME_SCREEN_SHOWN = "Welcome Screen Shown"
        private const val POST_MANAGEMENT_LAST_MODIFIED_TIME = "last_modified_time"
        private const val DEVICE_FCM_TOKEN = "FCM token"
    }

    private val mPreferences = context.getSharedPreferences(PREFERENCES, 0)
    private val builder = GsonBuilder()
    private var mUserToken: UserToken? = null
    var lastModifiedTime: Long
        get() = mPreferences.getLong(POST_MANAGEMENT_LAST_MODIFIED_TIME, -1)
        set(value) {
            mPreferences.edit()
                    .putLong(POST_MANAGEMENT_LAST_MODIFIED_TIME, value)
                    .apply()
        }
    var userToken: UserToken?
        get() {
            if (!userLoggedIn()) return null
            if (mUserToken == null) {
                mUserToken = builder.create().fromJson(mPreferences.getString(USER_TOKEN, null), UserToken::class.java)
            }
            return mUserToken
        }
        set(token) {
            mPreferences
                    .edit()
                    .putString(USER_TOKEN, builder.create().toJson(token))
                    .apply()
            mUserToken = token
        }

    fun setFCMToken(fcmToken: String) {
        setStringWithKey(DEVICE_FCM_TOKEN, fcmToken)
    }

    fun getFCMToken(): String {
        return getStringWithKey(DEVICE_FCM_TOKEN)
    }

    fun userLoggedIn(): Boolean {
        return mPreferences.getString(USER_TOKEN, null) != null
    }

    fun welcomeScreenShown(): Boolean {
        return mPreferences.getBoolean(WELCOME_SCREEN_SHOWN, false)
    }

    fun setWelcomeScreenShown(shown: Boolean) {
        mPreferences.edit()
                .putBoolean(WELCOME_SCREEN_SHOWN, shown)
                .apply()
    }

    fun setStringWithKey(key: String, value: String) {
        mPreferences.edit()
                .putString(key, value)
                .apply()
    }

    fun getStringWithKey(key: String): String {
        return mPreferences.getString(key, "") ?: ""
    }

    fun reset() {
        mPreferences.edit().clear().apply()
    }
}