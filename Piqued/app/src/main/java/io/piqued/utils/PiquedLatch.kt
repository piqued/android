package io.piqued.utils

import android.os.Handler
import android.os.Looper
import java.util.concurrent.CountDownLatch

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/24/17.
 * Piqued Inc.
 */
class PiquedLatch(count: Int) {
    interface PiquedLatchCallback {
        fun onCountDown()
    }

    private val mLatch: CountDownLatch = CountDownLatch(count)
    private var mCallback: PiquedLatchCallback? = null
    private val mHandler = Handler(Looper.getMainLooper())
    private var aborted = false
    fun setCallback(callback: PiquedLatchCallback?) {
        mCallback = callback
    }

    fun start() {
        object : Thread() {
            override fun run() {
                try {
                    mLatch.await()
                    if (mCallback != null && !aborted) {
                        val callback = mCallback
                        mCallback = null
                        if (callback != null) {
                            mHandler.post { callback.onCountDown() }
                        }
                    }
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }.start()
    }

    @Synchronized
    fun countDown() {
        mLatch.countDown()
    }

    fun cancel() {
        if (mLatch.count == 0L) {
            return
        }
        aborted = true
        while (mLatch.count > 0) {
            mLatch.countDown()
        }
    }

}