package io.piqued.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.webkit.URLUtil
import io.piqued.PiquedFragment
import io.piqued.activities.PiquedActivity

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/27/16.
 * Piqued Inc.
 */

object UIHelper {

    fun startActivity(activity: PiquedActivity, clazz: Class<out PiquedActivity>) {

        val intent = Intent(activity, clazz)

        activity.startActivity(intent)
        activity.finish()
    }

    fun startActivity(fragment: PiquedFragment, clazz: Class<out PiquedActivity>) {

        val intent = Intent(fragment.context, clazz)

        fragment.piquedActivity.startActivity(intent)
        fragment.piquedActivity.finish()
    }

    fun startActivity(c: Context, clazz: Class<out PiquedActivity>) {
        c.startActivity(Intent(c, clazz))
    }

    /**
     * Use this method if create a stack of activities is the intention.
     * @param fragment fragment this method is calling from
     * @param clazz PiquedActivity class that will be started
     */
    fun pushActivity(fragment: PiquedFragment, clazz: Class<out PiquedActivity>) {

        val intent = Intent(fragment.context, clazz)

        fragment.piquedActivity.startActivity(intent)
    }

    fun openUrlInBrowser(context: Context, websiteUrl: String) {

        if (URLUtil.isValidUrl(websiteUrl) && URLUtil.isNetworkUrl(websiteUrl)) {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(websiteUrl)))
        }
    }
}
