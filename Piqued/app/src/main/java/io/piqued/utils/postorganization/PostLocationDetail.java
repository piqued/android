package io.piqued.utils.postorganization;

import com.google.android.gms.maps.model.LatLng;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.piqued.database.post.CloudPost;
import io.piqued.cloud.model.piqued.Venue;
import io.piqued.utils.DistanceHelper;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/23/17.
 * Piqued Inc.
 */

public class PostLocationDetail {

    private static final long TIME_STAMP_TO_MILL = TimeUnit.SECONDS.toMillis(1);
    private static final int DISTANCE_THRESHOLD = 1000 * 30; // 30 KM
    private final long mPostId;
    private final long mCreatedAt;
    private final String mCountry;
    private final String mState;
    private final String mCity;
    private final LatLng mLatLng;


    public PostLocationDetail(CloudPost post, Venue venue) {
        mPostId = post.getPostId();
        mCreatedAt = post.getCreatedAt() * TIME_STAMP_TO_MILL;
        mCountry = venue.getCountry();
        mState = venue.getState();
        mCity = venue.getCity();
        mLatLng = new LatLng(post.getLatLng().latitude, post.getLatLng().longitude);
    }

    public long getCreatedAt() {
        return mCreatedAt;
    }

    public long getPostId() {
        return mPostId;
    }

    public boolean isSameMonth(PostLocationDetail detail) {
        Calendar calendarThis = Calendar.getInstance();
        Calendar calendarDetails = Calendar.getInstance();

        calendarThis.setTime(new Date(mCreatedAt));
        calendarDetails.setTime(new Date(detail.mCreatedAt));

        return calendarThis.get(Calendar.YEAR) == calendarDetails.get(Calendar.YEAR) &&
                calendarThis.get(Calendar.MONTH) == calendarDetails.get(Calendar.MONTH);
    }

    public boolean isSameDay(PostLocationDetail details) {

        Calendar calendarThis = Calendar.getInstance();
        Calendar calendarDetails = Calendar.getInstance();

        calendarThis.setTime(new Date(mCreatedAt));
        calendarDetails.setTime(new Date(details.mCreatedAt));

        return calendarThis.get(Calendar.YEAR) == calendarDetails.get(Calendar.YEAR) &&
                calendarThis.get(Calendar.DAY_OF_YEAR) == calendarDetails.get(Calendar.DAY_OF_YEAR);
    }

    public boolean isSameCity(PostLocationDetail details) {

        if (mCountry == null || details.mCountry == null || !mCountry.equals(details.mCountry)) {
            return false;
        }

        if (mState != null && details.mState != null && !mState.equals(details.mState)) {
            return false;
        }

        return mCity != null && details.mCity != null && mCity.equals(details.mCity);
    }

    public boolean isNearBy(PostLocationDetail details) {

        boolean isNearBy = false;
        int compareCount = 0;

        if (mCountry != null && details.mCountry != null) {
            compareCount ++;
            isNearBy &= mCountry.equals(details.mCountry);
        }

        if (mState != null && details.mState != null) {
            compareCount ++;
            isNearBy &= mState.equals(details.mState);
        }

        if (mCity != null && details.mCity != null) {
            compareCount ++;
            isNearBy &= mCity.equals(details.mCity);
        }

        if (compareCount > 1) {
            return isNearBy;
        } else {
            return DistanceHelper.INSTANCE.getDistanceInMeters(mLatLng, details.mLatLng) < DISTANCE_THRESHOLD;
        }
    }

    public String getCity() {
        return mCity;
    }

    public String getState() {
        return mState;
    }

    public String getCountry() {
        return mCountry;
    }
}
