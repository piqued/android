package io.piqued.utils.upload;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Size;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.FutureTarget;
import com.google.gson.Gson;

import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import io.piqued.api.PiquedApiCallback;
import io.piqued.api.PiquedNetworkApi;
import io.piqued.api.PiquedRequest;
import io.piqued.cloud.model.piqued.ImageContainer;
import io.piqued.cloud.model.piqued.PreSignResponse;
import io.piqued.cloud.util.CloudPreference;
import io.piqued.cloud.util.ImageUploadProgressListener;
import io.piqued.model.imageupload.ImageUploadResult;
import io.piqued.model.imageupload.PreSignObject;
import io.piqued.module.GlideApp;
import io.piqued.utils.Constants;
import io.piqued.utils.ImageUtil;
import io.piqued.database.util.PiquedLogger;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 4/29/17.
 * Piqued Inc.
 */

public class ImageUploadUtil extends PiquedNetworkApi {

    private static final String PRE_SIGN_URL = "/photos/cache/presign";
    private static final String TAG = ImageUploadUtil.class.getSimpleName();
    private static final String ACTION_IMAGE_UPLOAD_PROGRESS = "image_upload_progress";
    private static final String KEY_BYTES_SENT = "bytes_sent";
    private static final String KEY_CONTENT_LENGTH = "content_length";

    public ImageUploadUtil(@NonNull Context context, @NonNull CloudPreference preferences) {
        super(context, preferences);
    }

    /**
     * Step one, fetch presign
     * @param getPreSignCallback callback
     */
    public void fetchPreSign(PiquedApiCallback<PreSignObject> getPreSignCallback) {

        // this is where we fetch
        networkHelper(new PiquedRequest(PRE_SIGN_URL, NetworkAction.GET), null, new JSONCallback<PreSignObject>(getPreSignCallback) {
            @Override
            protected PreSignObject onReceiveResult(String responseString) {

                Gson gson = new Gson();

                return gson.fromJson(responseString, PreSignObject.class);
            }
        });
    }

    /**
     * Step two, upload image
     * @param preSign
     */
    public void uploadImageToCache(
            final int index,
            @NonNull final PreSignObject preSign,
            @NonNull final String imageUrl,
            @NonNull final ImageContainer imageBase64,
            ImageUploadProgressListener listener,
            PiquedApiCallback<ImageUploadResult> callback) {

        networkHelper(preSign, imageUrl, imageBase64, listener, new JSONCallback<ImageUploadResult>(callback) {
            @Override
            protected ImageUploadResult onReceiveResult(String responseString) {

                Gson gson = new Gson();

                ImageUploadResult result = new ImageUploadResult();

                result.setOriginalIndex(index);

                if (!responseString.isEmpty()) {
                    PreSignResponse preSignResponse = gson.fromJson(responseString, PreSignResponse.class);

                    result.id = preSignResponse.getPreSignId();
                    result.storage = preSignResponse.getStorageType();
                    result.sizeInBytes = preSignResponse.getMataData().getSizeInByte();
                } else {
                    String[] keys = preSign.fields.get("key").split("/");

                    result.id = keys[keys.length - 1];
                    result.storage = "cache";
                    result.sizeInBytes = imageBase64.getSizeInBytes();
                }

                return result;
            }
        });
    }

    public static Size getImageSize(@NonNull String imageUrl, int maxValue) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageUrl, options);

        final int imageWidth = options.outWidth;
        final int imageHeight = options.outHeight;

        Size result;

        if (imageWidth > maxValue || imageHeight > maxValue) {
            final double imageRatio = (float) imageWidth / (float) imageHeight;
            if (imageRatio > 1) {
                result = new Size(maxValue, (int) Math.round(maxValue / imageRatio));
            } else {
                result = new Size((int) Math.round(maxValue * imageRatio), maxValue);
            }
        } else {
            result = new Size(imageWidth, imageHeight);
        }

        return result;
    }

    public static ImageContainer createBase64ImagesWithSize(@NonNull Context context,
                                                            final String imageUrl) {
        return createBase64ImagesWithSize(context,
                imageUrl,
                new Size(Constants.PROFILE_IMAGE_SIZE, Constants.PROFILE_IMAGE_SIZE));
    }

    public static ImageContainer createBase64ImagesWithSize(@NonNull Context context,
                                                            final String imageUrl,
                                                            Size size) {
        try {

            FutureTarget<Bitmap> target = GlideApp.with(context)
                    .asBitmap()
                    .load(imageUrl)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .submit(size.getWidth(), size.getHeight());

            Bitmap image = target.get();

            return new ImageContainer(imageUrl, ImageUtil.INSTANCE.imageToByteArrayBased64(image));

        } catch (InterruptedException e) {
            PiquedLogger.e(TAG, e);
        } catch (ExecutionException e) {
            PiquedLogger.e(TAG, e);
        }

        return null;
    }

    public static void broadcastImageUploadProgress(@NonNull Context context, long byteSent, long contentLength) {

        Intent intent = new Intent(ACTION_IMAGE_UPLOAD_PROGRESS);
        intent.putExtra(KEY_BYTES_SENT, byteSent);
        intent.putExtra(KEY_CONTENT_LENGTH, contentLength);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void registerImageUploadProgressBroadcast(@NonNull Context context, @NonNull BroadcastReceiver receiver) {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_IMAGE_UPLOAD_PROGRESS);

        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);
    }

    public static void unregisterImageUploadProgressBroadcast(@NonNull Context context, @NonNull BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
    }
}
