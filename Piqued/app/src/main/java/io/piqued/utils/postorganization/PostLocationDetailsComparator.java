package io.piqued.utils.postorganization;

import java.util.Comparator;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/24/17.
 * Piqued Inc.
 */

public class PostLocationDetailsComparator implements Comparator<PostLocationDetail> {
    @Override
    public int compare(PostLocationDetail left, PostLocationDetail right) {

        if (left.getCreatedAt() > right.getCreatedAt()) {
            return -1;
        } else if (left.getCreatedAt() < right.getCreatedAt()) {
            return 1;
        }
        return 0;
    }
}
