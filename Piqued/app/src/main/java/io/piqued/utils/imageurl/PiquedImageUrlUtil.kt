package io.piqued.utils.imageurl

import java.net.URL

/**
 * This is url object that's meant to facilitate glide
 * Created by Kenny M. Liou on 9/15/18.
 * Piqued Inc.
 *
 */
class PiquedImageUrlUtil(imageUrl: String?, baseUrl: String) {

    private val realUrl: URL
    init {
        val imageUrlObj = URL(imageUrl)
        val baseUrlObj = URL(baseUrl)

        if (imageUrlObj.host == "" || imageUrlObj.protocol != "https") {
            realUrl = URL(
                    baseUrlObj.protocol,
                    baseUrlObj.host,
                    baseUrlObj.port,
                    imageUrlObj.file)
        } else {
            realUrl = imageUrlObj
        }
    }

    fun getURL() : URL {
        return realUrl
    }
}
