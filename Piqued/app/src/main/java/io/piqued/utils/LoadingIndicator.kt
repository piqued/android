package io.piqued.utils

import android.app.ProgressDialog

import io.piqued.activities.PiquedActivity
import io.piqued.R

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/29/16.
 * Piqued Inc.
 */

class LoadingIndicator(private val mActivity: PiquedActivity) {

    private var mDialog: ProgressDialog? = null

    fun show() {

        mDialog = ProgressDialog(mActivity, R.style.Piqued_ProgressDialog)
        mDialog?.setCancelable(false)
        mDialog?.setProgressStyle(android.R.style.Widget_ProgressBar_Small)
        mDialog?.show()
    }

    fun dismiss() {

        mDialog?.dismiss()
    }
}
