package io.piqued.utils.imageurl

import io.piqued.database.post.CloudPost
import io.piqued.utils.Constants.KEY_POST_IMAGE

class CloudPostUrl(post: CloudPost, baseUrl: String):
        PiquedImageUrl(post.getImageUrl(), baseUrl)
{
    private val cacheKeyValue = KEY_POST_IMAGE + post.postId.toString()

    override fun getCacheKey(): String {
        return cacheKeyValue
    }

    override fun toString(): String {
        return cacheKey
    }
}
