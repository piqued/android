package io.piqued.utils

import com.bumptech.glide.load.model.GlideUrl
import io.piqued.database.post.CloudPost

/**
 *
 * Created by Kenny M. Liou on 5/19/18.
 * Piqued Inc.
 *
 */
class PostProfileUrl(post: CloudPost, baseUrl: String):
        GlideUrl(if (post.getUserImageUrl().startsWith("https")) post.getUserImageUrl() else baseUrl + post.getUserImageUrl()) {

    val userId = post.userId

    override fun getCacheKey(): String {
        return userId.toString()
    }
}