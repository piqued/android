package io.piqued.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.fragment.app.DialogFragment;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;

import io.piqued.R;
import io.piqued.activities.PiquedActivity;

/**
 * Created by Kenny M. Liou on 11/4/17.
 * Piqued Inc.
 */

public class PiquedDialogFragment extends DialogFragment {

    private static final String KEY_TITLE_RES = "title_res";
    private static final String KEY_MESSAGE_RES = "message_res";

    public static PiquedDialogFragment newInstance(@StringRes int titleRes, @StringRes int messageRes) {

        Bundle args = new Bundle();
        args.putInt(KEY_TITLE_RES, titleRes);
        args.putInt(KEY_MESSAGE_RES, messageRes);

        PiquedDialogFragment fragment = new PiquedDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public String getTagName() {
        return PiquedDialogFragment.class.getName();
    }

    private SpannableStringBuilder createStringBuilder(@StringRes int stringRes, @DimenRes int textSize, @ColorRes int textColor) {

        final String string = getString(stringRes);
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(string);
        stringBuilder.setSpan(new AbsoluteSizeSpan(getResources().getDimensionPixelSize(textSize)), 0, string.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        stringBuilder.setSpan(new ForegroundColorSpan(getResources().getColor(textColor)), 0, string.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        return stringBuilder;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        SpannableStringBuilder titleBuilder = createStringBuilder(getArguments().getInt(KEY_TITLE_RES), R.dimen.text_size_16, R.color.black2);
        SpannableStringBuilder descriptionBuilder = createStringBuilder(getArguments().getInt(KEY_MESSAGE_RES), R.dimen.text_size_16, R.color.black4);

        return new AlertDialog.Builder(getContext())
                .setTitle(titleBuilder)
                .setMessage(descriptionBuilder)
                .setPositiveButton(R.string.let_me_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((PiquedActivity) getActivity()).onDialogPositiveClicked();
                    }
                })
                .setNegativeButton(R.string.leave_unsaved, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((PiquedActivity) getActivity()).onDialogNegativeClicked();
                    }
                })
                .create();
    }
}
