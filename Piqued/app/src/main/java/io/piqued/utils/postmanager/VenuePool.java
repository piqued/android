package io.piqued.utils.postmanager;

import java.util.HashMap;

import io.piqued.cloud.model.piqued.Venue;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/22/17.
 * Piqued Inc.
 */

public class VenuePool {

    private final HashMap<Long, Venue> mVenuePool = new HashMap<>();

    public Venue getVenue(long postId) {

        if (mVenuePool.containsKey(postId)) {
            return mVenuePool.get(postId);
        }

        return null;
    }

    public void setVenue(long postId, Venue venue) {

        mVenuePool.put(postId, venue);
    }
}
