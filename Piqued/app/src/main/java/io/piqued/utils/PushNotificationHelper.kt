package io.piqued.utils

import android.text.TextUtils
import io.piqued.api.PiquedApiCallback
import io.piqued.api.piqued.PiquedApi
import io.piqued.cloud.model.piqued.UserToken
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger

/**
 *
 * Created by Kenny M. Liou on 11/18/17.
 * Piqued Inc.
 *
 */
class PushNotificationHelper {

    companion object {

        private val TAG = PushNotificationHelper::class.java.simpleName

        fun handlePushNotificationSubscription(
                subscribe: Boolean,
                fcmToken: String,
                api: PiquedApi,
                token: UserToken,
                callback: PiquedApiCallback<Boolean>) {
            if (TextUtils.isEmpty(fcmToken)) {
                PiquedLogger.i(TAG, "FCM token is not setup yet, notification registration will be ignored")
                callback.onFailure(PiquedErrorCode.DEVICE_INIT_ERROR, "Expect to have Device identifier but get null")
                return
            }
            if (subscribe) {
                subscribePushNotification(fcmToken, token, api, callback)
            } else {
                unsubscribePushNotification(fcmToken, token, api, callback)
            }
        }

        private fun subscribePushNotification(
                deviceId: String,
                token: UserToken,
                api: PiquedApi,
                callback: PiquedApiCallback<Boolean>
        ) {
            api.registerForPushNotification(deviceId, token, object : PiquedApiCallback<Boolean> {
                override fun onSuccess(data: Boolean) {
                    PiquedLogger.i(TAG, "push notif subscription success!")
                    callback.onSuccess(data)
                }

                override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                    PiquedLogger.i(TAG, "push notif subscription FAILED!")
                    PiquedLogger.e(TAG, errorCode, payload)
                    callback.onFailure(errorCode, payload)
                }
            })
        }

        private fun unsubscribePushNotification(
                deviceId: String,
                token: UserToken,
                api: PiquedApi,
                callback: PiquedApiCallback<Boolean>
        ) {
            api.unregisterForPushNotification(deviceId, token, object : PiquedApiCallback<Boolean> {
                override fun onSuccess(data: Boolean) {
                    PiquedLogger.i(TAG, "push notif UN-subscription success!")
                    callback.onSuccess(data)
                }

                override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                    PiquedLogger.i(TAG, "push notif UN-subscription FAILED!")
                    PiquedLogger.e(TAG, errorCode, payload)
                    callback.onFailure(errorCode, payload)
                }
            })
        }
    }
}