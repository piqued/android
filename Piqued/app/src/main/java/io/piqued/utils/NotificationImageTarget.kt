package io.piqued.utils

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.bumptech.glide.request.Request
import com.bumptech.glide.request.target.SizeReadyCallback
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import io.piqued.database.util.PiquedLogger

abstract class NotificationImageTarget: Target<Bitmap> {
    override fun onStart() {}

    override fun onStop() {}

    override fun onDestroy() {}

    override fun onLoadStarted(placeholder: Drawable?) {}

    override fun onLoadFailed(errorDrawable: Drawable?) {
        PiquedLogger.i("NotificationImageTarget", "Load image for notification failed")
    }

    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
        onImageReady(resource)
    }

    override fun onLoadCleared(placeholder: Drawable?) {}

    override fun getSize(cb: SizeReadyCallback) {
        cb.onSizeReady(480, 480)
    }

    override fun removeCallback(cb: SizeReadyCallback) {}

    override fun setRequest(request: Request?) {}

    override fun getRequest(): Request? {
        return null
    }

    abstract fun onImageReady(image: Bitmap)
}