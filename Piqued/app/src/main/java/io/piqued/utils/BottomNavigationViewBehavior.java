package io.piqued.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

/**
 * Created by Kenny M. Liou on 11/11/18.
 * Piqued Inc.
 */
public class BottomNavigationViewBehavior extends CoordinatorLayout.Behavior<BottomNavigationView> {

    public BottomNavigationViewBehavior() {

    }

    public BottomNavigationViewBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(@NonNull CoordinatorLayout parent, @NonNull BottomNavigationView child, @NonNull View dependency) {
        return dependency instanceof Snackbar.SnackbarLayout;
    }

    @Override
    public boolean onDependentViewChanged(@NonNull CoordinatorLayout parent, @NonNull BottomNavigationView child, @NonNull View dependency) {

        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) parent.getLayoutParams();
        params.height = (int) (dependency.getHeight() - dependency.getTranslationY() + child.getHeight());

        parent.setLayoutParams(params);

        return true;
    }
}
