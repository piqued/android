package io.piqued.utils.modelmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.piqued.api.PiquedApiCallback;
import io.piqued.cloud.model.piqued.ImageContainer;
import io.piqued.database.user.User;
import io.piqued.database.util.PiquedErrorCode;
import io.piqued.database.util.PiquedLogger;
import io.piqued.model.EditUserData;
import io.piqued.utils.AccountManager;
import io.piqued.utils.Preferences;
import io.piqued.utils.upload.ImageUploadUtil;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/28/17.
 * Piqued Inc.
 */

public class PiquedUserManager {

    private static final String MY_USER = "my_user";
    private static final String TAG = PiquedUserManager.class.getSimpleName();
    private static final String ACTION_UPDATE_FOLLOW = "follow_user_update";
    private static final String KEY_FOLLOW_USER_ID = "follow_user_id";
    private static final String KEY_FOLLOWED_VALUE = "followed";

    public static void registerFollowUpdate(Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, new IntentFilter(ACTION_UPDATE_FOLLOW));
    }

    public static boolean isFollowUpdateBroadcast(Intent intent) {
        return ACTION_UPDATE_FOLLOW.equals(intent.getAction());
    }

    public static long getUserIdFromBroadcast(Bundle bundle) {

        return bundle.getLong(KEY_FOLLOW_USER_ID, -1);
    }

    public static boolean getIsFollowedFromBroadcast(Bundle bundle) {

        return bundle.getBoolean(KEY_FOLLOWED_VALUE, false);
    }

    public static void notifyUserFollowUpdated(Context context, long userId, boolean followed) {

        Intent intent = new Intent(ACTION_UPDATE_FOLLOW);
        intent.putExtra(KEY_FOLLOW_USER_ID, userId);
        intent.putExtra(KEY_FOLLOWED_VALUE, followed);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private final Preferences mPreferences;
    private final AccountManager mAccountManager;

    private GsonBuilder builder = new GsonBuilder();

    private UserPool mUserPool = new UserPool();
    private User mMyUser;

    public PiquedUserManager(Preferences preferences, AccountManager accountManager) {
        mPreferences = preferences;
        mAccountManager = accountManager;
        
        mMyUser = builder.create().fromJson(mPreferences.getStringWithKey(MY_USER), User.class);
    }

    public void setMyUserRecord(User user) {

        Gson gson = builder.create();

        mPreferences.setStringWithKey(MY_USER, gson.toJson(user));

        mMyUser = user;
    }

    public void updateUserData(final Context context,
                               final EditUserData userData,
                               final PiquedApiCallback<User> callback) {

        new Thread(() -> updateUserData(userData.getName(),
                userData.getEmail(),
                userData.getPassword(),
                userData.hasImageData() ?
                        ImageUploadUtil.createBase64ImagesWithSize(context, userData.getImageUrl()) :
                        null,
                callback)).start();
    }

    private void updateUserData(@Nullable final String userName,
                                @Nullable final String userEmail,
                                @Nullable final String userPassword,
                                @Nullable ImageContainer imageData,
                                final PiquedApiCallback<User> callback) {

        mAccountManager.updateUserData(userName, userEmail, userPassword, imageData, new PiquedApiCallback<User>() {
            @Override
            public void onSuccess(User newUser) {

                mAccountManager.fetchUser(newUser.getUserId(), new PiquedApiCallback<User>() {
                    @Override
                    public void onSuccess(User data) {
                        mMyUser = data;

                        setMyUserRecord(mMyUser);

                        callback.onSuccess(mMyUser);
                    }

                    @Override
                    public void onFailure(PiquedErrorCode errorCode, String payload) {
                        callback.onFailure(errorCode, payload);
                    }
                });
            }

            @Override
            public void onFailure(PiquedErrorCode errorCode, String payload) {
                callback.onFailure(errorCode, payload);
            }
        });
    }

    public void fetchMyUserRecordFromCloud(final PiquedApiCallback<User> callback) {

        if (mPreferences.getUserToken() != null) {
            mAccountManager.fetchUser(mPreferences.getUserToken().getUserId(), new PiquedApiCallback<User>() {
                @Override
                public void onSuccess(User data) {

                    mMyUser = data;

                    setMyUserRecord(mMyUser);

                    callback.onSuccess(mMyUser);
                }

                @Override
                public void onFailure(PiquedErrorCode errorCode, String payload) {
                    PiquedLogger.e(TAG, "Failed to fetch new user data, use cache data if any");
                    callback.onSuccess(getMyUserRecord());
                }
            });
        } else {
            throw new IllegalStateException("Calling fetchMyUserRecord when user is not logged in");
        }
    }

    @Nullable
    public User getMyUserRecord() {

        User myUser = mMyUser;

        if (myUser == null) {
            Gson gson = builder.create();

            String myUserString = mPreferences.getStringWithKey(MY_USER);

            if (myUserString != null && myUserString.length() > 0) {
                myUser = gson.fromJson(myUserString, User.class);
            }
        }

        return myUser;
    }

    public boolean isUserLoggedIn() {
        return mPreferences.userLoggedIn();
    }

    public boolean isAccountOwner(@NonNull User user) {
        return isAccountOwner(user.getUserId());
    }

    public boolean isAccountOwner(long userId) {
        if (userId > -1 && mMyUser != null) {
            return userId == mMyUser.getUserId();
        }

        return false;
    }

    public void getUserInfo(Long userId, PiquedApiCallback<User> callback) {

        getUserInfo(false, userId, callback);
    }

    private void getUserInfo(boolean forceFetch, Long userId, PiquedApiCallback<User> callback) {
        if (mUserPool.getUser(userId) != null && !forceFetch) {
            callback.onSuccess(mUserPool.getUser(userId));
        } else {
            mAccountManager.fetchUser(userId, callback);
        }
    }

    public void updateUserFollowStatus(long userId, boolean followed) {

        if (userId != mMyUser.getUserId()) {
            User user = mUserPool.getUser(userId);
            if (user != null) {
                user.setFollowed(followed);
            }
        }
    }

    public void isUserFollowed(final Long userId, final PiquedApiCallback<Boolean> callback) {
        if (mUserPool.getUser(userId) != null) {
            callback.onSuccess(mUserPool.getUser(userId).isFollowed());
        } else {
            mAccountManager.fetchUser(userId, new PiquedApiCallback<User>() {
                @Override
                public void onSuccess(User user) {

                    mUserPool.putUser(user);

                    callback.onSuccess(user.isFollowed());
                }

                @Override
                public void onFailure(PiquedErrorCode errorCode, String payload) {
                    PiquedLogger.e(TAG, "Failed to get user with id: " + userId, errorCode, payload);
                }
            });
        }
    }
}
