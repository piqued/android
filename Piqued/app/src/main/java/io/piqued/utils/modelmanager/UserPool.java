package io.piqued.utils.modelmanager;

import java.util.HashMap;

import io.piqued.database.user.User;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 7/20/17.
 * Piqued Inc.
 */

public class UserPool {

    private final HashMap<Long, User> mUserPool = new HashMap<>();

    public User getUser(long userId) {
        if (mUserPool.containsKey(userId)) {
            return mUserPool.get(userId);
        }

        return null;
    }

    public void putUser(User user) {
        mUserPool.put(user.getUserId(), user);
    }
}
