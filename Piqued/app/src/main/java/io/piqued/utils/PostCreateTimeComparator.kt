package io.piqued.utils

import io.piqued.database.post.CloudPost
import java.util.*

/**
 *
 * Created by Kenny M. Liou on 2/10/18.
 * Piqued Inc.
 *
 */
class PostCreateTimeComparator: Comparator<CloudPost> {
    override fun compare(left: CloudPost?, right: CloudPost?): Int {

        if (left == null && right == null) {
            return 0
        }

        if (left == null) {
            return 1
        }

        if (right == null) {
            return -1
        }

        return if (left.createdAt < right.createdAt) 1 else -1
    }

}