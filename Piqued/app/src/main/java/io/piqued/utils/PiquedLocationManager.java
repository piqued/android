package io.piqued.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import io.piqued.activities.PiquedActivity;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/30/16.
 * Piqued Inc.
 */

public class PiquedLocationManager implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final long LOCATION_UPDATE_INTERVAL = TimeUnit.SECONDS.toMillis(3);
    private final Context mContext;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private PiquedActivity mCurrentActivity;

    private final ArrayList<LocationCallback> mCallbacks = new ArrayList<>();

    public PiquedLocationManager(Context context) {
        mContext = context;

    }

    public void onPause() {
        mCallbacks.clear();
    }

    public void onResume(PiquedActivity activity) {

        mCurrentActivity = activity;

        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            buildGoogleApiClient();
            mGoogleApiClient.connect();
        }
    }

    private synchronized void buildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (mLocationRequest == null) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(LOCATION_UPDATE_INTERVAL);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        }

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            mCurrentActivity.requestLocationPermission();
        } else {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //TODO: do something
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //TODO: do something
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;

        broadcastLocationUpdate(mLastLocation);
    }

    public void getCurrentLocation(LocationCallback callback) {

        if (!mCallbacks.contains(callback)) {
            if (mLastLocation != null) {
                callback.onLocationUpdated(mLastLocation);

                if (!callback.onceOnly()) {
                    mCallbacks.add(callback);
                }
            } else {
                mCallbacks.add(callback);
            }
        }
    }

    private void broadcastLocationUpdate(Location location) {

        if (mCallbacks.size() > 0) {

            Iterator<LocationCallback> i = mCallbacks.iterator();

            ArrayList<LocationCallback> shouldRemove = new ArrayList<>();

            while(i.hasNext()) {
                LocationCallback callback = i.next();
                callback.onLocationUpdated(location);

                if (callback.onceOnly()) {
                    shouldRemove.add(callback);
                }
            }

            mCallbacks.removeAll(shouldRemove);

        }
    }
}
