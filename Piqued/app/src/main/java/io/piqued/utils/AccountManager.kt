package io.piqued.utils

import io.piqued.api.PiquedApiCallback
import io.piqued.api.piqued.PiquedApi
import io.piqued.cloud.model.piqued.ImageContainer
import io.piqued.cloud.model.piqued.PiquedToken
import io.piqued.cloud.model.piqued.UserToken
import io.piqued.cloud.util.CloudPreference
import io.piqued.database.user.User
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.model.imageupload.ImageUploadResult
import io.piqued.model.imageupload.PreSignObject

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 11/22/16.
 * Piqued Inc.
 */

class AccountManager(private val mApi: PiquedApi,
                     private val mPreferences: Preferences,
                     private val cloudPreference: CloudPreference) {

    companion object {

        private val TAG = AccountManager::class.java.simpleName
    }

    private val mRegisterDeviceTokenCallback = object : PiquedApiCallback<Boolean> {
        override fun onSuccess(success: Boolean) {
            PiquedLogger.i(TAG, "Notification subscription successful")
        }

        override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
            PiquedLogger.e(TAG, "Failed to register device token for some reason", errorCode, payload)
        }
    }

    enum class SupportedSite(val siteName: String) {
        FACEBOOK("facebook")
    }

    private fun refreshUserToken(userToken: UserToken, callback: LoginCallback) {

        mPreferences.userToken = userToken
        cloudPreference.setToken(PiquedToken(userToken.userId, userToken.site, userToken.token, userToken.secret))

        callback.onLoginSuccess()
        callback.onFetchingUserInfo()

        fetchUser(userToken.userId.toLong(), object : PiquedApiCallback<User> {
            override fun onSuccess(data: User) {
                registerDeviceForPushNotification(mRegisterDeviceTokenCallback)
                callback.onFetchingUserInfoSuccess(data)
            }

            override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                callback.onFetchingUserInfoFailed(errorCode, payload)
            }
        })
    }

    fun createAccount(email: String, password: String, userName: String, callback: LoginCallback) {
        mApi.createAccount(email, password, userName, object : PiquedApiCallback<UserToken> {
            override fun onSuccess(data: UserToken) {
                refreshUserToken(data, callback)
            }

            override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                callback.onLoginFailed(errorCode, payload)
            }

        })
    }

    fun loginWithOauth(site: SupportedSite, token: String, callback: LoginCallback) {

        mApi.loginWithSiteToken(site.siteName, token, object : PiquedApiCallback<UserToken> {
            override fun onSuccess(data: UserToken) {
                refreshUserToken(data, callback)
            }

            override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                callback.onLoginFailed(errorCode, payload)
            }
        })
    }

    fun login(email: String, password: String, callback: LoginCallback) {

        mApi.loginToUser(email, password, object : PiquedApiCallback<UserToken> {
            override fun onSuccess(data: UserToken) {
                refreshUserToken(data, callback)
            }

            override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                callback.onLoginFailed(errorCode, payload)
            }
        })
    }

    fun fetchUser(userId: Long, callback: PiquedApiCallback<User>) {
        mApi.fetchUserInfo(mPreferences.userToken, userId, callback)
    }

    fun updateUserData(userName: String?,
                       userEmail: String?,
                       userPassword: String?,
                       imageData: ImageContainer?,
                       callback: PiquedApiCallback<User>) {

        val userToken = mPreferences.userToken

        if (userToken != null) {
            if (imageData != null) {
                mApi.getPreSign(object : PiquedApiCallback<PreSignObject> {
                    override fun onSuccess(data: PreSignObject) {
                        mApi.uploadImageToPreSign(-1, data, imageData.imageUrl, imageData, null, object : PiquedApiCallback<ImageUploadResult> {
                            override fun onSuccess(uploadResult: ImageUploadResult) {
                                mApi.updateUserData(userName, userEmail, userPassword, uploadResult, userToken, callback)
                            }

                            override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                                PiquedLogger.e(TAG, "Failed to upload image data")
                                callback.onFailure(errorCode, payload)
                            }
                        })
                    }

                    override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                        PiquedLogger.e(TAG, "Failed to fetch presign object", errorCode, payload)
                    }
                })
            } else {
                mApi.updateUserData(userName, userEmail, userPassword, null, userToken, callback)
            }
        } else {
            callback.onFailure(PiquedErrorCode.USER_AUTH_FAILED, "User Token does not exist")
        }
    }

    fun registerDeviceForPushNotification(callback: PiquedApiCallback<Boolean>) {
        handlePushNotificationSubscription(true, callback)
    }

    fun unregisterDeviceForPushNotification(callback: PiquedApiCallback<Boolean>) {
        handlePushNotificationSubscription(false, callback)
    }

    private fun handlePushNotificationSubscription(subscribe: Boolean, callback: PiquedApiCallback<Boolean>) {
        val fcmToken = mPreferences.getFCMToken()
        println("Register with fcm token $fcmToken")
        val token = mPreferences.userToken
        if (token == null) {
            PiquedLogger.i(TAG, "User token is not set up, notification registration will be ignored")
            callback.onFailure(PiquedErrorCode.USER_LOGIN_REQUIRED, "User is not logged in when register for push notification")
            return
        }
        PushNotificationHelper.handlePushNotificationSubscription(subscribe, fcmToken, mApi, token, callback)
    }
}
