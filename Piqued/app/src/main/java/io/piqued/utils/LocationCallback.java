package io.piqued.utils;

import android.location.Location;

import androidx.annotation.NonNull;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/30/16.
 * Piqued Inc.
 */

public abstract class LocationCallback {

    public boolean onceOnly() {
        return false;
    }

    public abstract void onLocationUpdated(@NonNull Location location);
}
