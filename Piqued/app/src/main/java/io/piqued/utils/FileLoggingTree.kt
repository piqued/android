package io.piqued.utils

import android.content.Context
import android.util.Log
import io.piqued.database.util.PiquedLogger
import timber.log.Timber
import java.io.FileOutputStream


/**
 *
 * Created by Kenny M. Liou on 10/27/18.
 * Piqued Inc.
 *
 */
class FileLoggingTree(val context: Context): Timber.DebugTree() {

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {

        try {
            val file = PiquedLogger.getCurrentLogFile(context)

            val fileOutputStream = FileOutputStream(file, true)

            val stringBuilder = StringBuilder()

            stringBuilder.append(tag)
            stringBuilder.append(" ")
            stringBuilder.append(message)
            stringBuilder.append(" ")
            stringBuilder.append(t.toString())
            stringBuilder.append("\n")

            fileOutputStream.write(stringBuilder.toString().toByteArray())
            fileOutputStream.close()
        } catch (e: Exception) {
            Log.e(FileLoggingTree::class.java.simpleName, "Error while logging into file : " + e.toString())
        }
    }
}