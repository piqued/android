package io.piqued.utils.imageurl

import io.piqued.database.post.CloudPost
import io.piqued.utils.Constants.KEY_POST_IMAGE_SMALL

/**
 *
 * Created by Kenny M. Liou on 9/6/18.
 * Piqued Inc.
 *
 */
class CloudPostSmallImageUrl(post: CloudPost, baseUrl: String):
        PiquedImageUrl(post.getSmallImageUrl(), baseUrl)
{
    private val cacheKeyValue = KEY_POST_IMAGE_SMALL + post.postId.toString()

    override fun getCacheKey(): String {
        return cacheKeyValue
    }

    override fun toString(): String {
        return cacheKey
    }
}
