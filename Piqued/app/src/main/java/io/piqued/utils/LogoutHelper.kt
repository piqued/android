package io.piqued.utils

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.text.TextUtils
import com.bumptech.glide.Glide
import com.facebook.login.LoginManager
import io.piqued.activities.PiquedActivity
import io.piqued.activities.SplashActivity
import io.piqued.api.PiquedApiCallback
import io.piqued.cloud.repository.ActivityRepository
import io.piqued.cloud.repository.PostRepository
import io.piqued.cloud.repository.UserRepository
import io.piqued.cloud.util.CloudPreference
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger

/**
 *
 * Created by Kenny M. Liou on 11/18/17.
 * Piqued Inc.
 *
 */
class LogoutHelper {

    companion object {

        @JvmStatic
        fun onLogout(context: Context,
                     preference : Preferences,
                     accountManager: AccountManager,
                     cloudPreference: CloudPreference,
                     currentActivity: PiquedActivity,
                     postRepository: PostRepository,
                     userRepository: UserRepository,
                     activityRepository: ActivityRepository
        ) {
            val currentHostUrl = cloudPreference.getHostUrl()
            val currentShareUrl = cloudPreference.getShareHostUrl()

            if (!TextUtils.isEmpty(currentHostUrl)) {
                cloudPreference.setHostUrl(currentHostUrl)
            }

            if (!TextUtils.isEmpty(currentShareUrl)) {
                cloudPreference.setShareHostUrl(currentShareUrl)
            }

            accountManager.unregisterDeviceForPushNotification(object : PiquedApiCallback<Boolean> {
                override fun onSuccess(data: Boolean) {
                    PiquedLogger.i("LogoutHelper", "unregister push notif successful!")
                    wipeLocalData(context, currentActivity, postRepository, userRepository, activityRepository)
                    preference.reset()
                    cloudPreference.reset()
                    LoginManager.getInstance().logOut()
                }

                override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                    PiquedLogger.i("LogoutHelper", "unregister push notif FAILED!")
                    PiquedLogger.e("LogoutHelper", errorCode, payload)
                }
            })
        }

        private fun wipeLocalData(
                context: Context,
                currentActivity: PiquedActivity,
                postRepository: PostRepository,
                userRepository: UserRepository,
                activityRepository: ActivityRepository
        ) {
            AsyncTask.execute {

                postRepository.reset()
                userRepository.reset()
                activityRepository.reset()

                Glide.get(currentActivity.applicationContext).clearDiskCache()

                currentActivity.runOnUiThread {
                    Glide.get(currentActivity.applicationContext).clearMemory()

                    currentActivity.finish()

                    val intent = Intent(context, SplashActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    context.startActivity(intent)
                }
            }
        }
    }
}