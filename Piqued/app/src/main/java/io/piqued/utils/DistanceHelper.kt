package io.piqued.utils

import android.location.Location
import android.location.LocationManager
import androidx.annotation.DrawableRes
import com.google.android.gms.maps.model.LatLng
import io.piqued.R
import io.piqued.database.post.CloudPost
import java.text.DecimalFormat

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/20/17.
 * Piqued Inc.
 */

object DistanceHelper {

    private val MILE_TO_METER = 1609.34
    private val sDecimalFormat = DecimalFormat("#.00")

    fun getDistanceInInteger(currentLocation: Location, post: CloudPost): Double {
        val postLocation = Location("")
        postLocation.latitude = post.getLatLng().latitude
        postLocation.longitude = post.getLatLng().longitude

        return getDistanceInMiles(currentLocation, postLocation)
    }

    fun getDistanceInString(current: Location, destination: Location): String {

        val distanceInMiles = getDistanceInMiles(current, destination)

        return if (distanceInMiles < 0.01) {
            "0.0"
        } else {
            sDecimalFormat.format(distanceInMiles)
        }
    }

    private fun getDistanceInMiles(current: Location?, destination: Location?): Double {

        if (current == null || destination == null) return 0.0

        val distanceInMeters = current.distanceTo(destination)

        return distanceInMeters / MILE_TO_METER
    }

    fun getDistanceInMeters(disLeft: LatLng, disRight: LatLng): Float {

        val left = Location(LocationManager.GPS_PROVIDER)
        left.latitude = disLeft.latitude
        left.longitude = disLeft.longitude

        val right = Location(LocationManager.GPS_PROVIDER)
        right.latitude = disRight.latitude
        right.longitude = disRight.longitude

        return left.distanceTo(right)
    }

    fun getDistanceInString(current: Location, post: CloudPost): String {
        val right = Location(LocationManager.GPS_PROVIDER)
        right.latitude = post.getLatLng().latitude
        right.longitude = post.getLatLng().longitude

        return getDistanceInString(current, right)
    }

    @DrawableRes
    fun getDistanceIcon(current: Location, post: CloudPost): Int {
        val destination = Location(LocationManager.GPS_PROVIDER)
        destination.latitude = post.getLatLng().latitude
        destination.longitude = post.getLatLng().longitude

        val distanceInMiles = getDistanceInMiles(current, destination)

        return if (distanceInMiles < 1) {
            R.drawable.distance_1
        } else if (distanceInMiles < 5) {
            R.drawable.distance_2
        } else if (distanceInMiles < 100) {
            R.drawable.distance_3
        } else {
            R.drawable.distance_4
        }
    }
}
