package io.piqued.utils

import com.bumptech.glide.load.Options
import com.bumptech.glide.load.model.*
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader
import io.piqued.database.post.CloudPost
import java.io.InputStream

/**
 *
 * Created by Kenny M. Liou on 5/12/18.
 * Piqued Inc.
 *
 */
class PostLoader(concreteLoader: ModelLoader<GlideUrl, InputStream>, modelCache: ModelCache<CloudPost, GlideUrl>) : BaseGlideUrlLoader<CloudPost>(concreteLoader, modelCache) {

    override fun getUrl(model: CloudPost?, width: Int, height: Int, options: Options?): String {

        return model?.getImageUrl() ?: ""
    }

    override fun handles(model: CloudPost): Boolean {
        return true
    }

    companion object {
        val urlCache = ModelCache<CloudPost, GlideUrl>(300)

        @JvmField
        val factory = object: ModelLoaderFactory<CloudPost, InputStream> {
            override fun build(multiFactory: MultiModelLoaderFactory): ModelLoader<CloudPost, InputStream> {
                return PostLoader(multiFactory.build(GlideUrl::class.java, InputStream::class.java), urlCache)
            }

            override fun teardown() {
            }
        }
    }
}

