package io.piqued.utils

import androidx.lifecycle.LiveData
import io.piqued.cloud.repository.ActivityRepository
import io.piqued.cloud.util.Resource
import io.piqued.database.event.Event

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/21/17.
 * Piqued Inc.
 */
class EventManager(
        private val repository: ActivityRepository
) {
    fun getActivityLiveData(): LiveData<Resource<List<Event>>> {
        return repository.getActivityLiveData(Constants.DEFAULT_FETCH_PAGE_SIZE)
    }

    fun fetchActivities(page: Int) {
        repository.fetchActivities(page, Constants.DEFAULT_FETCH_PAGE_SIZE)
    }

    fun fetchLatestActivity() {
        repository.fetchActivities(1, 1)
    }

    fun getLastFetchSize(): Int {
        return repository.getLastFetchSize()
    }

    fun clearNotification() {
        repository.clearNotification()
    }
}