package io.piqued.utils.imageurl

import com.bumptech.glide.load.model.GlideUrl

/**
 *
 * Created by Kenny M. Liou on 9/15/18.
 * Piqued Inc.
 *
 */
open class PiquedImageUrl(imageUrl: String?, baseUrl: String): GlideUrl(PiquedImageUrlUtil(imageUrl, baseUrl).getURL())
