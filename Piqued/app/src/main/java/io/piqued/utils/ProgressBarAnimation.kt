package io.piqued.utils

import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.ProgressBar

/**
 *
 * Created by Kenny M. Liou on 11/11/18.
 * Piqued Inc.
 *
 */
class ProgressBarAnimation(
        private val progressBar: ProgressBar,
        private val newValue: Float
) : Animation() {

    private val oldValue = progressBar.progress

    override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
        super.applyTransformation(interpolatedTime, t)

        val value = oldValue + (newValue - oldValue) * interpolatedTime
        progressBar.progress = value.toInt()
    }
}