package io.piqued.utils

import android.content.Context
import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import io.piqued.R
import io.piqued.cloud.util.CloudPreference
import io.piqued.database.event.Actor
import io.piqued.database.event.Event
import io.piqued.database.post.CloudPost
import io.piqued.database.user.User
import io.piqued.module.GlideApp
import io.piqued.utils.imageurl.*
import io.piqued.views.home.map.utils.PostMarkerTarget

/**
 *
 * Created by Kenny M. Liou on 5/19/18.
 * Piqued Inc.
 *
 */
class ImageDownloadUtil(
        val baseUrl: String
) {

    companion object {

        private var instance: ImageDownloadUtil? = null

        @JvmStatic
        fun getDownloadUtilInstance(context: Context): ImageDownloadUtil {

            if (instance == null) {
                val preference = CloudPreference(context)

                instance = ImageDownloadUtil(preference.getHostUrl())
            }

            return instance!!
        }
    }

    fun loadActorImageWithCallback(context: Context, event: Event, callback: NotificationImageTarget) {
         GlideApp.with(context)
                 .asBitmap()
                 .load(ActorImageUrl(event.actor, baseUrl))
                 .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                 .circleCrop()
                 .into(callback)
    }

    fun loadActorImage(actor: Actor?, imageView: ImageView) {
        if (actor != null) {
            loadImage(ActorImageUrl(actor, baseUrl), imageView, R.drawable.blank_profile)
        } else {
            imageView.setImageResource(R.drawable.blank_profile)
        }
    }

    fun loadEventSmallPostImage(event: Event, imageView: ImageView) {
        if (event.eventTarget.smallImages.isNotEmpty()) {
            loadImage(EventSmallPostUrl(event, baseUrl), imageView, R.drawable.explorer_blank)
        } else {
            imageView.setImageResource(R.drawable.explorer_blank)
        }
    }

    fun loadPostImage(post: CloudPost, imageView: ImageView) {
        loadImage(CloudPostUrl(post, baseUrl), imageView, R.drawable.explorer_blank)
    }

    fun loadPostImage(postId: Long, imageIndex: Int, imageUrl: String, imageView: ImageView) {
        loadImage(MultiPostIdAndUrl(postId, imageIndex, imageUrl, baseUrl), imageView, R.drawable.explorer_blank)
    }

    fun loadPostImage(postId: Long, imageUrl: String, imageView: ImageView) {
        loadImage(PostIdAndUrl(postId, imageUrl, baseUrl), imageView, R.drawable.explorer_blank)
    }

    fun loadPostUserImage(post: CloudPost, imageView: ImageView) {
        if (post.getUserImageUrl().isNotEmpty()) {
            loadImage(CloudPostProfileUrl(post, baseUrl), imageView, R.drawable.blank_profile)
        } else {
            imageView.setImageResource(R.drawable.blank_profile)
        }
    }

    fun loadUserImage(user: User?, imageView: ImageView) {
        if (user != null && !user.profileImageUrl.isNullOrEmpty()) {
            loadImage(UserImageUrl(user, baseUrl), imageView, R.drawable.blank_profile)
        } else {
            imageView.setImageResource(R.drawable.blank_profile)
        }
    }

    private fun loadImage(o: Any, imageView: ImageView, @DrawableRes placeHolder: Int) {
        GlideApp.with(imageView.context)
                .load(o)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .centerCrop()
                .placeholder(placeHolder)
                .dontAnimate()
                .into(imageView)
    }

    fun loadMarkerImage(context: Context, post: CloudPost, markerTarget: PostMarkerTarget) {
        if (post.getSmallImageUrl().isNotEmpty()) {

            val options = RequestOptions()
                    .centerCrop()

            GlideApp.with(context)
                    .asBitmap()
                    .apply(options)
                    .load(CloudPostSmallImageUrl(post, baseUrl))
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(markerTarget)
        }
    }
}