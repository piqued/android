package io.piqued.utils;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.piqued.R;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/20/17.
 * Piqued Inc.
 */

public abstract class StringUtil {

    private static Calendar startingDate;

    public static SpannableString createStringWithTypeFaces(final Context context,
                                                            final String string1,
                                                            final String string2) {

        SpannableString ss = new SpannableString(string1 + string2);

        ss.setSpan(new TextAppearanceSpan(context, R.style.Piqued_TextView_Bookmark_Distance_Number), 0, string1.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ss.setSpan(new TextAppearanceSpan(context, R.style.Piqued_TextView_Bookmark_Distance_Text), string1.length(), string1.length() + string2.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

        return ss;
    }

    private static Calendar getStartingDate() {

        if (startingDate == null) {
            startingDate = Calendar.getInstance();
            startingDate.setTime(new Date(1));
        }

        return startingDate;
    }

    private static int getDifference(Calendar current, Calendar past, int field) {

        int diff = current.get(field) - past.get(field);

        if (diff <= 0) {
            return 0;
        } else {
            return diff;
        }
    }

    public static long getDifferencesInMilli(long timeStamp) {
        // create past, assuming timestamp is in second
        Calendar past = Calendar.getInstance();
        past.setTimeInMillis(timeStamp * 1000);

        // create now
        Calendar current = Calendar.getInstance();

        return current.getTimeInMillis() - past.getTimeInMillis();
    }

    public static String getTimeStringLong(Context context, long timeStamp) {

        final long diffInMilli = getDifferencesInMilli(timeStamp);

        long diffInDays = TimeUnit.MILLISECONDS.toDays(diffInMilli);

        if (diffInDays >= 365) {

            final int years = (int) (diffInDays / 365);

            if (years > 1) {
                return context.getString(R.string.years_ago_long, years);
            } else {
                return context.getString(R.string.year_ago_long, years);
            }
        }

        if (diffInDays >= 30) {

            final int months = (int) (diffInDays / 30);

            if (months > 1) {
                return context.getString(R.string.months_ago_long, months);
            } else {
                return context.getString(R.string.month_ago_long, months);
            }
        }

        if (diffInDays > 0) {

            if (diffInDays > 1) {
                return context.getString(R.string.days_ago_long, diffInDays);
            } else {
                return context.getString(R.string.day_ago_long, diffInDays);
            }
        }

        long diffInHours = TimeUnit.MILLISECONDS.toHours(diffInMilli);

        if (diffInHours > 0) {
            if (diffInHours > 1) {
                return context.getString(R.string.hours_ago_long, diffInHours);
            } else {
                return context.getString(R.string.hour_ago_long, diffInHours);
            }
        }

        long diffInMins = TimeUnit.MILLISECONDS.toMinutes(diffInMilli);
        if (diffInMins > 0) {
            if (diffInMins > 1) {
                return context.getString(R.string.minutes_ago_long, diffInMins);
            } else {
                return context.getString(R.string.minute_ago_long, diffInMins);
            }
        }

        long diffInSecs = TimeUnit.MILLISECONDS.toSeconds(diffInMilli);
        if (diffInSecs > 1 || diffInSecs < 1) {
            return context.getString(R.string.seconds_ago_long, diffInSecs);
        } else {
            return context.getString(R.string.second_ago_long, diffInSecs);
        }
    }

    public static String getTimeString(Context context, long timeStamp) {

        if (timeStamp < 1) return ""; // invalid input

        final long diffInMilli = getDifferencesInMilli(timeStamp);

        long diffInDays = TimeUnit.MILLISECONDS.toDays(diffInMilli);

        if (diffInDays >= 365) {
            return context.getString(R.string.years_ago, diffInDays / 365);
        }

        if (diffInDays >= 30) {
            return context.getString(R.string.months_ago, diffInDays / 30);
        }

        if (diffInDays > 0) {
            return context.getString(R.string.days_ago, diffInDays);
        }

        long diffInHours = TimeUnit.MILLISECONDS.toHours(diffInMilli);
        if (diffInHours > 0) {
            return context.getString(R.string.hours_ago, diffInHours);
        }

        long diffInMins = TimeUnit.MILLISECONDS.toMinutes(diffInMilli);
        if (diffInMins > 0) {
            return context.getString(R.string.minutes_ago, diffInMins);
        }

        long diffInSecs = TimeUnit.MILLISECONDS.toSeconds(diffInMilli);
        if (diffInSecs >= 0) {
            return context.getString(R.string.seconds_ago, diffInSecs);
        }

        throw new IllegalArgumentException("Illegal time stamp: " + timeStamp);
    }

    public static String getIntegerString(int value) {
        return String.format(Locale.getDefault(), "%d", value);
    }
}
