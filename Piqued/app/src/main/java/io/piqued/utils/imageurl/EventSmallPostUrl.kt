package io.piqued.utils.imageurl

import io.piqued.database.event.Event
import io.piqued.utils.Constants.KEY_EVENT_IMAGE_SMALL

/**
 *
 * Created by Kenny M. Liou on 9/3/18.
 * Piqued Inc.
 *
 */
class EventSmallPostUrl(event: Event, baseUrl: String):
        PiquedImageUrl(event.eventTarget.smallImages, baseUrl)
{
    private val cacheKeyValue = KEY_EVENT_IMAGE_SMALL + event.eventTarget.id.toString()

    override fun getCacheKey(): String {
        return cacheKeyValue
    }

    override fun toString(): String {
        return cacheKey
    }
}
