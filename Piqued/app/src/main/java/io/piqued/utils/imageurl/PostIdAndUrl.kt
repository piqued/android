package io.piqued.utils.imageurl

import io.piqued.utils.Constants.KEY_POST_IMAGE

/**
 *
 * Created by Kenny M. Liou on 5/19/18.
 * Piqued Inc.
 *
 */
class PostIdAndUrl(val postId: Long, postImageUrl: String, baseUrl: String):
        PiquedImageUrl(postImageUrl, baseUrl)
{
    private val cacheKeyValue = KEY_POST_IMAGE + postId.toString()

    override fun getCacheKey(): String {
        return cacheKeyValue
    }

    override fun toString(): String {
        return cacheKey
    }
}
