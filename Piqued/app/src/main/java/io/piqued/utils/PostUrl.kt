package io.piqued.utils

import com.bumptech.glide.load.model.GlideUrl
import io.piqued.database.post.CloudPost

/**
 *
 * Created by Kenny M. Liou on 5/19/18.
 * Piqued Inc.
 *
 */
class PostUrl(val post: CloudPost, baseUrl: String):
        GlideUrl(if (post.getImageUrl().startsWith("https")) post.getImageUrl() else baseUrl + post.getImageUrl()) {

    override fun getCacheKey(): String {
        return post.postId.toString()
    }

    override fun toString(): String {
        return cacheKey
    }
}