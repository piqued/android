package io.piqued.utils.postmanager

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.os.AsyncTask
import android.os.Bundle
import androidx.annotation.IntRange
import androidx.lifecycle.LiveData
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.maps.model.LatLng
import io.piqued.api.PiquedApiCallback
import io.piqued.api.PiquedGetVenueCallback
import io.piqued.api.piqued.PiquedApi
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.model.piqued.UserToken
import io.piqued.cloud.model.piqued.Venue
import io.piqued.cloud.profile.ProfileRepository
import io.piqued.cloud.repository.ActivityRepository
import io.piqued.cloud.repository.PostRepository
import io.piqued.cloud.util.PostsCallback
import io.piqued.cloud.util.RegionBounds
import io.piqued.cloud.util.Resource
import io.piqued.database.UserId
import io.piqued.database.bookmark.Bookmark
import io.piqued.database.post.CloudPost
import io.piqued.database.post.PostId
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedErrorCode.PERMISSION_DENIED
import io.piqued.database.util.PiquedErrorCode.POST_NOT_VALID
import io.piqued.model.Comment
import io.piqued.utils.Preferences
import io.piqued.utils.modelmanager.PiquedUserManager

/**
 * Created by kennymliou on 11/16/16.
 * Piqued Inc.
 */

class PostManager(private val context: Context,
                  private val userManager: PiquedUserManager,
                  private val piquedApi: PiquedApi,
                  private val mPreference: Preferences,
                  private val postRepository: PostRepository,
                  private val activityRepository: ActivityRepository,
                  private val profileRepository: ProfileRepository) {

    private val mCloudPostPool = CloudPostPool()
    private val mVenuePool = VenuePool()

    private val token: UserToken?
        get() = mPreference.userToken

    val lastModifiedTime: Long
        get() = mPreference.lastModifiedTime

    fun getCloudPost(bookmark: Bookmark): CloudPost? {
        return getCloudPost(bookmark.postId)
    }

    fun getCloudPost(postId: Long): CloudPost? {
        return mCloudPostPool.getPost(postId)
    }

    fun getCloudPost(postId: PostId): CloudPost? {
        return mCloudPostPool.getPost(postId.postId)
    }

    fun addCloudPostToMemoryCache(posts: List<CloudPost>) {
        mCloudPostPool.setPosts(posts)
    }

    fun fetchPost(postId: Long, callback: PiquedApiCallback<PostId>) {

        val result = getCloudPost(postId)

        if (result != null) {
            callback.onSuccess(PostId(result.postId))
        }

        piquedApi.getPostById(postId, token!!, object : PiquedApiCallback<CloudPost> {
            override fun onSuccess(post: CloudPost) {
                mCloudPostPool.setPost(post)

                callback.onSuccess(PostId(post.postId))
            }

            override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                callback.onFailure(errorCode, payload)
            }
        })

    }

    fun getPostAsync(postId: Long, callback: PiquedApiCallback<CloudPost>) {
        piquedApi.getPostById(postId, token, object : PiquedApiCallback<CloudPost> {
            override fun onSuccess(post: CloudPost) {
                mCloudPostPool.setPost(post)

                callback.onSuccess(post)
            }

            override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                callback.onFailure(errorCode, payload)
            }
        })
    }

    fun fetchMorePosts(location: Location, page: Int) {
        val realPage = if (page < 1) 1 else page

        postRepository.fetchPostsBasedOnLocationAndUpdateDB(realPage, location)
    }

    fun getPostsBasedOnLocation(location: Location, page: Int): LiveData<Resource<List<CloudPost>>> {

        val realPage = if (page < 1) 1 else page

        return postRepository.fetchPostsBasedOnLocation(realPage, location)
    }

    fun getPostsBasedOnLocationOnMap(outerBounds: RegionBounds, callback: PostsCallback) {

        postRepository.fetchPostsBasedOnMap(1, outerBounds, PostsCallback { currentPage, posts, isLast ->
            if (posts != null) {
                mCloudPostPool.setPosts(posts)
            }
            callback.onPosts(currentPage, posts, isLast)
        })
    }

    fun getBookmarkPostsAsLiveData() : LiveData<Resource<List<CloudPost>>> {

        return profileRepository.getBookmarkPosts()
    }

    fun getBookmarksAsLiveData() : LiveData<Resource<List<Bookmark>>> {
        return profileRepository.getBookmarks()
    }

    fun refreshBookmarks() {
        profileRepository.refreshBookmarks()
    }

    fun getVenuesFromPost(postId: Long, callback: PiquedGetVenueCallback?) {

        val venue = mVenuePool.getVenue(postId)

        if (venue != null) {
            callback?.onSuccess(postId, venue)
        } else {

            val post = getCloudPost(postId)

            if (post != null) {
                getVenuesFromPost(post, callback)
            } else {
                val cloudPost = getCloudPost(postId)

                if (cloudPost != null) {
                    getVenuesFromPost(cloudPost, callback)
                } else {
                    callback?.onFailure(null, null)
                }
            }
        }
    }

    private fun getVenuesFromPost(post: CloudPost, callback: PiquedGetVenueCallback?) {
        piquedApi.getVenuesFromPost(post, object : PiquedApiCallback<Venue> {
            override fun onSuccess(data: Venue?) {
                mVenuePool.setVenue(post.postId, data)
                callback?.onSuccess(post.postId, data)
            }

            override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                callback?.onFailure(errorCode, payload)
            }

        })
    }

    fun getPostComments(post: CloudPost, callback: PiquedApiCallback<List<Comment>>) {
        piquedApi.getComments(post, token, callback)
    }

    fun makeNewComment(post: CloudPost, message: String, callback: PiquedApiCallback<Comment>) {
        piquedApi.postComment(post, token!!, message, callback)
    }

    fun setLikePost(postId: Long, liked: Boolean, callback: PiquedApiCallback<Long>) {
        setPostReaction(postId, if (liked) CloudPost.Reaction.LIKE else CloudPost.Reaction.NONE, callback)
    }

    fun setPostReaction(postId: Long, reaction: CloudPost.Reaction, callback: PiquedApiCallback<Long>) {
        val post = getCloudPost(postId)

        if (post != null) {
            piquedApi.reactToPost(post, reaction.ordinal, token!!, object : PiquedApiCallback<Void?> {
                override fun onSuccess(data: Void?) {

                    mPreference.lastModifiedTime = System.currentTimeMillis()

                    val localPost = getCloudPost(postId)

                    if (localPost != null) {
                        localPost.myReaction = reaction.ordinal
                        localPost.setTotalLikes(localPost.getTotalLikes() + if (localPost.myReaction == CloudPost.Reaction.LIKE.ordinal) 1 else -1)
                    }

                    notifyPostUpdated(context, postId)

                    callback.onSuccess(postId)
                }

                override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                    callback.onFailure(errorCode, payload)
                }
            })
        } else {
            callback.onFailure(POST_NOT_VALID, null)
        }
    }

    fun bookmarkPost(postId: Long, bookmarked: Boolean, callback: PiquedCloudCallback<Long>) {

        profileRepository.bookmarkPost(postId, bookmarked, object : PiquedCloudCallback<Long> {
            override fun onSuccess(result: Long?) {

                val post = mCloudPostPool.getPost(postId)

                if (post != null) {
                    post.bookmarked = bookmarked

                    post.totalBookmarks += if (bookmarked) 1 else -1
                }

                callback.onSuccess(result)
            }

            override fun onFailure(error: Throwable?, message: String?) {
                callback.onFailure(error, message)
            }

        })
    }

    fun getUserPostHistory(userId: UserId, @IntRange(from = 2) page: Int) {

        postRepository.getUserPostHistoryWithPage(userId, page)
    }

    fun getUserPostHistoryAsLiveData(userId: UserId): LiveData<Resource<List<CloudPost>>> {
        return postRepository.getUserPostHistory(userId)
    }

    fun getAccountOwnerPostHistory(@IntRange(from = 2) page: Int) {
        postRepository.getAccountOwnerPostHistory(page)
    }

    fun getAccountOwnerPostHistoryAsLiveData(): LiveData<Resource<List<CloudPost>>> {
        val accountOwner = userManager.myUserRecord

        if (accountOwner != null) {
            return postRepository.getAccountOwnerPostHistory(UserId(accountOwner.userId))
        }

        throw IllegalStateException("User needs to login before getAccountOwnerPostHistoryAsLiveData is called")
    }

    fun searchVenueNearLatLng(latLng: LatLng, callback: PiquedApiCallback<List<Venue>>) {

        piquedApi.searchVenueNearLatLng(latLng, callback)
    }

    fun searchVenueWithQuery(query: String?, latLng: LatLng, callback: PiquedApiCallback<List<Venue>>) {
        piquedApi.searchVenueLatLngWithQuery(query, latLng, callback)
    }

    fun deletePost(post: CloudPost, callback: PiquedApiCallback<Void?>) {

        val token = mPreference.userToken

        if (token != null) {
            if (post.userId == token.userId.toLong()) {
                piquedApi.deletePost(post, token, object : PiquedApiCallback<Void?> {
                    override fun onSuccess(data: Void?) {

                        mPreference.lastModifiedTime = System.currentTimeMillis()

                        mCloudPostPool.deletePost(post) // delete from pool

                        AsyncTask.execute {
                            postRepository.deletePostFromLocal(post)
                            activityRepository.deletePost(post.postId)
                        }

                        callback.onSuccess(null)
                    }

                    override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                        callback.onFailure(errorCode, payload)
                    }
                })
            } else {
                callback.onFailure(PERMISSION_DENIED, "Post that does not belong to user cannot be deleted.")
            }
        }
    }

    fun followUser(userId: Long, followed: Boolean, callback: PiquedApiCallback<Long>) {

        val token = mPreference.userToken

        if (token != null) {
            piquedApi.followUser(userId, followed, token, object : PiquedApiCallback<Void?> {
                override fun onSuccess(data: Void?) {

                    mPreference.lastModifiedTime = System.currentTimeMillis()

                    userManager.updateUserFollowStatus(userId, followed)
                    PiquedUserManager.notifyUserFollowUpdated(context, userId, followed)
                    callback.onSuccess(userId)
                }

                override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                    callback.onFailure(errorCode, payload)
                }
            })
        }
    }

    fun followPostOwner(postId: Long, followed: Boolean, callback: PiquedApiCallback<Long>) {

        val token = mPreference.userToken

        if (token != null) {
            val post = getCloudPost(postId)

            if (post != null && post.userId != token.userId.toLong()) {
                piquedApi.followUser(post.userId, followed, token, object : PiquedApiCallback<Void?> {
                    override fun onSuccess(data: Void?) {

                        mPreference.lastModifiedTime = System.currentTimeMillis()

                        val localPost = getCloudPost(postId)

                        userManager.updateUserFollowStatus(localPost!!.userId, followed)

                        PiquedUserManager.notifyUserFollowUpdated(context, localPost.userId, followed)
                        callback.onSuccess(localPost.userId)
                    }

                    override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                        callback.onFailure(errorCode, payload)
                    }
                })
            }
        }
    }

    companion object {

        private val ACTION_POST_UPDATED = "post_updated"
        private val KEY_POST_ID = "post_id"

        fun registerPostUpdate(context: Context, receiver: BroadcastReceiver) {

            LocalBroadcastManager.getInstance(context).registerReceiver(receiver, IntentFilter(ACTION_POST_UPDATED))
        }

        fun isPostUpdatedBroadcast(intent: Intent): Boolean {
            return ACTION_POST_UPDATED == intent.action
        }

        fun getPostIdFromBroadcast(bundle: Bundle): Long {

            return bundle.getLong(KEY_POST_ID, -1)
        }

        fun notifyPostUpdated(context: Context, postId: Long) {

            val intent = Intent(ACTION_POST_UPDATED)
            intent.putExtra(KEY_POST_ID, postId)

            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }
    }
}
