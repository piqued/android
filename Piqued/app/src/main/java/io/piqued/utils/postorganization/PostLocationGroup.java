package io.piqued.utils.postorganization;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/23/17.
 * Piqued Inc.
 */

public class PostLocationGroup {

    private final PostLocationDetail mHead;
    private final ArrayList<PostLocationDetail> mGroup = new ArrayList<>();
    private final ArrayList<String> mCityList = new ArrayList<>();
    private final ArrayList<String> mStateList = new ArrayList<>();
    private final ArrayList<String> mCountryList = new ArrayList<>();
    private final SimpleDateFormat mDateFormat = new SimpleDateFormat("MMM dd, yyyy");
    private final SimpleDateFormat mMonthFormat = new SimpleDateFormat("MMM");
    private final SimpleDateFormat mDayFormat = new SimpleDateFormat("dd");
    private final SimpleDateFormat mYearFormat = new SimpleDateFormat("yyyy");
    private Date mStartDate;
    private Date mEndDate;

    public PostLocationGroup(PostLocationDetail head) {

        mHead = head;

        mStartDate = new Date(mHead.getCreatedAt());
        mEndDate = mStartDate;

        mCityList.add(mHead.getCity());
        mStateList.add(mHead.getState());
        mCountryList.add(mHead.getCountry());

        addToGroup(mHead);
    }

    public List<PostLocationDetail> getDetails() {
        return mGroup;
    }

    private void addToList(String value, List<String> list) {
        if (value != null && !value.isEmpty() && !list.contains(value)) {
            list.add(value);
        }
    }
    public void addToGroup(PostLocationDetail details) {
        mGroup.add(details);

        addToList(details.getCity(), mCityList);
        addToList(details.getState(), mStateList);
        addToList(details.getCountry(), mCountryList);

        mEndDate.setTime(details.getCreatedAt());
    }

    public boolean belongToGroup(PostLocationDetail details) {

        if (mGroup.size() > 1) {
            PostLocationDetail last = mGroup.get(mGroup.size() - 1);

            if (last.isSameDay(details)) {
                return true;
            } else if (mHead.isSameCity(details) && mHead.isSameMonth(details)) {
                return true;
            } else {
                return false;
            }
        } else {
            return mHead.isSameDay(details) || mHead.isSameCity(details);
        }
    }

    public String getLocationString(Context context) {

        if (mCityList.size() > 1) {
            return mCityList.get(0) + " - " + mCityList.get(mCityList.size() - 1);
        } else {

            Locale defaultLocal = Locale.getDefault();

            if (mCountryList.size() > 0 && !defaultLocal.getDisplayCountry().equals(mCountryList.get(0))) {
                return mCityList.get(0) + ", " + mCountryList.get(0);
            } else {
                if (mStateList.size() > 0) {
                    return mCityList.get(0) + ", " + mStateList.get(0);
                } else {
                    return mCityList.get(0);
                }
            }
        }
    }

    public String getDateString(Context context) {

        Calendar start = Calendar.getInstance();
        start.setTime(mStartDate);

        Calendar end = Calendar.getInstance();
        end.setTime(mEndDate);

        if (start.compareTo(end) == 0) {
            return mDateFormat.format(mStartDate);
        } else {
            String month = mMonthFormat.format(mStartDate);

            return month + " " + mDayFormat.format(mStartDate) + " - " + mDayFormat.format(mEndDate) + mYearFormat.format(mStartDate);
        }
    }
}
