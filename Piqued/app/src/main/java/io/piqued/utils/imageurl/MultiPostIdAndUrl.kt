package io.piqued.utils.imageurl

import io.piqued.utils.Constants

/**
 *
 * Created by Kenny M. Liou on 11/10/18.
 * Piqued Inc.
 *
 */
class MultiPostIdAndUrl(val postId: Long, val imageIndex: Int, postImageUrl: String, baseUrl: String):
        PiquedImageUrl(postImageUrl, baseUrl)
{
    private val cacheKeyValue = Constants.KEY_POST_IMAGE + postId.toString() + "_" + imageIndex

    override fun getCacheKey(): String {
        return cacheKeyValue
    }

    override fun toString(): String {
        return cacheKey
    }
}