package io.piqued.utils;

import androidx.annotation.NonNull;

import io.piqued.database.user.User;
import io.piqued.database.util.PiquedErrorCode;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/3/16.
 * Piqued Inc.
 */

public interface LoginCallback {

    void onLoginSuccess();
    void onLoginFailed(PiquedErrorCode errorCode, String payload);
    void onFetchingUserInfo();
    void onFetchingUserInfoSuccess(@NonNull User user);
    void onFetchingUserInfoFailed(PiquedErrorCode errorCode, String payload);
}
