package io.piqued.utils;

import android.animation.Animator;
import android.os.Looper;
import android.view.View;

import java.util.concurrent.TimeUnit;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/24/16.
 * Piqued Inc.
 */

public abstract class AnimationHelper {

    public static void fadeOut(final View view, long milliseconds) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            // main thread
            view.setAlpha(1f);
            view.animate()
                    .alpha(0)
                    .setDuration(milliseconds)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {
                            view.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });
        } else {
            throw new IllegalThreadStateException("Let's not do animation on non main thread");
        }
    }

    public static void fadeIn(View view) {
        fadeIn(view, TimeUnit.SECONDS.toMillis(2));
    }

    public static void fadeIn(View view, long milliseconds) {

        if (Looper.myLooper() == Looper.getMainLooper()) {
            // main thread
            view.setAlpha(0);
            view.setVisibility(View.VISIBLE);
            view.animate()
                    .alpha(1f)
                    .setDuration(milliseconds)
                    .setListener(null);
        } else {
            throw new IllegalThreadStateException("Let's not do animation on non main thread");
        }
    }
}
