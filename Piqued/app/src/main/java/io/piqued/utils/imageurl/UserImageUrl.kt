package io.piqued.utils.imageurl

import io.piqued.database.user.User
import io.piqued.utils.Constants.KEY_USER_IMAGE

/**
 *
 * Created by Kenny M. Liou on 6/2/18.
 * Piqued Inc.
 *
 */
// This method assume user has proper profileImageUrl
class UserImageUrl(user: User, baseUrl: String):
        PiquedImageUrl(user.profileImageUrl!!, baseUrl)
{
    private val cacheKeyValue = KEY_USER_IMAGE + user.userId.toString()

    override fun getCacheKey(): String {
        return cacheKeyValue
    }

    override fun toString(): String {
        return cacheKey
    }
}
