package io.piqued.utils;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 11/26/16.
 * Piqued Inc.
 */

public interface PiquedOnBackHandler {

    boolean onBackPressed();
}
