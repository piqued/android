package io.piqued.utils

import android.text.TextUtils

/**
 *
 * Created by Kenny M. Liou on 3/27/18.
 * Piqued Inc.
 *
 */
abstract class PasswordUtil {
    companion object {
        @JvmStatic
        fun isPasswordValid(password: String): Boolean {
            return !TextUtils.isEmpty(password) && password.length > 6
        }
    }
}