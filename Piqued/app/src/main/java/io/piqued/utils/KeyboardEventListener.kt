package io.piqued.utils

/**
 *
 * Created by Kenny M. Liou on 1/13/18.
 * Piqued Inc.
 *
 */
interface KeyboardEventListener {

    fun onKeyboardHidden(isHidden: Boolean, keyboardHeight: Int)
}