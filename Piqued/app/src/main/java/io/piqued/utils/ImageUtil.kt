package io.piqued.utils

import android.graphics.Bitmap
import android.media.ExifInterface
import android.net.Uri
import com.google.android.gms.maps.model.LatLng
import io.piqued.database.util.PiquedLogger
import java.io.ByteArrayOutputStream
import java.io.IOException

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 4/15/17.
 * Piqued Inc.
 */

object ImageUtil {

    private val TAG = ImageUtil::class.java.simpleName

    fun getImageLatLng(imageUri: Uri): LatLng? {

        try {
            val exif = ExifInterface(imageUri.path)
            val LATITUDE = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE)
            val LATITUDE_REF = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF)
            val LONGITUDE = exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE)
            val LONGITUDE_REF = exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF)

            val latitude: Double?
            val longitude: Double?

            if (LATITUDE != null
                    && LATITUDE_REF != null
                    && LONGITUDE != null
                    && LONGITUDE_REF != null) {
                if (LATITUDE_REF == "N") {
                    latitude = convertToDegree(LATITUDE)
                } else {
                    latitude = 0 - convertToDegree(LATITUDE)!!
                }

                if (LONGITUDE_REF == "E") {
                    longitude = convertToDegree(LONGITUDE)
                } else {
                    longitude = 0 - convertToDegree(LONGITUDE)!!
                }

                PiquedLogger.d(TAG, "Successfully read lat lng value from image. lat: " +
                        java.lang.Double.toString(latitude!!) +
                        ", lng: " +
                        java.lang.Double.toString(longitude!!) +
                        "\nImage Uri: " + imageUri)

                return LatLng(latitude, longitude)
            } else {
                PiquedLogger.d(TAG, "Image has no lat lng value from exif")
                return null
            }

        } catch (e: IOException) {
            PiquedLogger.e(TAG, "Failed to read exif from image: $imageUri")
            return null
        }

    }

    private fun convertToDegree(stringDMS: String): Double? {
        val result: Double?
        val DMS = stringDMS.split(",".toRegex(), 3).toTypedArray()

        val stringD = DMS[0].split("/".toRegex(), 2).toTypedArray()
        val D0 = java.lang.Double.valueOf(stringD[0])
        val D1 = java.lang.Double.valueOf(stringD[1])
        val FloatD = D0 / D1

        val stringM = DMS[1].split("/".toRegex(), 2).toTypedArray()
        val M0 = java.lang.Double.valueOf(stringM[0])
        val M1 = java.lang.Double.valueOf(stringM[1])
        val FloatM = M0 / M1

        val stringS = DMS[2].split("/".toRegex(), 2).toTypedArray()
        val S0 = java.lang.Double.valueOf(stringS[0])
        val S1 = java.lang.Double.valueOf(stringS[1])
        val FloatS = S0 / S1

        result = FloatD + FloatM / 60 + FloatS / 3600

        return result
    }

    fun imageToByteArrayBased64(imageSource: Bitmap): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        imageSource.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)

        return byteArrayOutputStream.toByteArray()
    }
}
