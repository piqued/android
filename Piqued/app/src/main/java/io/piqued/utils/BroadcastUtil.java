package io.piqued.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import static io.piqued.utils.Constants.ACTION_POST_DELETED;
import static io.piqued.utils.Constants.ACTION_SHOW_MINI_BASKET;
import static io.piqued.utils.Constants.KEY_DELETED_POST_ID;
import static io.piqued.utils.Constants.KEY_MINI_BASKET_POST_ID;

/**
 * Created by Kenny M. Liou on 8/11/17.
 * Piqued Inc.
 */

public abstract class BroadcastUtil {

    public static void registerReceiverWithMultipleAction(@NonNull Context context, BroadcastReceiver receiver, String... actions) {

        IntentFilter filter = new IntentFilter();

        for (String action : actions) {
            filter.addAction(action);
        }

        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, filter);
    }

    public static void registerMiniBasketState(@NonNull Context context, BroadcastReceiver receiver) {
        BroadcastUtil.registerReceiverWithMultipleAction(context, receiver, Constants.ACTION_SHOW_MINI_BASKET);
    }

    public static void unregisterMiniBasketState(@NonNull Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
    }

    public static void registerLocationPermissionBroadcast(@NonNull Context context, BroadcastReceiver receiver) {
        BroadcastUtil.registerReceiverWithMultipleAction(context, receiver, Constants.ACTION_LOCATION_PERMISSION_RESULT);
    }

    public static void unregisterLocationPermissionBroadcast(@NonNull Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
    }

    public static void registerPostDeleteBroadcast(@NonNull Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, new IntentFilter(ACTION_POST_DELETED));
    }

    public static void unregisterPostDeleteBroadcast(@NonNull Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
    }

    public static void sendPostDeleteBroadcast(@NonNull Context context, long postId) {

        Intent deleteIntent = new Intent(ACTION_POST_DELETED);
        deleteIntent.putExtra(KEY_DELETED_POST_ID, postId);

        LocalBroadcastManager.getInstance(context).sendBroadcast(deleteIntent);
    }

    public static long readDeletedPostIdFromIntent(@NonNull Intent intent) {

        return intent.getLongExtra(KEY_DELETED_POST_ID, -1);
    }

    public static void sendShowMiniBasketBroadcast(@NonNull Context context, long postId) {
        Intent miniBasketIntent = new Intent(ACTION_SHOW_MINI_BASKET);
        miniBasketIntent.putExtra(KEY_MINI_BASKET_POST_ID, postId);

        LocalBroadcastManager.getInstance(context).sendBroadcast(miniBasketIntent);
    }

    public static long readMiniBasketPostId(@NonNull Intent intent) {
        return intent.getLongExtra(KEY_MINI_BASKET_POST_ID, -1);
    }
}
