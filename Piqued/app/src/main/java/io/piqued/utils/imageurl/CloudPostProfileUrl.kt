package io.piqued.utils.imageurl

import io.piqued.database.post.CloudPost
import io.piqued.utils.Constants.KEY_USER_IMAGE

class CloudPostProfileUrl(post: CloudPost, baseUrl: String):
        PiquedImageUrl(post.getUserImageUrl(), baseUrl)
{
    private val cacheKeyValue = KEY_USER_IMAGE + post.userId

    override fun getCacheKey(): String {
        return cacheKeyValue
    }

    override fun toString(): String {
        return cacheKey
    }
}
