package io.piqued.utils;

import android.content.Context;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/20/17.
 * Piqued Inc.
 */

public class DividerItemDecoration extends androidx.recyclerview.widget.DividerItemDecoration {

    public DividerItemDecoration(Context context) {
        super(context, VERTICAL);
    }
}
