package io.piqued.utils;

import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.text.Spanned;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.piqued.database.post.CloudPost;
import io.piqued.database.post.PostId;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/27/17.
 * Piqued Inc.
 */

public abstract class Constants {

    // Common

    public static final int DEFAULT_SPAN_SIZE = 4;

    // Image CacheKey
    public static final String KEY_USER_IMAGE = "UserImage_";
    public static final String KEY_USER_IMAGE_SMALL = "UserImageSmall_";
    public static final String KEY_POST_IMAGE = "PostImage_";
    public static final String KEY_POST_IMAGE_SMALL = "PostImageSmall_";
    public static final String KEY_EVENT_IMAGE_SMALL = "EventImageSmall_";

    // Request Code
    public static final int EDIT_USER_REQUEST_CODE = 1000;
    public static final int EDIT_USER_SUCCESS = 0;
    public static final int EDIT_USER_FAILED = -1;


    // Time related
    public static final long NAVBAR_ANIMATION_DURATION = TimeUnit.MILLISECONDS.toMillis(200);
    public static final long CAMERA_ANIMATION_DURATION = TimeUnit.MILLISECONDS.toMillis(300);
    public static final long IMAGE_UPLOAD_PROGRESS_BAR_VISIBILITY_ANIMATION_DURATION = TimeUnit.MILLISECONDS.toMillis(200);

    // Permissions
    public static final int EXTERNAL_STORAGE_READ_REQUEST = 1000;
    public static final int FINE_LOCATION_PERMISSIONS_REQUEST = 1001;

    // Facebook usage disclaimer
    public static final Spanned disclaimer = Html.fromHtml("We'll never post to Facebook without permission. By using Piqued you agree to <a style=\"color:#F5A623;\" href=\"https://piqued.io/terms-of-service.html\"><b>Terms of Service</b></a>, and <a style=\"color:#F5A623;\" href=\"https://piqued.io/privacy-policy.html\"><b>Privacy</b></a>.");

    // General broadcast action
    public static final String ACTION_SEND_PERMISSION_RESULT = "action_permission_result";
    public static final String ACTION_LOCATION_PERMISSION_RESULT = "location_permission_result";
    public static final String ACTION_SHOW_MINI_BASKET = "show_mini_basket";

    // General broadcast key
    public static final String KEY_PERMISSION_REQUEST = "key_permission_result";

    // Query
    public static final String[] PhotoGalleryProjection = new String[] {
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
            MediaStore.Images.Media.BUCKET_ID,
            MediaStore.Images.Media.DATA
    };

    public static final String FIELD_CREDS = "creds";
    public static final String FIELD_TOKEN_ID = "token";
    public static final String FIELD_SECRET = "secret";

    // Create BasketInfo fields
    public static final String FIELD_POST = "post";
    public static final String FIELD_LOCATION_LAT = "lat";
    public static final String FIELD_LOCATION_LNG = "lng";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_DESCRIPTION = "description";
    public static final String FIELD_LOCATION_NAME = "location_name";
    public static final String FIELD_ADDRESS = "address";
    public static final String FIELD_FOURSQUARE_VENUE_ID = "foursquare_venue_id";
    public static final String FIELD_FOURSQUARE_CC = "foursquare_cc";
    public static final String FIELD_FOURSQUARE_CITY = "foursquare_city";
    public static final String FIELD_FOURSQUARE_STATE = "foursquare_state";
    public static final String FIELD_YELP_ID = "yelp_id";
    public static final String FIELD_CATEGORIES = "categories";
    public static final String FIELD_FOURSQUARE_CATEGORIES = "categories";
    public static final String FIELD_IMAGES = "images";
    public static final String FIELD_PRIMARY_COLOR = "primaryColor";

    public static final int PROFILE_IMAGE_SIZE = 480;

    public static final String ROOT = Environment.getExternalStorageDirectory().toString();

    // Actions
    public static final String ACTION_POST_DELETED = "post_deleted";
    public static final String ACTION_POST_UPLOADED = "post_uploaded";
    public static final String ACTION_POST_UPLOAD_FAILED = "post_upload_failed";

    public static final String KEY_DELETED_POST_ID = "deleted_post_id";
    public static final String KEY_MINI_BASKET_POST_ID = "mini_basket_post_id";

    // Follow user
    public static final String FIELD_FOLLOW = "follow";
    public static final String FIELD_FOLLOWED = "followed";

    // Edit User data
    public static final String FIELD_USER = "user";
    public static final String FIELD_USER_NAME = "name";
    public static final String FIELD_USER_EMAIL = "email";
    public static final String FIELD_USER_PASSWORD = "password";
    public static final String FIELD_USER_PASSWORD_CONFIRMATION = "password_confirmation";
    public static final String FIELD_AVATAR = "avatar";
    public static final String FIELD_AVATAR_ID = "id";
    public static final String FIELD_AVATAR_STORAGE = "storage";
    public static final String FIELD_AVATAR_METADATA = "metadata";


    public static final int DEFAULT_FETCH_PAGE_SIZE = 25;

    // Fragment State KEYS
    public static final Type POST_LIST_TYPE = new TypeToken<List<PostId>>() {}.getType();
    public static final Type MINI_BASKET_POST_TYPE = new TypeToken<List<CloudPost>>() {}.getType();
    public static final String KEY_POSTS_LOADED = "posts";
    public static final String KEY_EVENTS = "events";
    public static final String KEY_LAST_LOAD_TIME = "last_load_time";
    public static final String KEY_LAST_LOCATION = "last_location";
    public static final String KEY_MINI_BASKET_VISIBLE = "mini_basket_visible";
    public static final String KEY_MINI_BASKET_POSTS = "mini_basket_posts";


}
