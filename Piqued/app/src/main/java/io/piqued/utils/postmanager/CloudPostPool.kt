package io.piqued.utils.postmanager

import androidx.collection.LongSparseArray
import io.piqued.database.post.CloudPost
import io.piqued.database.post.PostId
import java.util.ArrayList

class CloudPostPool {

    private val mPostPool = LongSparseArray<CloudPost>()

    fun getPost(postId: Long): CloudPost? {

        return if (mPostPool.indexOfKey(postId) > -1) {
            mPostPool[postId]
        } else null

    }

    fun setPost(post: CloudPost) {

        if (mPostPool.indexOfKey(post.postId) > -1) {
            mPostPool[post.postId]?.mergeNewData(post)
        } else {
            mPostPool.append(post.postId, post)
        }
    }

    fun setPosts(posts: List<CloudPost>): List<PostId> {

        val postIds = ArrayList<PostId>()

        for (post in posts) {
            setPost(post)
            postIds.add(PostId(post.postId))
        }

        return postIds
    }

    fun deletePost(post: CloudPost) {

        if (mPostPool[post.postId] != null) {
            mPostPool.remove(post.postId)
        }
    }
}