package io.piqued.utils.imageurl

import io.piqued.database.event.Actor
import io.piqued.utils.Constants.KEY_USER_IMAGE_SMALL

/**
 *
 * Created by Kenny M. Liou on 5/19/18.
 * Piqued Inc.
 *
 */
class ActorImageUrl(actor: Actor, baseUrl: String):
        PiquedImageUrl(actor.profileImage, baseUrl)
{
    private val cacheKeyValue = KEY_USER_IMAGE_SMALL + actor.id.toString()

    override fun getCacheKey(): String {
        return cacheKeyValue
    }

    override fun toString(): String {
        return cacheKey
    }
}
