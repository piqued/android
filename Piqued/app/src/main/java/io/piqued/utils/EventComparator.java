package io.piqued.utils;

import java.util.Comparator;

import io.piqued.database.event.Event;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/21/17.
 * Piqued Inc.
 */

public class EventComparator implements Comparator<Event> {
    @Override
    public int compare(Event event, Event t1) {

        if (event == null && t1 == null) {
            return 0;
        }

        if (event == null) {
            return 1;
        }

        if (t1 == null) {
            return -1;
        }

        long difference = event.getCreatedAt() - t1.getCreatedAt();

        if (difference > 0) {
            return -1;
        } else if (difference < 0) {
            return 1;
        } else {
            return 0;
        }
    }
}
