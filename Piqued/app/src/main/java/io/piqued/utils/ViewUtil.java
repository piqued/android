package io.piqued.utils;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import io.piqued.R;
import io.piqued.activities.PiquedActivity;

/**
 * Created by Kenny M. Liou on 8/6/17.
 * Piqued Inc.
 */

public abstract class ViewUtil {

    public static void initializeSwipeRefreshLayout(@NonNull SwipeRefreshLayout layout, SwipeRefreshLayout.OnRefreshListener listener) {

        layout.setColorSchemeResources(
                R.color.refresh_color_1,
                R.color.refresh_color_2,
                R.color.refresh_color_3,
                R.color.refresh_color_4,
                R.color.refresh_color_5);

        if (listener != null) {
            layout.setOnRefreshListener(listener);
        }
    }

    public static void initializeSwipeRefreshLayoutWithOffset(
            @NonNull SwipeRefreshLayout layout,
            SwipeRefreshLayout.OnRefreshListener listener,
            int start,
            int end) {

        initializeSwipeRefreshLayout(layout, listener);

        layout.setProgressViewOffset(false, start, end);
    }

    public static void presentSnackbar(@NonNull PiquedActivity activity, @StringRes int message) {

        activity.showSnackBar(message);
    }
}
