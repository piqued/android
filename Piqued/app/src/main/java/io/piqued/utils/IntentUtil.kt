package io.piqued.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import io.piqued.R
import io.piqued.activities.FloatingActivity
import io.piqued.database.util.PiquedLogger
import java.io.File

/**
 *
 * Created by Kenny M. Liou on 10/27/18.
 * Piqued Inc.
 *
 */
class IntentUtil {

    companion object {
        fun sendLogToPiqued(context: Context) {

            PiquedLogger.prepareLogFiles(context, object : PiquedLogger.Callback {
                override fun onLogFileReady(file: File) {
                    val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.parse(context.getString(R.string.report_email_address)))
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Log")
                    emailIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.support_email_log_default_title))
                    emailIntent .putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file))

                    context.startActivity(emailIntent)
                }
            })
        }
        @JvmStatic
        fun runIfUserLoggedIn(context: Context, userLoggedIn: Boolean, execution: LoginRequiredExecution) {
            if (userLoggedIn) {
                execution.runIfLoggedIn()
            } else {
                showSignUpPopup(context)
            }
        }

        fun showSignUpPopup(context: Context) {
            context.startActivity(Intent(context, FloatingActivity::class.java))
        }
    }
}