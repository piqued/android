package io.piqued.module

import android.app.Application
import android.content.Context
import android.view.WindowManager
import dagger.Module
import dagger.Provides
import io.piqued.BuildConfig
import io.piqued.R
import io.piqued.api.piqued.PiquedApi
import io.piqued.api.piqued.PiquedApiImpl
import io.piqued.cloud.PiquedCloudImpl
import io.piqued.cloud.profile.ProfileRepository
import io.piqued.cloud.repository.*
import io.piqued.cloud.retrofit.AppExecutors
import io.piqued.cloud.util.CloudPreference
import io.piqued.database.event.EventDatabase
import io.piqued.database.post.PostDatabase
import io.piqued.database.user.UserDatabase
import io.piqued.model.MetricValues
import io.piqued.utils.AccountManager
import io.piqued.utils.EventManager
import io.piqued.utils.PiquedLocationManager
import io.piqued.utils.Preferences
import io.piqued.utils.modelmanager.PiquedUserManager
import io.piqued.utils.postmanager.PostManager
import javax.inject.Singleton

/**
 * Created by Kenny M. Liou on 11/16/16.
 * Piqued Inc.
 */

@Module
class AppModule(private val mApplication: Application) {

    @Provides
    @Singleton
    internal fun providePostManager(userManager: PiquedUserManager,
                                    piquedApi: PiquedApi,
                                    preferences: Preferences,
                                    postRepository: PostRepository,
                                    activityRepository: ActivityRepository,
                                    profileRepository: ProfileRepository): PostManager {
        return PostManager(mApplication.baseContext,
                userManager,
                piquedApi,
                preferences,
                postRepository,
                activityRepository,
                profileRepository)
    }

    @Provides
    @Singleton
    internal fun providePreferences(): Preferences {
        return Preferences(mApplication.baseContext)
    }

    @Provides
    @Singleton
    internal fun provideCloudPreferences(): CloudPreference {
        return CloudPreference(mApplication.baseContext)
    }

    @Provides
    @Singleton
    internal fun provideAccountManager(piquedApi: PiquedApi,
                                       preferences: Preferences,
                                       cloudPreference: CloudPreference): AccountManager {
        return AccountManager(piquedApi, preferences, cloudPreference)
    }

    @Provides
    @Singleton
    internal fun providePiquedLocationManager(): PiquedLocationManager {
        return PiquedLocationManager(mApplication.baseContext)
    }

    @Provides
    @Singleton
    internal fun provideEventManager(repository: ActivityRepository): EventManager {
        return EventManager(repository)
    }

    @Provides
    @Singleton
    internal fun provideUserManager(preferences: Preferences,
                                    accountManager: AccountManager): PiquedUserManager {
        return PiquedUserManager(preferences, accountManager)
    }

    @Provides
    @Singleton
    internal fun provideMetricValues(): MetricValues {
        return MetricValues(mApplication.baseContext, mApplication.applicationContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager)
    }

    @Provides
    @Singleton
    internal fun providePiquedAPI(cloudPreference: CloudPreference): PiquedApi {
        return PiquedApiImpl(mApplication.baseContext, cloudPreference)
    }

    @Provides
    @Singleton
    internal fun providePiquedCloud(cloudPreference: CloudPreference): PiquedCloudImpl {

        val hostUrl = cloudPreference.getHostUrl()

        if (hostUrl.isEmpty()) {
            cloudPreference.setHostUrl(BuildConfig.PIQUED_HOST_URL)
        }

        return PiquedCloudImpl(cloudPreference)
    }

    @Provides
    @Singleton
    internal fun providePostDatabase(): PostDatabase {
        return PostDatabase.getInstance(mApplication.baseContext)
    }

    @Provides
    @Singleton
    internal fun provideUserDatabase(): UserDatabase {
        return UserDatabase.getInstance(mApplication.baseContext)
    }

    @Provides
    @Singleton
    internal fun provideEventDatabase(): EventDatabase {
        return EventDatabase.getInstance(mApplication.baseContext)
    }

    @Provides
    @Singleton
    internal fun provideAppExecutor(): AppExecutors {
        return AppExecutors()
    }

    @Provides
    @Singleton
    internal fun provideTwitterRepository(piquedCloud: PiquedCloudImpl): TwitterRepository {
        return TwitterRepository(piquedCloud)
    }

    @Provides
    @Singleton
    internal fun provideYelpRepository(piquedCloud: PiquedCloudImpl): YelpRepository {
        return YelpRepository(piquedCloud, mApplication.getString(R.string.yelp_token))
    }

    @Provides
    @Singleton
    internal fun provideFourSquareRepository(piquedCloud: PiquedCloudImpl): FourSquareRepository {
        return FourSquareRepository(
                mApplication.getString(R.string.four_square_client_id),
                mApplication.getString(R.string.four_square_client_secret),
                piquedCloud)
    }

    @Provides
    @Singleton
    internal fun providePostRepository(piquedCloud: PiquedCloudImpl,
                                       postDatabase: PostDatabase,
                                       appExecutors: AppExecutors): PostRepository {

        return PostRepository(piquedCloud, postDatabase, appExecutors)
    }

    @Provides
    @Singleton
    internal fun provideProfileRepository(piquedCloud: PiquedCloudImpl,
                                          profileDatabase: PostDatabase,
                                          appExecutors: AppExecutors): ProfileRepository {
        return ProfileRepository(piquedCloud, profileDatabase, appExecutors)
    }

    @Provides
    @Singleton
    internal fun provideActivityRepository(piquedCloud: PiquedCloudImpl,
                                           eventDatabase: EventDatabase,
                                           appExecutors: AppExecutors): ActivityRepository {
        return ActivityRepository(piquedCloud, eventDatabase, appExecutors)
    }

    @Provides
    @Singleton
    internal fun provideUserRepository(piquedCloud: PiquedCloudImpl,
                                       userDatabase: UserDatabase,
                                       appExecutors: AppExecutors
    ): UserRepository {
        return UserRepository(piquedCloud, userDatabase, appExecutors)
    }
}
