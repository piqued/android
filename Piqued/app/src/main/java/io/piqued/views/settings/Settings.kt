package io.piqued.views.settings

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/11/17.
 * Piqued Inc.
 */

enum class Settings private constructor(val category: SettingsCategory, val viewType: SettingsItemViewType) {

    FOLLOWING(SettingsCategory.FOLLOWING, SettingsItemViewType.TITLE),
    FOLLOWING_FB(SettingsCategory.FOLLOWING, SettingsItemViewType.BUTTON_WITH_BOLD),
    FOLLOWING_CONTACTS(SettingsCategory.FOLLOWING, SettingsItemViewType.BUTTON_WITH_BOLD),
    SOCIAL_NETWORK(SettingsCategory.SOCIAL_NETWORK, SettingsItemViewType.TITLE),
    SOCIAL_NETWORK_FB(SettingsCategory.SOCIAL_NETWORK, SettingsItemViewType.SOCIAL_MEDIA),
    SOCIAL_NETWORK_IN(SettingsCategory.SOCIAL_NETWORK, SettingsItemViewType.SOCIAL_MEDIA),
    SOCIAL_NETWORK_TW(SettingsCategory.SOCIAL_NETWORK, SettingsItemViewType.SOCIAL_MEDIA),
    MY_ACCOUNT(SettingsCategory.MY_ACCOUNT, SettingsItemViewType.TITLE),
    MY_ACCOUNT_EDIT(SettingsCategory.MY_ACCOUNT, SettingsItemViewType.BUTTON),
    MY_ACCOUNT_PASSWORD(SettingsCategory.MY_ACCOUNT, SettingsItemViewType.BUTTON),
    MY_ACCOUNT_PRIVATE(SettingsCategory.MY_ACCOUNT, SettingsItemViewType.SWITCH),
    MY_ACCOUNT_DISTANCE(SettingsCategory.MY_ACCOUNT, SettingsItemViewType.DISTANCE),
    MY_ACCOUNT_MAP_P(SettingsCategory.MY_ACCOUNT, SettingsItemViewType.BUTTON),
    NOTIFICATION(SettingsCategory.NOTIFICATIONS, SettingsItemViewType.TITLE),
    NOTIFICATION_OVERALL(SettingsCategory.NOTIFICATIONS, SettingsItemViewType.SWITCH),
    NOTIFICATION_VIBRATE(SettingsCategory.NOTIFICATIONS, SettingsItemViewType.SWITCH),
    NOTIFICATION_SOUND(SettingsCategory.NOTIFICATIONS, SettingsItemViewType.SWITCH),
    NOTIFICATION_COMMENTS(SettingsCategory.NOTIFICATIONS, SettingsItemViewType.SWITCH),
    SECURITY(SettingsCategory.SECURITY, SettingsItemViewType.TITLE),
    SECURITY_TAS(SettingsCategory.SECURITY, SettingsItemViewType.BUTTON),
    SECURITY_REPORT(SettingsCategory.SECURITY, SettingsItemViewType.BUTTON),
    APP_VERSION(SettingsCategory.SECURITY, SettingsItemViewType.APP_VERSION),
    DEVELOPER(SettingsCategory.DEVELOPER, SettingsItemViewType.TITLE),
    DEVELOPER_SEND_LOG(SettingsCategory.DEVELOPER, SettingsItemViewType.BUTTON),
    DEVELOPER_READ_ALL_LOGS(SettingsCategory.DEVELOPER, SettingsItemViewType.BUTTON),
    LOGOUT(SettingsCategory.LOGOUT, SettingsItemViewType.LOGOUT);


    companion object {

        private val values = Settings.values()

        val count: Int
            get() = values.size

        fun fromInt(index: Int): Settings {
            if (index > -1 && index < values.size) {
                return values[index]
            }

            throw IllegalArgumentException("Illegal index value for enum " + Settings::class.java.name + ": " + index)
        }
    }
}
