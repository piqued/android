package io.piqued.views.profile

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/28/17.
 * Piqued Inc.
 */

enum class ProfileViewType {
    HEADER,
    STATS,
    SWITCH,
    POST_TITLE,
    SINGLE_POST,
    EMPTY_POST;

    companion object {

        private val values = ProfileViewType.values()

        fun fromInt(index: Int): ProfileViewType {
            if (index > -1 && index < values.size) {
                return values[index]
            }

            throw IllegalArgumentException("Illegal index value for enum ProfileViewType: $index")
        }
    }
}
