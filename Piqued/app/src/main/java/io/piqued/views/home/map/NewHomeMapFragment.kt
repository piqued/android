package io.piqued.views.home.map

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterManager
import io.piqued.R
import io.piqued.cloud.util.RegionBounds
import io.piqued.database.post.CloudPost
import io.piqued.database.post.PostId
import io.piqued.database.util.PiquedLogger
import io.piqued.utils.LocationCallback
import io.piqued.utils.PiquedLocationManager
import io.piqued.utils.PostCreateTimeComparator
import io.piqued.utils.ViewUtil
import io.piqued.utils.postmanager.PostManager
import io.piqued.viewmodel.HomeMapViewModel
import io.piqued.views.component.SearchBoxListener
import io.piqued.views.home.HomeFunctionProvider
import io.piqued.views.home.HomeSubFragment
import io.piqued.views.home.PostSortStyle
import io.piqued.views.home.map.utils.MapItem
import io.piqued.views.home.map.utils.PostPinRenderer
import kotlinx.android.synthetic.main.fragment_new_home_map.*
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.ArrayList

/**
 *
 * Created by Kenny M. Liou on 9/5/18.
 * Piqued Inc.
 *
 */
class NewHomeMapFragment(provider: HomeFunctionProvider): HomeSubFragment(provider) {

    companion object {
        private val TAG = NewHomeMapFragment::class.java.simpleName
        private const val DEFAULT_ZOOM_LEVEL = 15.0F

        fun newInstance(provider: HomeFunctionProvider): NewHomeMapFragment {
            return NewHomeMapFragment(provider)
        }
    }

    @Inject
    internal lateinit var postManager: PostManager
    @Inject
    internal lateinit var locationManager: PiquedLocationManager

    private lateinit var viewModel: HomeMapViewModel
    private lateinit var clusterManager: ClusterManager<MapItem>
    private val fetchDataHandler = Handler()
    private val postComparator = PostCreateTimeComparator()
    private var googleMap: GoogleMap? = null
    private var currentLocationObj: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        viewModel = ViewModelProviders.of(homeActivity!!).get(HomeMapViewModel::class.java)

        viewModel.getPosts().observe(this, Observer<LinkedHashSet<CloudPost>> { t ->
            if (t != null && t.isNotEmpty()) {

                viewModel.lastLoadTime = System.currentTimeMillis()

                updateCluster(t)
            }

            fetchDataHandler.postDelayed({
                newHomeMapRefreshLayout.isRefreshing = false
            }, 2000)
        })

        fetchCurrentLocation()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_new_home_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        newHomeMapSearchBox.setType(PostSortStyle.MAP_VIEW)
        newHomeMapSearchBox.setSearchBoxListener(searchBoxListener)
        newHomeMapSearchBox.setIsShrunk(true, false)

        newHomeMapRefreshLayout.isEnabled = false

        ViewUtil.initializeSwipeRefreshLayoutWithOffset(
                newHomeMapRefreshLayout,
                null,
                newHomeMapRefreshLayout.progressViewStartOffset + newHomeMapSearchBox.height,
                newHomeMapRefreshLayout.progressViewEndOffset + newHomeMapSearchBox.height)

        newHomeMapMap.onCreate(savedInstanceState)

        try {
            MapsInitializer.initialize(view.context)
        } catch (e: Exception) {
            PiquedLogger.d(TAG, "Failed to initialize Map")
        }

        newHomeMapMap.getMapAsync(mapReadyCallback)

        newHomeMapRootView.setOnApplyWindowInsetsListener { _, windowInsets ->
            if (windowInsets.systemWindowInsetBottom > 0) {
                val params = newHomeBottomNavigationPlaceHolder.layoutParams as ConstraintLayout.LayoutParams
                params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, windowInsets.systemWindowInsetBottom)
            }

            windowInsets.consumeSystemWindowInsets()
        }
    }

    private fun showPostInMiniBasketIfAny() {
        if (provider.miniBasketPostId > -1) {
            val postToShow = postManager.getCloudPost(provider.miniBasketPostId)
            provider.miniBasketPostId = -1

            if (postToShow != null) {
                showMiniBasketWithData(postToShow)

                moveCamera(postToShow.getLatLng().latitude, postToShow.getLatLng().longitude)
            }
        }
    }

    private fun setupMap(map: GoogleMap) {
        if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //TODO register permission broadcast
        } else {
            map.setOnCameraMoveStartedListener { reason ->
                if (REASON_GESTURE == reason) {
                    setPostPreviewVisible(false, true)
                }
            }
            map.isMyLocationEnabled = true
            map.setPadding(0, getMapTopPadding(), 0, 0)
            map.setOnCameraIdleListener(clusterManager)
            map.setOnMarkerClickListener(clusterManager)
            map.setOnCameraMoveListener(cameraMoveListener)
            map.uiSettings.isMapToolbarEnabled = false
            map.setOnMapClickListener(mapClickListener)

            if (viewModel.cameraPosition == null) {
                if (currentLocationObj == null) {
                    fetchCurrentLocation()
                } else {
                    moveCamera(currentLocationObj, DEFAULT_ZOOM_LEVEL, false)
                }
            } else {
                moveCamera(viewModel.cameraPosition!!)
            }
        }
    }

    private fun getMapTopPadding(): Int {
        return piquedActivity.statusBarHeight + newHomeMapSearchBox.height + Math.round(resources.getDimension(R.dimen.space_20))
    }

    private fun fetchData() {

        val mapObj = googleMap
        if (mapObj != null && view != null) {
            newHomeMapRefreshLayout.isRefreshing = true

            val bounds = mapObj.projection.visibleRegion.latLngBounds

            viewModel.loadPostBasedOnLocation(postManager, RegionBounds.createRegionBound(bounds.northeast, bounds.southwest))
        }
    }

    private fun updateCluster(hashSet: LinkedHashSet<CloudPost>?) {

        if (hashSet != null && hashSet.isNotEmpty()) {

            for (post: CloudPost in hashSet) {
                clusterManager.addItem(MapItem(post))
            }

            clusterManager.cluster()
        }
    }

    private fun showMiniBasketWithData(post: CloudPost) {
        showMiniBasketWithData(listOf(post))
    }

    private fun showMiniBasketWithData(posts: List<CloudPost>) {
        if (posts.isNotEmpty()) {

            val modifiableList = ArrayList<CloudPost>(posts)
            Collections.sort(modifiableList, postComparator)

            setPostPreviewData(getPostIds(posts))
            setPostPreviewVisible(true, true)

        } else {
            setPostPreviewVisible(false, true)
        }
    }

    private fun fetchCurrentLocation() {
        locationManager.getCurrentLocation(object : LocationCallback() {
            override fun onLocationUpdated(location: Location) {
                currentLocationObj = location

                if (googleMap != null) {
                    moveCamera(currentLocationObj, DEFAULT_ZOOM_LEVEL, false)
                }
            }

            override fun onceOnly(): Boolean {
                return true
            }
        })
    }

    private val searchBoxListener = SearchBoxListener {
        setPostPreviewVisible(false, false)
        switchToView(PostSortStyle.LIST_VIEW)
    }

    override fun setPostPreviewVisible(visible: Boolean, animate: Boolean) {
        super.setPostPreviewVisible(visible, animate)

        viewModel.miniBasketVisible = visible

        if (visible) {
            viewModel.miniBasketList.clear()

            viewModel.miniBasketList.addAll(getPostIds(provider.miniBasketList))
        }
    }

    /**
     * Life Cycle
     */

    override fun onStart() {
        super.onStart()

        newHomeMapMap.onStart()
    }

    override fun onStop() {

        newHomeMapMap.onStop()

        super.onStop()
    }

    override fun onResume() {
        super.onResume()

        newHomeMapMap.onResume()

        newHomeMapSearchBox.setIsShrunk(true, false)

        if (googleMap != null && viewModel.cameraPosition != null) {
            moveCamera(viewModel.cameraPosition!!)

            fetchData()
        }

        if (viewModel.lastLoadTime > -1 && System.currentTimeMillis() - viewModel.lastLoadTime > TimeUnit.MINUTES.toMillis(5)) {
            viewModel.validateAllPosts(postManager);
        }

        if (viewModel.miniBasketVisible && viewModel.miniBasketList.isNotEmpty()) {

            setPostPreviewData(viewModel.miniBasketList)

            setPostPreviewVisible(visible = true, animate = false)
        }

        showPostInMiniBasketIfAny()
    }

    override fun onPause() {
        newHomeMapMap.onPause()

        fetchDataHandler.removeCallbacksAndMessages(null)

        if (googleMap != null) {
            viewModel.cameraPosition = googleMap!!.cameraPosition
        }

        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()

        if (newHomeMapMap != null) {
            newHomeMapMap.onDestroy()
        }
    }

    override fun onLowMemory() {
        if (newHomeMapMap != null) {
            newHomeMapMap.onLowMemory()
        }
        super.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (newHomeMapMap != null) {
            newHomeMapMap.onSaveInstanceState(outState)
        }
    }

    /**
     * Map helpers
     */

    private fun moveCamera(latitude: Double, longitude: Double) {
        val mapObj = googleMap

        if (mapObj != null) {
            moveCamera(latitude, longitude, mapObj.cameraPosition.zoom, true)
        } else {
            val newLoc = Location("")
            newLoc.latitude = latitude
            newLoc.longitude = longitude
            currentLocationObj = newLoc
        }
    }

    private fun moveCamera(location: Location?, zoomLevel: Float, animate: Boolean) {
        if (location != null) {
            moveCamera(location.latitude, location.longitude, zoomLevel, animate)
        }
    }

    private fun moveCamera(latitude: Double, longitude: Double, zoomLevel: Float, animate: Boolean) {

        moveCamera(CameraPosition.Builder()
                .target(LatLng(latitude, longitude))
                .zoom(zoomLevel)
                .build(),
                animate)
    }

    private fun moveCamera(newCameraPosition: CameraPosition) {
        moveCamera(newCameraPosition, false)
    }

    private fun moveCamera(newCameraPosition: CameraPosition, animate: Boolean) {
        val googleMapView = googleMap
        if (googleMapView != null) {
            if (animate) {
                googleMapView.animateCamera(CameraUpdateFactory.newCameraPosition(newCameraPosition))
            } else {
                googleMapView.moveCamera(CameraUpdateFactory.newCameraPosition(newCameraPosition))
            }

            viewModel.cameraPosition = newCameraPosition
        }
    }

    private val mapReadyCallback = OnMapReadyCallback { mapObj ->
        val currentContext = context
        if (isUIActive && currentContext != null) {
            googleMap = mapObj

            clusterManager = ClusterManager(currentContext, googleMap)
            clusterManager.renderer = PostPinRenderer(currentContext, mapObj, clusterManager)
            clusterManager.setOnClusterClickListener(clusterCluckListener)
            clusterManager.setOnClusterItemClickListener(clusterItemClickListener)

            setupMap(mapObj)
        }
    }

    private val cameraMoveListener = GoogleMap.OnCameraMoveListener {
        val mapObj = googleMap

        if (mapObj != null) {
            fetchDataHandler.removeCallbacksAndMessages(null)
            fetchDataHandler.postDelayed({
                fetchData()
            }, 300)
        }
    }

    private val mapClickListener = GoogleMap.OnMapClickListener {
        setPostPreviewVisible(false, true)
    }

    private val clusterCluckListener = ClusterManager.OnClusterClickListener<MapItem> { cluster ->
        if (cluster.size > 0) {

            val posts = ArrayList<CloudPost>()

            for (item: MapItem in cluster.items) {
                posts.add(item.post)
            }

            moveCamera(cluster.position.latitude, cluster.position.longitude)

            showMiniBasketWithData(posts)

            true
        } else {
            false
        }
    }

    private val clusterItemClickListener = ClusterManager.OnClusterItemClickListener<MapItem> { clusterItem ->

        moveCamera(clusterItem.position.latitude, clusterItem.position.longitude)

        showMiniBasketWithData(clusterItem.post)

        true
    }

    private fun getPostIds(posts: List<CloudPost>) : ArrayList<PostId> {
        val result = ArrayList<PostId>()

        for (post in posts) {
            result.add(PostId(post.postId))
        }

        return result
    }
}