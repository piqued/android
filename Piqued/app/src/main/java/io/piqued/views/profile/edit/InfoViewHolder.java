package io.piqued.views.profile.edit;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import io.piqued.database.user.User;
import io.piqued.model.EditUserData;

/**
 * Created by Kenny M. Liou on 9/30/17.
 * Piqued Inc.
 */

abstract public class InfoViewHolder extends RecyclerView.ViewHolder {

    InfoViewHolder(View itemView) {
        super(itemView);
    }

    protected Context getContext() {
        return itemView.getContext();
    }

    abstract void onBind(InfoType type, @NonNull User user, @NonNull EditUserData editUserData);
}
