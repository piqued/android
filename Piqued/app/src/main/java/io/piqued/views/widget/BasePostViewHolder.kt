package io.piqued.views.widget

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Outline
import android.location.Location
import android.text.TextUtils
import android.view.MotionEvent
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.kingfisher.easyviewindicator.RecyclerViewIndicator
import io.piqued.R
import io.piqued.api.PiquedApiCallback
import io.piqued.api.PiquedGetVenueCallback
import io.piqued.basket.BasketActionHandler
import io.piqued.basket.BasketImageAdapter
import io.piqued.cloud.model.piqued.Venue
import io.piqued.database.post.CloudPost
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.utils.*
import io.piqued.utils.customview.CheckableImageButton
import io.piqued.utils.modelmanager.PiquedUserManager
import io.piqued.utils.postmanager.PostManager

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 7/22/17.
 * Piqued Inc.
 */

@SuppressLint("ClickableViewAccessibility")
abstract class BasePostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    protected var handler: BasketActionHandler? = null
        private set
    private var cloudPost: CloudPost? = null

    private val context: Context
        get() = itemView.context

    private val mOnTouchDownCheck = View.OnTouchListener { view, motionEvent ->
        var result = false
        if (MotionEvent.ACTION_DOWN == motionEvent.action && handler != null) {
            if (!handler!!.isUserLoggedIn) {
                IntentUtil.showSignUpPopup(view.context)
                result = true
            }
        }
        result
    }
    private val mOnLikedClicked = CheckableImageButton.OnCheckedChangeListener { _, isChecked ->
        if (cloudPost != null && isChecked != cloudPost!!.liked()) {
            if (isChecked) {
                animationView.playAnimation()
            }

            if (cloudPost != null) {
                handler!!.onLikePost(cloudPost!!.postId, isChecked)
            }
        }
    }

    private val mGetVenueCallback = object : PiquedGetVenueCallback {
        override fun onSuccess(postId: Long, data: Venue) {
            if (cloudPost?.postId == postId) {
                venueNameView.text = data.venueName
            }
        }

        override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
            // no venue info :(
            PiquedLogger.e(TAG, errorCode, payload)
        }
    }

    private val mOnBookmarkedClickListener = CheckableImageButton.OnCheckedChangeListener { _, bookmarked ->
        if (cloudPost != null && bookmarked != cloudPost!!.bookmarked) {
            IntentUtil.runIfUserLoggedIn(context, handler!!.isUserLoggedIn, object : LoginRequiredExecution {
                override fun runIfLoggedIn() {
                    handler!!.onBookMarkPost(cloudPost!!.postId, bookmarked)
                }
            })
        }
    }

    private val mOnFollowedClickListener = CompoundButton.OnCheckedChangeListener { _, followed ->
        if (handler != null && cloudPost != null) {
            handler!!.onFollowUser(cloudPost!!.postId, followed)
        }
    }

    private val mOnOpenPost = View.OnClickListener {
        if (handler != null && cloudPost != null) {
            handler!!.onOpenPost(cloudPost!!.postId)
        }
    }

    private val mFollowBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            if (PiquedUserManager.isFollowUpdateBroadcast(intent)) {
                val args = intent.extras

                updateFollowedButton(PiquedUserManager.getUserIdFromBroadcast(args!!), PiquedUserManager.getIsFollowedFromBroadcast(args))
            }
        }
    }

    private val mLikeBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            if (PostManager.isPostUpdatedBroadcast(intent)) {
                val args = intent.extras

                val postId = PostManager.getPostIdFromBroadcast(args!!)

                cloudPost?.mergeNewData(handler?.getPost(postId))

                updateLikeButton(postId)
                updateBookmarkButton(postId)
            }
        }
    }

    private val mOnOpenUserProfile = View.OnClickListener {v ->
        if (handler != null) {
            handler!!.openPostOwnerProfile(v.context, cloudPost!!.postId)
        }
    }

    private val mOnOpenMapWithPost = View.OnClickListener {
        if (handler != null) {
            handler!!.openMapWithPost(cloudPost!!.postId)
        }
    }

    protected open val mapButton: ImageButton?
        get() = null

    protected abstract val userImage: ImageView
    protected abstract val postRecyclerView: RecyclerView
    protected abstract val postImageCount: TextView
    protected abstract val postImageIndicator: RecyclerViewIndicator
    protected abstract val distanceImage: ImageView
    protected abstract val infoBubbleView: View
    protected abstract val postTime: TextView
    protected abstract val userNameView: TextView
    protected abstract val venueNameView: TextView
    protected abstract val descriptionView: TextView
    protected abstract val distanceTextView: TextView
    protected abstract val likeButton: CheckableImageButton
    protected abstract val bookmarkButton: CheckableImageButton
    protected abstract val followButton: ToggleButton
    protected abstract val youButton: Button
    protected abstract val animationView: LottieAnimationView

    private val basketImageAdapter = BasketImageAdapter()
    private val snapHelper = PagerSnapHelper()

    private val mOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            when(newState) {
                RecyclerView.SCROLL_STATE_IDLE -> {
                    val position = (recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()

                    postImageCount.text = recyclerView.context.getString(R.string.post_image_index, (position + 1), basketImageAdapter.itemCount)
                }
            }
        }
    }

    protected fun initializeView() {

        val cornerRadius = context.resources.getDimensionPixelSize(R.dimen.space_08).toFloat()

        postRecyclerView.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                outline?.setRoundRect(0, 0, view!!.width, (view.height + cornerRadius).toInt(), cornerRadius)
            }
        }
        postRecyclerView.clipToOutline = true

        postRecyclerView.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        postRecyclerView.adapter = basketImageAdapter
        postRecyclerView.addOnScrollListener(mOnScrollListener)
        snapHelper.attachToRecyclerView(postRecyclerView)

        basketImageAdapter.onClickListener = mOnOpenPost

        infoBubbleView.setOnClickListener(mOnOpenPost)

        bookmarkButton.setOnCheckedChangeListener(mOnBookmarkedClickListener)
        followButton.setOnCheckedChangeListener(mOnFollowedClickListener)
        likeButton.setOnTouchListener(mOnTouchDownCheck)
        bookmarkButton.setOnTouchListener(mOnTouchDownCheck)
        followButton.setOnTouchListener(mOnTouchDownCheck)
        PiquedUserManager.registerFollowUpdate(context, mFollowBroadcastReceiver)
        PostManager.registerPostUpdate(context, mLikeBroadcastReceiver)

        userImage.setOnClickListener(mOnOpenUserProfile)
        userNameView.setOnClickListener(mOnOpenUserProfile)

        mapButton?.setOnClickListener(mOnOpenMapWithPost)
    }

    fun setPost(userManager: PiquedUserManager,
                handler: BasketActionHandler,
                location: Location,
                post: CloudPost) {

        this.handler = handler

        cloudPost = post

        if (post.images.size > 1) {
            postImageCount.visibility = View.VISIBLE
            postImageIndicator.visibility = View.VISIBLE
            postImageIndicator.setRecyclerView(postRecyclerView)
            postImageCount.text = itemView.context.getString(R.string.post_image_index, 1, post.images.size)
        } else {
            postImageCount.visibility = View.GONE
            postImageIndicator.visibility = View.GONE
        }

        basketImageAdapter.updateImaged(post)
        basketImageAdapter.notifyDataSetChanged()

        if (!TextUtils.isEmpty(cloudPost!!.getUserImageUrl())) {

            ImageDownloadUtil.getDownloadUtilInstance(itemView.context).loadPostUserImage(cloudPost!!, userImage)
        } else {
            userImage.setImageResource(R.drawable.blank_profile)
        }

        populateUserInfo(location)

        distanceImage.setImageResource(DistanceHelper.getDistanceIcon(location, cloudPost!!))
        postTime.text = context.getString(R.string.explore_post_time, StringUtil.getTimeString(context, post.createdAt))

        updateLikeButton(cloudPost!!.postId)

        updateBookmarkButton(cloudPost!!.postId)

        val userId = post.userId

        if (userManager.myUserRecord != null) {
            if (userId != userManager.myUserRecord!!.userId) {

                youButton.visibility = View.GONE
                followButton.visibility = View.VISIBLE

                userManager.isUserFollowed(post.userId, object : PiquedApiCallback<Boolean> {
                    override fun onSuccess(followed: Boolean?) {
                        updateFollowedButton(userId, followed!!)
                    }

                    override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                        PiquedLogger.e(TAG, "Failed to fetch user info with id: $userId", errorCode, payload)
                    }
                })
            } else {
                youButton.visibility = View.VISIBLE
                followButton.visibility = View.INVISIBLE
                followButton.setOnClickListener(null)
                followButton.isChecked = false
            }
        }
    }

    private fun updateFollowedButton(userId: Long?, followed: Boolean) {
        if (cloudPost != null && userId == cloudPost!!.userId) {
            followButton.setOnCheckedChangeListener(null)
            followButton.isChecked = followed
            followButton.setOnCheckedChangeListener(mOnFollowedClickListener)
        }
    }

    private fun updateLikeButton(postId: Long) {
        if (cloudPost != null && postId == cloudPost!!.postId) {

            likeButton.setOnCheckedChangeListener(null)
            likeButton.isChecked = cloudPost!!.liked()
            likeButton.setOnCheckedChangeListener(mOnLikedClicked)
        }
    }

    private fun updateBookmarkButton(postId: Long) {

        if (cloudPost != null && postId == cloudPost!!.postId) {

            bookmarkButton.setOnCheckedChangeListener(null)
            bookmarkButton.isChecked = cloudPost!!.bookmarked
            bookmarkButton.setOnCheckedChangeListener(mOnBookmarkedClickListener)
        }
    }

    @Synchronized
    private fun populateUserInfo(location: Location) {

        if (cloudPost != null) {
            userNameView.text = cloudPost!!.userName
            userNameView.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent))

            descriptionView.text = cloudPost!!.description
            descriptionView.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent))

            distanceTextView.text = context.getString(R.string.explore_distance,
                    DistanceHelper.getDistanceInString(location,
                            cloudPost!!))

            updateBookmarkButton(cloudPost!!.postId)

            venueNameView.text = cloudPost!!.locationName

            if (TextUtils.isEmpty(cloudPost!!.locationName)) {
                handler!!.getVenueInfo(cloudPost!!.postId, mGetVenueCallback)
            }
        }
    }

    companion object {

        private val TAG = BasePostViewHolder::class.java.simpleName
    }
}
