package io.piqued.views.settings

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import io.piqued.BuildConfig
import io.piqued.PiquedFragment
import io.piqued.R
import io.piqued.activities.EditProfileActivity
import io.piqued.cloud.repository.ActivityRepository
import io.piqued.cloud.repository.PostRepository
import io.piqued.cloud.repository.UserRepository
import io.piqued.cloud.util.CloudPreference
import io.piqued.logviewer.LogViewerActivity
import io.piqued.utils.*
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.toolbar.view.*
import javax.inject.Inject

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/4/17.
 * Piqued Inc.
 */

class SettingsFragment : PiquedFragment(), SettingsDelegate {

    @Inject
    internal lateinit var mPreferences: Preferences
    @Inject
    internal lateinit var cloudPreference: CloudPreference
    @Inject
    internal lateinit var postRepository: PostRepository
    @Inject
    internal lateinit var userRepository: UserRepository
    @Inject
    internal lateinit var activityRepository: ActivityRepository
    @Inject
    internal lateinit var accountManager: AccountManager

    override fun getTagName(): String {
        return TAG
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        application.getComponent().inject(this)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.adapter = SettingsAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(view.context)
    }

    override fun onResume() {
        super.onResume()

        updateActionBar(settingsToolbar.toolbar, R.string.activity_settings_title)
    }

    private fun sendLogToPiqued() {
        val currentContext = context

        if (currentContext != null) {
            IntentUtil.sendLogToPiqued(currentContext)
        }
    }

    override fun onButtonPressed(type: Settings) {
        when (type) {
            Settings.DEVELOPER_SEND_LOG -> {
                sendLogToPiqued()
            }
            Settings.DEVELOPER_READ_ALL_LOGS -> {
                startActivity(LogViewerActivity::class.java)
            }
            Settings.MY_ACCOUNT_EDIT -> {
                EditProfileActivity.editMyProfile(this)
            }
            Settings.SECURITY_TAS -> {
                UIHelper.openUrlInBrowser(piquedActivity, BuildConfig.PIQUED_TERMS_OF_SERVICE_URL)
            }
            else -> return
        }
    }

    override fun onSwitchClicked(type: Settings) {
        when (type) {
            Settings.NOTIFICATION_OVERALL -> {
                openNotificationSettings()
            }
            else -> return
        }
    }

    private fun openNotificationSettings() {
        val intent = Intent()
        intent.action = "android.settings.APP_NOTIFICATION_SETTINGS"

        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            intent.putExtra("android.provider.extra.APP_PACKAGE", piquedActivity.packageName)
        } else if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            intent.putExtra("app_package", piquedActivity.packageName)
            intent.putExtra("app_uid", piquedActivity.applicationInfo.uid)
        } else {
            intent.action = android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            intent.addCategory(Intent.CATEGORY_DEFAULT)
            intent.data = Uri.parse("package:" + piquedActivity.packageName)
        }

        startActivity(intent)
    }

    override fun onLogout() {

        LogoutHelper.onLogout(context!!,
                mPreferences,
                accountManager,
                cloudPreference,
                piquedActivity,
                postRepository,
                userRepository,
                activityRepository)
    }

    companion object {

        private val TAG = SettingsFragment::class.java.simpleName

        fun newInstance(): SettingsFragment {

            return SettingsFragment()
        }
    }
}
