package io.piqued.views.activities

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.R
import io.piqued.database.event.Event
import io.piqued.database.event.EventType
import io.piqued.utils.ImageDownloadUtil
import io.piqued.utils.StringUtil
import kotlinx.android.synthetic.main.your_activity_recycler_view.view.*

/**
 *
 * Created by Kenny M. Liou on 9/3/18.
 * Piqued Inc.
 *
 */

class YourActivityViewHolder(
        parent: ViewGroup,
        val handler: Handler)
    : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.your_activity_recycler_view, parent, false)
) {

    interface Handler {
        fun openProfile(userId: Long)
        fun openPost(postId: Long)
        fun followUser(userId: Long, followed: Boolean)
        fun isFollowingUser(userId: Long): Boolean
    }

    companion object {
        private val TAG = YourActivityViewHolder::class.java.simpleName
    }

    private var event: Event? = null
    private var settingFollowingState = false

    init {
        itemView.user_image.setOnClickListener {
            handler.openProfile(event!!.actor.id)
        }
        itemView.user_name.setOnClickListener {
            handler.openProfile(event!!.actor.id)
        }
        itemView.activity_wrap.setOnClickListener {
            handler.openPost(event!!.eventTarget.id)
        }
        itemView.follow_button.setOnCheckedChangeListener { _, follow ->
            val currentEvent = event
            if (currentEvent!!.type == EventType.Relationship && !settingFollowingState) {
                handler.followUser(currentEvent.actor.id, follow)
            }
        }
    }

    fun setEvent(newEvent: Event) {
        //if same event then we ignore
        if (event == newEvent) return

        itemView.post_image.visibility = View.GONE
        itemView.follow_button.visibility = View.GONE

        event = newEvent

        itemView.post_image.setImageResource(R.drawable.explorer_blank)
        itemView.user_image.setImageResource(R.drawable.blank_profile)
        itemView.user_name.text = newEvent.actor.name
        itemView.date_text.text = StringUtil.getTimeString(itemView.context, newEvent.createdAt)

        val actor = newEvent.actor

        if (newEvent.type == EventType.Relationship) {
            itemView.post_image.visibility = View.GONE
            itemView.follow_button.visibility = View.VISIBLE
            settingFollowingState = true
            itemView.follow_button.isChecked = handler.isFollowingUser(actor.id)
            settingFollowingState = false
        } else {
            itemView.post_image.visibility = View.VISIBLE
            itemView.follow_button.visibility = View.GONE
        }

        ImageDownloadUtil.getDownloadUtilInstance(itemView.context)
                .loadEventSmallPostImage(newEvent, itemView.post_image)

        if (!actor.profileImage.isNullOrEmpty()) {
            ImageDownloadUtil.getDownloadUtilInstance(itemView.context)
                    .loadActorImage(actor, itemView.user_image)
        }

        when (event?.type) {

            EventType.Bookmark -> {
                itemView.message.setText(R.string.activity_bookmark)
            }
            EventType.Comment -> {
                itemView.message.text = event?.message
            }
            EventType.Reaction -> {
                itemView.message.setText(R.string.activity_reaction)
            }
            EventType.Relationship -> {
                itemView.message.setText(R.string.activity_followed)
            }
        }
    }
}