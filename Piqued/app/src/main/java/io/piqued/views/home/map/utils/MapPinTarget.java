package io.piqued.views.home.map.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;

import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;

import io.piqued.R;

/**
 * Created by Kenny M. Liou on 9/16/17.
 * Piqued Inc.
 */

public abstract class MapPinTarget implements Target<Drawable> {

    private final int mWidth;
    private final int mHeight;

    MapPinTarget(Context context) {
        mWidth = (int) context.getResources().getDimension(R.dimen.map_pin_size_normal);
        mHeight = (int) context.getResources().getDimension(R.dimen.map_pin_size_normal);
    }

    @Override
    public void onLoadStarted(@Nullable Drawable placeholder) {

    }

    @Override
    public void onLoadFailed(@Nullable Drawable errorDrawable) {

    }

    @Override
    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
        onImageReady(resource);
    }

    @Override
    public void onLoadCleared(@Nullable Drawable placeholder) {

    }

    @Override
    public void getSize(SizeReadyCallback cb) {

        cb.onSizeReady(mWidth, mHeight);
    }

    @Override
    public void removeCallback(SizeReadyCallback cb) {

    }

    @Override
    public void setRequest(@Nullable Request request) {

    }

    @Nullable
    @Override
    public Request getRequest() {
        return null;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    abstract void onImageReady(Drawable resource);
}
