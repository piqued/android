package io.piqued.views.profile

import android.content.Context
import io.piqued.database.post.PostId
import io.piqued.database.user.User
import io.piqued.views.home.PostSortStyle

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/28/17.
 * Piqued Inc.
 */
interface ProfileDataProvider {
    val user: User?
    val userImage: String?
    val sharesCount: Int
    val followersCount: Int
    val followingCount: Int
    val likeCount: Int
    val commentCount: Int
    val bookmarkCount: Int
    val profileType: ProfileType
    val isOwner: Boolean
    val isUserReady: Boolean
    val isFollowed: Boolean
    val isUserLoggedIn: Boolean
    fun onSwitchToView(listView: PostSortStyle?)
    fun onEditButtonClicked()
    fun setUserFollowed(follow: Boolean)
    fun displayItem(totalCountSoFar: Int, itemIndex: Int)
    fun onOpenBasket(postId: PostId)
    fun getUserName(c: Context): String
    fun signUpOrLogin(c: Context)
}