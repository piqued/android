package io.piqued.views.bookmark

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import io.piqued.R
import io.piqued.database.post.CloudPost
import io.piqued.utils.PiquedLocationManager
import kotlinx.android.synthetic.main.view_holder_bookmark.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/20/17.
 * Piqued Inc.
 */

class BookmarkViewHolder(parent: ViewGroup,
                         private val mViewHolderListener: BookmarkViewHolderListener?)
    : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_bookmark, parent, false)
) {

    private val mAdapter: BookmarkPagerAdapter
    private var mPost: CloudPost? = null

    private val mListener = object : BookmarkPagerListener {
        override fun onBookmarkClicked() {
            mViewHolderListener!!.onBookmarkRemove(mPost, this@BookmarkViewHolder)
            itemView.view_pager.currentItem = 1
        }

        override fun onUndoClicked() {
            itemView.view_pager.currentItem = 0
            mViewHolderListener!!.onBookmarkRemoveUndo()
            itemView.view_pager.setSwipeEnable(true)
        }

        override fun onOpenPost(postId: Long) {
            mViewHolderListener!!.onOpenPost(mPost)
        }

        override fun isUiActive(): Boolean {
            return mViewHolderListener != null && mViewHolderListener.isUiActive
        }

        override fun getParentContext(): Context {
            return mViewHolderListener!!.parentContext
        }
    }

    private val mOnPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }

        override fun onPageSelected(position: Int) {
            if (position == 1) {
                mViewHolderListener!!.onBookmarkRemove(mPost, this@BookmarkViewHolder)
                itemView.view_pager.setSwipeEnable(false)
            }
        }

        override fun onPageScrollStateChanged(state: Int) {

        }
    }

    init {
        mAdapter = BookmarkPagerAdapter(parent.context, mListener)

        itemView.view_pager.adapter = mAdapter
        itemView.view_pager.currentItem = 0
        itemView.view_pager.addOnPageChangeListener(mOnPageChangeListener)
    }

    fun setPost(context: Context, cloudPost: CloudPost, locationManager: PiquedLocationManager) {

        if (mPost == null || mPost!!.postId != cloudPost.postId) {

            mPost = cloudPost
            mAdapter.onBind(context, mPost, locationManager)
            mAdapter.notifyDataSetChanged()
            itemView.view_pager.visibility = View.VISIBLE
            itemView.view_pager.currentItem = 0
            itemView.view_pager.setSwipeEnable(true)
        }
    }

    fun onRemoved() {
        itemView.view_pager.visibility = View.GONE
    }
}
