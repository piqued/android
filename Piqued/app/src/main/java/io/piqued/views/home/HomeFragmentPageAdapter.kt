package io.piqued.views.home

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import io.piqued.views.home.list.HomeListFragment
import io.piqued.views.home.map.NewHomeMapFragment

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/30/16.
 * Piqued Inc.
 */

class HomeFragmentPageAdapter(val provider: HomeFunctionProvider, fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return PostSortStyle.getItemCount()
    }

    override fun createFragment(position: Int): Fragment {
        val type = PostSortStyle.fromInt(position)

        val subFragment: HomeSubFragment

        subFragment = when (type) {
            PostSortStyle.LIST_VIEW -> {
                HomeListFragment.newInstance(provider)
            }
            PostSortStyle.MAP_VIEW -> {
                NewHomeMapFragment.newInstance(provider)
            }
            else -> {
                throw IllegalArgumentException("Unexpected position: $position")
            }
        }

        return subFragment
    }


}
