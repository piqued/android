package io.piqued.views.home.map;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.piqued.R;
import io.piqued.activities.HomeActivity;
import io.piqued.cloud.util.RegionBounds;
import io.piqued.database.post.CloudPost;
import io.piqued.database.post.PostId;
import io.piqued.database.util.PiquedLogger;
import io.piqued.databinding.FragmentHomeMapBinding;
import io.piqued.utils.BroadcastUtil;
import io.piqued.utils.Constants;
import io.piqued.utils.LocationCallback;
import io.piqued.utils.PiquedLocationManager;
import io.piqued.utils.PostCreateTimeComparator;
import io.piqued.utils.ViewUtil;
import io.piqued.utils.postmanager.PostManager;
import io.piqued.views.component.SearchBoxListener;
import io.piqued.views.home.HomeFunctionProvider;
import io.piqued.views.home.HomeSubFragment;
import io.piqued.views.home.PostSortStyle;
import io.piqued.views.home.map.utils.MapItem;
import io.piqued.views.home.map.utils.MapPinRenderer;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/30/16.
 * Piqued Inc.
 */

public class HomeMapFragment extends HomeSubFragment {

    private static final String TAG = HomeMapFragment.class.getSimpleName();
    private static final int DEFAULT_ZOOM_LEVEL = 15;

    public static HomeMapFragment newInstance(HomeFunctionProvider provider) {

        return new HomeMapFragment(provider);
    }

    @Override
    public String getTagName() {
        return TAG;
    }

    @Inject
    PiquedLocationManager mLocationManager;
    @Inject
    PostManager mPostManager;

    private final PostCreateTimeComparator mPostComparator = new PostCreateTimeComparator();

    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private FragmentHomeMapBinding mBinding;
    private ClusterManager<MapItem> mClusterManager;
    private List<PostId> mPostLoaded = new ArrayList<>();
    private List<PostId> mPostInMap = new ArrayList<>();
    private List<CloudPost> mMiniBasketList = new ArrayList<>();
    private Handler mHandler = new Handler();
    private LatLng mLastNorthEast;
    private LatLng mLastSouthWest;
    private CameraPosition mCameraPosition;
    private long mLastLoadTime = -1;
    private Bundle localSavedInstanceState = null;
    private boolean mMiniBasketVisible = false;

    public HomeMapFragment(HomeFunctionProvider provider) {
        super(provider);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setUserVisibleHint(false);

        getApplication().getComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mBinding = FragmentHomeMapBinding.inflate(inflater, container, false);

        if (savedInstanceState != null) {
            restoreInstance(savedInstanceState);
        } else if (localSavedInstanceState != null) {
            restoreInstance(localSavedInstanceState);
        }


        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapView = mBinding.homeMapView;
        mMapView.onCreate(savedInstanceState);

        try {
            MapsInitializer.initialize(view.getContext());
        } catch (Exception e) {
            PiquedLogger.d(getTagName(), "Failed to initialize Map");
        }

        mMapView.getMapAsync(mMapReadyCallback);

        mBinding.homeMapSearchBox.setType(PostSortStyle.MAP_VIEW);
        mBinding.homeMapSearchBox.setSearchBoxListener(mListener);

        mBinding.refreshLayout.setEnabled(false);

        ViewUtil.initializeSwipeRefreshLayoutWithOffset(
                mBinding.refreshLayout,
                null,
                mBinding.refreshLayout.getProgressViewStartOffset() + mBinding.homeMapSearchBox.getHeight(),
                mBinding.refreshLayout.getProgressViewEndOffset() + mBinding.homeMapSearchBox.getHeight()
        );
    }

    private void restoreInstance(Bundle savedInstanceState) {

        if (savedInstanceState != null) {

            mCameraPosition = savedInstanceState.getParcelable(Constants.KEY_LAST_LOCATION);

            mLastLoadTime = savedInstanceState.getLong(Constants.KEY_LAST_LOAD_TIME, -1);

            mMiniBasketVisible = savedInstanceState.getBoolean(Constants.KEY_MINI_BASKET_VISIBLE);

            String postsInJsonString = savedInstanceState.getString(Constants.KEY_POSTS_LOADED);

            if (!TextUtils.isEmpty(postsInJsonString)) {

                Gson gson = new GsonBuilder().create();

                List<PostId> posts = gson.fromJson(postsInJsonString, Constants.POST_LIST_TYPE);

                for (PostId postId : posts) {
                    if (!mPostLoaded.contains(postId)) {
                        mPostLoaded.add(postId);
                    }
                }
            }

            String miniBasketJson = savedInstanceState.getString(Constants.KEY_MINI_BASKET_POSTS);
            if (!TextUtils.isEmpty(miniBasketJson)) {
                Gson gson = new GsonBuilder().create();

                List<CloudPost> posts = gson.fromJson(miniBasketJson, Constants.MINI_BASKET_POST_TYPE);

                mMiniBasketList.clear();

                for (CloudPost post : posts) {
                    if (!mMiniBasketList.contains(post)) {
                        mMiniBasketList.add(post);
                    }
                }
            }
        }
    }

    private final OnMapReadyCallback mMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {

            if (isUIActive() && getContext() != null) {
                mGoogleMap = googleMap;

                mGoogleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
                    @Override
                    public void onCameraMoveStarted(int i) {
                        if (REASON_GESTURE == i) {
                            setPostPreviewVisible(false, true);
                        }
                    }
                });

                mClusterManager = new ClusterManager<>(getContext(), mGoogleMap);

                mClusterManager.setRenderer(new MapPinRenderer(getContext(), mGoogleMap, mClusterManager));
                mClusterManager.setOnClusterClickListener(mOnClusterClickListener);
                mClusterManager.setOnClusterItemClickListener(mOnClusterItemClickListener);

                setupMap();

                if (mPostLoaded.size() > 0) {
                    validateAllPings();

                    for (PostId postId : mPostLoaded) {
                        addPin(postId);
                    }
                }

                if (getHomeActivity().haveLocationPermission()) {

                    if (mCameraPosition == null) {
                        mLocationManager.getCurrentLocation(mGetLocationCallback);
                    }

                    if (mGoogleMap != null && mCameraPosition != null) {
                        mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
                    }

                    if (mPostLoaded.size() > 0) {
                        refreshCluster();
                    }
                }
            }
        }
    };

    private int getMapTopPadding() {
        return getPiquedActivity().getStatusBarHeight() + mBinding.homeMapSearchBox.getHeight() + Math.round(getResources().getDimension(R.dimen.space_20));
    }

    private int getMapBottomPadding() {
        return getPiquedActivity().getNavigationBarHeight() + ((HomeActivity) getPiquedActivity()).getBottomNavigationHeight();
    }

    private final LocationCallback mGetLocationCallback = new LocationCallback() {

        @Override
        public boolean onceOnly() {
            return true;
        }

        @Override
        public void onLocationUpdated(Location location) {

            if (location != null) {
                setCamera(location);

                fetchData();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        fetchDataOnResume();

        BroadcastUtil.registerPostDeleteBroadcast(getContext(), mPostDeleteReceiver);
        BroadcastUtil.registerMiniBasketState(getContext(), mShowMiniBasketReceiver);

        showPostInMiniBasketIfAny();
    }

    @Override
    public void onPause() {
        BroadcastUtil.unregisterMiniBasketState(getContext(), mShowMiniBasketReceiver);
        BroadcastUtil.unregisterPostDeleteBroadcast(getContext(), mPostDeleteReceiver);

        if (mMapView != null) {
            mMapView.onPause();

            if (mGoogleMap != null) {
                mCameraPosition = mGoogleMap.getCameraPosition();
            }
        }

        mBinding.refreshLayout.setRefreshing(false);

        Bundle outState = new Bundle();

        onSaveInstanceState(outState);

        localSavedInstanceState = outState;

        super.onPause();
    }

    @Override
    public void onDestroy() {

        BroadcastUtil.unregisterLocationPermissionBroadcast(getContext(), mLocationPermissionReceiver);

        if (mMapView != null) {
            mMapView.onDestroy();
            mMapView = null;
        }

        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        if (mMapView != null) {
            mMapView.onLowMemory();
        }
        super.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mCameraPosition != null) {
            outState.putParcelable(Constants.KEY_LAST_LOCATION, mCameraPosition);
        }

        if (mLastLoadTime > -1) {
            outState.putLong(Constants.KEY_LAST_LOAD_TIME, mLastLoadTime);
        }

        if (mPostLoaded.size() > 0) {

            Gson gson = new GsonBuilder().create();

            outState.putString(Constants.KEY_POSTS_LOADED, gson.toJson(mPostLoaded, Constants.POST_LIST_TYPE));
        }

        if (mMiniBasketList.size() > 0) {

            Gson gson = new GsonBuilder().create();

            outState.putString(Constants.KEY_MINI_BASKET_POSTS, gson.toJson(mMiniBasketList, Constants.MINI_BASKET_POST_TYPE));
        }

        outState.putBoolean(Constants.KEY_MINI_BASKET_VISIBLE, mMiniBasketVisible);

        localSavedInstanceState = null;
    }

    private void showPostInMiniBasketIfAny() {
        if (getProvider().getMiniBasketPostId() > -1) {

            PostId postId = new PostId(getProvider().getMiniBasketPostId());
            CloudPost postToShow = mPostManager.getCloudPost(postId);
            getProvider().setMiniBasketPostId(-1);

            if (postToShow != null) {
                List<PostId> posts = new ArrayList<>();
                posts.add(postId);

                onShowMiniBasket(posts);

                panCamera(postToShow.getLatLng().latitude, postToShow.getLatLng().longitude);
            }
        }
    }

    private void setupMap() {

        // Show my location button
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            BroadcastUtil.registerLocationPermissionBroadcast(getContext(), mLocationPermissionReceiver);
        } else {
            if (mGoogleMap != null) {
                mGoogleMap.setMyLocationEnabled(true);
                mGoogleMap.setPadding(0, getMapTopPadding(), 0, getMapBottomPadding());
                mGoogleMap.setOnCameraIdleListener(mClusterManager);
                mGoogleMap.setOnMarkerClickListener(mClusterManager);
                mGoogleMap.setOnCameraMoveListener(mCameraMoveListener);
                mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
                mGoogleMap.setOnMapClickListener(mOnMapClickListener);
            }
        }
    }

    private void fetchDataOnResume() {

        if (!getHomeActivity().haveLocationPermission() && !getHomeActivity().locationPermissionDenied()) {
            BroadcastUtil.registerLocationPermissionBroadcast(getContext(), mLocationPermissionReceiver);
            if (!getHomeActivity().locationPermissionDenied()) {
                getHomeActivity().requestForLocationPermission();
            }
        } else {
            if (mMapView != null) {
                mMapView.onResume();
            }

            if (mBinding != null) {
                mBinding.homeMapSearchBox.setIsShrunk(true, false);
            }

            if (mGoogleMap != null && mCameraPosition != null) {
                mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
            }

            if (mLastLoadTime > -1 && System.currentTimeMillis() - mLastLoadTime > TimeUnit.MINUTES.toMillis(5)) {
                validateAllPings();
            }
        }
    }

    private void setCamera(Location location) {
        moveCamera(location.getLatitude(), location.getLongitude(), DEFAULT_ZOOM_LEVEL, false);
    }

    private void panCamera(double latitude, double longitude) {
        moveCamera(latitude, longitude, mGoogleMap.getCameraPosition().zoom, true);
    }

    private void moveCamera(double latitude, double longitude, float zoomLevel, boolean animate) {
        if (mGoogleMap != null) {
            CameraPosition position = new CameraPosition.Builder()
                    .target(new LatLng(latitude, longitude))
                    .zoom(zoomLevel)
                    .build();

            if (animate) {
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
            } else {
                mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(position));
            }

            mCameraPosition = position;
        }
    }

    private void validateAllPings() {

        if (mPostLoaded.size() > 0) {
            List<PostId> newPostList = new ArrayList<>();

            for (PostId postId : mPostLoaded) {

                CloudPost post = mPostManager.getCloudPost(postId);
                if (post != null) {

                    newPostList.add(new PostId(post.getPostId()));
                }
            }

            if (newPostList.size() != mPostLoaded.size()) {

                mPostInMap.clear();

                mClusterManager.clearItems();

                mPostLoaded.clear();
                mPostLoaded.addAll(newPostList);

                refreshCluster();
            }
        }
    }

    private void getPostBasedOnLocation(RegionBounds bounds) {

        mBinding.refreshLayout.setRefreshing(true);

        mPostManager.getPostsBasedOnLocationOnMap(bounds, (currentPage, posts, isLast) -> {

            if (isUIActive()) {

                mLastLoadTime = System.currentTimeMillis();

                updatePostLoaded(posts);
                refreshCluster();

                mHandler.postDelayed(() -> mBinding.refreshLayout.setRefreshing(false), 2000);
            }
        });
    }

    private void updatePostLoaded(List<CloudPost> postIds) {

        for (CloudPost cloudPost : postIds) {

            PostId postId = new PostId(cloudPost.getPostId());

            if (mPostLoaded.indexOf(postId) < 0) {
                mPostLoaded.add(postId);
            }
        }
    }

    private void refreshCluster() {
        int itemAdded = 0;
        for (PostId postId: mPostLoaded) {
            if (mPostInMap.indexOf(postId) < 0) {
                mPostInMap.add(postId);
                addPin(postId);
                itemAdded ++;
            }
        }

        if (itemAdded > 0) {
            mClusterManager.cluster();
        }
    }

    private void addPin(PostId postId) {

        CloudPost post = mPostManager.getCloudPost(postId);

        mClusterManager.addItem(new MapItem(post));
    }

    private void fetchData() {

        PiquedLogger.d(TAG, "fetching data");

        if (mGoogleMap != null) {
            LatLngBounds bounds = mGoogleMap.getProjection().getVisibleRegion().latLngBounds;

            getPostBasedOnLocation(RegionBounds.createRegionBound(bounds.northeast, bounds.southwest));
        } else {
            // map is not ready yet
        }
    }

    private final SearchBoxListener mListener = new SearchBoxListener() {
        @Override
        public void onButtonPressed(PostSortStyle currentType) {

            setPostPreviewVisible(false, false);
            switchToView(PostSortStyle.LIST_VIEW);
        }
    };

    private final GoogleMap.OnCameraMoveListener mCameraMoveListener = new GoogleMap.OnCameraMoveListener() {
        @Override
        public void onCameraMove() {
            LatLngBounds bounds = mGoogleMap.getProjection().getVisibleRegion().latLngBounds;
            final LatLng northEast = bounds.northeast;
            final LatLng southWest = bounds.southwest;

            mHandler.removeCallbacksAndMessages(null);

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mLastNorthEast != null && mLastSouthWest != null) {
                        if (northEast.latitude != mLastNorthEast.latitude &&
                                northEast.longitude != mLastNorthEast.longitude &&
                                southWest.latitude != mLastSouthWest.latitude &&
                                southWest.longitude != mLastSouthWest.longitude) {
                            fetchData();
                        } else {
                            mLastNorthEast = northEast;
                            mLastSouthWest = southWest;
                        }
                    } else {
                        mLastNorthEast = northEast;
                        mLastSouthWest = southWest;
                        fetchData();
                    }
                }
            }, 300);
        }
    };

    private final ClusterManager.OnClusterClickListener<MapItem> mOnClusterClickListener = cluster -> {

        if (cluster.getSize() > 0) {
            List<PostId> postIds = new ArrayList<>();

            for (MapItem item : cluster.getItems()) {
                postIds.add(new PostId(item.getPost().getPostId()));
            }

            panCamera(cluster.getPosition().latitude, cluster.getPosition().longitude);

            onShowMiniBasket(postIds);

            return true;
        } else {
            return false;
        }
    };

    private final ClusterManager.OnClusterItemClickListener mOnClusterItemClickListener = clusterItem -> {

        MapItem item = (MapItem) clusterItem;

        List<PostId> postIds = new ArrayList<>();
        postIds.add(new PostId(item.getPost().getPostId()));

        panCamera(item.getPosition().latitude, item.getPosition().longitude);

        onShowMiniBasket(postIds);

        return true;
    };

    private final GoogleMap.OnMapClickListener mOnMapClickListener = new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {

            setPostPreviewVisible(false, true);
        }
    };

    private final BroadcastReceiver mPostDeleteReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            validateAllPings();
        }
    };

    private final BroadcastReceiver mShowMiniBasketReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showPostInMiniBasketIfAny();
        }
    };

    private final BroadcastReceiver mLocationPermissionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constants.ACTION_LOCATION_PERMISSION_RESULT.equals(intent.getAction())) {

                boolean result = intent.getBooleanExtra(Constants.KEY_PERMISSION_REQUEST, false);

                if (result) {
                    fetchDataOnResume();

                    setupMap();
                }
            }
        }
    };

    private void onShowMiniBasket(@NonNull List<PostId> posts) {

        setPostPreviewData(posts);
        setPostPreviewVisible(true, true);

        mMiniBasketList.clear();

        List<CloudPost> miniBasketList = getProvider().getMiniBasketList();

        for (CloudPost post : miniBasketList) {
            if (!mMiniBasketList.contains(post)) {
                mMiniBasketList.add(post);
            }
        }
    }

    @Override
    protected void setPostPreviewVisible(boolean visible, boolean animate) {

        mMiniBasketVisible = visible;

        super.setPostPreviewVisible(visible, animate);
    }
}
