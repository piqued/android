package io.piqued.views.home.map.utils

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import io.piqued.R
import io.piqued.database.post.CloudPost
import io.piqued.utils.ImageDownloadUtil
import kotlinx.android.synthetic.main.view_new_cluster_item_marker.view.*
import kotlinx.android.synthetic.main.view_new_cluster_marker.view.*

/**
 *
 * Created by Kenny M. Liou on 9/6/18.
 * Piqued Inc.
 *
 */
class PostPinRenderer(
        val context: Context,
        val map: GoogleMap,
        clusterManager: ClusterManager<MapItem>
): DefaultClusterRenderer<MapItem>(context, map, clusterManager) {

    private val clusterItemTargets = HashSet<ClusterItemMarkerTarget>()
    private val clusterTargets = HashSet<ClusterMarkerTarget>()

    private val clusterIconGenerator = PiquedIconGenerator(context)
    private val clusterItemIconGenerator = PiquedIconGenerator(context)
    private val clusterIconView = LayoutInflater.from(context)
            .inflate(R.layout.view_new_cluster_marker, null, false)
    private val clusterItemIconView = LayoutInflater.from(context)
            .inflate(R.layout.view_new_cluster_item_marker, null, false)
    private val markerSize = context.resources.getDimension(R.dimen.map_marker_size).toInt()

    init {

        clusterIconView.layoutParams = ViewGroup.LayoutParams(markerSize, markerSize)
        clusterItemIconView.layoutParams = ViewGroup.LayoutParams(markerSize, markerSize)

        clusterIconGenerator.setContentView(clusterIconView)
        clusterItemIconGenerator.setContentView(clusterItemIconView)
    }

    override fun onBeforeClusterItemRendered(item: MapItem?, markerOptions: MarkerOptions?) {
        super.onBeforeClusterItemRendered(item, markerOptions)

        clusterItemIconView.clusterItemImageView.setImageResource(R.drawable.map_image_placeholder)

        markerOptions?.icon(BitmapDescriptorFactory.fromBitmap(clusterItemIconGenerator.makeIcon()))
    }

    override fun onClusterItemRendered(clusterItem: MapItem?, marker: Marker?) {
        super.onClusterItemRendered(clusterItem, marker)

        if (marker != null && clusterItem != null) {

            val targetMarker = ClusterItemMarkerTarget(marker)

            clusterItemTargets.add(targetMarker) // create reference

            ImageDownloadUtil.getDownloadUtilInstance(context)
                    .loadMarkerImage(
                            context,
                            clusterItem.post,
                            object : PostMarkerTarget(markerSize) {
                                override fun onImageReady(resource: Bitmap) {
                                    clusterItemIconView.clusterItemImageView.setImageBitmap(resource)
                                    targetMarker.marker.setIcon(BitmapDescriptorFactory.fromBitmap(clusterItemIconGenerator.makeIcon()))
                                    clusterItemTargets.remove(targetMarker)
                                }
                            }
                    )
        }
    }

    override fun onBeforeClusterRendered(cluster: Cluster<MapItem>?, markerOptions: MarkerOptions?) {
        clusterIconView.clusterCount.visibility = View.GONE

        clusterIconView.clusterImageView.setImageResource(R.drawable.map_image_placeholder)

        markerOptions?.icon(BitmapDescriptorFactory.fromBitmap(clusterIconGenerator.makeIcon()))
    }

    override fun onClusterRendered(cluster: Cluster<MapItem>?, marker: Marker?) {
        super.onClusterRendered(cluster, marker)

        if (cluster != null && marker != null) {

            var mostRecentPost: CloudPost? = null

            for (item: MapItem in cluster.items) {
                if (mostRecentPost == null) {
                    mostRecentPost = item.post
                } else {
                    if (mostRecentPost.createdAt < item.post.createdAt) {
                        mostRecentPost = item.post
                    }
                }
            }

            if (mostRecentPost != null) {
                val targetMarker = ClusterMarkerTarget(marker, cluster.items.size)

                clusterTargets.add(targetMarker)

                ImageDownloadUtil.getDownloadUtilInstance(context)
                        .loadMarkerImage(
                                context,
                                mostRecentPost,
                                object : PostMarkerTarget(markerSize) {
                                    override fun onImageReady(resource: Bitmap) {
                                        clusterIconView.clusterImageView.setImageBitmap(resource)
                                        clusterIconView.clusterCount.visibility = View.VISIBLE
                                        clusterIconView.clusterCount.text = targetMarker.count.toString()

                                        try {
                                            targetMarker.marker.setIcon(BitmapDescriptorFactory.fromBitmap(clusterIconGenerator.makeIcon()))
                                        } catch (e: IllegalArgumentException) {
                                            // ignore Unmanaged descriptor for now
                                        }

                                        clusterTargets.remove(targetMarker)
                                    }

                                })
            }
        }
    }

    override fun shouldRenderAsCluster(cluster: Cluster<MapItem>): Boolean {
        return cluster.size > 1
    }
}