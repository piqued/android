package io.piqued.views.profile.edit

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R
import io.piqued.model.EditUserData
import io.piqued.database.user.User
import kotlinx.android.synthetic.main.view_holder_edit_button.view.*

/**
 *
 * Created by Kenny M. Liou on 3/24/18.
 * Piqued Inc.
 *
 */
internal class EditButtonViewHolder(parent: ViewGroup, val callback: Callback) :
        InfoViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_edit_button, parent, false)) {

    interface Callback {
        fun editEmail()
        fun editPassword()
    }

    lateinit var type: InfoType

    init {
        itemView.root_view.setOnClickListener {
            if (type == InfoType.PASSWORD) {
                callback.editPassword()
            } else if (type == InfoType.EMAIL) {
                callback.editEmail()
            }
        }
    }

    override fun onBind(type: InfoType, user: User, userData: EditUserData) {

        this.type = type

        if (type == InfoType.EMAIL) {
            itemView.icon_view.setImageResource(R.drawable.profile_email)
            if (!TextUtils.isEmpty(userData.email)) {
                itemView.value.text = userData.email
            } else {
                itemView.value.text = user.email
            }
        } else if (type == InfoType.PASSWORD) {
            itemView.icon_view.setImageResource(R.drawable.profile_password)
            itemView.value.setText(R.string.settings_change_password)
        }
    }

}