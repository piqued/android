package io.piqued.views.widget

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/18/17.
 * Piqued Inc.
 */

class ViewPagerWithSwipeControl @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ViewPager(context, attrs) {

    private var mSwipeEnabled = true

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (mSwipeEnabled) super.onInterceptTouchEvent(event) else false
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (mSwipeEnabled) super.onTouchEvent(event) else false
    }

    fun setSwipeEnable(enable: Boolean) {
        mSwipeEnabled = enable
    }
}
