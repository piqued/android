package io.piqued.views.home;

import android.location.Location;

import androidx.annotation.NonNull;

import java.util.List;

import io.piqued.database.post.CloudPost;
import io.piqued.basket.BasketActionHandler;
import io.piqued.database.post.PostId;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/14/17.
 * Piqued Inc.
 */

public interface HomeFunctionProvider {

    void switchToHomeScreenType(@NonNull PostSortStyle type);
    void setPostPreviewData(List<PostId> posts);
    void setPostPreviewVisible(boolean visible, boolean animate);
    void setMiniBasketPostId(long postId);

    Location getCurrentLocation();
    BasketActionHandler getBasketActionHandler();
    List<CloudPost> getMiniBasketList();
    long getMiniBasketPostId();
}
