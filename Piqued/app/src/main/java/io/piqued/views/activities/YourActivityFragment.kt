package io.piqued.views.activities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.ncapdevi.fragnav.FragNavController
import io.piqued.R
import io.piqued.api.PiquedApiCallback
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.Status
import io.piqued.cloud.repository.UserRepository
import io.piqued.cloud.util.Resource
import io.piqued.database.event.Event
import io.piqued.database.post.PostId
import io.piqued.database.user.User
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.utils.Constants
import io.piqued.utils.DividerItemDecoration
import io.piqued.utils.EventManager
import io.piqued.utils.ViewUtil
import io.piqued.utils.modelmanager.PiquedUserManager
import io.piqued.utils.postmanager.PostManager
import io.piqued.views.DoubleTapFragment
import io.piqued.views.activities.util.ActivityFragmentActionBroadcast
import kotlinx.android.synthetic.main.fragment_your_activity.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 9/3/18.
 * Piqued Inc.
 *
 */
class YourActivityFragment: DoubleTapFragment() {

    companion object {
        private val TAG = YourActivityFragment::class.java.simpleName

        fun newInstance(): YourActivityFragment {
            return YourActivityFragment()
        }
    }

    @Inject
    internal lateinit var eventManager: EventManager
    @Inject
    internal lateinit var postManager: PostManager
    @Inject
    internal lateinit var userManager: PiquedUserManager
    @Inject
    internal lateinit var userRepository: UserRepository

    private var lastLoadTime = -1L
    private var noMoreData = false

    private val dataHandler = object : ActivitiesAdapter.DataHandler {
        override fun fetchMoreDate() {
            fetchEventData(false)
        }
    }

    private val viewHolderHandler = object : YourActivityViewHolder.Handler {
        override fun openProfile(userId: Long) {
            val currentContext = context
            if (isUIActive && currentContext != null) {
                ActivityFragmentActionBroadcast.openProfile(currentContext, userId)
            }
        }

        override fun openPost(postId: Long) {
            val currentContext = context
            if (isUIActive && currentContext != null) {
                ActivityFragmentActionBroadcast.openPost(currentContext, PostId(postId))
            }
        }

        override fun followUser(userId: Long, followed: Boolean) {
            postManager.followUser(userId, followed, object : PiquedApiCallback<Long> {
                override fun onSuccess(data: Long?) {
                    // YAY
                    PiquedLogger.i(TAG, "user follow state changed: " + followed)
                }

                override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                    PiquedLogger.e(TAG, "Failed to update follow status", errorCode, payload)
                }
            })
        }

        override fun isFollowingUser(userId: Long): Boolean {
            for (user in followingUserList) {
                if (user.userId == userId) {
                    return true
                }
            }

            return false
        }
    }


    private lateinit var eventAdapter: ActivitiesAdapter

    private val refreshListener = SwipeRefreshLayout.OnRefreshListener {
        fetchEventData(true)
    }

    private val followingUserList = ArrayList<User>()

    private val followingUserListObserver = Observer<Resource<List<User>>> {resource ->
        if (resource != null) {
            if (resource.data != null) {

                followingUserList.clear()
                followingUserList.addAll(resource.data!!)

                eventAdapter.notifyDataSetChanged()
            }
        }
    }

    override fun getTagName(): String {
        return TAG
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        eventAdapter = ActivitiesAdapter(dataHandler, viewHolderHandler)

        if (userManager.myUserRecord != null) {
            userRepository.getUserFollowing(userManager.myUserRecord!!.userId).observe(this, followingUserListObserver)
        }

        eventManager.getActivityLiveData().observe(this, eventCallbacks)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_your_activity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val currentContext = view.context

        activityListRecyclerView.addItemDecoration(DividerItemDecoration(currentContext))
        activityListRecyclerView.layoutManager = LinearLayoutManager(currentContext)
        activityListRecyclerView.adapter = eventAdapter

        ViewUtil.initializeSwipeRefreshLayout(refreshLayout, refreshListener)

        if (savedInstanceState != null) {
            lastLoadTime = savedInstanceState.getLong(Constants.KEY_LAST_LOAD_TIME)
        }
    }

    override fun onResume() {
        super.onResume()

        if (userManager.myUserRecord != null) {

            userRepository.getUserFollowing(userManager.myUserRecord!!.userId, object : PiquedCloudCallback<List<User>> {
                override fun onSuccess(result: List<User>?) {}
                override fun onFailure(error: Throwable?, message: String?) {}
            })
        }

        if (lastLoadTime < 0 || TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - lastLoadTime) > 5) {
            fetchEventData(true)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putLong(Constants.KEY_LAST_LOAD_TIME, lastLoadTime)
    }

    private fun fetchEventData(force: Boolean) {
        if (force) {
            eventManager.fetchActivities(1)
            return
        }

        if (!noMoreData) {
            refreshLayout.isRefreshing = true
            eventManager.fetchActivities(eventAdapter.itemCount / Constants.DEFAULT_FETCH_PAGE_SIZE + 1)
        } else {
            refreshLayout.isRefreshing = false
        }
    }

    override fun onTabDoubleTapped(backStackController: FragNavController) {
        activityListRecyclerView.post {
            activityListRecyclerView.smoothScrollToPosition(0)
        }
    }

    /**
     * Callbacks
     */

    private val eventCallbacks = Observer<Resource<List<Event>>> { t ->
        if (t != null) {
            if (t.status == Status.LOADING) {
                if (!refreshLayout.isRefreshing) {
                    refreshLayout.isRefreshing = true
                }
            } else if (t.status == Status.SUCCESS) {
                refreshLayout.isRefreshing = false
                noMoreData = eventManager.getLastFetchSize() < Constants.DEFAULT_FETCH_PAGE_SIZE
            }

            val result = t.data
            if (result != null && result.isNotEmpty()) {
                eventAdapter.updateData(result)
                eventAdapter.notifyDataSetChanged()
            }
        }
    }
}