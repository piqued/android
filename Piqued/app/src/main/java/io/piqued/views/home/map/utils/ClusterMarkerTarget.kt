package io.piqued.views.home.map.utils

import com.google.android.gms.maps.model.Marker

/**
 *
 * Created by Kenny M. Liou on 9/6/18.
 * Piqued Inc.
 *
 */
class ClusterMarkerTarget(
        val marker: Marker,
        val count: Int
)