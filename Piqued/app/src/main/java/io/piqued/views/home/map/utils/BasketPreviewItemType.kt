package io.piqued.views.home.map.utils

/**
 *
 * Created by Kenny M. Liou on 2/24/18.
 * Piqued Inc.
 *
 */
enum class BasketPreviewItemType {
    EMPTY,
    HANDLE,
    POST,
    FOOTER;

    companion object {
        val values = BasketPreviewItemType.values()

        @JvmStatic
        fun fromInt(index: Int): BasketPreviewItemType {

            if (index > -1 && index < values.size) {
                return values[index]
            }

            throw IllegalArgumentException("Illegal index for type: " + BasketPreviewItemType::class.java.getName() + ", index: " + index)
        }
    }
}