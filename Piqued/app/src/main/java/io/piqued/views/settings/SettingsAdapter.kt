package io.piqued.views.settings

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.BuildConfig
import io.piqued.viewholders.settings.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/11/17.
 * Piqued Inc.
 */

class SettingsAdapter(private val mDelegate: SettingsDelegate) : RecyclerView.Adapter<SettingsBaseViewHolder>() {

    private val items = ArrayList<Settings>()

    init {

        items.add(Settings.MY_ACCOUNT)
        items.add(Settings.MY_ACCOUNT_EDIT)
        items.add(Settings.NOTIFICATION)
        items.add(Settings.NOTIFICATION_OVERALL)
        items.add(Settings.SECURITY)
        items.add(Settings.SECURITY_TAS)
        items.add(Settings.APP_VERSION)

        if ("production" != BuildConfig.PIQUED_BUILD_ENV) {
            items.add(Settings.DEVELOPER)
            items.add(Settings.DEVELOPER_SEND_LOG)
            items.add(Settings.DEVELOPER_READ_ALL_LOGS)
        }
        items.add(Settings.LOGOUT)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingsBaseViewHolder {

        val type = Settings.fromInt(viewType)

        val result: SettingsBaseViewHolder

        when (type.viewType) {
            SettingsItemViewType.TITLE -> {
                val title = SettingsTitleViewHolder(parent)
                title.setCategory(type.category)
                result = title
            }
            SettingsItemViewType.BUTTON, SettingsItemViewType.BUTTON_WITH_BOLD -> result = SettingsButtonViewHolder(parent)
            SettingsItemViewType.APP_VERSION -> result = SettingsDisplayDataViewHolder(parent)
            SettingsItemViewType.SOCIAL_MEDIA -> result = SettingsSocialMediaViewHolder(parent)
            SettingsItemViewType.SWITCH -> result = SettingsSwitchViewHolder(parent)
            SettingsItemViewType.DISTANCE -> result = SettingsToggleViewHolder(parent)
            SettingsItemViewType.LOGOUT -> result = SettingsLogoutViewHolder(parent)
        }

        result.setSettingType(type)
        result.setDelegate(mDelegate)

        return result
    }

    override fun onBindViewHolder(holder: SettingsBaseViewHolder, position: Int) {

        holder.onBindingData()
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].ordinal
    }

    override fun getItemCount(): Int {
        return items.size
    }
}
