package io.piqued.views.profile.edit;

import android.view.MenuItem;

import java.io.File;

import io.piqued.PiquedFragment;
import io.piqued.activities.EditProfileActivity;

/**
 * Created by Kenny M. Liou on 10/21/17.
 * Piqued Inc.
 */

public abstract class EditProfileBaseFragment extends PiquedFragment {

    protected EditProfileActivity getEditProfileActivity() {
        return (EditProfileActivity) getActivity();
    }

    public boolean onBackPressed() {
        return false; // we don't handle this
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getEditProfileActivity().onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected File getTempFile() {
        return getEditProfileActivity().createTempUserImageFile();
    }
}
