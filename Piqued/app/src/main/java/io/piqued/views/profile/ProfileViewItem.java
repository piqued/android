package io.piqued.views.profile;

import java.util.List;

import io.piqued.database.post.PostId;
import io.piqued.utils.postorganization.PostLocationGroup;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/25/17.
 * Piqued Inc.
 */

public class ProfileViewItem {

    private ProfileViewType mType;
    private PostLocationGroup mGroup;
    private List<PostId> mPostIds;
    private PostId mSinglePostId;
    private int mIndexInGroup;

    public ProfileViewItem(ProfileViewType type) {
        mType = type;
    }

    public ProfileViewItem(PostLocationGroup group) {
        this(ProfileViewType.POST_TITLE);

        mGroup = group;
    }

    public ProfileViewItem(PostId postId) {
        this(ProfileViewType.SINGLE_POST);

        mSinglePostId = postId;
    }

    public ProfileViewType getType() {
        return mType;
    }

    public List<PostId> getPostIds() {
        return mPostIds;
    }

    public PostLocationGroup getGroup() {
        return mGroup;
    }

    public PostId getSinglePostId() {
        return mSinglePostId;
    }
}
