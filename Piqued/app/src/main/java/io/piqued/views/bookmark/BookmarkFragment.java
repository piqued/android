package io.piqued.views.bookmark;

import android.content.Context;
import android.graphics.Rect;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ncapdevi.fragnav.FragNavController;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.piqued.R;
import io.piqued.api.PiquedApiCallback;
import io.piqued.basket.BasketFragment;
import io.piqued.cloud.PiquedCloudCallback;
import io.piqued.cloud.Status;
import io.piqued.cloud.util.Resource;
import io.piqued.database.post.CloudPost;
import io.piqued.database.util.PiquedErrorCode;
import io.piqued.database.util.PiquedLogger;
import io.piqued.databinding.FragmentBookmarkBinding;
import io.piqued.utils.DistanceHelper;
import io.piqued.utils.DividerItemDecoration;
import io.piqued.utils.LocationCallback;
import io.piqued.utils.PiquedLocationManager;
import io.piqued.utils.Preferences;
import io.piqued.utils.ViewUtil;
import io.piqued.utils.postmanager.PostManager;
import io.piqued.views.HomeActivityFragment;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/29/16.
 * Piqued Inc.
 */

public class BookmarkFragment extends HomeActivityFragment {

    private static final long REMOVE_BOOKMARK_WAIT_TIME = TimeUnit.SECONDS.toMillis(2);
    private static final String TAG = BookmarkFragment.class.getSimpleName();

    public static String getStaticTagName() {
        return TAG;
    }

    public static BookmarkFragment newInstance() {
        return new BookmarkFragment();
    }

    @Inject
    PostManager mPostManager;
    @Inject
    PiquedLocationManager mLocationManager;
    @Inject
    Preferences preferences;

    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private final List<CloudPost> bookmarks = new ArrayList<>();

    private BookmarkViewHolder mViewHolderToRemove;
    private CloudPost mPostToRemove;
    private Location mLocation;

    @Override
    public String getTagName() {
        return getStaticTagName();
    }

    private FragmentBookmarkBinding mBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getApplication().getComponent().inject(this);

        if (preferences.userLoggedIn()) {
            mPostManager.getBookmarkPostsAsLiveData().observe(this, new Observer<Resource<List<CloudPost>>>() {
                @Override
                public void onChanged(Resource<List<CloudPost>> data) {

                    mBinding.refreshLayout.setRefreshing(data.getStatus() == Status.LOADING);

                    if (data.getData() != null) {
                        bookmarks.clear();

                        bookmarks.addAll(data.getData());

                        updateBookmark();
                    }
                }
            });
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mBinding = FragmentBookmarkBinding.inflate(inflater, container, false);

        mBinding.setNavigationBarHeight(getPiquedActivity().getNavigationBarHeight());

        mBinding.bookmarkRecyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        mBinding.bookmarkRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.bookmarkRecyclerView.setAdapter(mAdapter);

        mBinding.bookmarkRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(@NotNull RecyclerView rv, @NotNull MotionEvent e) {

                boolean shouldIntercept = e.getAction() == MotionEvent.ACTION_DOWN && mPostToRemove != null;

                if (shouldIntercept) {

                    Rect rect = new Rect();
                    mViewHolderToRemove.itemView.getHitRect(rect);

                    if (!rect.contains((int) e.getX(), (int) e.getY())) {
                        checkAndRemovePost();
                    } else {
                        return false;
                    }
                }

                return shouldIntercept;
            }

            @Override
            public void onTouchEvent(@NotNull RecyclerView rv, @NotNull MotionEvent e) {}

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
        });

        ViewUtil.initializeSwipeRefreshLayout(mBinding.refreshLayout, mOnRefreshListener);

        mBinding.navToolbar.profileToolbar.setTitle(R.string.bookmark_title);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.setOnApplyWindowInsetsListener((v, insets) -> {
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) mBinding.bottomPlaceHolder.getLayoutParams();
            params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, insets.getSystemWindowInsetBottom());
            mBinding.bottomPlaceHolder.setLayoutParams(params);

            return insets.consumeSystemWindowInsets();
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        updateScreenIfEmpty();

        mLocationManager.getCurrentLocation(mLocationCallback);
    }

    private final LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationUpdated(final Location location) {

            mLocation = location;

            if (preferences.userLoggedIn()) {
                updateBookmark();   
            }
        }

        @Override
        public boolean onceOnly() {
            return true;
        }
    };

    private void updateBookmark() {

        if (mLocation != null && bookmarks.size() > 0) {
            Collections.sort(bookmarks, (leftPost, rightPost) -> {

                if (leftPost == null && rightPost == null) {
                    return 0;
                }

                if (leftPost == null) {
                    return -1;
                }

                if (rightPost == null) {
                    return 1;
                }

                double leftDistance = DistanceHelper.INSTANCE.getDistanceInInteger(mLocation, leftPost);
                double rightDistance = DistanceHelper.INSTANCE.getDistanceInInteger(mLocation, rightPost);

                if (Math.abs(leftDistance - rightDistance) < 0.1) {
                    return leftPost.getCreatedAt() > rightPost.getCreatedAt() ? -1 : 1;
                }

                return leftDistance < rightDistance ? -1 : 1;
            });
        }

        mAdapter.notifyDataSetChanged();

        if (mBinding.refreshLayout.isRefreshing()) {
            mBinding.refreshLayout.setRefreshing(false);
        }

        updateScreenIfEmpty();
    }

    private void checkAndRemovePost() {

        if (mPostToRemove != null) {
            CloudPost post = mPostToRemove;
            BookmarkViewHolder viewHolder = mViewHolderToRemove;
            mViewHolderToRemove = null;
            mPostToRemove = null;

            viewHolder.onRemoved();
            removePost(post);
        }
    }

    private void fetchBookmarkData() {

        mPostManager.refreshBookmarks();
    }

    private void updateScreenIfEmpty() {
        if (bookmarks.size() > 0) {
            mBinding.bookmarkEmptyView.setVisibility(View.GONE);
            mBinding.refreshLayout.setVisibility(View.VISIBLE);
        } else {
            mBinding.bookmarkEmptyView.setVisibility(View.VISIBLE);
            mBinding.refreshLayout.setVisibility(View.GONE);
        }
    }

    private void removePost(CloudPost post) {

        final int index = bookmarks.indexOf(post);

        if (index > -1) {
            bookmarks.remove(index);
            mAdapter.notifyItemRemoved(index);

            mPostManager.bookmarkPost(post.getPostId(), false, new PiquedCloudCallback<Long>() {

                @Override
                public void onSuccess(Long postId) {

                    mAdapter.notifyDataSetChanged();

                    updateScreenIfEmpty();
                }

                @Override
                public void onFailure(@org.jetbrains.annotations.Nullable Throwable error, @org.jetbrains.annotations.Nullable String message) {
                    PiquedLogger.e(TAG, PiquedErrorCode.NETWORK, message);
                }
            });
        }
    }

    private final RecyclerView.Adapter<BookmarkViewHolder> mAdapter = new RecyclerView.Adapter<BookmarkViewHolder>() {
        @Override
        public BookmarkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new BookmarkViewHolder(parent, mViewHolderListener);
        }

        @Override
        public void onBindViewHolder(BookmarkViewHolder holder, int position) {

            holder.setPost(
                    getContext(),
                    bookmarks.get(position),
                    mLocationManager);
        }

        @Override
        public int getItemCount() {

            return bookmarks.size();
        }
    };

    private final BookmarkViewHolderListener mViewHolderListener = new BookmarkViewHolderListener() {
        @Override
        public void onBookmarkRemove(CloudPost post, BookmarkViewHolder viewHolder) {
            mViewHolderToRemove = viewHolder;
            mPostToRemove = post;
            mHandler.postDelayed(new RemovePostRunnable(post.getPostId()), REMOVE_BOOKMARK_WAIT_TIME);
        }

        @Override
        public void onBookmarkRemoveUndo() {
            mPostToRemove = null;
            mViewHolderToRemove = null;
            mHandler.removeCallbacksAndMessages(null);
        }

        @Override
        public void onOpenPost(CloudPost post) {
            // TODO this is a hack, please fix BasketFragment so it takes posts that's not cashed locally
            mPostManager.getPostAsync(post.getPostId(), new PiquedApiCallback<CloudPost>() {
                @Override
                public void onSuccess(CloudPost data) {
                    pushFragment(BasketFragment.newInstance(post.getPostId()));
                }

                @Override
                public void onFailure(PiquedErrorCode errorCode, String payload) {
                    // oh crap
                    PiquedLogger.e(TAG, "Failed to load post: " + post.getPostId());
                }
            });
        }

        @Override
        public boolean isUiActive() {
            return BookmarkFragment.this.isUIActive();
        }

        @Override
        public Context getParentContext() {
            return getContext();
        }
    };

    private final SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (isUIActive()) {
                fetchBookmarkData();
            }
        }
    };

    @Override
    public void onTabDoubleTapped(@NotNull FragNavController backStackController) {
        mBinding.bookmarkRecyclerView.smoothScrollToPosition(0);
    }

    private class RemovePostRunnable implements Runnable {
        private final long mPostId;

        RemovePostRunnable(long postId) {
            mPostId = postId;
        }

        @Override
        public void run() {
            if (mPostToRemove != null && mPostId == mPostToRemove.getPostId()) {
                CloudPost postToRemove = mPostToRemove;
                mPostToRemove = null;
                removePost(postToRemove);
            }
        }
    }
}
