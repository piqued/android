package io.piqued.views.profile.edit

import android.net.Uri
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.piqued.R
import io.piqued.model.EditUserData
import io.piqued.database.user.User
import io.piqued.module.GlideApp
import kotlinx.android.synthetic.main.view_holder_edit_photo.view.*

/**
 * Created by Kenny M. Liou on 9/30/17.
 * Piqued Inc.
 */

internal class EditPhotoViewHolder(
        parent: ViewGroup,
        private val callback: Callback)
    : InfoViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_edit_photo, parent, false)
) {

    internal interface Callback {
        fun openSelectImageView()
    }

    init {

        itemView.userImage.setOnClickListener {
            callback.openSelectImageView()
        }
        itemView.changePhotoButton.setOnClickListener {
            callback.openSelectImageView()
        }
    }

    internal override fun onBind(type: InfoType, user: User, userData: EditUserData) {

        val imageUri: Uri?
        if (userData.imageData != null) {
            imageUri = userData.imageData
        } else if (!TextUtils.isEmpty(user.profileImageUrl)) {
            imageUri = Uri.parse(user.profileImageUrl)
        } else {
            imageUri = null
        }

        if (imageUri != null) {
            GlideApp.with(context)
                    .load(imageUri)
                    .centerCrop()
                    .circleCrop()
                    .into(itemView.userImage)
        }
    }
}
