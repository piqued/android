package io.piqued.views.home

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import io.piqued.activities.HomeBaseActivity

/**
 *
 * Created by Kenny M. Liou on 2/10/18.
 * Piqued Inc.
 *
 */
class HomeUtil {

    companion object {

        const val ACTION_NOTIFY_UI_SHOULD_SHOW = "notify_ui_should_show"
        const val KEY_UI_SHOULD_HIDE = "ui_hide_value"
        const val ACTION_HOME_DOUBLE_TAPPED = "home_double_tapped"

        @JvmStatic
        fun sendUIHideNotification(homeActivity: HomeBaseActivity,
                                   shouldHide: Boolean) {
            sendUIHideNotification(homeActivity, shouldHide, true)
        }
        @JvmStatic
        fun sendUIHideNotification(homeActivity: HomeBaseActivity,
                                   shouldHide: Boolean,
                                   shouldAnimate: Boolean) {
            val intent = Intent(ACTION_NOTIFY_UI_SHOULD_SHOW)
            intent.putExtra(KEY_UI_SHOULD_HIDE, shouldHide)

            LocalBroadcastManager.getInstance(homeActivity).sendBroadcast(intent)

            homeActivity.setNavBarHidden(shouldHide, shouldAnimate)
        }

        @JvmStatic
        fun sendDoubleTapSignal(context: Context) {

            val intent = Intent(ACTION_HOME_DOUBLE_TAPPED)

            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }

        @JvmStatic
        fun configureBroadcastReceiver(context: Context, action: String, broadcastReceiver: BroadcastReceiver, isRegister: Boolean) {

            if (isRegister) {
                LocalBroadcastManager.getInstance(context).registerReceiver(broadcastReceiver, IntentFilter(action))
            } else {
                LocalBroadcastManager.getInstance(context).unregisterReceiver(broadcastReceiver)
            }
        }
    }
}