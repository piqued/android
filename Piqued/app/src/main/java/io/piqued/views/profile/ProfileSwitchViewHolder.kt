package io.piqued.views.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.piqued.R
import io.piqued.views.home.PostSortStyle

import io.piqued.views.home.PostSortStyle.LIST_VIEW
import io.piqued.views.home.PostSortStyle.MAP_VIEW
import kotlinx.android.synthetic.main.view_holder_profile_switch.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/28/17.
 * Piqued Inc.
 */

class ProfileSwitchViewHolder(parent: ViewGroup)
    : ProfileViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_profile_switch, parent, false)
) {

    internal var mStyle = LIST_VIEW // default

    private val mOnClickListener = View.OnClickListener {
        when (mStyle) {
            LIST_VIEW -> {
                mProvider.onSwitchToView(mStyle)
                mStyle = MAP_VIEW
            }
            MAP_VIEW -> {
                mProvider.onSwitchToView(mStyle)
                mStyle = LIST_VIEW
            }
        }
    }

    init {

        itemView.toggleButton.setOnClickListener(mOnClickListener)
        itemView.toggleButton.setEnabled(false) //TODO: fix me when map view is implemented
    }

    internal override fun onProviderReady(provider: ProfileDataProvider) {

    }
}
