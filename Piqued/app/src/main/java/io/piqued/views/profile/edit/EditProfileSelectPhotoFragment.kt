package io.piqued.views.profile.edit

import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import androidx.recyclerview.widget.GridLayoutManager
import com.isseiaoki.simplecropview.CropImageView
import com.isseiaoki.simplecropview.callback.CropCallback
import com.isseiaoki.simplecropview.callback.LoadCallback
import com.isseiaoki.simplecropview.callback.SaveCallback
import io.piqued.R
import io.piqued.createbasket.fragment.album.AlbumImageAdapter
import io.piqued.createbasket.fragment.album.AlbumImageViewHolder
import io.piqued.database.album.ImageAlbum
import io.piqued.utils.LoadingIndicator
import io.piqued.database.util.PiquedLogger
import io.piqued.views.profile.ProfileItemDecoration
import kotlinx.android.synthetic.main.fragment_edit_profile_select_photo.*
import kotlinx.android.synthetic.main.toolbar.view.*
import java.io.File
import java.io.FileNotFoundException
import java.io.PrintWriter

/**
 *
 * Created by Kenny M. Liou on 9/29/18.
 * Piqued Inc.
 *
 */

class EditProfileSelectPhotoFragment: EditProfileBaseFragment() {

    private lateinit var albumImageAdapter: AlbumImageAdapter
    private lateinit var allImageCursor: Cursor
    private lateinit var loadingIndicator: LoadingIndicator
    private var dataColumnIndex: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.edit_profile_select_photo_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.edit_profile_select_photo_save -> {
                cropAndSavePhoto()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_edit_profile_select_photo, container, false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        albumImageAdapter = AlbumImageAdapter(adapterOwner, viewHolderProvider)
        allImageCursor = ImageAlbum.createAllImageCursor(view.context)
        dataColumnIndex = allImageCursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)

        val gridLayoutManager = GridLayoutManager(view.context, 4)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return 1
            }
        }

        recyclerView.layoutManager = gridLayoutManager
        recyclerView.adapter = albumImageAdapter
        recyclerView.addItemDecoration(ProfileItemDecoration(0, view.context))

        cropImageView.setCropMode(CropImageView.CropMode.CIRCLE)

        loadingIndicator = LoadingIndicator(editProfileActivity)
    }

    override fun onResume() {
        super.onResume()

        val newUserImageUri = editProfileActivity.getNewUserImageUri()
        if (newUserImageUri != null ) {
            updateImage(newUserImageUri)
        } else {
            updateImage(getImageUri(0))
        }

        // load images
        albumImageAdapter.notifyDataSetChanged()

        setSupportActionBar(includeToolbar.toolbar, R.string.edit_profile_select_photo_title, false)
    }

    private fun cropAndSavePhoto() {
        loadingIndicator.show()

        val tempFile = tempFile

        if (tempFile.exists()) {
            // cleanup
            try {
                val writer = PrintWriter(tempFile)
                writer.close()

                cropImageView.cropAsync(onCropListener)

            } catch (e: FileNotFoundException) {
                PiquedLogger.e(tagName, "Failed to open temp file to store profile image")
            }
        }
    }

    private fun saveImage(image: Bitmap) {
        cropImageView.save(image)
                .execute(Uri.fromFile(tempFile), object : SaveCallback {

                    override fun onSuccess(uri: Uri?) {
                        PiquedLogger.i(tagName, "User new profile image saved to temp file.")
                        if (uri != null) {
                            PiquedLogger.i(tagName, "User new profile image file uri: " + uri.path)
                            // we completed crop and save, exit to previous screen
                            editProfileActivity.setNewUserImageUri(uri)
                            loadingIndicator.dismiss()
                            goBack()
                        } else {
                            PiquedLogger.i(tagName, "User new profile image file uri not found.")
                        }
                    }

                    override fun onError(e: Throwable?) {
                        PiquedLogger.e(tagName, "Failed to save image. " + e?.message)
                    }
                })
    }

    private fun updateImage(imageUri: Uri) {

        cropImageView.load(imageUri).execute(object : LoadCallback {
            override fun onSuccess() {
                // good job
            }

            override fun onError(e: Throwable?) {
                PiquedLogger.e(tagName, "Failed to load image: " + e?.message)
            }
        })
    }

    private fun getImageUri(position: Int): Uri {

        allImageCursor.moveToPosition(position)
        val filePath = allImageCursor.getString(dataColumnIndex)

        return Uri.fromFile(File(filePath))
    }

    private val onCropListener = object : CropCallback
    {
        override fun onSuccess(cropped: Bitmap?) {
            if (cropped != null) {
                saveImage(cropped)
            }
        }

        override fun onError(e: Throwable?) {
            PiquedLogger.e(tagName, "Failed to crop image. " + e?.message)
        }
    }

    private val adapterOwner = object : AlbumImageAdapter.AdapterOwner {
        override fun getImageUriAtPosition(position: Int): Uri {
            return getImageUri(position)
        }

        override fun getImageCount(): Int {
            return allImageCursor.count
        }

    }

    private val viewHolderProvider = object : AlbumImageViewHolder.Provider {
        override fun getSelectionIndex(imageUri: Uri): Int {
            return -1
        }

        override fun canAddMoreImage(): Boolean {
            return true
        }

        override fun onSelectImage(imageUri: Uri, viewHolderPosition: Int) {
            updateImage(imageUri)
        }

        override fun onRemoveImage(imageUri: Uri, viewHolderPosition: Int) {
            // do nothing
        }
    }
}