package io.piqued.views.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.piqued.R
import io.piqued.databinding.ViewHolderProfileNoPostBinding

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/4/17.
 * Piqued Inc.
 */

class ProfileNoPostViewHolder(parent: ViewGroup)
    : ProfileViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_profile_no_post, parent, false)
) {
    private val binding: ViewHolderProfileNoPostBinding = ViewHolderProfileNoPostBinding.bind(itemView)
    internal override fun onProviderReady(provider: ProfileDataProvider) {
        if (provider.profileType == ProfileType.EMPTY) {
            binding.noPostMessage.visibility = View.GONE
        } else {
            binding.noPostMessage.visibility = View.VISIBLE
        }
    }
}
