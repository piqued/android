package io.piqued.views.welcome

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

import io.piqued.R

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/7/16.
 * Piqued Inc.
 */

enum class WelcomeStep constructor(@param:DrawableRes @field:DrawableRes
                                           val imageRes: Int, @param:StringRes @field:StringRes
                                           val stringRes: Int) {

    STEP_1(R.drawable.welcome1, 0),
    STEP_2(R.drawable.welcome2, R.string.step_2_string),
    STEP_3(R.drawable.welcome3, R.string.step_3_string),
    STEP_4(R.drawable.welcome4, R.string.step_4_string);


    companion object {

        val values = WelcomeStep.values()

        fun maxIndex(): Int {
            return values.size - 1
        }

        fun fromInt(index: Int): WelcomeStep {
            if (index > -1 && index < values.size) {
                return values[index]
            }

            throw IllegalArgumentException("Illegal index value for enum 'WelcomeStep': $index")
        }
    }
}
