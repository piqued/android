package io.piqued.views.profile

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import io.piqued.R
import io.piqued.utils.ImageDownloadUtil
import io.piqued.utils.IntentUtil
import io.piqued.utils.LoginRequiredExecution
import kotlinx.android.synthetic.main.view_holder_profile_header.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/28/17.
 * Piqued Inc.
 */

@SuppressLint("ClickableViewAccessibility")
internal class ProfileHeaderViewHolder(parent: ViewGroup)
    : ProfileViewHolder(
        LayoutInflater.from(parent.context)
                .inflate(R.layout.view_holder_profile_header, parent, false)
) {

    init {
        itemView.profileUserImage.setOnClickListener {
            openEditProfileIfAvailable()
        }
        itemView.profileUserName.setOnClickListener {
            openEditProfileIfAvailable()
        }
        itemView.profileEditButton.setOnClickListener {
            openEditProfileIfAvailable()
        }
        itemView.profileSignUpButton.setOnClickListener {
            showSignUpView()
        }

        itemView.profileFollowButton.setOnCheckedChangeListener { view, follow ->
            if (mProvider != null && follow != mProvider.isFollowed) {
                IntentUtil.runIfUserLoggedIn(view.context, mProvider.isUserReady, object: LoginRequiredExecution {
                    override fun runIfLoggedIn() {
                        mProvider.setUserFollowed(follow)
                    }
                })
            }
        }

        itemView.profileFollowButton.setOnTouchListener { v, event ->
            var result = false
            if (MotionEvent.ACTION_DOWN == event.action) {
                if (!mProvider.isUserLoggedIn) {
                    IntentUtil.showSignUpPopup(v.context)
                    result = true
                }
            }
            result
        }
    }

    internal override fun onProviderReady(provider: ProfileDataProvider) {
        if (provider.profileType == ProfileType.EMPTY || provider.isUserReady) {
            updateView()
        }
    }

    private fun updateView() {
        itemView.profileUserName.text = mProvider.getUserName(itemView.context)

        when (mProvider.profileType) {
            ProfileType.USER -> {
                itemView.profileEditButton.visibility = View.GONE
                itemView.profileFollowButton.visibility = View.VISIBLE
                itemView.profileSignUpButton.visibility = View.GONE
                itemView.profileFollowButton.isChecked = mProvider.isFollowed
                itemView.profileFollowButton.isEnabled = true
            }
            ProfileType.OWNER -> {
                itemView.profileFollowButton.visibility = View.GONE
                itemView.profileSignUpButton.visibility = View.GONE
                itemView.profileEditButton.visibility = View.VISIBLE
                itemView.profileEditButton.isEnabled = true
            }
            ProfileType.EMPTY -> {
                itemView.profileFollowButton.visibility = View.GONE
                itemView.profileSignUpButton.visibility = View.VISIBLE
                itemView.profileEditButton.visibility = View.GONE
                itemView.profileSignUpButton.isEnabled = true
            }
        }

        ImageDownloadUtil.getDownloadUtilInstance(itemView.context).loadUserImage(mProvider.user, itemView.profileUserImage)
    }

    private fun openEditProfileIfAvailable() {
        if (mProvider != null && mProvider.isOwner) {
            mProvider.onEditButtonClicked()
        }
    }

    private fun showSignUpView() {
        if (mProvider != null && mProvider.profileType == ProfileType.EMPTY) {
            mProvider.signUpOrLogin(itemView.context)
        }
    }
}
