package io.piqued.views.profile.edit;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import io.piqued.R;
import io.piqued.model.EditUserData;
import io.piqued.database.user.User;

/**
 * Created by Kenny M. Liou on 9/30/17.
 * Piqued Inc.
 */

public class EditTitleViewHolder extends InfoViewHolder {

    EditTitleViewHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_edit_profile_title, parent, false));
    }

    @Override
    void onBind(InfoType type, @NonNull User user, @NonNull EditUserData userData) {


    }
}
