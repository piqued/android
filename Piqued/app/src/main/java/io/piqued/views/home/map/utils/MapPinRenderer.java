package io.piqued.views.home.map.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.LongSparseArray;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import io.piqued.R;
import io.piqued.database.post.CloudPost;
import io.piqued.databinding.ViewBasketPinBinding;
import io.piqued.databinding.ViewClusterPinBinding;
import io.piqued.module.GlideApp;
import io.piqued.database.util.PiquedLogger;

/**
 * Created by Kenny M. Liou on 8/9/17.
 * Piqued Inc.
 */

public class MapPinRenderer extends DefaultClusterRenderer<MapItem> {

    private static final String TAG = MapPinRenderer.class.getSimpleName();

    private final LongSparseArray<Marker> mMarkerToPostMap = new LongSparseArray<>();

    private final Context mContext;
    private final ClusterManager<MapItem> mManager;
    private final PiquedIconGenerator mClusterIconGenerator;
    private final PiquedIconGenerator mItemGenerator;
    private final int mImageSize;
    private final ViewBasketPinBinding mBasketBinding;
    private final ViewClusterPinBinding mClusterBinding;

    public MapPinRenderer(Context context,
                   GoogleMap map,
                   ClusterManager<MapItem> clusterManager) {
        super(context, map, clusterManager);

        mContext = context;
        mManager = clusterManager;
        mClusterIconGenerator = new PiquedIconGenerator(mContext);
        mItemGenerator = new PiquedIconGenerator(mContext);

        mImageSize = (int) mContext.getResources().getDimension(R.dimen.map_pin_size_normal);

        View basketPin = LayoutInflater.from(mContext).inflate(R.layout.view_basket_pin, null);
        mBasketBinding = ViewBasketPinBinding.bind(basketPin);
        basketPin.measure(mImageSize, mImageSize);

        View clusterView = LayoutInflater.from(mContext).inflate(R.layout.view_cluster_pin, null);
        mClusterBinding = ViewClusterPinBinding.bind(clusterView);
        clusterView.measure(mImageSize, mImageSize);

        mItemGenerator.setContentView(basketPin);
        mClusterIconGenerator.setContentView(clusterView);
    }

    @Override
    protected void onBeforeClusterItemRendered(MapItem item, MarkerOptions markerOptions) {

        mBasketBinding.imageView.setImageResource(R.drawable.map_image_placeholder);

        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(mClusterIconGenerator.makeIcon()));
    }

    @Override
    protected void onClusterItemRendered(MapItem clusterItem, Marker marker) {

        CloudPost post = clusterItem.getPost();

        mMarkerToPostMap.append(post.getPostId(), marker);

        downloadAndUpdateImage(post);
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<MapItem> cluster, MarkerOptions markerOptions) {

        mClusterBinding.imageView.setImageResource(R.drawable.map_image_placeholder);
        mClusterBinding.clusterCount.setText(Integer.toString(cluster.getItems().size()));

        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(mClusterIconGenerator.makeIcon()));
    }

    @Override
    protected void onClusterRendered(Cluster<MapItem> cluster, Marker marker) {

        CloudPost mostRecentPost = null;

        for (MapItem item : cluster.getItems()) {
            if (mostRecentPost == null) {
                mostRecentPost = item.getPost();
            } else {
                if (mostRecentPost.getCreatedAt() < item.getPost().getCreatedAt()) {
                    mostRecentPost = item.getPost();
                }
            }
        }

        if (mostRecentPost != null) {

            downloadAndUpdateCluster(marker, mostRecentPost, cluster.getSize());
        }
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster<MapItem> cluster) {
        return cluster.getSize() > 1;
    }

    private void downloadAndUpdateImage(final CloudPost post) {

        PiquedLogger.d(TAG, "trying to download image for post: " + post.getPostId() + ", " + this.toString());

        GlideApp.with(mContext)
                .load(post.getSmallImageUrl())
                .centerCrop()
                .override(mImageSize, mImageSize)
                .into(new MapPinTarget(mContext) {
                    @Override
                    void onImageReady(Drawable resource) {
                        PiquedLogger.d(TAG, "download image for post " + post.getPostId() + " successful");

                        Marker marker = mMarkerToPostMap.get(post.getPostId());

                        if (marker == null) {
                            for (Marker m : mManager.getMarkerCollection().getMarkers()) {
                                if (m.getPosition().equals(post.getLatLng())) {
                                    marker = m;
                                    break;
                                }
                            }
                        }

                        if (marker != null) {
                            updateImage(marker, resource);
                        }
                    }
                });
    }

    private void downloadAndUpdateCluster(final Marker marker, final CloudPost post, final int clusterSize) {

        PiquedLogger.d(TAG, "trying to download image for cluster");

        GlideApp.with(mContext)
                .load(post.getSmallImageUrl())
                .centerCrop()
                .override(mImageSize, mImageSize)
                .into(new MapPinTarget(mContext) {
                    @Override
                    void onImageReady(Drawable resource) {
                        PiquedLogger.d(TAG, "download image for cluster successful");

                        updateClusterImage(marker, resource, clusterSize);
                    }
                });
    }

    private void updateImage(Marker marker, Drawable image) {

        if (marker != null && marker.isVisible()) {

            if (marker.getPosition() != null) {

                mBasketBinding.imageView.setImageDrawable(image);

                marker.setIcon(BitmapDescriptorFactory.fromBitmap(mItemGenerator.makeIcon()));
            }
        }
    }

    private void updateClusterImage(Marker marker, Drawable image, int clusterCount) {
        if (marker != null && marker.isVisible()) {

            mClusterBinding.imageView.setImageDrawable(image);
            mClusterBinding.clusterCount.setText(Integer.toString(clusterCount));

            marker.setIcon(BitmapDescriptorFactory.fromBitmap(mClusterIconGenerator.makeIcon()));

            PiquedLogger.d(TAG, "update cluster marker image successful");

        }
    }
}
