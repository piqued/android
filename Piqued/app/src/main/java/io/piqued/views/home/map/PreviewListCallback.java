package io.piqued.views.home.map;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 7/6/17.
 * Piqued Inc.
 */

public interface PreviewListCallback {

    int getFullListHeight();
    int getFooterHeight();
}
