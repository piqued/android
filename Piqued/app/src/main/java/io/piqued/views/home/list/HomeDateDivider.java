package io.piqued.views.home.list;

import android.content.Context;

import java.util.concurrent.TimeUnit;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import io.piqued.R;
import io.piqued.database.util.PiquedLogger;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 6/7/17.
 * Piqued Inc.
 */

public enum HomeDateDivider {

    ONE_DAY_AGO(TimeUnit.DAYS.toMillis(1), "01", R.string.divider_day_ago, R.drawable.time_divider_01),
    TWO_DAYS_AGO(TimeUnit.DAYS.toMillis(2), "02", R.string.divider_days_ago, R.drawable.time_divider_02),
    THREE_DAYS_AGO(TimeUnit.DAYS.toMillis(3), "03", R.string.divider_days_ago, R.drawable.time_divider_03),
    FOUR_DAYS_AGO(TimeUnit.DAYS.toMillis(4), "04", R.string.divider_days_ago, R.drawable.time_divider_04),
    FIVE_DAYS_AGO(TimeUnit.DAYS.toMillis(5), "05", R.string.divider_days_ago, R.drawable.time_divider_05),
    SIX_DAYS_AGO(TimeUnit.DAYS.toMillis(6), "06", R.string.divider_days_ago, R.drawable.time_divider_06),
    ONE_WEEK_AGO(TimeUnit.DAYS.toMillis(7), "01", R.string.divider_week_ago, R.drawable.time_divider_07),
    TWO_WEEKS_AGO(TimeUnit.DAYS.toMillis(14), "02", R.string.divider_weeks_ago, R.drawable.time_divider_14),
    THREE_WEEKS_AGO(TimeUnit.DAYS.toMillis(21), "03", R.string.divider_weeks_ago, R.drawable.time_divider_21),
    FOUR_WEEKS_AGO(TimeUnit.DAYS.toMillis(30), "04",R.string.divider_weeks_ago, R.drawable.time_divider_30),
    ONE_MONTH_AND_MORE(TimeUnit.DAYS.toMillis(60), "01+", R.string.divider_months_ago, R.drawable.time_divider_60);

    private static final String TAG = HomeDateDivider.class.getSimpleName();
    private static final HomeDateDivider[] values = HomeDateDivider.values();

    public static int getValueSize() {
        return values.length;
    }

    public static HomeDateDivider fromInt(int index) {
        if (index > -1 && index < values.length) {
            return values[index];
        }

        PiquedLogger.i(TAG, "invalid enum index value: " + index + " for enum " + TAG);
        return ONE_MONTH_AND_MORE;
    }

    private final long mTimeInMilli;

    private String mDigitalValue;
    @StringRes
    private final int mStringRes;
    @DrawableRes
    private final int mBackgroundRes;

    HomeDateDivider(long timeInMilli, String digitalValue, @StringRes int stringRes, @DrawableRes int backgroundRes) {
        mTimeInMilli = timeInMilli;
        mDigitalValue = digitalValue;
        mStringRes = stringRes;
        mBackgroundRes = backgroundRes;
    }

    public long getTimeInMilli() {
        return mTimeInMilli;
    }

    public String getTimeString(Context context) {

        return mDigitalValue + " " + context.getString(mStringRes);
    }

    public int getBackgroundRes() {
        return mBackgroundRes;
    }
}
