package io.piqued.views.profile

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.GsonBuilder
import com.ncapdevi.fragnav.FragNavController
import io.piqued.R
import io.piqued.activities.EditProfileActivity
import io.piqued.activities.SettingsActivity
import io.piqued.api.PiquedApiCallback
import io.piqued.cloud.Status
import io.piqued.cloud.repository.PostRepository
import io.piqued.cloud.util.Resource
import io.piqued.database.UserId
import io.piqued.database.post.CloudPost
import io.piqued.database.post.PostId
import io.piqued.database.user.User
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.login.NewLoginActivity
import io.piqued.utils.*
import io.piqued.utils.modelmanager.PiquedUserManager
import io.piqued.utils.postmanager.PostManager
import io.piqued.views.HomeActivityFragment
import io.piqued.views.home.PostSortStyle
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.toolbar_sub.view.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 9/7/18.
 * Piqued Inc.
 *
 */
class ProfileFragment: HomeActivityFragment(), ProfileDataProvider {

    /**
     * Static methods
     */
    companion object {
        private val TAG = ProfileFragment::class.java.simpleName
        private const val KEY_USER_ID = "user_id"
        private const val KEY_IS_ROOT = "is_root"

        @JvmStatic
        fun newInstance(): ProfileFragment {
            return newInstance(-1, true)
        }

        @JvmStatic
        fun newInstance(post: CloudPost): ProfileFragment {
            return newInstance(post.userId)
        }

        @JvmStatic
        fun newInstance(userId: Long): ProfileFragment {
            return newInstance(userId, false)
        }

        private fun newInstance(userId: Long, isRoot: Boolean): ProfileFragment {
            val args = Bundle()

            args.putLong(KEY_USER_ID, userId)
            args.putBoolean(KEY_IS_ROOT, isRoot)

            val fragment = ProfileFragment()
            fragment.arguments = args

            return fragment
        }
    }

    private data class ProfileIsRoot(
            val isRoot: Boolean
    )

    @Inject
    internal lateinit var userManager: PiquedUserManager
    @Inject
    internal lateinit var postManager: PostManager
    @Inject
    internal lateinit var preferences: Preferences

    private lateinit var adapter: NewProfileViewAdapter
    private lateinit var userId: UserId
    private lateinit var isRoot: ProfileIsRoot
    override var profileType: ProfileType = ProfileType.EMPTY
    private var userObject: User? = null
    private var isFetchingPosts = false
    private var pauseTimeStamp: Long = -1

    /**
     * Life Cycle related methods
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        if (arguments == null) {
            throw IllegalArgumentException("Missing arguments when open profile")
        }

        userId = UserId(arguments!!.getLong(KEY_USER_ID))
        isRoot = ProfileIsRoot(arguments!!.getBoolean(KEY_IS_ROOT))

        updateProfileType()
        setHasOptionsMenu(profileType != ProfileType.EMPTY)

        adapter = NewProfileViewAdapter(this, postManager)

        if (savedInstanceState != null) {
            val postIdsInJsonString = savedInstanceState.getString(Constants.KEY_POSTS_LOADED)

            val postIds = GsonBuilder().create().fromJson<List<PostId>>(postIdsInJsonString, Constants.POST_LIST_TYPE)

            adapter.insertPostIds(postIds)
        }

        performInitialFetch(userId)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // create layout manager
        val gridLayoutManager = GridLayoutManager(context!!, Constants.DEFAULT_SPAN_SIZE)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {

                return when (ProfileViewType.fromInt(adapter.getItemViewType(position))) {
                    ProfileViewType.SINGLE_POST -> 1
                    else -> Constants.DEFAULT_SPAN_SIZE
                }
            }
        }

        profileRecyclerView.adapter = adapter
        profileRecyclerView.layoutManager = gridLayoutManager
        profileRecyclerView.addItemDecoration(ProfileItemDecoration(Constants.DEFAULT_SPAN_SIZE - 1, view.context))

        ViewUtil.initializeSwipeRefreshLayout(
                profileSwipeRefreshLayout,
                onRefreshListener
        )

        profileRootView.setOnApplyWindowInsetsListener { _, windowInsets ->
            val params = profileBottomPlaceHolder.layoutParams as ConstraintLayout.LayoutParams
            params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, windowInsets.systemWindowInsetBottom)
            windowInsets.consumeSystemWindowInsets()
        }
    }

    override fun onResume() {
        super.onResume()

        setSupportActionBar(profileToolbar.profile_toolbar, R.string.tab_profile, !isRoot.isRoot)
        BroadcastUtil.registerReceiverWithMultipleAction(context!!, actionReceiver, Constants.ACTION_POST_UPLOADED, Constants.ACTION_POST_DELETED)

        val shouldReFetch = profileType == ProfileType.EMPTY && preferences.userLoggedIn()

        updateProfileType()
        setHasOptionsMenu(profileType != ProfileType.EMPTY)

        if (profileType != ProfileType.EMPTY && pauseTimeStamp > 0 && TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - pauseTimeStamp) > 1 || shouldReFetch) {
            profileSwipeRefreshLayout.isRefreshing = true
            performInitialFetch(userId)
        }

        adapter.validatePosts(postManager)
    }

    override fun onPause() {

        LocalBroadcastManager.getInstance(context!!).unregisterReceiver(actionReceiver)

        pauseTimeStamp = System.currentTimeMillis()

        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (profileType == ProfileType.OWNER) {
            inflater.inflate(R.menu.profile_own_menu, menu)
        } else {
            super.onCreateOptionsMenu(menu, inflater)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            R.id.settings -> {
                UIHelper.pushActivity(this, SettingsActivity::class.java)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString(Constants.KEY_POSTS_LOADED, GsonBuilder().create().toJson(adapter.postIds, Constants.POST_LIST_TYPE))
        outState.putLong(Constants.KEY_LAST_LOAD_TIME, pauseTimeStamp)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.EDIT_USER_REQUEST_CODE) {
            if (resultCode == Constants.EDIT_USER_SUCCESS) {
                // success
                fetchUser(object: PiquedApiCallback<User> {
                    override fun onSuccess(data: User?) {
                        userObject = data
                        adapter.notifyUserDataUpdated()
                    }

                    override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                        PiquedLogger.e(TAG, "Failed to fetch user", errorCode, payload)
                    }

                })
            }
        }
    }

    /**
     * Some other methods
     */

    override fun onTabDoubleTapped(backStackController: FragNavController) {
        val layoutManager = profileRecyclerView.layoutManager as LinearLayoutManager

        if (layoutManager.findFirstVisibleItemPosition() == 0 && profileRecyclerView.computeVerticalScrollOffset() == 0) {

            val backStack = backStackController.currentStack
            if (backStack != null && backStack.size > 1) {
                backStackController.popFragments(backStack.size - 1)
            }
        } else {
            profileRecyclerView.smoothScrollToPosition(0)
        }
    }

    private fun updateProfileType() {
        profileType = if (userId.userId < 0) {
            if (preferences.userLoggedIn()) {
                ProfileType.OWNER
            } else {
                ProfileType.EMPTY
            }
        } else {
            if (userManager.isAccountOwner(userId.userId)) {
                ProfileType.OWNER
            } else {
                ProfileType.USER
            }
        }
    }

    private fun performInitialFetch(userId: UserId) {
        updateProfileType()

        when (profileType) {
            ProfileType.OWNER -> {
                postManager.getAccountOwnerPostHistoryAsLiveData().observe(this, postLiveDataCallback)
                fetchData()
            }
            ProfileType.USER -> {
                postManager.getUserPostHistoryAsLiveData(userId).observe(this, postLiveDataCallback)
                fetchData()
            }
            else -> {
                // do nothing
            }
        }
    }

    private fun fetchDataForUser(user: User) {
        if (!isFetchingPosts) {
            isFetchingPosts = true

            if (adapter.postIds.size < user.totalPostCount) {
                val page = adapter.postIds.size / PostRepository.DEFAULT_POST_BATCH_SIZE + 1

                if (page > 1 || (page == 1 && adapter.postIds.size == 0)) {
                    if (userManager.isAccountOwner(user)) {
                        postManager.getAccountOwnerPostHistory(page)
                    } else {
                        postManager.getUserPostHistory(userId, page)
                    }
                } else {
                    isFetchingPosts = false
                }
            } else {
                isFetchingPosts = false
            }
        }
    }

    private fun fetchUser(callback: PiquedApiCallback<User>) {
        if (userId.userId > -1) {
            userManager.getUserInfo(userId.userId, callback)
        } else {
            val user = userManager.myUserRecord

            if (user != null) {
                callback.onSuccess(user)
            }

            userManager.fetchMyUserRecordFromCloud(callback)
        }
    }

    private fun fetchData() {
        fetchUser(object : PiquedApiCallback<User> {
            override fun onSuccess(data: User?) {
                userObject = data

                if (data != null) {
                    adapter.notifyUserDataUpdated()

                    fetchDataForUser(data)
                }
            }

            override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                PiquedLogger.e(TAG, "Failed to fetch user: ${userId.userId}", errorCode, payload)
            }

        })
    }

    /**
     * ProfileDataProvider implementation
     */
    override fun onOpenBasket(postId: PostId) {
        if (postId.postId > -1) {
            openBasket(postId)
        }
    }

    override fun displayItem(totalCountSoFar: Int, itemIndex: Int) {
        val currentUser = userObject
        if (currentUser != null) {
            if (!isFetchingPosts
                    && totalCountSoFar < currentUser.totalPostCount
                    && itemIndex >= totalCountSoFar * 0.75) {
                fetchDataForUser(currentUser)
            }
        }

    }

    override fun setUserFollowed(follow: Boolean) {
        val currentUser = userObject
        if (currentUser != null && !userManager.isAccountOwner(currentUser)) {
            postManager.followUser(currentUser.userId, follow, object : PiquedApiCallback<Long> {
                override fun onSuccess(data: Long?) {
                    userManager.updateUserFollowStatus(currentUser.userId, follow)
                }

                override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                    // TODO: show UI error message
                    PiquedLogger.e(TAG, "failed to follow user")
                }

            })
        }
    }

    override fun onEditButtonClicked() {
        EditProfileActivity.editMyProfile(this)
    }

    override fun onSwitchToView(listView: PostSortStyle?) {
        // TODO: implement me
    }

    override val isUserLoggedIn: Boolean
        get() = userManager.isUserLoggedIn

    override val isFollowed: Boolean
        get() = userObject?.isFollowed ?: false

    override val isUserReady: Boolean
        get() = userObject != null

    override val isOwner: Boolean
        get() = profileType == ProfileType.OWNER

    override val likeCount: Int
        get() = userObject?.likeCount ?: 0

    override val commentCount: Int
        get() = userObject?.commentCount ?: 0

    override val bookmarkCount: Int
        get() = userObject?.bookmarkCount ?: 0

    override val followingCount: Int
        get() = userObject?.followingCount ?: 0

    override val followersCount: Int
        get() = userObject?.followerCount ?: 0

    override val sharesCount: Int
        get() = userObject?.totalPostCount ?: 0

    override val userImage: String?
        get() = userObject?.profileImageUrl ?: ""

    override val user: User?
        get() = userObject

    override fun getUserName(c: Context): String {
        return if (profileType == ProfileType.EMPTY) {
            c.getString(R.string.profile_guest_user)
        } else {
            userObject?.name ?: ""
        }
    }

    override fun signUpOrLogin(c: Context) {
        UIHelper.startActivity(c, NewLoginActivity::class.java)
    }

    /**
     * Callbacks
     */
    private val postLiveDataCallback = Observer<Resource<List<CloudPost>>> { t ->
        if (t != null) {
            if (t.status == Status.SUCCESS) {
                isFetchingPosts = false
                profileSwipeRefreshLayout.isRefreshing = false
            }

            val result = t.data
            if (result != null && result.isNotEmpty()) {
                postManager.addCloudPostToMemoryCache(result)
                adapter.insertPosts(result)
            }
        }
    }

    private val onRefreshListener = SwipeRefreshLayout.OnRefreshListener {
        if (isUIActive && profileType != ProfileType.EMPTY) {
            adapter.clearData();
            fetchData()
        } else {
            profileSwipeRefreshLayout.isRefreshing = false
        }
    }

    private val actionReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null) {
                if (Constants.ACTION_POST_UPLOADED == intent.action) {
                    fetchData()
                } else if (Constants.ACTION_POST_DELETED == intent.action) {
                    adapter.validatePosts(postManager)
                }
            }
        }
    }
}