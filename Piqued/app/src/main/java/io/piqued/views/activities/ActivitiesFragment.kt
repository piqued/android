package io.piqued.views.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.ncapdevi.fragnav.FragNavController
import io.piqued.R
import io.piqued.api.PiquedApiCallback
import io.piqued.basket.BasketFragment
import io.piqued.database.post.PostId
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.utils.postmanager.PostManager
import io.piqued.views.DoubleTapFragment
import io.piqued.views.HomeActivityFragment
import io.piqued.views.activities.util.ActivityFragmentActionBroadcast
import kotlinx.android.synthetic.main.fragment_activities.*
import kotlinx.android.synthetic.main.toolbar_sub.view.*
import javax.inject.Inject

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/29/16.
 * Piqued Inc.
 */

class ActivitiesFragment : HomeActivityFragment() {

    @Inject
    internal lateinit var mPostManager: PostManager

    private var fragNavController: FragNavController? = null

    private val broadcastReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            if (isUIActive) {
                if (ActivityFragmentActionBroadcast.isActionOpenPost(intent) && ActivityFragmentActionBroadcast.getPostId(intent) > -1) {

                    val postId = ActivityFragmentActionBroadcast.getPostId(intent)

                    mPostManager.fetchPost(ActivityFragmentActionBroadcast.getPostId(intent), object : PiquedApiCallback<PostId> {
                        override fun onSuccess(data: PostId?) {
                            if (isUIActive) {
                                pushFragment(BasketFragment.newInstance(postId))
                            }
                        }

                        override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                            PiquedLogger.e(TAG, "Failed to fetch post with id: $postId", errorCode, payload)
                            if (isUIActive) {
                                // TODO: 8/5/17 show error message
                            }
                        }

                    })
                }
            }
        }
    }

    override fun getTagName(): String {
        return getStaticTagName()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_activities, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (fragNavController == null) {
            val builder = FragNavController.newBuilder(savedInstanceState, childFragmentManager, R.id.fragmentContainer)

            builder.rootFragment(YourActivityFragment.newInstance())

            fragNavController = builder.build()
        }

        rootView.setOnApplyWindowInsetsListener { _, windowInsets ->
            val params = bottomPlaceHolder.layoutParams as ConstraintLayout.LayoutParams
            params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, windowInsets.systemWindowInsetBottom)
            windowInsets.consumeSystemWindowInsets()
        }
    }

    override fun onResume() {
        super.onResume()

        updateActionBar(nav_toolbar.profile_toolbar, R.string.activity_title, false)

        ActivityFragmentActionBroadcast.registerForAction(context!!, broadcastReceiver)
    }

    override fun onPause() {

        ActivityFragmentActionBroadcast.unregisterAction(context!!, broadcastReceiver)

        super.onPause()
    }

    override fun onTabDoubleTapped(backStackController: FragNavController) {
        val controller = fragNavController
        if (controller != null) {
            val fragment = controller.currentFrag

            (fragment as? DoubleTapFragment)?.onTabDoubleTapped(controller)
        }
    }

    companion object {

        private val TAG = ActivitiesFragment::class.java.simpleName

        @JvmStatic
        fun getStaticTagName(): String {
            return TAG
        }

        @JvmStatic
        fun newInstance(): ActivitiesFragment {
            return ActivitiesFragment()
        }
    }
}
