package io.piqued.views.home.map.utils

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.bumptech.glide.request.Request
import com.bumptech.glide.request.target.SizeReadyCallback
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition

/**
 *
 * Created by Kenny M. Liou on 9/6/18.
 * Piqued Inc.
 *
 */

abstract class PostMarkerTarget(private val markerSize: Int): Target<Bitmap> {

    override fun onLoadStarted(placeholder: Drawable?) {

    }

    override fun onLoadFailed(errorDrawable: Drawable?) {

    }

    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
        onImageReady(resource)
    }

    override fun onLoadCleared(placeholder: Drawable?) {

    }

    override fun getSize(cb: SizeReadyCallback) {
        cb.onSizeReady(markerSize, markerSize)
    }

    override fun removeCallback(cb: SizeReadyCallback) {

    }

    override fun setRequest(request: Request?) {

    }

    override fun getRequest(): Request? {
        return null
    }

    override fun onStart() {

    }

    override fun onStop() {

    }

    override fun onDestroy() {

    }

    abstract fun onImageReady(resource: Bitmap)
}