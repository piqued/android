package io.piqued.views.profile

import android.view.LayoutInflater
import android.view.ViewGroup

import io.piqued.R
import io.piqued.utils.postmanager.PostManager
import io.piqued.utils.postorganization.PostLocationGroup
import kotlinx.android.synthetic.main.view_holder_profile_post_title.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/25/17.
 * Piqued Inc.
 */

class ProfilePostTitleViewHolder(parent: ViewGroup)
    : ProfileViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_profile_post_title, parent, false)
) {

    internal override fun onProviderReady(provider: ProfileDataProvider) {

    }

    override fun setViewItem(item: ProfileViewItem, manager: PostManager) {
        val group = item.group

        itemView.locationText.text = group.getLocationString(context)
        itemView.timeText.text = group.getDateString(context)
    }
}
