package io.piqued.views.activities

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.database.event.Event
import io.piqued.utils.EventComparator
import java.util.*

/**
 * Created by Kenny M. Liou on 8/11/17.
 * Piqued Inc.
 */
class ActivitiesAdapter(
        private val mDataHandler: DataHandler,
        private val mHandler: YourActivityViewHolder.Handler)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface DataHandler {
        fun fetchMoreDate()
    }

    private val loadedEvents = ArrayList<Event>()
    private val eventComparator = EventComparator()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0) {
            YourActivityEmptyViewHolder(parent)
        } else {
            YourActivityViewHolder(parent, mHandler)
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is YourActivityViewHolder) {
            viewHolder.setEvent(loadedEvents[position])
            if (position == loadedEvents.size - 1) {
                mDataHandler.fetchMoreDate()
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (loadedEvents.isEmpty()) 0 else 1
    }

    override fun getItemCount(): Int {
        return if (loadedEvents.isNotEmpty()) {
            loadedEvents.size
        } else {
            1 // empty state
        }
    }

    // NOTE: return added count
    fun updateData(events: List<Event>) {
        loadedEvents.clear()
        loadedEvents.addAll(events)
        loadedEvents.sortWith(eventComparator)
    }
}