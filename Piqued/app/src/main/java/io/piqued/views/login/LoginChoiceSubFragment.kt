package io.piqued.views.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.piqued.R
import kotlinx.android.synthetic.main.subfragment_login_buttons.*

/**
 *
 * Created by Kenny M. Liou on 1/20/18.
 * Piqued Inc.
 *
 */

class LoginChoiceSubFragment() : LoginSignUpBaseFragment() {

    companion object {
        fun newInstance(handler: LoginSignUpHandler) : LoginChoiceSubFragment {

            val fragment = LoginChoiceSubFragment()
            fragment.mHandler = handler

            return fragment
        }
    }

    private lateinit var mHandler : LoginSignUpHandler

    override fun getTagName(): String {
        return LoginChoiceSubFragment::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.subfragment_login_buttons, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // setup facebook login
        facebook_login.setOnClickListener {
            getHandler().performFacebookLogin()
        }
        // setup regular login
        login.setOnClickListener {
            getHandler().goToFragment(this, PiquedLoginSubFragment.newInstance(getHandler()))
        }
    }

    override fun onResume() {
        super.onResume()

        hideSoftKeyboard()

        getHandler().configureBottom(this, BottomButtonOption.SIGN_UP, BottomOption.DISCLAIMER)
    }

    override fun getHandler(): LoginSignUpHandler {
        return mHandler
    }
}