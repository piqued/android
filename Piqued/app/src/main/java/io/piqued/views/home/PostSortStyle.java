package io.piqued.views.home;

import androidx.annotation.DrawableRes;

import io.piqued.R;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/14/17.
 * Piqued Inc.
 */

public enum PostSortStyle {
    LIST_VIEW(R.drawable.map_toggle),
    MAP_VIEW(R.drawable.explorer_toggle);


    private static final PostSortStyle[] values = PostSortStyle.values();

    @DrawableRes
    private final int mImageRes;

    PostSortStyle(@DrawableRes int imageRes) {
        mImageRes = imageRes;
    }

    @DrawableRes
    public int getSearchBoxButtonImage() {
        return mImageRes;
    }

    public static PostSortStyle fromInt(int index) {

        if (index > -1 && index < values.length) {
            return values[index];
        }

        throw new IllegalArgumentException("Illegal index value: " + index + " for enum PostSortStyle");
    }

    public static int getItemCount() {
        return values.length;
    }
}

