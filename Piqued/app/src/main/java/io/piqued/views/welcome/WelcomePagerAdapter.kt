package io.piqued.views.welcome

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/17/16.
 * Piqued Inc.
 */

class WelcomePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

    override fun getItemCount(): Int {
        return WelcomeStep.values.size
    }

    override fun createFragment(position: Int): Fragment {
        return WelcomeSubFragment.getInstance(WelcomeStep.fromInt(position))
    }
}
