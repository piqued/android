package io.piqued.views

import io.piqued.PiquedFragment
import io.piqued.activities.HomeBaseActivity
import io.piqued.basket.BasketFragment
import io.piqued.database.post.PostId

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 6/3/17.
 * Piqued Inc.
 */

abstract class HomeActivityFragment : DoubleTapFragment() {

    override fun pushFragment(fragment: PiquedFragment?) {

        if (fragment == null) return

        val activity = piquedActivity as HomeBaseActivity

        activity.pushSubFragment(fragment)
    }

    fun openBasket(postId: PostId) {

        val fragment = BasketFragment.newInstance(postId)

        pushFragment(fragment)
    }

    fun openUserProfile(userId: Long) {
        val activity = piquedActivity as HomeBaseActivity
        activity.openUserProfile(userId)
    }

    open fun onBackPressed(): Boolean {
        return false
    }

    fun setNavigationBarHidden(hidden: Boolean, animate: Boolean) {

        val activity = piquedActivity as HomeBaseActivity
        activity.setNavBarHidden(hidden, animate)
    }
}
