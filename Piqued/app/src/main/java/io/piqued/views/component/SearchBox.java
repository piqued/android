package io.piqued.views.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.concurrent.TimeUnit;

import androidx.core.content.ContextCompat;
import io.piqued.R;
import io.piqued.databinding.ComponentSearchBoxBinding;
import io.piqued.views.home.PostSortStyle;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/2/17.
 * Piqued Inc.
 */

public class SearchBox extends LinearLayout {

    private static final long DEFAULT_SHRINK_ANIMATION_DURATION = TimeUnit.MILLISECONDS.toMillis(400);
    private static final long DEFAULT_EXPAND_ANIMATION_DURATION = TimeUnit.MILLISECONDS.toMillis(200);
    private ComponentSearchBoxBinding mBinding;
    private SearchBoxListener mListener;
    private PostSortStyle mType;
    private boolean mIsAnimating = false;
    private boolean mIsShrunk = false;

    public SearchBox(Context context) {
        this(context, null);
    }

    public SearchBox(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (!isInEditMode()) {
            LayoutInflater inflater = LayoutInflater.from(context);

            mBinding = ComponentSearchBoxBinding.inflate(inflater, this, true);
            mBinding.toggleButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onButtonPressed(mType);
                    }
                }
            });
        }
    }

    public void setIsShrunk(boolean isShrunk, boolean animate) {
        if (animate) {
            setIsShrunk(isShrunk);
        } else {

            mIsShrunk = isShrunk;

            final float value_h = isShrunk ? getResources().getDimension(R.dimen.shrunk_padding_h) : getResources().getDimension(R.dimen.normal_padding);
            final float value_v = isShrunk ? getResources().getDimension(R.dimen.shrunk_padding_v) : getResources().getDimension(R.dimen.normal_padding);
            final int valueIntH = Math.round(value_h);
            final int valueIntV = Math.round(value_v);
            final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mBinding.searchTextBox.getLayoutParams();
            params.setMargins(valueIntH, valueIntV, valueIntH, valueIntV);

            mBinding.searchTextBox.setLayoutParams(params);

            if (mIsShrunk) {
                mBinding.searchTextBox.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_corner_3));
            } else {
                mBinding.searchTextBox.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
            }
        }
    }

    public boolean isShrunk() {
        return mIsShrunk;
    }

    public void setIsShrunk(final boolean isShrunk) {

        if (isShrunk != mIsShrunk) {
            mIsAnimating = true;

            mIsShrunk = isShrunk;

            final float marginSize = getResources().getDimension(R.dimen.shrunk_padding_h);;
            final float startValue;
            final float endValue;

            if (isShrunk) {
                startValue = getResources().getDimension(R.dimen.normal_padding);
                endValue = getResources().getDimension(R.dimen.shrunk_padding_h);
            } else {
                startValue = getResources().getDimension(R.dimen.shrunk_padding_h);
                endValue = getResources().getDimension(R.dimen.normal_padding);
            }

            Animation marginAnimation = new Animation() {

                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {

                    final int value;

                    if (isShrunk) {
                        value = (int) (interpolatedTime * marginSize);
                    } else {
                        value = (int) ((1 - interpolatedTime) * marginSize);
                    }

                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mBinding.searchTextBox.getLayoutParams();
                    params.setMargins(value, value / 2, value, value / 2);

                    mBinding.searchTextBox.setLayoutParams(params);
                }
            };

            if (isShrunk) {
                marginAnimation.setDuration(DEFAULT_SHRINK_ANIMATION_DURATION);
            } else {
                marginAnimation.setDuration(DEFAULT_EXPAND_ANIMATION_DURATION);
            }
            marginAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mIsAnimating = false;

                    if (mIsShrunk) {
                        mBinding.searchTextBox.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_corner_3));
                    } else {
                        mBinding.searchTextBox.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            mBinding.searchTextBox.startAnimation(marginAnimation);
        }
    }

    public void setType(PostSortStyle type) {

        mType = type;
        mBinding.toggleButton.setImageDrawable(ContextCompat.getDrawable(getContext(), type.getSearchBoxButtonImage()));
    }

    public void setSearchBoxListener(SearchBoxListener listener) {
        mListener = listener;
    }
}
