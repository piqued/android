package io.piqued.views.profile.edit

import android.text.Editable
import android.text.InputType
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R
import io.piqued.database.user.User
import io.piqued.model.EditUserData
import kotlinx.android.synthetic.main.view_holder_edit_text.view.*

/**
 * Created by Kenny M. Liou on 9/30/17.
 * Piqued Inc.
 */

class EditTextViewHolder(parent: ViewGroup, private val mCallback: Callback)
    : InfoViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_edit_text, parent, false)
) {
    private var mType: InfoType? = null
    private var mIgnoreValueUpdate = false

    private val mTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun afterTextChanged(editable: Editable) {
            if (!mIgnoreValueUpdate) {
                mCallback.onValueChanged(mType!!, editable.toString().trim { it <= ' ' })
            }
        }
    }

    interface Callback {
        fun onValueChanged(type: InfoType, newValue: String)
    }

    init {
        itemView.edit_text.addTextChangedListener(mTextWatcher)
    }

    internal override fun onBind(type: InfoType, user: User, editUserData: EditUserData) {
        if (type == InfoType.NAME) {
            if (!TextUtils.isEmpty(editUserData.name)) {
                onBind(type, editUserData.name)
            } else {
                onBind(type, user.name)
            }
        }
    }

    internal fun onBind(type: InfoType, value: String?) {

        mType = type

        mIgnoreValueUpdate = true
        itemView.edit_text.setText(value)
        mIgnoreValueUpdate = false

        if (type == InfoType.PASSWORD) {
            itemView.edit_text.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
            itemView.icon_view.setImageResource(R.drawable.profile_password)
        } else if (type == InfoType.EMAIL) {
            itemView.edit_text.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            itemView.icon_view.setImageResource(R.drawable.profile_email)
        }
    }
}
