package io.piqued.views.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.piqued.R
import kotlinx.android.synthetic.main.subfragment_new_account_email.*

/**
 *
 * Created by Kenny M. Liou on 1/21/18.
 * Piqued Inc.
 *
 */
class NewAccountEmailSubFragment : LoginSignUpBaseFragment() {

    companion object {

        fun newInstance(handler: LoginSignUpHandler) : NewAccountEmailSubFragment {
            val fragment = NewAccountEmailSubFragment()
            fragment.mHandler = handler

            return fragment
        }
    }

    private lateinit var mHandler: LoginSignUpHandler

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.subfragment_new_account_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        next_button.setOnClickListener {
            getHandler().createAccountWithEmail(this, email_input.text.toString())
        }

        email_input.addTextChangedListener(mTextWatchListener)
    }

    override fun onResume() {
        super.onResume()

        next_button.visibility = View.VISIBLE

        getHandler().configureBottom(this, BottomButtonOption.SIGN_IN, BottomOption.EMPTY)

        showSoftKeyboard(email_input)
    }

    override fun onPause() {

        next_button.visibility = View.GONE
        super.onPause()
    }

    override fun getHandler(): LoginSignUpHandler {
        return mHandler
    }

    override fun getTagName(): String {
        return NewAccountEmailSubFragment::class.java.simpleName
    }

    private fun setNextButtonEnable(enable: Boolean) {

        next_button.isEnabled = enable
        next_button.alpha = if (enable) 1.0f else 0.3f
    }

    private val mTextWatchListener = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun afterTextChanged(p0: Editable?) {
            setNextButtonEnable(getHandler().isEmailValid(email_input.text.toString()))
        }
    }

}