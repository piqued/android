package io.piqued.views.home.list;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.piqued.database.post.PostId;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 6/6/17.
 * Piqued Inc.
 */

class HomeListViewItem {

    enum Type {
        TIME_DIVIDER,
        POST,
        EMPTY_POST,
        LOADING_MORE;

        private static final Type[] values = Type.values();

        public static Type fromInt(int index) {
            if (index > -1 && index < values.length) {
                return values[index];
            }

            throw new IllegalArgumentException("Illegal index value for enum " + Type.class.getSimpleName() + ": " + index);
        }
    }

    static HomeListViewItem newDivider(@NonNull HomeDateDivider divider) {
        return new HomeListViewItem(null, divider, Type.TIME_DIVIDER);
    }

    static HomeListViewItem newPostItem(@NonNull PostId postId) {
        return new HomeListViewItem(postId, null, Type.POST);
    }

    static HomeListViewItem newEmptyPostView() {
        return new HomeListViewItem(null, null, Type.EMPTY_POST);
    }

    static HomeListViewItem newLoadMorePostView() {
        return new HomeListViewItem(null, null, Type.LOADING_MORE);
    }

    private final PostId mPostId;
    private final HomeDateDivider mDividerValue;
    private final Type mType;

    private HomeListViewItem(@Nullable PostId postId,@Nullable HomeDateDivider divider, @NonNull Type type) {
        mPostId = postId;
        mDividerValue = divider;
        mType = type;
    }

    public Type getType() {
        return mType;
    }

    @Nullable
    public PostId getPostId() {
        return mPostId;
    }

    @Nullable
    HomeDateDivider getDividerValue() {
        return mDividerValue;
    }
}
