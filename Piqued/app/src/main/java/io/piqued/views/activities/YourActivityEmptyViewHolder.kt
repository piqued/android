package io.piqued.views.activities

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R

/**
 *
 * Created by Kenny M. Liou on 3/10/18.
 * Piqued Inc.
 *
 */
class YourActivityEmptyViewHolder(parent: ViewGroup) :
        RecyclerView.ViewHolder(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.view_holder_your_activity_empty,
                                parent,
                                false))