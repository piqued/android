package io.piqued.views.home.list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.piqued.R;
import io.piqued.activities.HomeBaseActivity;
import io.piqued.cloud.Status;
import io.piqued.cloud.util.Resource;
import io.piqued.database.bookmark.Bookmark;
import io.piqued.database.post.CloudPost;
import io.piqued.databinding.FragmentHomeListBinding;
import io.piqued.utils.BroadcastUtil;
import io.piqued.utils.Constants;
import io.piqued.utils.LocationCallback;
import io.piqued.utils.PiquedLocationManager;
import io.piqued.utils.ViewUtil;
import io.piqued.utils.modelmanager.PiquedUserManager;
import io.piqued.utils.postmanager.PostManager;
import io.piqued.views.component.SearchBoxListener;
import io.piqued.views.home.HomeFunctionProvider;
import io.piqued.views.home.HomeSubFragment;
import io.piqued.views.home.HomeUtil;
import io.piqued.views.home.PostSortStyle;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/30/16.
 * Piqued Inc.
 */

public class HomeListFragment extends HomeSubFragment {

    private static final String TAG = HomeListFragment.class.getSimpleName();

    private static final String SHRUNK = "shrunk";
    private static final int MIN_METER_DIFFERENCE = 5;


    private static final int VALUE_START_PAGE = 1;

    @Override
    public String getTagName() {
        return TAG;
    }

    public static void registerListScrollListener(Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, new IntentFilter(HomeUtil.ACTION_NOTIFY_UI_SHOULD_SHOW));
    }

    public static void unregisterListScrollListener(Context context, BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
    }

    public static boolean isUIShouldHideIntentAction(Intent intent) {
        return HomeUtil.ACTION_NOTIFY_UI_SHOULD_SHOW.equals(intent.getAction());
    }

    public static boolean shouldHideUI(Intent intent) {
        return intent.getBooleanExtra(HomeUtil.KEY_UI_SHOULD_HIDE, false);
    }

    public static HomeListFragment newInstance(HomeFunctionProvider provider) {
        return new HomeListFragment(provider);
    }

    @Inject
    PostManager mPostManager;
    @Inject
    PiquedUserManager mUserManager;
    @Inject
    PiquedLocationManager mLocationManager;

    private Location mCurrentLocation;

    private FragmentHomeListBinding mBinding;
    private HomePostListAdapter mPostAdapter;
    private int mCurrentPage = VALUE_START_PAGE; // page count starts with 1
    private int mOverallYScroll = 0;
    private long mLastModifiedTime = Long.MAX_VALUE;
    private long mLastLoadTime = -1;
    private boolean mCanScroll = false;
    private LinearLayoutManager mLayoutManager;

    private boolean isFetchingData = false;

    private final HashSet<Long> bookmarks = new HashSet<>();

    public HomeListFragment(HomeFunctionProvider provider) {
        super(provider);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setUserVisibleHint(false);

        getApplication().getComponent().inject(this);

        mPostAdapter = new HomePostListAdapter(
                getContext(),
                mUserManager,
                getProvider().getBasketActionHandler(),
                mListHandler);

        if (savedInstanceState != null) {
            mLastLoadTime = savedInstanceState.getLong(Constants.KEY_LAST_LOAD_TIME);
        }
        
        mPostManager.getBookmarksAsLiveData().observe(this, new Observer<Resource<List<Bookmark>>>() {

            @Override
            public void onChanged(Resource<List<Bookmark>> listResource) {

                bookmarks.clear();

                if (listResource.getData() != null) {
                    for (Bookmark bookmark : listResource.getData()) {
                        bookmarks.add(bookmark.getPostId());
                    }
                }

                mPostAdapter.notifyDataSetChanged();
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (mBinding == null) {
            mBinding = FragmentHomeListBinding.inflate(inflater, container, false);

            mLayoutManager = new LinearLayoutManager(getContext()) {
                @Override
                public boolean canScrollVertically() {
                    return mCanScroll;
                }
            };


            mBinding.homeListRecyclerView.setHasFixedSize(true);
            mBinding.homeListRecyclerView.setPadding(0, getRecyclerViewTopPadding(), 0, getRecyclerViewBottomPadding());
            mBinding.homeListRecyclerView.setLayoutManager(mLayoutManager);
            mBinding.homeListRecyclerView.setAdapter(mPostAdapter);

            mBinding.homeListSearchBox.setType(PostSortStyle.LIST_VIEW);
            mBinding.homeListSearchBox.setSearchBoxListener(mListener);

            ViewUtil.initializeSwipeRefreshLayoutWithOffset(
                    mBinding.swipeRefreshLayout,
                    mOnRefreshListener,
                    mBinding.swipeRefreshLayout.getProgressViewStartOffset() + mBinding.homeListSearchBox.getHeight(),
                    mBinding.swipeRefreshLayout.getProgressViewEndOffset() + mBinding.homeListSearchBox.getHeight()
                    );
        }

        if (savedInstanceState != null) {
            mBinding.homeListSearchBox.setIsShrunk(savedInstanceState.getBoolean(SHRUNK), false);
        }

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {

        super.onResume();

        mBinding.homeListRecyclerView.addOnScrollListener(mOnScrollListener);

        onResumeTryToFetchLocation();

        registerBroadcastEvent();
    }

    private void onResumeTryToFetchLocation() {
        if (getHomeActivity().haveLocationPermission()) {
            mBinding.swipeRefreshLayout.setVisibility(View.VISIBLE);
            mBinding.noLocationPermission.setVisibility(View.GONE);
            mBinding.permissionButton.setOnClickListener(null);

            if (mCurrentLocation == null) {
                mBinding.swipeRefreshLayout.setRefreshing(true);
            }

            if (mPostManager.getLastModifiedTime() > mLastModifiedTime) {

                fetchDataFromStart();
            } else {
                fetchCurrentLocation();
            }
        } else {
            if (getHomeActivity().locationPermissionDenied()) {
                mBinding.swipeRefreshLayout.setVisibility(View.GONE);
                mBinding.noLocationPermission.setVisibility(View.VISIBLE);
                mBinding.permissionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getHomeActivity().requestForLocationPermission();
                    }
                });
            } else {
                getHomeActivity().requestForLocationPermission();
            }
        }
    }

    @Override
    public void onPause() {

        unregisterBroadcastEvent();

        mBinding.homeListRecyclerView.removeOnScrollListener(mOnScrollListener);

        mLastModifiedTime = mPostManager.getLastModifiedTime();

        super.onPause();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Gson gson = new GsonBuilder().create();

        outState.putString(Constants.KEY_POSTS_LOADED, gson.toJson(mPostAdapter.getPosts(), Constants.POST_LIST_TYPE));

        if (mBinding != null) {
            outState.putBoolean(SHRUNK, mBinding.homeListSearchBox.isShrunk());
        }

        if (mLastLoadTime > -1) {
            outState.putLong(Constants.KEY_LAST_LOAD_TIME, mLastLoadTime);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void registerBroadcastEvent() {

        if (getContext() != null) {
            BroadcastUtil.registerReceiverWithMultipleAction(getContext(), mBroadcastReceiver, Constants.ACTION_POST_DELETED, Constants.ACTION_POST_UPLOADED);
            BroadcastUtil.registerLocationPermissionBroadcast(getContext(), mPermissionReceiver);
            HomeUtil.configureBroadcastReceiver(getContext(), HomeUtil.ACTION_HOME_DOUBLE_TAPPED, mDoubleTapReceiver, true);
        }
    }

    private void unregisterBroadcastEvent() {
        if (getContext() != null) {
            BroadcastUtil.unregisterLocationPermissionBroadcast(getContext(), mPermissionReceiver);
            BroadcastUtil.unregisterPostDeleteBroadcast(getContext(), mBroadcastReceiver);
            HomeUtil.configureBroadcastReceiver(getContext(), HomeUtil.ACTION_HOME_DOUBLE_TAPPED, mDoubleTapReceiver, false);
        }
    }

    private int getRecyclerViewTopPadding() {
        return getPiquedActivity().getStatusBarHeight() + getResources().getDimensionPixelSize(R.dimen.search_box_height);
    }

    private int getRecyclerViewBottomPadding() {
        return ((HomeBaseActivity) getPiquedActivity()).getBottomNavigationHeight();
    }

    private void fetchCurrentLocation() {

        mLocationManager.getCurrentLocation(mGetLocationCallback);
    }

    private void fetchPostsBasedOnLocation() {

        mPostManager.fetchMorePosts(mCurrentLocation, mCurrentPage);
    }

    private final SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (isUIActive()) {
                mCurrentPage = VALUE_START_PAGE; // reset page count

                fetchDataFromStart();
            }
        }
    };

    private void fetchDataFromStart() {

        mCurrentPage = VALUE_START_PAGE;

        if (mCurrentLocation != null) {
            fetchPostsBasedOnLocation();
        } else {
            fetchCurrentLocation();
        }
    }

    private void refetchPastBasedOnLocation() {

        if (getCurrentLocation() != null) {

            mPostManager.getPostsBasedOnLocation(getCurrentLocation(), 1).observe(HomeListFragment.this, resourceLiveData -> {
                mLastLoadTime = System.currentTimeMillis();

                if (resourceLiveData != null) {
                    isFetchingData = resourceLiveData.getStatus() == Status.LOADING;

                    if (!isFetchingData) {
                        System.out.println("DEBUG: Fetching finished: " + resourceLiveData.getStatus().name());
                    }
                    if (resourceLiveData.getData() != null) {

                        List<CloudPost> newPosts = resourceLiveData.getData();

                        mPostManager.addCloudPostToMemoryCache(newPosts);

                        updatePosts(newPosts, newPosts.size() < 50);

                        if (newPosts.size() > 0) {
                            mCurrentPage = mPostAdapter.getItemCount() / 50 + 1;

                            mBinding.swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                }
            });
        }
    }

    private final LocationCallback mGetLocationCallback = new LocationCallback() {
        @Override
        public void onLocationUpdated(Location location) {

            boolean hadLocation = mCurrentLocation != null;
            mCurrentLocation = location;

            if (!hadLocation ||
                    System.currentTimeMillis() - mLastLoadTime > TimeUnit.MINUTES.toMillis(5) ||
                    mCurrentLocation.distanceTo(location) >= MIN_METER_DIFFERENCE ||
                    mPostManager.getLastModifiedTime() > mLastModifiedTime) {

                refetchPastBasedOnLocation();
            }
        }

        @Override
        public boolean onceOnly() {
            return true;
        }
    };

    private void updatePosts(@NonNull List<CloudPost> posts, boolean isLast) {

        mCanScroll = posts.size() > 0;
        mPostAdapter.updatePosts(posts, isLast);
    }

    private final SearchBoxListener mListener = new SearchBoxListener() {
        @Override
        public void onButtonPressed(PostSortStyle currentType) {

            if (getHomeActivity().haveLocationPermission()) {
                switchToView(PostSortStyle.MAP_VIEW);
            }
        }
    };

    private final RecyclerView.OnScrollListener mOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            super.onScrolled(recyclerView, dx, dy);
            
            mOverallYScroll += dy;

            if (mOverallYScroll > 0) {
                mBinding.homeListSearchBox.setIsShrunk(true);
            } else {
                mBinding.homeListSearchBox.setIsShrunk(false);
            }

            if ((mOverallYScroll > 100 && dy > 0) || dy > 50) {
                // if we scroll down more than 100 or scroll down is the direction
                sendScrollNotification(true);
            }

            if (mOverallYScroll < 1 || dy < -50) {
                sendScrollNotification(false);
            }

            if (mLayoutManager.findFirstVisibleItemPosition() + mLayoutManager.getChildCount() >= mLayoutManager.getItemCount()) {
                // end of list
                sendScrollNotification(false);
            }
        }
    };

    private void sendScrollNotification(boolean shouldHide) {

        if (isUIActive() && getContext() != null) {

            HomeUtil.sendUIHideNotification(getHomeActivity(), shouldHide);
        }
    }

    @Override
    public Location getCurrentLocation() {
        return mCurrentLocation;
    }

    private final BroadcastReceiver mDoubleTapReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (isUIActive() && mBinding != null) {
                mBinding.homeListRecyclerView.smoothScrollToPosition(0);
            }
        }
    };

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constants.ACTION_POST_DELETED.equals(intent.getAction())) {

                mPostAdapter.clearAllPosts();
            }

            if (isUIActive()) {

                mBinding.swipeRefreshLayout.setRefreshing(true);

                fetchDataFromStart();
            }
        }
    };

    private final BroadcastReceiver mPermissionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constants.ACTION_LOCATION_PERMISSION_RESULT.equals(intent.getAction())) {

                boolean result = intent.getBooleanExtra(Constants.KEY_PERMISSION_REQUEST, false);

                if (result) {
                    onResumeTryToFetchLocation();
                }
            }
        }
    };

    private final HomePostListAdapter.Handler mListHandler = new HomePostListAdapter.Handler() {
        @Override
        public void loadMorePosts() {
            if (!isFetchingData && !mPostAdapter.reachEndOfList()) {
                isFetchingData = true;
                System.out.println("DEBUG: Fetching data: current page " + mCurrentPage + ", post count: " + mPostAdapter.getItemCount());
                fetchPostsBasedOnLocation();
            }
        }

        @Override
        @NonNull
        public Location getLocation() {
            return mCurrentLocation;
        }
    };
}
