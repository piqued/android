package io.piqued.views.home.map.utils

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

import io.piqued.database.post.CloudPost

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 6/17/17.
 * Piqued Inc.
 */

class MapItem(val post: CloudPost) : ClusterItem {

    override fun getPosition(): LatLng {

        return post.getLatLng()
    }

    override fun getTitle(): String? {
        return null
    }

    override fun getSnippet(): String? {
        return null
    }

    override fun equals(other: Any?): Boolean {

        return if (other != null && other is MapItem) {
            post.postId == other.post.postId
        } else false

    }

    override fun hashCode(): Int {
        return post.hashCode()
    }
}
