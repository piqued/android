package io.piqued.views.profile.edit

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import io.piqued.R
import io.piqued.database.user.User
import io.piqued.model.EditUserData
import io.piqued.utils.Constants
import io.piqued.utils.modelmanager.PiquedUserManager
import io.piqued.views.profile.edit.important.EditImportantInfoFragment
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import kotlinx.android.synthetic.main.toolbar.view.*
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 10/6/18.
 * Piqued Inc.
 *
 */

class EditProfileFragment: EditProfileBaseFragment() {

    @Inject
    lateinit var userManager: PiquedUserManager

    private var saveButton: MenuItem? = null
    private lateinit var infoAdapter: InfoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        setHasOptionsMenu(true)

        infoAdapter = InfoAdapter(adapterCallback)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.layoutManager = LinearLayoutManager(view.context)

        recyclerView.adapter = infoAdapter
    }

    override fun onResume() {
        super.onResume()

        updateActionBar(includeToolbar.toolbar, R.string.edit_profile_title)

        if (editProfileActivity.getModifiedUserData().imageData != null) {
            infoAdapter.notifyUserImageChange()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.edit_profile_menu, menu)
        saveButton = menu.findItem(R.id.edit_profile_save)

        updateSaveButton()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                return handleBackPressed()
            }
            R.id.edit_profile_save -> {
                editProfileActivity.saveAllChanges()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onBackPressed(): Boolean {
        return handleBackPressed()
    }

    private fun handleBackPressed(): Boolean {
        if (editProfileActivity.hasChanges()) {
            editProfileActivity.showChangeNotSaveDialog()
            return true
        } else {
            return false
        }
    }

    fun showSelectImageView() {
        NavHostFragment.findNavController(this).navigate(R.id.goToSelectPhoto)
    }

    private fun updateSaveButton() {
        val button = saveButton
        if (button != null) {
            button.isEnabled = editProfileActivity.hasChanges()
        }
    }

    private fun goToEditImportantInfo(infoType: InfoType, userEditValue: String) {

        NavHostFragment
                .findNavController(this)
                .navigate(R.id.goToEditImportantInfo,
                        EditImportantInfoFragment.createBundle(infoType, userEditValue))
    }

    private val adapterCallback = object : InfoAdapter.Callback {
        override fun nameChanged(newName: String?) {
            editProfileActivity.updateUserName(newName)
            updateSaveButton()
        }

        override fun openSelectImageView() {
            val permissionCheck = ContextCompat.checkSelfPermission(context!!,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
            val cameraPermission = ContextCompat.checkSelfPermission(context!!,
                    Manifest.permission.CAMERA)

            if (permissionCheck == PackageManager.PERMISSION_GRANTED
                    && cameraPermission == PackageManager.PERMISSION_GRANTED) {
                showSelectImageView()
            } else {
                LocalBroadcastManager
                        .getInstance(context!!)
                        .registerReceiver(permissionReceiver,
                                IntentFilter(Constants.ACTION_SEND_PERMISSION_RESULT))

                // ask for permission
                ActivityCompat.requestPermissions(piquedActivity,
                        arrayOf(
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA
                        ),
                        Constants.EXTERNAL_STORAGE_READ_REQUEST)
            }
        }

        override fun getOriginalUser(): User {

            val record = userManager.myUserRecord
            if (record != null) {
                return record
            }

            throw IllegalStateException("Cannot get user record, user is in a weird state")
        }

        override fun getEditUserData(): EditUserData {
            return editProfileActivity.getModifiedUserData()
        }

        override fun editEmail() {
            val emailValue =
                    editProfileActivity.getModifiedEmail() ?: originalUser.email

            goToEditImportantInfo(InfoType.EMAIL, emailValue)
        }

        override fun editPassword() {

            val passwordValue =
                    editProfileActivity.getModifiedPassword() ?: ""

            goToEditImportantInfo(InfoType.PASSWORD, passwordValue)
        }
    }

    private val permissionReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == Constants.ACTION_SEND_PERMISSION_RESULT) {

                LocalBroadcastManager.getInstance(getContext()!!).unregisterReceiver(this)

                val permissionResult = intent.getBooleanExtra(Constants.KEY_PERMISSION_REQUEST, false)

                if (permissionResult) {
                    showSelectImageView()
                }
            }
        }
    }
}
