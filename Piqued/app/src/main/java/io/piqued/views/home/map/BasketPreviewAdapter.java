package io.piqued.views.home.map;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.piqued.basket.BasketActionHandler;
import io.piqued.database.post.CloudPost;
import io.piqued.database.post.PostId;
import io.piqued.utils.PostCreateTimeComparator;
import io.piqued.utils.modelmanager.PiquedUserManager;
import io.piqued.utils.postmanager.PostManager;
import io.piqued.viewholders.home.BasketPreviewViewHolder;
import io.piqued.viewholders.home.FooterViewHolder;
import io.piqued.viewholders.home.MapPullHandleViewHolder;
import io.piqued.views.home.map.utils.BasketPreviewItem;
import io.piqued.views.home.map.utils.BasketPreviewItemType;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 6/24/17.
 * Piqued Inc.
 */

public class BasketPreviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final BasketPreviewItem emptyItem = new BasketPreviewItem(BasketPreviewItemType.EMPTY);
    private static final BasketPreviewItem handleItem = new BasketPreviewItem(BasketPreviewItemType.HANDLE);

    private final PostCreateTimeComparator postComparator = new PostCreateTimeComparator();
    private final List<BasketPreviewItem> mUIItemList = new ArrayList<>();
    private final List<CloudPost> mPosts = new ArrayList<>();

    private final BasketActionHandler mHandler;
    private final PreviewListCallback mCallback;
    private final PiquedUserManager mUserManager;
    private final PostManager mPostManager;

    public BasketPreviewAdapter(@NonNull BasketActionHandler handler,
                                @NonNull PiquedUserManager userManager,
                                @NonNull PostManager postManager,
                                @NonNull PreviewListCallback callback) {
        mHandler = handler;
        mUserManager = userManager;
        mPostManager = postManager;
        mCallback = callback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        BasketPreviewItemType type = BasketPreviewItemType.fromInt(viewType);

        switch (type) {
            case EMPTY:
                return new PreviewListEmptyViewHolder(parent);
            case HANDLE:
                return new MapPullHandleViewHolder(parent);
            case FOOTER:
                return new FooterViewHolder(parent);
            default:
                return new BasketPreviewViewHolder(parent);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PreviewListEmptyViewHolder) {
            ((PreviewListEmptyViewHolder) holder).updateViewHeight(mCallback.getFullListHeight());
        }

        if (holder instanceof MapPullHandleViewHolder) {
            ((MapPullHandleViewHolder) holder).setIconVisible(mPosts.size() > 1);
        }

        if (holder instanceof FooterViewHolder) {
            ((FooterViewHolder) holder).onBindView(mCallback.getFooterHeight());
        }

        if (holder instanceof BasketPreviewViewHolder) {

            BasketPreviewItem item = mUIItemList.get(position);

            if (mHandler.getCurrentLocation() != null && item.getPost() != null) {

                CloudPost post = mPostManager.getCloudPost(item.getPost().getPostId());

                if (post != null) {
                    System.out.println("updating like: binding post: " + post.getPostId() + ", instance: @" + Integer.toHexString(System.identityHashCode(post)));

                    ((BasketPreviewViewHolder) holder).setPost(
                            mUserManager,
                            mHandler,
                            mHandler.getCurrentLocation(),
                            post);
                }
            }
        }
    }

    @Override
    public int getItemViewType(int position) {

        return mUIItemList.get(position).getType().ordinal();
    }

    @Override
    public int getItemCount() {
        return mUIItemList.size();
    }

    public void onResume() {


        List<PostId> newPostIds = new ArrayList<>();

        for (CloudPost post: mPosts) {
            newPostIds.add(new PostId(post.getPostId()));
        }

        checkAndAppPosts(newPostIds);
        notifyDataSetChanged();
    }

    private void checkAndAppPosts(List<PostId> postIds) {

        if (postIds.size() < 1) return;

        mPosts.clear();

        for (PostId postId : postIds) {
            CloudPost postCache = mPostManager.getCloudPost(postId);

            if (postCache != null) {
                mPosts.add(postCache);
            }
        }

        Collections.sort(mPosts, postComparator);
    }

    /**
     *
     * @param postId post id of removed post
     * @return count of post previews after remove
     */
    public void notifyPostRemoved(long postId) {

        int index = -1;

        for (int i = 0; i < mPosts.size(); i++) {
            CloudPost post = mPosts.get(i);
            if (post.getPostId() == postId) {
                index = i;
                break;
            }
        }

        if (index > -1) {
            mPosts.remove(index);

            notifyDataSetChanged();
        }
    }

    public void setPosts(List<PostId> posts) {

        mUIItemList.clear();

        checkAndAppPosts(posts);

        mUIItemList.add(emptyItem);
        mUIItemList.add(handleItem);

        for (CloudPost post : mPosts) {
            mUIItemList.add(new BasketPreviewItem(post));
        }
    }

    public List<CloudPost> getPosts() {
        return mPosts;
    }
}
