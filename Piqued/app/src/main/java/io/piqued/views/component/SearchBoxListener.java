package io.piqued.views.component;

import io.piqued.views.home.PostSortStyle;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/14/17.
 * Piqued Inc.
 */

public interface SearchBoxListener {

    void onButtonPressed(PostSortStyle currentType);
}
