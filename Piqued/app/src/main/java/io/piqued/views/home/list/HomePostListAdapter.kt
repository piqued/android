package io.piqued.views.home.list

import android.content.Context
import android.location.Location
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.database.post.CloudPost
import io.piqued.database.post.PostId
import io.piqued.database.util.PiquedLogger
import io.piqued.utils.modelmanager.PiquedUserManager
import io.piqued.viewholders.home.EmptyPostViewHolder
import io.piqued.viewholders.home.HomeListLastItemLoading
import io.piqued.viewholders.home.SimplePostViewHolder
import io.piqued.viewholders.home.TimeDividerViewHolder
import io.piqued.basket.BasketActionHandler
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/2/17.
 * Piqued Inc.
 */

class HomePostListAdapter internal constructor(private val mContext: Context,
                                               private val mUserManager: PiquedUserManager,
                                               private val mHandler: BasketActionHandler,
                                               private val mListHandler: Handler) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val posts = ArrayList<CloudPost>()
    private val mViewItems = ArrayList<HomeListViewItem>()
    private var mLoadMore: Boolean = false
    private var mIsLast = false

    interface Handler {
        val location: Location
        fun loadMorePosts()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewTypeIndex: Int): RecyclerView.ViewHolder {

        val viewType = HomeListViewItem.Type.fromInt(viewTypeIndex)

        when (viewType) {
            HomeListViewItem.Type.TIME_DIVIDER -> return TimeDividerViewHolder(parent)
            HomeListViewItem.Type.EMPTY_POST -> return EmptyPostViewHolder(parent)
            HomeListViewItem.Type.LOADING_MORE -> return HomeListLastItemLoading(parent)
            else -> return SimplePostViewHolder(parent)
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        if (mViewItems.size > 0 && position < posts.size) {
            val item = mViewItems[position]

            if (viewHolder is SimplePostViewHolder) {

                viewHolder.setPost(mUserManager,
                        mHandler,
                        mListHandler.location,
                        posts[position])

                if (!mLoadMore && !mIsLast) {
                    if (position.toFloat() > posts.size.toFloat() * 0.6) {
                        mLoadMore = true
                        // send load more signal
                        PiquedLogger.i(TAG, "sending load more signal")
                        mListHandler.loadMorePosts()
                    }
                }
            } else if (viewHolder is TimeDividerViewHolder) {

                viewHolder.setTimeString(item.dividerValue!!)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {

        return if (mViewItems.size > 0) {
            mViewItems[position].type.ordinal
        } else {
            HomeListViewItem.Type.EMPTY_POST.ordinal
        }
    }

    override fun getItemCount(): Int {

        return if (mViewItems.size > 0) {
            mViewItems.size
        } else {
            EMPTY_POST_COUNT
        }
    }

    fun clearAllPosts() {
        posts.clear()
    }

    fun updatePosts(data: List<CloudPost>, isLast: Boolean) {

        mLoadMore = false
        mIsLast = isLast

        posts.clear()
        posts.addAll(data)

        Collections.sort(posts, object : Comparator<CloudPost> {
            override fun compare(post1: CloudPost?, post2: CloudPost?): Int {

                if (post1 == null && post2 == null) {
                    return 0
                }

                if (post1 == null) {
                    return 1
                }

                if (post2 == null) {
                    return -1
                }

                if (post1.createdAt < post2.createdAt) {
                    return 1
                } else if (post1.createdAt > post2.createdAt) {
                    return -1
                }

                return 0
            }

        })

        mViewItems.clear()
        val now = Calendar.getInstance()

        var dateDividerIndex = 0
        var dividerAdded = false
        for (post : CloudPost in posts) {
            if (dateDividerIndex < HomeDateDivider.getValueSize()) {

                val difference = now.timeInMillis - TimeUnit.SECONDS.toMillis(post.createdAt)

                var divider = HomeDateDivider.fromInt(dateDividerIndex)

                while (divider.timeInMilli < difference && dateDividerIndex <= HomeDateDivider.getValueSize()) {
                    dateDividerIndex++
                    divider = HomeDateDivider.fromInt(dateDividerIndex)
                    dividerAdded = false
                }

                if (divider != HomeDateDivider.ONE_DAY_AGO && !dividerAdded) {
                    mViewItems.add(HomeListViewItem.newDivider(HomeDateDivider.fromInt(dateDividerIndex - 1)))
                    dividerAdded = true
                }
            }

            mViewItems.add(HomeListViewItem.newPostItem(PostId(post.postId)))
        }

        if (!isLast) {
            mViewItems.add(HomeListViewItem.newLoadMorePostView())
        }

        notifyDataSetChanged()
    }

    fun getPosts(): List<CloudPost> {
        return posts
    }

    fun notifyPostRemoved(postId: Long) {
        var index = -1

        for (post: CloudPost in posts) {
            if (post.postId == postId) {
                index = posts.indexOf(post)
                break
            }
        }

        if (index > -1) {
            posts.removeAt(index)

            notifyItemRemoved(index)
        }
    }

    fun notifyPostUpdated(postId: Long) {
        var index = -1

        for (post: CloudPost in posts) {
            if (post.postId == postId) {
                index = posts.indexOf(post)
                break
            }
        }

        if (index > -1) {
            notifyItemChanged(index)
        }
    }

    fun reachEndOfList(): Boolean {
        return mIsLast
    }

    companion object {

        private val TAG = HomePostListAdapter::class.java.simpleName
        private val EMPTY_POST_COUNT = 4
    }
}
