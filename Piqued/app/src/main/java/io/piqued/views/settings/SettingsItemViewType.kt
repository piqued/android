package io.piqued.views.settings

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/11/17.
 * Piqued Inc.
 */

enum class SettingsItemViewType {
    TITLE,
    BUTTON,
    BUTTON_WITH_BOLD,
    SOCIAL_MEDIA,
    SWITCH,
    DISTANCE,
    APP_VERSION,
    LOGOUT
}
