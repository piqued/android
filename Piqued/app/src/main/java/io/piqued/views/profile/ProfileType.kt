package io.piqued.views.profile

enum class ProfileType {
    USER,
    OWNER,
    EMPTY
}