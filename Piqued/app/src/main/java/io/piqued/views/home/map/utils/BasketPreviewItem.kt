package io.piqued.views.home.map.utils

import io.piqued.database.post.CloudPost

/**
 *
 * Created by Kenny M. Liou on 2/24/18.
 * Piqued Inc.
 *
 */
class BasketPreviewItem {

    val type : BasketPreviewItemType
    val post: CloudPost?

    constructor(type: BasketPreviewItemType) {
        this.type = type
        post = null
    }

    constructor(post: CloudPost) {
        this.type = BasketPreviewItemType.POST
        this.post = post
    }
}