package io.piqued.views

import com.ncapdevi.fragnav.FragNavController

import io.piqued.PiquedFragment

/**
 * Created by Kenny M. Liou on 8/8/17.
 * Piqued Inc.
 */

abstract class DoubleTapFragment : PiquedFragment() {

    abstract fun onTabDoubleTapped(backStackController: FragNavController)
}
