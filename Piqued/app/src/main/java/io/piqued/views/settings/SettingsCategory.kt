package io.piqued.views.settings

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/11/17.
 * Piqued Inc.
 */

enum class SettingsCategory {
    FOLLOWING,
    SOCIAL_NETWORK,
    MY_ACCOUNT,
    NOTIFICATIONS,
    SECURITY,
    DEVELOPER,
    LOGOUT
}
