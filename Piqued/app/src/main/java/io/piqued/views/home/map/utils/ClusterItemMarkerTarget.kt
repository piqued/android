package io.piqued.views.home.map.utils

import com.google.android.gms.maps.model.Marker

/**
 *
 * Created by Kenny M. Liou on 9/6/18.
 * Piqued Inc.
 *
 */
class ClusterItemMarkerTarget(
        val marker: Marker
)