package io.piqued.views.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import io.piqued.R
import kotlinx.android.synthetic.main.login_details_subfragment.*
import net.hockeyapp.android.utils.Util.isValidEmail

/**
 *
 * Created by Kenny M. Liou on 1/20/18.
 * Piqued Inc.
 *
 */
class PiquedLoginSubFragment : LoginSignUpBaseFragment() {

    companion object {
        fun newInstance(handler : LoginSignUpHandler) : PiquedLoginSubFragment {
            val fragment = PiquedLoginSubFragment()
            fragment.mHandler = handler

            return fragment
        }
    }

    private lateinit var mHandler : LoginSignUpHandler

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login_details_subfragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        piqued_login_button.setOnClickListener {

            val email = email_edit_text.text.toString()
            val password = password_edit_text.text.toString()

            hideSoftKeyboard()

            if (isValidEmail(email) && isValidPassword(password)) {
                getHandler().performPiquedLogin(email, password)
            } else {

                val builder = AlertDialog.Builder(context!!)
                builder.setMessage(R.string.invalid_email_password)
                builder.setPositiveButton(R.string.ok_button, null)
                builder.create().show()
            }
        }

        facebook_login_button.setOnClickListener {
            getHandler().performFacebookLogin()
        }

        forgetPasswordButton.setOnClickListener {
            getHandler().startPasswordResetFlow()
        }
    }

    override fun onResume() {
        super.onResume()

        getHandler().configureBottom(this, BottomButtonOption.SIGN_UP, BottomOption.FORGOT_PASSWORD)

        email_edit_text.removeTextChangedListener(mTextWatcher)
        password_edit_text.removeTextChangedListener(mTextWatcher)

        email_edit_text.addTextChangedListener(mTextWatcher)
        password_edit_text.addTextChangedListener(mTextWatcher)
    }

    override fun onPause() {

        email_edit_text.removeTextChangedListener(mTextWatcher)
        password_edit_text.removeTextChangedListener(mTextWatcher)

        super.onPause()
    }

    override fun getTagName(): String {
        return PiquedLoginSubFragment::class.java.simpleName
    }

    override fun getHandler(): LoginSignUpHandler {
        return mHandler
    }

    private val mTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            piqued_login_button.setEnabled(isValidEmail(email_edit_text.text.toString()) && isValidPassword(password_edit_text.text.toString()))
        }
    }

    /**
     * Support methods
     */

    private fun isValidPassword(password: String): Boolean {
        return password.length > 5
    }

}