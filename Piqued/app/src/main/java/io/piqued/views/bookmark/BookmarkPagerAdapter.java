package io.piqued.views.bookmark;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.PagerAdapter;
import io.piqued.R;
import io.piqued.database.post.CloudPost;
import io.piqued.databinding.ViewBookmarkMainBinding;
import io.piqued.databinding.ViewBookmarkRemoveBinding;
import io.piqued.utils.DistanceHelper;
import io.piqued.utils.ImageDownloadUtil;
import io.piqued.utils.LocationCallback;
import io.piqued.utils.PiquedLocationManager;
import io.piqued.utils.StringUtil;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/18/17.
 * Piqued Inc.
 */

public class BookmarkPagerAdapter extends PagerAdapter {

    private CloudPost mPost;
    private PiquedLocationManager mLocationManager;
    private ViewBookmarkMainBinding mMainBinding;
    private ViewBookmarkRemoveBinding mRemoveBinding;
    private BookmarkPagerListener mListener;
    private ImageDownloadUtil downloadUtil;

    public BookmarkPagerAdapter(Context context, BookmarkPagerListener listener) {
        mListener = listener;
        downloadUtil = ImageDownloadUtil.getDownloadUtilInstance(context);
    }

    @Override
    public int getCount() {
        return 2; // main and delete
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = createView(mListener.getParentContext(), container, position);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    private View createView(Context context, ViewGroup parent, int position) {
        switch (position) {
            case 0:
                mMainBinding = ViewBookmarkMainBinding.inflate(LayoutInflater.from(context), parent, false);
                mMainBinding.bookmarkButton.setOnClickListener(mOnBookmarkClicked);
                mMainBinding.viewWrapper.setOnClickListener(mOpenPostListener);
                if (mPost != null && mLocationManager != null) {
                    updateMainView(context);
                }
                return mMainBinding.getRoot();
            case 1:
                mRemoveBinding = ViewBookmarkRemoveBinding.inflate(LayoutInflater.from(context), parent, false);
                mRemoveBinding.undoButton.setOnClickListener(mOnUndoClicked);
                return mRemoveBinding.getRoot();
        }

        return null;
    }

    void onBind(Context context, CloudPost post, PiquedLocationManager locationManager) {

        if (mPost == null || mPost.getPostId() != post.getPostId()) {
            mPost = post;
            mLocationManager = locationManager;

            if (mMainBinding != null) {
                updateMainView(context);
            }
        }
    }

    private void updateMainView(final Context context) {

        if (mListener.isUiActive()) {

            mMainBinding.bookmarkTitle.setText(mPost.getDescription());
            mMainBinding.bookmarkSubtitle.setText(mPost.getTitle());
            mMainBinding.bookmarkMultiImageIcon.setVisibility(mPost.getImages().size() > 1 ? View.VISIBLE : View.GONE);

            // FIXME: 3/18/17 there seems to have some flickering when location info is updated.
            mLocationManager.getCurrentLocation(new LocationCallback() {
                @Override
                public void onLocationUpdated(Location location) {
                    Location postLocation = new Location("");
                    postLocation.setLatitude(mPost.getLatLng().latitude);
                    postLocation.setLongitude(mPost.getLatLng().longitude);

                    String distance = DistanceHelper.INSTANCE.getDistanceInString(location, postLocation);
                    StringBuilder sb = new StringBuilder(distance);
                    sb.append(context.getString(R.string.distance_unit_short));

                    final String distanceText = " " + context.getString(R.string.bookmark_distance_text);

                    mMainBinding.distanceText.setText(StringUtil.createStringWithTypeFaces(context, sb.toString(), distanceText));
                }
            });

            downloadUtil.loadPostImage(mPost, mMainBinding.bookmarkImageView);
        }
    }

    private final View.OnClickListener mOnBookmarkClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mListener.onBookmarkClicked();
        }
    };

    private final View.OnClickListener mOnUndoClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mListener.onUndoClicked();
        }
    };

    private final View.OnClickListener mOpenPostListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mListener.onOpenPost(mPost.getPostId());
        }
    };
}
