package io.piqued.views.profile.edit;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import io.piqued.model.EditUserData;
import io.piqued.database.user.User;

/**
 * Created by Kenny M. Liou on 9/30/17.
 * Piqued Inc.
 */

public class InfoAdapter extends RecyclerView.Adapter<InfoViewHolder> {

    public interface Callback {
        void nameChanged(String newName);
        void openSelectImageView();
        User getOriginalUser();
        EditUserData getEditUserData();
        void editEmail();
        void editPassword();
    }

    private enum ViewType {
        PHOTO,
        EDIT_TEXT,
        EDIT_BUTTON,
        TITLE;

        private static final ViewType[] values = ViewType.values();

        public static ViewType fromInt(int index) {
            if (index > -1 && index < values.length) {
                return values[index];
            }

            throw new IllegalArgumentException("Illegal index value " + index + " for enum " + ViewType.class.getName());
        }
    }

    private final List<InfoType> mInfoList = new ArrayList<>();
    private Callback mCallback;

    InfoAdapter(@NonNull Callback callback) {

        mCallback = callback;

        mInfoList.add(InfoType.PHOTO);
        mInfoList.add(InfoType.NAME);
        mInfoList.add(InfoType.TITLE);
        mInfoList.add(InfoType.EMAIL);
        mInfoList.add(InfoType.PASSWORD);
    }

    @Override
    public InfoViewHolder onCreateViewHolder(ViewGroup parent, int viewTypeIndex) {

        ViewType viewType = ViewType.fromInt(viewTypeIndex);

        switch (viewType) {
            case PHOTO:
                return new EditPhotoViewHolder(parent, mOnEditPhotoListener);
            case EDIT_TEXT:
                return new EditTextViewHolder(parent, mOnNameChangeListener);
            case EDIT_BUTTON:
                return new EditButtonViewHolder(parent, mOnEditButtonListener);
            case TITLE:
                return new EditTitleViewHolder(parent);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(InfoViewHolder holder, int position) {

        holder.onBind(mInfoList.get(position), mCallback.getOriginalUser(), mCallback.getEditUserData());
    }

    @Override
    public int getItemViewType(int position) {

        InfoType type = mInfoList.get(position);

        switch (type) {
            case PHOTO:
                return ViewType.PHOTO.ordinal();
            case TITLE:
                return ViewType.TITLE.ordinal();
            case NAME:
                return ViewType.EDIT_TEXT.ordinal();
            case EMAIL:
                return ViewType.EDIT_BUTTON.ordinal();
            case PASSWORD:
                return ViewType.EDIT_BUTTON.ordinal();
            case MAP:
                return ViewType.EDIT_BUTTON.ordinal();
        }

        return -1;
    }

    @Override
    public int getItemCount() {
        return mInfoList.size();
    }

    private EditTextViewHolder.Callback mOnNameChangeListener = new EditTextViewHolder.Callback() {
        @Override
        public void onValueChanged(@NonNull InfoType type, @NonNull String newValue) {

            mCallback.nameChanged(newValue);
        }
    };

    private EditPhotoViewHolder.Callback mOnEditPhotoListener = new EditPhotoViewHolder.Callback() {
        @Override
        public void openSelectImageView() {
            mCallback.openSelectImageView();
        }
    };

    private EditButtonViewHolder.Callback mOnEditButtonListener = new EditButtonViewHolder.Callback() {
        @Override
        public void editEmail() {
            mCallback.editEmail();
        }

        @Override
        public void editPassword() {
            mCallback.editPassword();
        }
    };

    public void notifyUserImageChange() {
        notifyItemChanged(ViewType.PHOTO.ordinal());
    }
}
