package io.piqued.views.home

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ncapdevi.fragnav.FragNavController
import io.piqued.R
import io.piqued.activities.HomeActivity
import io.piqued.activities.HomeBaseActivity
import io.piqued.api.PiquedApiCallback
import io.piqued.api.PiquedGetVenueCallback
import io.piqued.basket.BasketActionHandler
import io.piqued.basket.BasketFragment.Companion.newInstance
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.createbasket.NewCreateBasketActivity
import io.piqued.database.post.CloudPost
import io.piqued.database.post.PostId
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger.Companion.d
import io.piqued.database.util.PiquedLogger.Companion.e
import io.piqued.model.MetricValues
import io.piqued.service.imageupload.BroadcastCreateBasketStatus
import io.piqued.service.imageupload.ImageUploadStatus
import io.piqued.utils.*
import io.piqued.utils.modelmanager.PiquedUserManager
import io.piqued.utils.postmanager.PostManager
import io.piqued.views.HomeActivityFragment
import io.piqued.views.home.HomeUtil.Companion.sendUIHideNotification
import io.piqued.views.home.list.HomeListFragment
import io.piqued.views.home.map.BasketPreviewAdapter
import io.piqued.views.home.map.PreviewListCallback
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : HomeActivityFragment(), HomeFunctionProvider {

    companion object {
        private const val KEY_CAMERA_HIDDEN = "camera_hidden"
        private val TAG = HomeFragment::class.java.simpleName
        fun newInstance() : HomeFragment {
            return HomeFragment()
        }
    }

    @Inject
    lateinit var piquedMetricValues: MetricValues
    @Inject
    lateinit var locationManager: PiquedLocationManager
    @Inject
    lateinit var preferences: Preferences
    @Inject
    lateinit var userManager: PiquedUserManager
    @Inject
    lateinit var postManager: PostManager

    private lateinit var previewAdapter: BasketPreviewAdapter
    private lateinit var pagerAdapter: HomeFragmentPageAdapter
    private var mPreviewContainerHeight = -1
    private var miniBasketToShowOnMap = -1L
    private var cameraButtonHidden = true
    private var cameraAnimating = false
    private var currentLocation: Location? = null

    private val updatePostCallback = object : PiquedApiCallback<Long> {
        override fun onSuccess(result: Long?) {
            d(TAG, "Update post successfully")
        }
        override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
            e(TAG, errorCode, payload)
        }
    }

    private val updatePostBookmarkCallback = object: PiquedCloudCallback<Long> {
        override fun onSuccess(result: Long?) { d(HomeFragment.TAG, "Update post successfully") }

        override fun onFailure(error: Throwable?, message: String?) {
            e(TAG, PiquedErrorCode.NETWORK, message)
        }
    }

    private val basketActionHandler = object: BasketActionHandler {
        override fun onOpenPost(postId: Long) {
            if (postManager.getCloudPost(postId) != null) {
                // make sure bottom is show
                setCameraButtonHidden(hidden = false, animate = false)
                val activity = piquedActivity as HomeBaseActivity
                activity.setNavBarHidden(hidden = false, animate = false)
                activity.pushSubFragment(newInstance(postId))
            }
        }

        override fun onBookMarkPost(postId: Long, bookmarked: Boolean) {
            postManager.bookmarkPost(postId, bookmarked, updatePostBookmarkCallback)
        }

        override fun onLikePost(postId: Long, liked: Boolean) {
            postManager.setLikePost(postId, liked, updatePostCallback)
        }

        override fun onFollowUser(postId: Long, followed: Boolean) {
            postManager.followPostOwner(postId, followed, updatePostCallback)
        }

        override fun getVenueInfo(postId: Long, callback: PiquedGetVenueCallback?) {
            postManager.getVenuesFromPost(postId, callback)
        }

        override fun openPostOwnerProfile(context: Context, postId: Long) {
            setCameraButtonHidden(hidden = false, animate = false)
            val post: CloudPost? = postManager.getCloudPost(postId)
            if (post != null) {
                setNavigationBarHidden(hidden = false, animate = true)
                openUserProfile(post.userId)
            }
        }

        override fun openMapWithPost(postId: Long) {
            // 1. set post to open
            // 2. switch to map fragment and show bottom navigation
            // 3. Map fragment look up mini basket to open, opens it and set mini basket to -1
            if (isUIActive) {
                miniBasketPostId = postId
                sendUIHideNotification((piquedActivity as HomeActivity), shouldHide = false, shouldAnimate = false)
                home_view_pager.currentItem = PostSortStyle.MAP_VIEW.ordinal
            }
        }

        override val isUserLoggedIn: Boolean
            get() = preferences.userLoggedIn()
        override val currentLocation: Location?
            get() = this@HomeFragment.currentLocation

        override fun getPost(postId: Long): CloudPost? {
            return postManager.getCloudPost(postId)
        }
    }

    private val scrollReceiver = object : BroadcastReceiver() {
        override fun onReceive(c: Context?, t: Intent?) {
            if (HomeListFragment.isUIShouldHideIntentAction(t)) {
                val shouldHideUI = HomeListFragment.shouldHideUI(t)
                if (shouldHideUI != cameraButtonHidden && PostSortStyle.MAP_VIEW.ordinal != home_view_pager.currentItem) {
                    setCameraButtonHidden(shouldHideUI, true)
                }
            }
        }
    }

    private val imageUploadReceiver = object : BroadcastReceiver() {
        override fun onReceive(c: Context?, t: Intent?) {
            onUploadImageStatusUpdated(t!!)
        }
    }

    private val deleteReceiver = object : BroadcastReceiver() {
        override fun onReceive(c: Context?, t: Intent?) {
            val postId = BroadcastUtil.readDeletedPostIdFromIntent(t!!)
            previewAdapter.notifyPostRemoved(postId)
            reloadPostPreview()
        }

    }

    private val previewListCallback = object: PreviewListCallback {
        override fun getFullListHeight(): Int {
            return getPreviewContainerHeight()
        }

        override fun getFooterHeight(): Int {
            return piquedMetricValues.navigationBarHeight
        }

    }

    private val permissionReceiver = object : BroadcastReceiver() {
        override fun onReceive(c: Context?, t: Intent?) {
            if (Constants.ACTION_SEND_PERMISSION_RESULT == t?.action) {
                if (t.getBooleanExtra(Constants.KEY_PERMISSION_REQUEST, false)) {
                    startBasketCreation()
                }
            }
        }

    }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationUpdated(location: Location) {
            var shouldNotifyAdapter = false
            if (currentLocation == null) {
                shouldNotifyAdapter = true
            }
            currentLocation = location
            if (isUIActive && shouldNotifyAdapter) {
                previewAdapter.notifyDataSetChanged()
            }
        }
    }

    private val onCameraClickedListener = View.OnClickListener { v ->
        runIfLoggedIn(preferences, object : LoginRequiredExecution {
            override fun runIfLoggedIn() {
                val permissionCheck = ContextCompat.checkSelfPermission(v.context, Manifest.permission.READ_EXTERNAL_STORAGE)
                val cameraPermission = ContextCompat.checkSelfPermission(v.context, Manifest.permission.CAMERA)
                if (permissionCheck == PackageManager.PERMISSION_GRANTED && cameraPermission == PackageManager.PERMISSION_GRANTED) {
                    startBasketCreation()
                } else {
                    LocalBroadcastManager.getInstance(view!!.context).registerReceiver(permissionReceiver, IntentFilter(Constants.ACTION_SEND_PERMISSION_RESULT))

                    // ask for permission
                    ActivityCompat.requestPermissions(piquedActivity, arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA),
                            Constants.EXTERNAL_STORAGE_READ_REQUEST)
                }
            }

        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        application.getComponent().inject(this)
        locationManager.getCurrentLocation(locationCallback)
        pagerAdapter = HomeFragmentPageAdapter(this, this)
        application.uploadStatusData.observe(this, Observer<Intent> { t ->
            if (t != null) {
                onUploadImageStatusUpdated(t)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        home_view_pager.adapter = HomeFragmentPageAdapter(this, this)
        home_view_pager.isUserInputEnabled = false
        camera_button.setOnClickListener(onCameraClickedListener)
        if (savedInstanceState != null) {
            cameraButtonHidden = savedInstanceState.getBoolean(KEY_CAMERA_HIDDEN)
            setCameraButtonHidden(cameraButtonHidden, false)
        }

        previewAdapter = BasketPreviewAdapter(basketActionHandler, userManager, postManager, previewListCallback)

        post_preview_container.adapter = previewAdapter
        post_preview_container.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
        post_preview_container.setHasFixedSize(true)
        post_preview_container.setCallback(previewListCallback)

        root_view.setOnApplyWindowInsetsListener { _, insets ->
            val bottomSpace = insets.systemWindowInsetBottom
            // shift bottom
            val params = systemBottomGuideLine.layoutParams as ConstraintLayout.LayoutParams
            params.guideEnd = bottomSpace + piquedMetricValues.homeBottomNavigationBarHeight
            systemBottomGuideLine.layoutParams = params
            insets
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(KEY_CAMERA_HIDDEN, cameraButtonHidden)
    }

    override fun onResume() {
        super.onResume()
        BroadcastUtil.registerPostDeleteBroadcast(context!!, deleteReceiver)
        BroadcastCreateBasketStatus.registerBroadcast(context!!, imageUploadReceiver)
        HomeListFragment.registerListScrollListener(context!!, scrollReceiver)
        previewAdapter.onResume()
    }

    override fun onPause() {
        locationManager.onPause()
        HomeListFragment.unregisterListScrollListener(context!!, scrollReceiver)
        BroadcastCreateBasketStatus.unregisterBroadcast(context!!, imageUploadReceiver)
        BroadcastUtil.unregisterPostDeleteBroadcast(context!!, deleteReceiver)
        super.onPause()
    }

    override fun onBackPressed(): Boolean {
        if (post_preview_container.computeVerticalScrollOffset() > 0) {
            setPostPreviewVisible(false, true)
            return true
        }
        return super.onBackPressed()
    }



    override fun onTabDoubleTapped(backStackController: FragNavController) {
        if (isUIActive && context != null) {
            if (home_view_pager.currentItem == PostSortStyle.MAP_VIEW.ordinal) {
                switchToHomeScreenType(PostSortStyle.LIST_VIEW)
            } else {
                HomeUtil.sendDoubleTapSignal(context!!)
            }
        }
    }

    override fun setMiniBasketPostId(postId: Long) {
        miniBasketToShowOnMap = postId
    }

    override fun getMiniBasketPostId(): Long {
        return miniBasketToShowOnMap
    }

    override fun getMiniBasketList(): MutableList<CloudPost> {
        return previewAdapter.posts
    }

    override fun getCurrentLocation(): Location {
        return currentLocation!!
    }

    override fun setPostPreviewData(posts: MutableList<PostId>?) {
        previewAdapter.setPosts(posts)
        previewAdapter.notifyDataSetChanged()
    }

    override fun setPostPreviewVisible(visible: Boolean, animate: Boolean) {
        if (PostSortStyle.MAP_VIEW.ordinal == home_view_pager.currentItem) {
            post_preview_container.post {
                if (animate) {
                    if (visible) {
                        post_preview_container.smoothScrollToPosition(2)
                    } else {
                        post_preview_container.smoothScrollToPosition(0)
                    }
                } else {
                    if (visible) {
                        post_preview_container.scrollToPosition(2)
                    } else {
                        post_preview_container.scrollToPosition(0)
                    }
                }
            }
        }
    }

    override fun getBasketActionHandler(): BasketActionHandler {
        return basketActionHandler
    }

    override fun switchToHomeScreenType(type: PostSortStyle) {

        val activity = piquedActivity as HomeActivity
        activity.setNavBarHidden(hidden = false, animate = false)

        if (PostSortStyle.LIST_VIEW == type) {
            setPostPreviewVisible(visible = false, animate = false)
        }

        home_view_pager.currentItem = type.ordinal

        setCameraButtonHidden(PostSortStyle.MAP_VIEW == type, false)
    }

    private fun setCameraButtonHidden(hidden: Boolean, animate: Boolean) {
        if (cameraButtonHidden != hidden) {
            cameraButtonHidden = hidden
            if (!cameraAnimating) {
                cameraAnimating = true
                val params = systemBottomGuideLine.layoutParams as ConstraintLayout.LayoutParams
                val offset : Float = if (hidden) {
                    params.guideEnd + piquedMetricValues.cameraButtonMarginBottom + camera_button.height.toFloat()
                } else { 0f }
                val animator = ObjectAnimator.ofFloat(camera_button, View.TRANSLATION_Y, offset)
                animator.duration = if (animate) Constants.CAMERA_ANIMATION_DURATION else 0
                animator.addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        cameraAnimating = false
                    }
                })
                animator.start()
            }
        }
    }

    private fun onUploadImageStatusUpdated(t: Intent) {
        when (BroadcastCreateBasketStatus.getUploadStatus(t)) {
            ImageUploadStatus.STARTING -> camera_button.visibility = View.GONE
            ImageUploadStatus.UPLOADING -> camera_button.visibility = View.GONE
            ImageUploadStatus.FINISHED -> camera_button.visibility = View.VISIBLE
            ImageUploadStatus.ERROR -> camera_button.visibility = View.VISIBLE
            else -> {
                // do nothing
            }
        }
    }

    private fun startBasketCreation() {
        startActivity(NewCreateBasketActivity::class.java)
    }

    private fun getPreviewContainerHeight(): Int {
        if (mPreviewContainerHeight < 0) {
            mPreviewContainerHeight = root_view.height - context!!.resources.getDimensionPixelSize(R.dimen.search_box_height)
        }
        return mPreviewContainerHeight
    }

    private fun reloadPostPreview() {
        val postCount = previewAdapter.posts.size

        when {
            postCount > 1 -> {
                setPostPreviewVisible(visible = true, animate = false)
            }
            postCount > 0 -> {
                setPostPreviewVisible(visible = true, animate = false)
            }
            else -> {
                setPostPreviewVisible(visible = false, animate = false)
            }
        }
    }
}