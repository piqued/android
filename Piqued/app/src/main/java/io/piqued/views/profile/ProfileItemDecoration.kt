package io.piqued.views.profile

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.piqued.R

/**
 * This is used to decorate only the post section in profile items
 * Created by Kenny M. Liou on 6/9/18.
 * Piqued Inc.
 *
 */
class ProfileItemDecoration(private val startingCount: Int,
                            context: Context): RecyclerView.ItemDecoration() {

    private val regularSpace = context.resources.getDimension(R.dimen.space_05).toInt()

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val positionInList = parent.getChildLayoutPosition(view) - startingCount;

        if (positionInList > -1) {
            // defaults
            outRect.left = regularSpace / 2
            outRect.right = regularSpace / 2
            outRect.top = regularSpace / 2
            outRect.bottom = regularSpace / 2
        }
    }
}