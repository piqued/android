package io.piqued.views.profile

import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R
import io.piqued.databinding.ViewHolderProfileStatusBinding
import io.piqued.utils.StringUtil
import io.piqued.utils.postmanager.PostManager

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/28/17.
 * Piqued Inc.
 */
class ProfileStatusViewHolder(parent: ViewGroup) :
        ProfileViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_profile_status, parent, false)) {
    private val mBinding: ViewHolderProfileStatusBinding = ViewHolderProfileStatusBinding.bind(itemView)

    public override fun onProviderReady(provider: ProfileDataProvider) {
        updateView()
    }

    override fun setViewItem(item: ProfileViewItem, manager: PostManager) {
        super.setViewItem(item, manager)
        updateView()
    }

    private fun updateView() {
        if (mProvider.profileType == ProfileType.EMPTY) {
            updateStats()
        } else if (mProvider.isUserReady) {
            updateStats()
        }
    }

    private fun updateStats() {
        mBinding.sharesValue.text = StringUtil.getIntegerString(mProvider.sharesCount)
        mBinding.followersValue.text = StringUtil.getIntegerString(mProvider.followersCount)
        mBinding.followingValue.text = StringUtil.getIntegerString(mProvider.followingCount)
        mBinding.likeCount.text = StringUtil.getIntegerString(mProvider.likeCount)
        mBinding.commentCount.text = StringUtil.getIntegerString(mProvider.commentCount)
        mBinding.bookmarkCount.text = StringUtil.getIntegerString(mProvider.bookmarkCount)
    }
}