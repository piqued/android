package io.piqued.views.activities.util

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import io.piqued.database.post.PostId

/**
 *
 * Created by Kenny M. Liou on 2/3/18.
 * Piqued Inc.
 *
 */
class ActivityFragmentActionBroadcast {
    companion object {

        private val OPEN_PROFILE = "open_profile"
        private val OPEN_POST = "open_post"
        private val USER_ID = "user_id"
        private val POST_ID = "post_id"

        fun registerForAction(context: Context, broadcastReceiver: BroadcastReceiver) {

            val filter = IntentFilter()
            filter.addAction(OPEN_PROFILE)
            filter.addAction(OPEN_POST)

            LocalBroadcastManager.getInstance(context).registerReceiver(broadcastReceiver, filter)
        }

        fun unregisterAction(context: Context, broadcastReceiver: BroadcastReceiver) {

            LocalBroadcastManager.getInstance(context).unregisterReceiver(broadcastReceiver)
        }

        @JvmStatic
        fun openProfile(context: Context, userId: Long) {

            val intent = Intent(OPEN_PROFILE)
            intent.putExtra(USER_ID, userId)

            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }

        @JvmStatic
        fun openPost(context: Context, postId: PostId) {

            val intent = Intent(OPEN_POST)
            intent.putExtra(POST_ID, postId.postId)

            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }

        fun isActionOpenPost(intent: Intent): Boolean {
            return intent.action == OPEN_POST
        }

        fun isActionOpenProfile(intent: Intent): Boolean {
            return intent.action == OPEN_PROFILE
        }

        fun getPostId(intent: Intent): Long {

            if (isActionOpenPost(intent)) {
                return intent.getLongExtra(POST_ID, -1)
            }

            return -1
        }

        fun getUserId(intent: Intent): Long {
            if (isActionOpenProfile(intent)) {
                return intent.getLongExtra(USER_ID, -1)
            }

            return -1
        }
    }
}