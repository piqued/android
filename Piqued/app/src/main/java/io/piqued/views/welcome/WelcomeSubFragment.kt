package io.piqued.views.welcome

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import io.piqued.PiquedFragment
import io.piqued.R
import kotlinx.android.synthetic.main.fragment_welcome_sub.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/17/16.
 * Piqued Inc.
 */

class WelcomeSubFragment : PiquedFragment() {

    private lateinit var mStep: WelcomeStep

    override fun getTagName(): String {
        return TAG
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mStep = arguments?.getSerializable(KEY_STEP) as WelcomeStep
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        return inflater.inflate(R.layout.fragment_welcome_sub, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        welcomeImage.setImageDrawable(ContextCompat.getDrawable(view.context, mStep.imageRes))

        if (mStep != WelcomeStep.STEP_1) {
            welcomeTitle.visibility = View.GONE
            welcomeString.setText(mStep.stringRes)
            welcomeString.visibility = View.VISIBLE
        } else {
            welcomeTitle.visibility = View.VISIBLE
            welcomeString.visibility = View.GONE
        }
    }

    companion object {

        private val TAG = WelcomeSubFragment::class.java.simpleName

        private const val KEY_STEP = "step"

        fun getInstance(step: WelcomeStep): WelcomeSubFragment {

            val args = Bundle()
            args.putSerializable(KEY_STEP, step)

            val result = WelcomeSubFragment()

            result.arguments = args

            return result
        }
    }
}
