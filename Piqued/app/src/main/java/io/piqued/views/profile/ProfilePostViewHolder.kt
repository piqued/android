package io.piqued.views.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.piqued.R
import io.piqued.database.post.PostId
import io.piqued.utils.ImageDownloadUtil
import io.piqued.utils.postmanager.PostManager
import kotlinx.android.synthetic.main.view_holder_profile_single_post.view.*

/**
 *
 * Created by Kenny M. Liou on 6/2/18.
 * Piqued Inc.
 *
 */
class ProfilePostViewHolder(parent: ViewGroup): ProfileViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_profile_single_post, parent, false)
) {

    private var postId: PostId? = null

    override fun onProviderReady(provider: ProfileDataProvider?) {
        itemView.singlePostImageView.setOnClickListener {
            val currentPostId = postId
            if (currentPostId != null) {
                mProvider.onOpenBasket(currentPostId)
            }
        }
    }

    override fun setViewItem(item: ProfileViewItem, manager: PostManager) {

        val post = manager.getCloudPost(item.singlePostId)

        postId = item.singlePostId

        if (post != null) {
            ImageDownloadUtil.getDownloadUtilInstance(itemView.context).loadPostImage(post.postId, 0, post.images[0].smallImageUrl, itemView.singlePostImageView)

            itemView.profilePostThumbnailMultiImageIcon.visibility = if (post.images.size > 1) View.VISIBLE else View.GONE
        }
    }

}