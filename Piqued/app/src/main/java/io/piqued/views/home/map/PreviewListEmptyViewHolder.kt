package io.piqued.views.home.map

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

import io.piqued.R
import kotlinx.android.synthetic.main.view_holder_preview_list_empty.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 7/6/17.
 * Piqued Inc.
 */

internal class PreviewListEmptyViewHolder(parent: ViewGroup)
    : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_holder_preview_list_empty, parent, false)
) {

    fun updateViewHeight(viewHeight: Int) {

        val params = itemView.emptyView.layoutParams as RecyclerView.LayoutParams

        if (params.height != viewHeight) {
            params.height = viewHeight

            itemView.emptyView.layoutParams = params
        }
    }
}
