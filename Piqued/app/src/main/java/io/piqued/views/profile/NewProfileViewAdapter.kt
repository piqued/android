package io.piqued.views.profile

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.database.post.CloudPost
import io.piqued.database.post.PostId
import io.piqued.utils.postmanager.PostManager
import java.util.*

/**
 *
 * Created by Kenny M. Liou on 6/2/18.
 * Piqued Inc.
 *
 */
class NewProfileViewAdapter(val provider: ProfileDataProvider,
                            val postManager: PostManager):
        RecyclerView.Adapter<ProfileViewHolder>()
{
    private val viewItems = ArrayList<ProfileViewItem>()
    val postIds = ArrayList<PostId>()

    init {
        refreshViewItems()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileViewHolder {

        val viewHolder  = when (ProfileViewType.fromInt(viewType)) {
            ProfileViewType.HEADER -> ProfileHeaderViewHolder(parent)
            ProfileViewType.STATS -> ProfileStatusViewHolder(parent)
            ProfileViewType.SWITCH -> ProfileSwitchViewHolder(parent)
            ProfileViewType.SINGLE_POST -> ProfilePostViewHolder(parent)
            ProfileViewType.POST_TITLE -> ProfilePostTitleViewHolder(parent)
            ProfileViewType.EMPTY_POST -> ProfileNoPostViewHolder(parent)
        }

        viewHolder.setProvider(provider)

        return viewHolder
    }

    override fun getItemViewType(position: Int): Int {
        return viewItems[position].type.ordinal
    }

    override fun getItemCount(): Int {

        return viewItems.size
    }

    override fun onBindViewHolder(holder: ProfileViewHolder, position: Int) {
        val item = viewItems[position]
        holder.setViewItem(item, postManager)

        if (item.type == ProfileViewType.SINGLE_POST) {
            provider.displayItem(postIds.size, postIds.indexOf(item.singlePostId))
            holder.onProviderReady(provider)
        }

        if (item.type == ProfileViewType.HEADER) {
            holder.onProviderReady(provider)
        }

        if (item.type == ProfileViewType.EMPTY_POST) {
            holder.onProviderReady(provider)
        }
    }

    private fun refreshViewItems() {

        viewItems.clear()

        viewItems.add(ProfileViewItem(ProfileViewType.HEADER))
        viewItems.add(ProfileViewItem(ProfileViewType.STATS))
        viewItems.add(ProfileViewItem(ProfileViewType.SWITCH))

        if (postIds.size < 1) {
            viewItems.add(ProfileViewItem(ProfileViewType.EMPTY_POST))
        } else {

            for (postId in postIds) {

                viewItems.add(ProfileViewItem(postId))
            }
        }
    }

    fun insertPosts(newPosts: List<CloudPost>) {
        var itemInserted = 0

        for (post in newPosts) {
            val postId = PostId(post.postId)
            if (postIds.indexOf(postId) < 0) {
                postIds.add(postId)
                itemInserted++
            }
        }

        refreshViewItems()

        notifyDataSetChanged()
    }

    fun insertPostIds(newPostIds: List<PostId>) {

        var itemInserted = 0

        for (postId in newPostIds) {
            if (postIds.indexOf(postId) < 0) {
                postIds.add(postId)
                itemInserted++
            }
        }

        if (itemInserted > 0) {

            refreshViewItems()

            notifyDataSetChanged()
        }
    }

    fun validatePosts(postManager: PostManager) {
        if (postIds.size > 0) {
            val newPosts = java.util.ArrayList<PostId>()

            for (postId in postIds) {
                if (postManager.getCloudPost(postId) != null) {
                    newPosts.add(postId)
                }
            }

            if (newPosts.size < postIds.size) {
                postIds.clear()

                insertPostIds(newPosts)

                notifyDataSetChanged()
            }
        }
    }

    fun notifyUserDataUpdated() {

        notifyItemChanged(ProfileViewType.HEADER.ordinal)
        notifyItemChanged(ProfileViewType.STATS.ordinal)
    }

    fun clearData() {
        postIds.clear()
    }
}