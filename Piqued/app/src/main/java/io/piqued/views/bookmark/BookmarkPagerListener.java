package io.piqued.views.bookmark;

import android.content.Context;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/18/17.
 * Piqued Inc.
 */

public interface BookmarkPagerListener {
    void onBookmarkClicked();
    void onUndoClicked();
    void onOpenPost(long postId);

    boolean isUiActive();

    Context getParentContext();
}
