package io.piqued.views.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.piqued.R
import kotlinx.android.synthetic.main.subfragment_new_account_name_password.*

/**
 *
 * Created by Kenny M. Liou on 1/22/18.
 * Piqued Inc.
 *
 */
class NewAccountNamePasswordSubFragment : LoginSignUpBaseFragment() {

    companion object {

        fun newInstance(handler: LoginSignUpHandler, email: String) : NewAccountNamePasswordSubFragment {

            val fragment = NewAccountNamePasswordSubFragment()

            fragment.handler = handler
            fragment.email = email

            return fragment
        }
    }

    private lateinit var handler: LoginSignUpHandler
    private lateinit var email : String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.subfragment_new_account_name_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        next_button.setOnClickListener {

            handler.createAccount(email, password_input.text.toString().trim(), root_view.text.toString().trim())
        }
    }

    override fun onResume() {
        super.onResume()

        next_button.visibility = View.VISIBLE
        getHandler().configureBottom(this, BottomButtonOption.NONE, BottomOption.EMPTY)

        root_view.removeTextChangedListener(mTextWatcher)
        password_input.removeTextChangedListener(mTextWatcher)

        root_view.addTextChangedListener(mTextWatcher)
        password_input.addTextChangedListener(mTextWatcher)

        showSoftKeyboard(root_view)
    }

    override fun onPause() {

        root_view.removeTextChangedListener(mTextWatcher)
        password_input.removeTextChangedListener(mTextWatcher)
        next_button.visibility = View.GONE

        super.onPause()
    }

    override fun getHandler(): LoginSignUpHandler {
        return handler
    }

    override fun getTagName(): String {
        return NewAccountNamePasswordSubFragment::class.java.simpleName
    }

    private fun validateInput() {

        val nextButtonEnable = getHandler().isNameValid(root_view.text.toString()) && getHandler().isPasswordValid(password_input.text.toString())
        next_button.isEnabled = nextButtonEnable
        next_button.alpha = if (nextButtonEnable) 1.0f else 0.3f
    }

    private val mTextWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            validateInput()
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    }
}