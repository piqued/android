package io.piqued.views.home

import android.location.Location
import io.piqued.PiquedFragment
import io.piqued.activities.HomeBaseActivity
import io.piqued.database.post.PostId

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/14/17.
 * Piqued Inc.
 */
abstract class HomeSubFragment(val provider: HomeFunctionProvider) : PiquedFragment() {
    protected fun switchToView(style: PostSortStyle?) {
        provider.switchToHomeScreenType(style!!)
    }

    protected fun setPostPreviewData(posts: List<PostId?>) {
        provider.setPostPreviewData(posts)
    }

    protected open fun setPostPreviewVisible(visible: Boolean, animate: Boolean) {
        provider.setPostPreviewVisible(visible, animate)
    }

    protected open val currentLocation: Location?
        get() = provider.currentLocation

    val homeActivity: HomeBaseActivity?
        get() = activity as HomeBaseActivity?

}