package io.piqued.views.home.map

import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import io.piqued.R
import io.piqued.cloud.util.RegionBounds
import io.piqued.database.post.CloudPost
import io.piqued.utils.LocationCallback
import io.piqued.utils.PiquedLocationManager
import io.piqued.utils.ViewUtil
import io.piqued.utils.postmanager.PostManager
import io.piqued.viewmodel.HomeMapViewModel
import io.piqued.views.component.SearchBoxListener
import io.piqued.views.home.HomeFunctionProvider
import io.piqued.views.home.HomeSubFragment
import io.piqued.views.home.PostSortStyle
import kotlinx.android.synthetic.main.fragment_home_mapbox.*
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 9/4/18.
 * Piqued Inc.
 *
 */

class HomeMapboxFragment(provider: HomeFunctionProvider): HomeSubFragment(provider) {

    companion object {
        private val TAG = HomeMapboxFragment::class.java.simpleName
        private const val DEFAULT_ZOOM_LEVEL = 15.0

        fun newInstance(provider: HomeFunctionProvider): HomeMapboxFragment {
            return HomeMapboxFragment(provider)
        }
    }

    @Inject
    internal lateinit var postManager: PostManager
    @Inject
    internal lateinit var locationManager: PiquedLocationManager

    private lateinit var viewModel: HomeMapViewModel

    private val geoDataSource = GeoJsonSource("Piqued");

    private var mapView: MapboxMap? = null

    private var currentLocationObj: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        viewModel = ViewModelProviders.of(homeActivity!!).get(HomeMapViewModel::class.java)
        viewModel.getPosts().observe(this, Observer<LinkedHashSet<CloudPost>> { _ ->

            // what am I doing here?

        })

        Mapbox.getInstance(context!!, "pk.eyJ1Ijoia2VubnlsaW91IiwiYSI6ImNqbG9wY29udTBvbmUzcHMxb2dmY3Q3aG0ifQ.o-5bYZEodUs2qpU9rnCWfA")

        locationManager.getCurrentLocation(object : LocationCallback() {
            override fun onLocationUpdated(location: Location) {

                currentLocationObj = location

                updateMap()
            }

            override fun onceOnly(): Boolean {
                return true
            }
        })
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home_mapbox, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeMapSearchBox.setType(PostSortStyle.MAP_VIEW)
        homeMapSearchBox.setSearchBoxListener(searchBoxListener)
        homeMapSearchBox.setIsShrunk(true, false)

        refreshLayout.isEnabled = false

        ViewUtil.initializeSwipeRefreshLayoutWithOffset(
                refreshLayout,
                null,
                refreshLayout.progressViewStartOffset + homeMapSearchBox.height,
                refreshLayout.progressViewEndOffset + homeMapSearchBox.height)

        map.onCreate(savedInstanceState)
        map.getMapAsync(mapReadyCallback)

        bottomSpacing.setOnApplyWindowInsetsListener { _, windowInsets ->
            windowInsets.consumeSystemWindowInsets()
        }
    }

    private val searchBoxListener = SearchBoxListener {
        setPostPreviewVisible(false, false)
        switchToView(PostSortStyle.LIST_VIEW)
    }

    /**
     * Map box related methods
     */

    override fun getTagName(): String {
        return TAG
    }

    private val mapReadyCallback = OnMapReadyCallback { mapboxMap ->
        if (isUIActive) {
            mapView = mapboxMap

            updateMap()
        }
    }

    private fun updateMap() {
        val mapViewObj = mapView
        val locationObj = currentLocationObj

        if (mapViewObj != null && locationObj != null) {
            mapViewObj.cameraPosition = CameraPosition.Builder()
                    .zoom(DEFAULT_ZOOM_LEVEL)
                    .target(LatLng(locationObj.latitude, locationObj.longitude))
                    .build()

            mapViewObj.addSource(geoDataSource)

            updateMapData()
        }
    }

    private fun updateMapData() {

        val mapViewObj = mapView

        if (mapViewObj != null) {
            val bounds = mapViewObj.projection.visibleRegion.latLngBounds

            viewModel.loadPostBasedOnLocation(postManager,
                    RegionBounds(
                            bounds.southWest.latitude,
                            bounds.northEast.latitude,
                            bounds.southWest.longitude,
                            bounds.northEast.longitude))
        }

    }

    override fun onStart() {
        super.onStart()

        map.onStart()
    }

    override fun onResume() {
        super.onResume()

        map.onResume()
    }

    override fun onPause() {
        map.onPause()
        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (map != null && !map.isDestroyed) {
            map.onSaveInstanceState(outState);
        }
    }

    override fun onStop() {
        map.onStop()
        super.onStop()
    }

    override fun onLowMemory() {
        if (map != null && !map.isDestroyed) {
            map.onLowMemory()
        }
        super.onLowMemory()
    }

    override fun onDestroy() {
        map.onDestroy()
        super.onDestroy()
    }
}