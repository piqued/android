package io.piqued.views.profile.edit.important

import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import io.piqued.R
import io.piqued.database.user.User
import io.piqued.utils.PasswordUtil
import io.piqued.utils.modelmanager.PiquedUserManager
import io.piqued.views.profile.edit.EditImportantInfoAdapter
import io.piqued.views.profile.edit.EditProfileBaseFragment
import io.piqued.views.profile.edit.InfoType
import kotlinx.android.synthetic.main.fragment_edit_important.*
import kotlinx.android.synthetic.main.toolbar.view.*
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 3/24/18.
 * Piqued Inc.
 *
 */
class EditImportantInfoFragment: EditProfileBaseFragment() {

    companion object {

        private val KEY_INFO_TYPE = "info_type"
        private val KEY_INFO_VALUE = "info_value"

        fun createBundle(type: InfoType, userEditValue: String): Bundle {

            val bundle = Bundle()

            bundle.putSerializable(KEY_INFO_TYPE, type)
            bundle.putString(KEY_INFO_VALUE, userEditValue)

            return bundle
        }
    }

    @Inject
    lateinit var mUserManager: PiquedUserManager

    private lateinit var infoType: InfoType
    private var userEditNewValue = ""

    lateinit var adapter: EditImportantInfoAdapter
    lateinit var menuDoneButton: MenuItem


    override fun getTagName(): String {
        return EditImportantInfoFragment::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        setHasOptionsMenu(true)

        val bundle = arguments
        if (bundle != null) {
            infoType = bundle.getSerializable(KEY_INFO_TYPE) as InfoType
            userEditNewValue = bundle.getString(KEY_INFO_VALUE, "")
        }
    }

    override fun onResume() {
        super.onResume()

        updateActionBar(includeToolbar.toolbar, R.string.edit_profile_title)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.edit_important_profile_menu, menu)

        menuDoneButton = menu.findItem(R.id.edit_profile_done)
        menuDoneButton.isEnabled = false

        evaluateDoneButton()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.edit_profile_done) {
            updateUserEditInfo()
            goBack()
            return true
        } else {
            // on back pressed
            updateUserEditInfo()
            return super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_edit_important, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val oldValue = if (infoType == InfoType.EMAIL && userEditNewValue.isEmpty()) getOriginalUser()!!.email else userEditNewValue

        adapter = EditImportantInfoAdapter(infoType, oldValue, adapterCallback)

        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.adapter = adapter
    }

    private fun getOriginalUser() : User? {
        return mUserManager.getMyUserRecord()
    }

    private fun updateUserEditInfo() {

        val myRecord = getOriginalUser()
        if (myRecord != null) {
            if (infoType == InfoType.EMAIL) {
                if (myRecord.email != userEditNewValue) {
                    editProfileActivity.setNewUserEmail(userEditNewValue)
                } else {
                    editProfileActivity.setNewUserEmail(null)
                }
            } else if (infoType == InfoType.PASSWORD) {
                if (PasswordUtil.isPasswordValid(userEditNewValue)) {
                    editProfileActivity.setNewUserPassword(userEditNewValue)
                } else {
                    menuDoneButton.isEnabled = false
                }
            }
        }
    }

    private val adapterCallback = object : EditImportantInfoAdapter.Callback {
        override fun onValueChanged(infoType: InfoType, newValue: String) {

            userEditNewValue = newValue

            evaluateDoneButton()
        }
    }

    private fun evaluateDoneButton() {
        if (infoType == InfoType.PASSWORD) {
            if (PasswordUtil.isPasswordValid(userEditNewValue)) {
                menuDoneButton.isEnabled = true
            }
        }

        if (infoType == InfoType.EMAIL) {
            val myRecord = getOriginalUser()

            menuDoneButton.isEnabled = (myRecord != null && !userEditNewValue.isEmpty() && myRecord.email != userEditNewValue)
        }
    }
}