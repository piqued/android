package io.piqued.views.bookmark;

import android.content.Context;

import io.piqued.database.post.CloudPost;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/18/17.
 * Piqued Inc.
 */

public interface BookmarkViewHolderListener {

    void onBookmarkRemove(CloudPost post, BookmarkViewHolder viewHolder);
    void onBookmarkRemoveUndo();
    void onOpenPost(CloudPost post);

    boolean isUiActive();

    Context getParentContext();
}
