package io.piqued.views.profile.edit

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 *
 * Created by Kenny M. Liou on 3/24/18.
 * Piqued Inc.
 *
 */
class EditImportantInfoAdapter(type: InfoType, oldValue: String, val callback: Callback): RecyclerView.Adapter<EditTextViewHolder>() {

    interface Callback {
        fun onValueChanged(infoType: InfoType, newValue: String)
    }

    private class AdapterItem(val type: InfoType, val oldValue: String)

    private val items = ArrayList<AdapterItem>()

    init {
        if (type == InfoType.EMAIL) {
            items.add(AdapterItem(InfoType.EMAIL, oldValue))
        } else if (type == InfoType.PASSWORD) {
            items.add(AdapterItem(InfoType.PASSWORD, oldValue))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EditTextViewHolder {
        return EditTextViewHolder(parent, mCallback)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: EditTextViewHolder, position: Int) {

        val item = items[position]

        holder.onBind(item.type, item.oldValue)
    }

    private val mCallback = object: EditTextViewHolder.Callback {
        override fun onValueChanged(type: InfoType, newValue: String) {
            callback.onValueChanged(type, newValue)
        }
    }
}