package io.piqued.views.settings

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/11/17.
 * Piqued Inc.
 */

interface SettingsDelegate {

    fun onButtonPressed(type: Settings)
    fun onSwitchClicked(type: Settings)
    fun onLogout()
}
