package io.piqued.views.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import io.piqued.R
import io.piqued.cloud.model.piqued.Venue
import kotlinx.android.synthetic.main.component_add_location.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 4/5/17.
 * Piqued Inc.
 */

class LocationInputView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : RelativeLayout(context, attrs, defStyleAttr) {

    init {

        View.inflate(context, R.layout.component_add_location, this)
    }

    fun setVenue(venue: Venue?) {

        venueWrap.visibility = if (venue == null) View.GONE else View.VISIBLE
        addLocationHintString.visibility = if (venue == null) View.VISIBLE else View.GONE

        if (venue != null) {
            addLocationVenueName.text = venue.venueName
            addLocationVenueAddress.text = venue.fullAddress
        }
    }

    override fun setOnClickListener(listener: View.OnClickListener?) {

        findViewById<View>(R.id.button).setOnClickListener(listener)
    }
}
