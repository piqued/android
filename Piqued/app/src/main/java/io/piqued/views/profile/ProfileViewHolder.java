package io.piqued.views.profile;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import io.piqued.utils.postmanager.PostManager;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/28/17.
 * Piqued Inc.
 */

public abstract class ProfileViewHolder extends RecyclerView.ViewHolder {

    protected ProfileDataProvider mProvider;
    public ProfileViewHolder(View itemView) {
        super(itemView);
    }

    public void setProvider(ProfileDataProvider provider) {
        mProvider = provider;
    }

    protected Context getContext() {
        return itemView.getContext();
    }

    public void setViewItem(@NonNull ProfileViewItem item,@NonNull PostManager manager) {
    }

    abstract void onProviderReady(ProfileDataProvider provider);
}
