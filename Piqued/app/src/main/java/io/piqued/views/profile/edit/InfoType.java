package io.piqued.views.profile.edit;

/**
 * Created by Kenny M. Liou on 9/30/17.
 * Piqued Inc.
 */

public enum InfoType {

    PHOTO,
    TITLE,
    NAME,
    EMAIL,
    PASSWORD,
    MAP
}
