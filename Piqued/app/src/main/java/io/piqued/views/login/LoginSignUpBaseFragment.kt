package io.piqued.views.login

import io.piqued.PiquedFragment

/**
 *
 * Created by Kenny M. Liou on 1/20/18.
 * Piqued Inc.
 *
 */
abstract class LoginSignUpBaseFragment() : PiquedFragment() {

    protected abstract fun getHandler() : LoginSignUpHandler
}

enum class BottomOption {
    EMPTY,
    DISCLAIMER,
    FORGOT_PASSWORD
}

enum class BottomButtonOption {
    NONE,
    SIGN_UP,
    SIGN_IN
}

interface LoginSignUpHandler {

    fun createAccountWithEmail(sender: PiquedFragment, email: String)
    fun createAccount(email: String, password: String, userName: String)

    fun isNameValid(userName: String) : Boolean
    fun isPasswordValid(password: String) : Boolean
    fun isEmailValid(email: String) : Boolean

    fun configureBottom(sender: PiquedFragment, buttonOption: BottomButtonOption, bottomText: BottomOption)

    fun performPiquedLogin(userName : String, password : String)
    fun performFacebookLogin()
    fun goToFragment(sender: PiquedFragment?, fragment: LoginSignUpBaseFragment)
    fun startPasswordResetFlow()
}