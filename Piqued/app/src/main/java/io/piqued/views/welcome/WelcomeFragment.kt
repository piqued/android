package io.piqued.views.welcome

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2
import io.piqued.Piqued
import io.piqued.PiquedFragment
import io.piqued.R
import io.piqued.activities.HomeActivity
import io.piqued.utils.AnimationHelper
import io.piqued.utils.Preferences
import io.piqued.utils.UIHelper
import kotlinx.android.synthetic.main.fragment_welcome.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/7/16.
 * Piqued Inc.
 */

class WelcomeFragment : PiquedFragment() {

    @Inject
    internal lateinit var mPreferences: Preferences

    private var mShouldAutoScroll: Boolean = false
    private var mHandler: Handler? = null
    private var mShouldPerformAnimation = false

    private val mOnViewPagerTouchedListener = View.OnTouchListener { v, event ->
        mShouldAutoScroll = false
        showDoneButton()
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
            }
            MotionEvent.ACTION_UP -> v.performClick()
            else -> {
            }
        }
        false
    }

    private val mPageChangeListener = object: ViewPager2.OnPageChangeCallback() {
        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            if (position == WelcomeStep.maxIndex() && mShouldPerformAnimation) {
                showDoneButton()
            }
        }

        override fun onPageSelected(position: Int) {
            val step = WelcomeStep.fromInt(position)

            when (step) {
                WelcomeStep.STEP_4 -> {
                    mShouldPerformAnimation = true
                    handleViewPagerIndicator(step)
                }
                WelcomeStep.STEP_1, WelcomeStep.STEP_2, WelcomeStep.STEP_3 -> handleViewPagerIndicator(step)
            }
        }
    }

    private val mOnDoneClickedListener = View.OnClickListener {
        mPreferences.setWelcomeScreenShown(true)

        UIHelper.startActivity(this@WelcomeFragment, HomeActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (piquedActivity.application as Piqued).getComponent().inject(this)

        mShouldAutoScroll = true
        mHandler = Handler(Looper.getMainLooper())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        welcomeViewPager.adapter = WelcomePagerAdapter(piquedActivity)
        welcomeViewPager.registerOnPageChangeCallback(mPageChangeListener)
        welcomeViewPager.setOnTouchListener(mOnViewPagerTouchedListener)
        welcomeButton.setOnClickListener(mOnDoneClickedListener)
    }

    override fun getTagName(): String {
        return TAG
    }

    override fun onResume() {
        super.onResume()

        if (mShouldAutoScroll) {

            performAutoScroll()
        }
    }

    private fun performAutoScroll() {
        mHandler!!.postDelayed({
            if (isUIActive && mShouldAutoScroll) {
                val index = welcomeViewPager.currentItem

                if (index >= WelcomeStep.values.size - 1) {
                    mShouldAutoScroll = false
                }

                welcomeViewPager.setCurrentItem(index + 1, true)
                performAutoScroll()
            }
        }, TimeUnit.SECONDS.toMillis(3))
    }

    private fun showDoneButton() {
        if (welcomeButton.alpha < 0.1) {
            AnimationHelper.fadeIn(welcomeButton)
        }
    }

    private fun handleViewPagerIndicator(step: WelcomeStep) {

        dot1.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.white_dot_50))
        dot2.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.white_dot_50))
        dot3.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.white_dot_50))

        if (step === WelcomeStep.STEP_1) {
            if (welcomeIndicator.alpha > 0) {
                AnimationHelper.fadeOut(welcomeIndicator, 300)
            }
        } else {
            if (welcomeIndicator.alpha < 0.1) {
                AnimationHelper.fadeIn(welcomeIndicator, 300)
            }
        }

        when (step) {
            WelcomeStep.STEP_1 -> {
            }
            WelcomeStep.STEP_2 -> dot1.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.white_dot))
            WelcomeStep.STEP_3 -> dot2.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.white_dot))
            WelcomeStep.STEP_4 -> dot3.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.white_dot))
        }
    }

    companion object {

        private val TAG = WelcomeFragment::class.java.simpleName

        fun newInstance(): WelcomeFragment {
            return WelcomeFragment()
        }
    }

}
