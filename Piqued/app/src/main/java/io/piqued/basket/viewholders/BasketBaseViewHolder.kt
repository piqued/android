package io.piqued.basket.viewholders

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.viewholders.viewitem.BasketViewItem

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/25/17.
 * Piqued Inc.
 */

abstract class BasketBaseViewHolder(
        itemView: View,
        protected val dataProvider: BasketDataProvider
) : RecyclerView.ViewHolder(itemView) {

    protected fun getContext(): Context {
        return itemView.context
    }

    abstract fun onBindViewHolder(viewHolderItem: BasketViewItem)
}
