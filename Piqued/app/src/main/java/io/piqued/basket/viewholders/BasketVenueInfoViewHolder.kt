package io.piqued.basket.viewholders

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.LatLng
import io.piqued.R
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.viewholders.viewitem.BasketViewItem
import io.piqued.cloud.model.piqued.Venue
import kotlinx.android.synthetic.main.view_holder_basket_venue_info.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/28/17.
 * Piqued Inc.
 */

class BasketVenueInfoViewHolder(
        parent: ViewGroup,
        dataProvider: BasketDataProvider
): BasketBaseViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_venue_info, parent, false),
        dataProvider
) {

    private var mVenue: Venue? = null

    private val mOnAddressClickedListener = View.OnClickListener {

        val venue = dataProvider.venue
        val post = dataProvider.post

        if (venue != null && post != null) {
            dataProvider.onAddressClicked(
                    venue.fullAddress,
                    LatLng(post.getLatLng().latitude,
                            post.getLatLng().longitude)
            )
        }
    }

    private val mOnPhoneCallClicked = View.OnClickListener {

        if (mVenue != null) {
            dataProvider.onPhoneNumberClicked(mVenue!!.phoneNumber)
        }
    }

    init {
        setNotAvailable(itemView.priceTierText)
    }

    override fun onBindViewHolder(viewHolderItem: BasketViewItem) {

        mVenue = dataProvider.venue

        if (mVenue != null) {
            onSetVenue(mVenue!!)
        }
    }

    fun onSetVenue(venue: Venue) {

        updateAddress(venue.fullAddress)
        updateHours(venue.hasHourData(), venue.isOpenNow, venue.openStatus)
        updatePhone(venue.phoneNumber)
        updatePrice(venue.priceTier)
    }

    private fun updateAddress(address: String?) {
        if (address != null && address.length > 0) {
            itemView.locationIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.basket_directions))
            itemView.addressButton.setOnClickListener(mOnAddressClickedListener)
            updateText(itemView.venueAddress, address)
        } else {
            itemView.locationIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.basket_directions_na))
            setNotAvailable(itemView.venueAddress)
        }
    }

    private fun updateHours(hasData: Boolean, isOpen: Boolean, openStatus: String?) {

        if (hasData && openStatus != null && openStatus.length > 0) {

            itemView.hoursIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.basket_hours))

            updateText(itemView.venueHours, openStatus)
            if (!isOpen) {
                itemView.venueHours.setTextColor(ContextCompat.getColor(getContext(), R.color.red1))
            }

            return
        }

        itemView.hoursIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.basket_hours_na))
        setNotAvailable(itemView.venueHours)
    }

    private fun updatePhone(phoneNumber: String?) {
        if (phoneNumber != null && phoneNumber.length > 0) {
            itemView.phoneIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.basket_phone))
            itemView.phoneCallButton.setOnClickListener(mOnPhoneCallClicked)
            updateText(itemView.venuePhone, phoneNumber)
        } else {
            itemView.phoneIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.basket_phone_na))
            setNotAvailable(itemView.venuePhone)
        }
    }

    private fun updatePrice(priceTier: Int) {

        if (priceTier > -1) {

            val priceString = getContext().getString(R.string.basket_price_tier)
            val priceText = SpannableString(priceString)
            priceText.setSpan(ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 0, priceTier, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
            priceText.setSpan(ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.gray6)), priceTier, priceString.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)

            itemView.priceIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.basket_price))
            itemView.priceTierText.text = priceText

        } else {

            itemView.priceIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.basket_price_na))
            setNotAvailable(itemView.priceTierText)
        }
    }

    private fun setNotAvailable(textView: TextView) {
        val na = getContext().getString(R.string.basket_not_available)
        val textWithColor = SpannableString(na)
        textWithColor.setSpan(ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.gray6)), 0, na.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
        textView.text = textWithColor
    }

    private fun updateText(textView: TextView, value: String) {
        val textWithColor = SpannableString(value)
        textWithColor.setSpan(ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 0, value.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
        textView.text = textWithColor
    }
}
