package io.piqued.basket.viewholders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import io.piqued.R
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.relatedcontent.BasketRelatedContentAdapter
import io.piqued.basket.viewholders.viewitem.BasketViewItem
import kotlinx.android.synthetic.main.view_holder_basket_related_content.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/11/17.
 * Piqued Inc.
 */

class BasketRelatedContentViewHolder(
        parent: ViewGroup,
        handler: BasketDataProvider
): BasketBaseViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_related_content, parent, false),
        handler
) {

    private val mAdapter = BasketRelatedContentAdapter(handler)

    init {
        itemView.buttons.layoutManager = LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false)
        val divider = DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL)
        divider.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.related_content_divider)!!)
        itemView.buttons.addItemDecoration(divider)
        itemView.buttons.adapter = mAdapter
    }

    override fun onBindViewHolder(viewHolderItem: BasketViewItem) {
        if (dataProvider.post != null) {
            mAdapter.setPost(dataProvider.post)
            mAdapter.notifyDataSetChanged()
        }
    }
}
