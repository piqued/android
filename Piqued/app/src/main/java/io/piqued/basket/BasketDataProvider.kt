package io.piqued.basket

import android.content.Context
import androidx.annotation.DrawableRes
import com.airbnb.lottie.LottieAnimationView
import com.google.android.gms.maps.model.LatLng
import io.piqued.R
import io.piqued.basket.relatedcontent.RelatedContentItem
import io.piqued.cloud.model.piqued.Venue
import io.piqued.database.post.CloudPost
import io.piqued.model.Comment
import io.piqued.utils.customview.CheckableImageButton

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/25/17.
 * Piqued Inc.
 */

interface BasketDataProvider {

    val post: CloudPost?
    val venue: Venue?
    val comments: List<Comment>?
    val isLiked: Boolean
    val isUserLoggedIn: Boolean

    enum class RelatedContentType(@DrawableRes val imageRes: Int) {
        NA(0),
        FOUR_SQUARE(R.drawable.icon_foursquare),
        YELP(R.drawable.icon_yelp)
    }

    fun onLoadMoreTips()
    fun onRelatedContentClicked(type: RelatedContentItem)
    fun onPhoneNumberClicked(phoneNumber: String)
    fun onMapClicked()
    fun onAddressClicked(address: String, latLng: LatLng)
    fun onUserClicked(context: Context, userId: Long)
    fun onOpenComment()
    fun onShare(post: CloudPost)
    fun onLikePost(postId: Long, liked: Boolean, animationView: LottieAnimationView, imageButton: CheckableImageButton)
    fun onBookmark(postId: Long, bookmarked: Boolean)
    fun onPostImageClicked(post: CloudPost)

    // helper
    fun getToolbarHeight(): Int
    fun getStatusBarHeight(): Int
}
