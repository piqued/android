package io.piqued.basket.viewholders

import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.viewholders.viewitem.BasketViewItem
import kotlinx.android.synthetic.main.view_holder_basket_load_more_button.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/11/17.
 * Piqued Inc.
 */

class BasketLoadMoreTipViewHolder(
        parent: ViewGroup,
        dataProvider: BasketDataProvider
): BasketBaseViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_load_more_button, parent, false),
        dataProvider
) {
    init {

        itemView.loadMoreButton.setOnClickListener { dataProvider.onLoadMoreTips() }
    }

    override fun onBindViewHolder(viewHolderItem: BasketViewItem) {
        // does nothing
    }
}
