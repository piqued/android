package io.piqued.basket.comment

import android.content.Context
import io.piqued.database.user.User

/**
 *
 * Created by Kenny M. Liou on 9/8/18.
 * Piqued Inc.
 *
 */
interface CommentDataHandler {
    fun onUserClicked(context: Context, user: User)
}