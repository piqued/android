package io.piqued.basket.viewholders

import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.BasketViewType
import io.piqued.basket.viewholders.viewitem.BasketCommentItem
import io.piqued.basket.viewholders.viewitem.BasketTipItem
import io.piqued.basket.viewholders.viewitem.BasketViewItem
import io.piqued.model.Comment
import io.piqued.utils.PiquedLocationManager

/**
 *
 * Created by Kenny M. Liou on 11/15/18.
 * Piqued Inc.
 *
 */
class NewBasketViewAdapter(
        private val mProvider: BasketDataProvider,
        private val mLocationManager: PiquedLocationManager,
        private val percentageUpdateListener: OnPercentageUpdateListener
): RecyclerView.Adapter<BasketBaseViewHolder>() {

    companion object {
        private const val TIP_SHOWN_STEP = 30
    }

    interface OnScrollResponder {
        fun getPercentage(dy: Int, statusBarHeight: Int, toolbarHeight: Int): Float
    }

    interface OnPercentageUpdateListener {
        fun onPercentageUpdate(percentage: Float)
    }

    private var scrollResponder: OnScrollResponder? = null
    private val viewItems = ArrayList<BasketViewItem>()
    private var mTipShouldShow = 5 // initial value
    private var comment: Comment? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewTypeOrdinal: Int): BasketBaseViewHolder {
        val viewType = BasketViewType.fromInt(viewTypeOrdinal)

        return when(viewType) {

            BasketViewType.IMAGE -> {
                val viewHolder = BasketImageContainerViewHolder(parent, mProvider)
                scrollResponder = viewHolder
                viewHolder
            }
            BasketViewType.INFO -> BasketInfoViewHolder(parent, mProvider)
            BasketViewType.COMMENT -> BasketCommentViewHolder(parent, mProvider)
            BasketViewType.COMMENT_SHOW_MORE -> BasketCommentShowMoreViewHolder(parent, mProvider)
            BasketViewType.MAP -> BasketMapViewHolder(parent, mProvider)
            BasketViewType.VENUE_INFO -> BasketVenueInfoViewHolder(parent, mProvider)
            BasketViewType.RELATED_CONTENT -> BasketRelatedContentViewHolder(parent, mProvider)
            BasketViewType.TIP_COUNT -> BasketTipsCountViewHolder(parent, mProvider)
            BasketViewType.TIP_CELL -> BasketTipViewHolder(parent, mProvider)
            BasketViewType.NO_TIPS -> BasketNoTipsViewHolder(parent, mProvider)
            BasketViewType.LOAD_MORE -> BasketLoadMoreTipViewHolder(parent, mProvider)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return viewItems[position].viewType.ordinal
    }

    override fun getItemCount(): Int {
        return viewItems.size
    }

    fun updatePostStats() {
        notifyItemChanged(BasketViewType.INFO.ordinal)
    }

    fun updateAdapterData() {
        viewItems.clear()

        if (mProvider.post != null) {
            viewItems.add(BasketViewItem(BasketViewType.IMAGE))
            viewItems.add(BasketViewItem(BasketViewType.INFO))

            // construct comments from post
            viewItems.add(BasketCommentItem(Comment.fromPost(mProvider.post!!)))

            // one more comment from other user
            if (!mProvider.comments.isNullOrEmpty()) {
                comment = mProvider.comments!!.get(0)
            }

            if (comment != null) {
                viewItems.add(BasketCommentItem(comment!!))

                if (mProvider.comments!!.size > 1) {
                    viewItems.add(BasketViewItem(BasketViewType.COMMENT_SHOW_MORE))
                }
            }

            viewItems.add(BasketViewItem(BasketViewType.MAP))

            viewItems.add(BasketViewItem(BasketViewType.VENUE_INFO))

            viewItems.add(BasketViewItem(BasketViewType.RELATED_CONTENT))

            viewItems.add(BasketViewItem(BasketViewType.TIP_COUNT))

            if (mProvider.venue != null) {
                val tipsCount = Math.min(mProvider.venue!!.tipCount, mTipShouldShow)

                for (i in 0 until tipsCount) {
                    viewItems.add(BasketTipItem(mProvider.venue!!.getTip(i)))
                }

                if (mProvider.venue!!.tipCount > mTipShouldShow) {
                    viewItems.add(BasketViewItem(BasketViewType.LOAD_MORE))
                }
            }

        } else {
            // show loading
        }
    }



    override fun onBindViewHolder(holder: BasketBaseViewHolder, position: Int) {

        holder.onBindViewHolder(viewItems[position])

        if (holder is BasketInfoViewHolder) {
            holder.onSetLocationManager(mLocationManager)
        }
    }

    fun loadMoreTips() {
        mTipShouldShow += TIP_SHOWN_STEP
    }

    val scrollListener = object: RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val layoutManager = recyclerView.layoutManager!! as LinearLayoutManager

            if (layoutManager.findFirstVisibleItemPosition() == BasketViewType.IMAGE.ordinal) {
                percentageUpdateListener.onPercentageUpdate(scrollResponder?.getPercentage(recyclerView.computeVerticalScrollOffset(), mProvider.getStatusBarHeight(), mProvider.getToolbarHeight()) ?: 0f)
            } else {
                percentageUpdateListener.onPercentageUpdate(1f)
            }

        }
    }
}