package io.piqued.basket.viewholders.viewitem

import io.piqued.cloud.model.piqued.VenueTip
import io.piqued.basket.BasketViewType

/**
 *
 * Created by Kenny M. Liou on 11/15/18.
 * Piqued Inc.
 *
 */
data class BasketTipItem(
        val tip: VenueTip
): BasketViewItem(BasketViewType.TIP_CELL)