package io.piqued.basket.relatedcontent

import io.piqued.basket.BasketDataProvider

/**
 *
 * Created by Kenny M. Liou on 8/27/18.
 * Piqued Inc.
 *
 */

data class RelatedContentItem(val type: BasketDataProvider.RelatedContentType, val contentURI: String)