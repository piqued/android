package io.piqued.basket.viewholders

import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.piqued.R
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.viewholders.viewitem.BasketCommentItem
import io.piqued.basket.viewholders.viewitem.BasketViewItem
import io.piqued.model.Comment
import io.piqued.module.GlideApp
import io.piqued.utils.StringUtil
import kotlinx.android.synthetic.main.view_holder_basket_comment.view.*
import java.util.concurrent.TimeUnit

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/12/17.
 * Piqued Inc.
 */

class BasketCommentViewHolder(
        parent: ViewGroup,
        dataProvider: BasketDataProvider
): BasketBaseViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_comment, parent, false),
        dataProvider
) {

    private lateinit var mComment: Comment
    private val mHandler = Handler(Looper.getMainLooper())

    private val mUpdateTimeRunnable = Runnable {
        updateTimeText()
        checkTimeUpdate()
    }

    private val mOnOpenProfileClickListener = View.OnClickListener { view ->

        dataProvider.onUserClicked(view.context, mComment.user.userId)
    }

    init {
        itemView.user_image.setOnClickListener(mOnOpenProfileClickListener)
        itemView.user_name.setOnClickListener(mOnOpenProfileClickListener)
        itemView.setOnClickListener {
            dataProvider.onOpenComment()
        }
    }

    override fun onBindViewHolder(viewHolderItem: BasketViewItem) {
        if (viewHolderItem is BasketCommentItem) {

            mComment = viewHolderItem.comment
            updateViewHolder()
        }
    }

    private fun updateViewHolder() {

        updateTimeText()
        itemView.comment_text.text = mComment.message
        itemView.user_name.text = mComment.user.name

        GlideApp.with(getContext())
                .load(mComment.user.profileImageUrl)
                .placeholder(R.drawable.blank_profile)
                .centerCrop()
                .circleCrop()
                .into(itemView.user_image)

        checkTimeUpdate()
    }

    private fun checkTimeUpdate() {

        val diffInMilli = StringUtil.getDifferencesInMilli(mComment.timeStamp)

        val diffInHours = TimeUnit.MILLISECONDS.toHours(diffInMilli)

        if (diffInHours > 0) {
            return  // we don't care
        }

        val diffInMins = TimeUnit.MILLISECONDS.toMinutes(diffInMilli)

        if (diffInMins > 0) {
            mHandler.postDelayed(mUpdateTimeRunnable, TimeUnit.MINUTES.toMillis(1))
        } else {
            mHandler.postDelayed(mUpdateTimeRunnable, TimeUnit.SECONDS.toMillis(5))
        }
    }

    private fun updateTimeText() {
        itemView.post_time.text = StringUtil.getTimeStringLong(getContext(), mComment.timeStamp)
    }
}
