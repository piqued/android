package io.piqued.basket.viewholders

import android.annotation.SuppressLint
import android.location.Location
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import io.piqued.R
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.viewholders.viewitem.BasketViewItem
import io.piqued.database.post.CloudPost
import io.piqued.model.Comment
import io.piqued.utils.*
import kotlinx.android.synthetic.main.view_holder_basket_info.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/25/17.
 * Piqued Inc.
 */

class BasketInfoViewHolder(
        parent: ViewGroup,
        dataProvider: BasketDataProvider
) : BasketBaseViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_info, parent, false),
        dataProvider
) {

    private var mComments: List<Comment>? = null

    private val mOnCheckedChanged = CompoundButton.OnCheckedChangeListener { _, checked -> onBookmarked(checked) }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationUpdated(location: Location) {
            val post = dataProvider.post

            if (post != null) {
                val postLocation = Location("")
                postLocation.latitude = post.getLatLng().latitude
                postLocation.longitude = post.getLatLng().longitude

                val distance = DistanceHelper.getDistanceInString(location, postLocation)
                val sb = StringBuilder(distance)
                sb.append(" ")
                sb.append(getContext().getString(R.string.distance_unit_short))

                val distanceText = " " + getContext().getString(R.string.basket_away)

                itemView.basketDistanceText.text = StringUtil.createStringWithTypeFaces(getContext(), sb.toString(), distanceText)
            }
        }
    }

    private val onCommentClicked = View.OnClickListener {
        if (dataProvider.post != null) {
            dataProvider.onOpenComment()
        }
    }

    private val onLikeClicked = View.OnClickListener {v ->
        IntentUtil.runIfUserLoggedIn(v.context, dataProvider.isUserLoggedIn, object : LoginRequiredExecution {
            override fun runIfLoggedIn() {
                val post = dataProvider.post

                if (post != null) {
                    val liked = post.myReaction == CloudPost.Reaction.NONE.ordinal

                    dataProvider.onLikePost(post.postId, liked, itemView.basketLikeAnimationView, itemView.basketLikeImageButton)
                }
            }
        })
    }

    @SuppressLint("ClickableViewAccessibility")
    private val onBookmarkTouchedListener = View.OnTouchListener { v, event ->
        var result = false
        if (MotionEvent.ACTION_DOWN == event.action && !dataProvider.isUserLoggedIn) {
            IntentUtil.showSignUpPopup(v.context)
            result = true
        }
        result
    }

    init {
        itemView.basketBookmarkToggleButton.setOnCheckedChangeListener(mOnCheckedChanged)
        itemView.basketBookmarkToggleButton.setOnTouchListener(onBookmarkTouchedListener)
        itemView.commentButton.setOnClickListener(onCommentClicked)
        itemView.basketLikeButton.setOnClickListener(onLikeClicked)
        itemView.shareButton.setOnClickListener { v ->
            IntentUtil.runIfUserLoggedIn(v.context, dataProvider.isUserLoggedIn, object : LoginRequiredExecution {
                override fun runIfLoggedIn() {
                    val post = dataProvider.post
                    if (post != null) {
                        dataProvider.onShare(post)
                    }
                }
            })
        }
    }

    fun onBind(comments: List<Comment>) {
        mComments = comments
    }

    override fun onBindViewHolder(viewHolderItem: BasketViewItem) {

        val post = dataProvider.post
        if (post != null) {
            onSetPost(post)
        }

        val comment = dataProvider.comments

        if (comment != null) {
            onBind(comment)
        }
    }

    private fun onSetPost(post: CloudPost) {
        itemView.basketBookmarkToggleButton.isChecked = post.bookmarked
        itemView.basketLikeImageButton.isChecked = post.liked()
        itemView.basketNameText.text = post.title

        updateStats(post)
    }

    private fun updateStats(post: CloudPost) {

        val stringBuilder = StringBuilder()

        if (post.totalBookmarks > 0) {
            stringBuilder.append(String.format(getContext().getString(R.string.basket_stats_bookmarks), post.totalBookmarks))
        }

        if (post.getTotalLikes() > 0) {
            stringBuilder.append(" ")
            stringBuilder.append(String.format(getContext().getString(R.string.basket_stats_likes), post.getTotalLikes()))
        }

        itemView.basketStatsTextView.text = stringBuilder.toString()
    }

    private fun onBookmarked(bookmarked: Boolean) {

        val post = dataProvider.post

        if (post != null) {
            if (bookmarked != post.bookmarked) {
                post.bookmarked = bookmarked

                dataProvider.onBookmark(post.postId, bookmarked)
            }
        }
    }

    fun onSetLocationManager(manager: PiquedLocationManager) {

        manager.getCurrentLocation(mLocationCallback)
    }
}
