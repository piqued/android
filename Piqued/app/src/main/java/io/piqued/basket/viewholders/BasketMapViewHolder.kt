package io.piqued.basket.viewholders

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import io.piqued.R
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.viewholders.viewitem.BasketViewItem
import kotlinx.android.synthetic.main.view_holder_basket_map.view.*

/**
 *
 * Created by Kenny M. Liou on 9/15/18.
 * Piqued Inc.
 *
 */

class BasketMapViewHolder(
        parent: ViewGroup,
        dataProvider: BasketDataProvider
): BasketBaseViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_map, parent, false),
        dataProvider
), OnMapReadyCallback {

    companion object {
        private const val ZOOM_LEVEL = 13f
    }

    private var mGoogleMap: GoogleMap? = null

    init {
        itemView.basketMapView.onCreate(Bundle.EMPTY)
        itemView.basketMapView.getMapAsync(this)
        itemView.basketMapButton.setOnClickListener {

            dataProvider.onMapClicked()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {

        mGoogleMap = googleMap

        updateMapLocation()
    }

    override fun onBindViewHolder(viewHolderItem: BasketViewItem) {
        updateMapLocation()
    }

    private fun updateMapLocation() {
        val post = dataProvider.post
        val googleMap = mGoogleMap
        if (post != null && googleMap != null) {

            val postPosition = post.getLatLng()

            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(postPosition, ZOOM_LEVEL))

            googleMap.addMarker(MarkerOptions()
                    .position(postPosition)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.basket_map_location)))

            googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
            googleMap.uiSettings.isMapToolbarEnabled = false
            googleMap.uiSettings.isZoomControlsEnabled = false
        }
    }
}