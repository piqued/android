package io.piqued.basket.viewholders.viewitem

import io.piqued.basket.BasketViewType

/**
 *
 * Created by Kenny M. Liou on 11/15/18.
 * Piqued Inc.
 *
 */

open class BasketViewItem(
        val viewType: BasketViewType
)