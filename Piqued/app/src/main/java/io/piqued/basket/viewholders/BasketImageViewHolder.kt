package io.piqued.basket.viewholders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import io.piqued.R
import io.piqued.utils.ImageDownloadUtil
import kotlinx.android.synthetic.main.view_holder_basket_image.view.*

/**
 *
 * Created by Kenny M. Liou on 11/10/18.
 * Piqued Inc.
 *
 */
class BasketImageViewHolder(parent: ViewGroup, onClickListener: View.OnClickListener)
    : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_image, parent, false)
) {

    init {
        itemView.basketImageView.setOnClickListener(onClickListener)
    }

    fun onBind(index: Int, postId: Long, imageUrl: String) {

        ImageDownloadUtil.getDownloadUtilInstance(itemView.context)
                .loadPostImage(postId, index, imageUrl, itemView.basketImageView)
    }
}