package io.piqued.basket

import android.content.Context
import android.location.Location
import io.piqued.api.PiquedGetVenueCallback
import io.piqued.database.post.CloudPost

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/23/17.
 * Piqued Inc.
 */
interface BasketActionHandler {
    fun onOpenPost(postId: Long)
    fun onBookMarkPost(postId: Long, bookmarked: Boolean)
    fun onLikePost(postId: Long, liked: Boolean)
    fun onFollowUser(postId: Long, followed: Boolean)
    fun getVenueInfo(postId: Long, callback: PiquedGetVenueCallback?)
    fun openPostOwnerProfile(context: Context, postId: Long)
    fun openMapWithPost(postId: Long)
    val isUserLoggedIn: Boolean
    val currentLocation: Location?
    fun getPost(postId: Long): CloudPost?
}