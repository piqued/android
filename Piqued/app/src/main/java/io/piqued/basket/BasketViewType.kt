package io.piqued.basket

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/25/17.
 * Piqued Inc.
 */

enum class BasketViewType {

    IMAGE,
    INFO,
    COMMENT,
    COMMENT_SHOW_MORE,
    MAP,
    VENUE_INFO,
    RELATED_CONTENT,
    TIP_COUNT,

    // free form after this
    TIP_CELL,
    NO_TIPS,
    LOAD_MORE;


    companion object {

        private val types = values()

        fun fromInt(index: Int): BasketViewType {
            if (index > -1 && index < types.size) {
                return types[index]
            }

            throw IllegalArgumentException("Illegal index for enum " + BasketViewType::class.java.simpleName + ": " + index)
        }
    }
}
