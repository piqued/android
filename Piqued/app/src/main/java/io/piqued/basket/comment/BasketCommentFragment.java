package io.piqued.basket.comment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ncapdevi.fragnav.FragNavController;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

import javax.inject.Inject;

import io.piqued.Piqued;
import io.piqued.R;
import io.piqued.activities.HomeBaseActivity;
import io.piqued.api.PiquedApiCallback;
import io.piqued.basket.viewholders.BasketFlatCommentViewHolder;
import io.piqued.database.post.CloudPost;
import io.piqued.database.util.PiquedErrorCode;
import io.piqued.database.util.PiquedLogger;
import io.piqued.databinding.FragmentBasketCommentBinding;
import io.piqued.model.Comment;
import io.piqued.model.MetricValues;
import io.piqued.utils.Preferences;
import io.piqued.utils.postmanager.PostManager;
import io.piqued.views.DoubleTapFragment;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/11/17.
 * Piqued Inc.
 */

public class BasketCommentFragment extends DoubleTapFragment {

    private static final String TAG = BasketCommentFragment.class.getSimpleName();

    private static final String KEY_POST = "post";
    private static final String KEY_COMMENTS = "comments";

    public static BasketCommentFragment newInstance(CloudPost post, List<Comment> comments) {
        
        Bundle args = new Bundle();

        args.putParcelable(KEY_POST, post);
        ArrayList<Comment> commentList = new ArrayList<>();
        commentList.addAll(comments);

        args.putParcelableArrayList(KEY_COMMENTS, commentList);

        BasketCommentFragment fragment = new BasketCommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    MetricValues mMetric;
    @Inject
    PostManager mManager;
    @Inject
    Preferences preferences;

    private FragmentBasketCommentBinding mBinding;
    private CloudPost mPost;
    private List<Comment> mComments = new ArrayList<>();
    private CommentAdapter mAdapter = new CommentAdapter();

    @Override
    public String getTagName() {
        return TAG;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((Piqued) getPiquedActivity().getApplication()).getComponent().inject(this);

        List<Comment> comments;
        Comment userComment;

        if (savedInstanceState != null) {
            mPost = savedInstanceState.getParcelable(KEY_POST);
            comments = savedInstanceState.getParcelableArrayList(KEY_COMMENTS);
            userComment = Comment.fromPost(mPost);
        } else {
            mPost = getArguments().getParcelable(KEY_POST);
            comments = getArguments().getParcelableArrayList(KEY_COMMENTS);
            userComment = Comment.fromPost(mPost);
        }

        buildCommentList(comments, userComment);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mBinding = FragmentBasketCommentBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final LinearLayoutManager manager = new LinearLayoutManager(getContext());
        mBinding.comments.setLayoutManager(manager);
        mBinding.comments.setAdapter(mAdapter);
        mBinding.sendButton.setOnClickListener(mOnSendClickedListener);

        mBinding.commentRootView.setOnApplyWindowInsetsListener((v, insets) -> {

            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) mBinding.toolbarBackground.getLayoutParams();

            params.height = insets.getSystemWindowInsetTop();

            mBinding.toolbarBackground.setLayoutParams(params);

            return insets.consumeSystemWindowInsets();
        });

        setSupportActionBar(mBinding.navToolbar.toolbar, R.string.empty, true);

        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mPost != null) {
            outState.putParcelable(KEY_POST, mPost);
        }

        if (mComments != null && mComments.size() > 0) {
            ArrayList<Comment> comments = new ArrayList<>();
            comments.addAll(mComments);
            outState.putParcelableArrayList(KEY_COMMENTS, comments);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getPiquedActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        updateGuideLine(mBinding.bottomSpace);

        mBinding.comments.scrollToPosition(mComments.size() - 1);
    }

    @Override
    protected boolean shouldTrackKeyboardVisible() {
        return true;
    }

    private final View.OnClickListener mOnSendClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            runIfLoggedIn(preferences, () -> {
                final String comment = mBinding.commentInput.getText().toString();

                if (comment.length() > 0) {
                    mBinding.sendButton.setEnabled(false);

                    mManager.makeNewComment(mPost, mBinding.commentInput.getText().toString(), mPostCommentCallback);
                }
            });
        }
    };

    private final PiquedApiCallback<Comment> mPostCommentCallback = new PiquedApiCallback<Comment>() {
        @Override
        public void onSuccess(Comment data) {

            buildCommentList(data);

            mAdapter.notifyDataSetChanged();

            mBinding.commentInput.setText("");

            mBinding.sendButton.setEnabled(true);
        }

        @Override
        public void onFailure(PiquedErrorCode errorCode, String payload) {
            // on no, we failed
            PiquedLogger.e(TAG, errorCode, payload);
        }
    };

    private final CommentDataHandler commentHandler = (context, user) -> ((HomeBaseActivity) getPiquedActivity()).openUserProfile(user.getUserId());


    @Override
    public void onTabDoubleTapped(@NotNull FragNavController backStackController) {

        LinearLayoutManager layoutManager = (LinearLayoutManager) mBinding.comments.getLayoutManager();

        if (layoutManager != null) {
            if (layoutManager.findFirstVisibleItemPosition() == 0 && mBinding.comments.computeVerticalScrollOffset() == 0) {
                Stack<Fragment> backStack = backStackController.getCurrentStack();
                if (backStack != null && backStack.size() > 1) {
                    backStackController.popFragments(backStack.size() - 1);
                }
            } else {
                mBinding.comments.smoothScrollToPosition(0);
            }
        }
    }

    private class CommentAdapter extends RecyclerView.Adapter<BasketFlatCommentViewHolder> {

        @Override
        @NonNull
        public BasketFlatCommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new BasketFlatCommentViewHolder(parent);
        }

        @Override
        public void onBindViewHolder(@NonNull BasketFlatCommentViewHolder holder, int position) {

            holder.setCommentHandler(commentHandler);
            holder.setComment(mComments.get(position));
        }

        @Override
        public int getItemCount() {
            return mComments.size();
        }
    }

    @Override
    protected void onKeyboardHidden(boolean isHidden, int keyboardHeight) {
        if (isHidden) {
            updateGuideLine(mBinding.bottomSpace);
        } else {
            updateGuideLine(mBinding.bottomSpace, keyboardHeight + mMetric.getHomeBottomNavigationBarHeight());
        }
    }

    private void buildCommentList(List<Comment> comments, Comment userComment) {
        mComments.clear();

        for (Comment comment : comments) {
            if (!mComments.contains(comment)) {
                mComments.add(comment);
            }
        }

        buildCommentList(userComment);
    }

    private void buildCommentList(Comment userComment) {
        if (!mComments.contains(userComment)) {
            mComments.add(userComment);
        }

        // organize commits
        Collections.sort(mComments, new Comparator<Comment>() {
            @Override
            public int compare(Comment left, Comment right) {

                return left.getTimeStamp() < right.getTimeStamp() ? -1: 1;
            }
        });
    }
}
