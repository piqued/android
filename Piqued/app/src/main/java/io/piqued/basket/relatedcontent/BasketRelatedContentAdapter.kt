package io.piqued.basket.relatedcontent

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.basket.BasketDataProvider
import io.piqued.database.post.CloudPost

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/11/17.
 * Piqued Inc.
 */

class BasketRelatedContentAdapter(private val basketDataHandler: BasketDataProvider)
    : RecyclerView.Adapter<RelatedContentViewHolder>() {

    private val itemList = ArrayList<RelatedContentItem>()

    fun setPost(post: CloudPost?) {

        itemList.clear()

        if (post != null) {
            if (post.fourSquareId != null && !post.fourSquareId.isNullOrEmpty()) {
                itemList.add(RelatedContentItem(BasketDataProvider.RelatedContentType.FOUR_SQUARE, post.fourSquareId!!))
            }

            if (post.yelpId != null && !post.yelpId.isNullOrEmpty()) {
                itemList.add(RelatedContentItem(BasketDataProvider.RelatedContentType.YELP, post.yelpId!!))
            }
        } else {
            itemList.add(RelatedContentItem(BasketDataProvider.RelatedContentType.NA, ""))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RelatedContentViewHolder {
        return RelatedContentViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RelatedContentViewHolder, position: Int) {

        holder.setRelatedContentItem(itemList[position], basketDataHandler)
    }

    override fun getItemCount(): Int {

        return itemList.size
    }
}
