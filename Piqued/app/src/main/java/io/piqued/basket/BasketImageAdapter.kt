package io.piqued.basket

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.database.post.CloudPost
import io.piqued.database.post.PiquedImage
import io.piqued.basket.viewholders.BasketImageViewHolder

/**
 *
 * Created by Kenny M. Liou on 11/10/18.
 * Piqued Inc.
 *
 */
class BasketImageAdapter: RecyclerView.Adapter<BasketImageViewHolder>() {

    var onClickListener: View.OnClickListener? = null

    private val imageList = ArrayList<PiquedImage>()
    private var postId = 0L
    private val internalClickListener = View.OnClickListener { view ->
        onClickListener?.onClick(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BasketImageViewHolder {
        return BasketImageViewHolder(parent, internalClickListener)
    }

    override fun getItemCount(): Int {
        return imageList.size
    }

    override fun onBindViewHolder(holder: BasketImageViewHolder, position: Int) {

        holder.onBind(position, postId, imageList[position].largeImageUrl)
    }

    fun updateImaged(post: CloudPost) {

        postId = post.postId

        imageList.clear()
        imageList.addAll(post.images)
    }


}