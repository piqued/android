package io.piqued.basket.viewholders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import io.piqued.R
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.BasketImageAdapter
import io.piqued.basket.viewholders.viewitem.BasketViewItem
import io.piqued.database.post.CloudPost
import kotlinx.android.synthetic.main.view_holder_basket_image_container.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/25/17.
 * Piqued Inc.
 */

class BasketImageContainerViewHolder(
        parent: ViewGroup,
        provider: BasketDataProvider
) : BasketBaseViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_image_container, parent, false),
        provider
), NewBasketViewAdapter.OnScrollResponder {

    private val adapter = BasketImageAdapter()
    private val pagerSnapHelper = PagerSnapHelper()

    private val mOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            when(newState) {
                RecyclerView.SCROLL_STATE_IDLE -> {
                    val position = (recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()

                    updateImageCount(position + 1, adapter.itemCount)
                }
            }
        }
    }

    init {
        itemView.basketImageRecyclerView.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        itemView.basketImageRecyclerView.adapter = adapter
        itemView.basketImageIndicator.setRecyclerView(itemView.basketImageRecyclerView)
        itemView.basketImageRecyclerView.addOnScrollListener(mOnScrollListener)
        pagerSnapHelper.attachToRecyclerView(itemView.basketImageRecyclerView)
    }

    private fun updateImageCount(currentIndex: Int, totalCount: Int) {
        itemView.basketImageCount.text = itemView.context.getString(R.string.post_image_index, currentIndex, totalCount)
    }

    override fun onBindViewHolder(viewHolderItem: BasketViewItem) {

        val post = dataProvider.post
        if (post != null) {
            onSetPost(post)
        }
    }

    private fun onSetPost(post: CloudPost) {

        if (post.images.size < 2) {
            itemView.basketImageIndicator.visibility = View.GONE
            itemView.basketImageCount.visibility = View.GONE
        } else {
            itemView.basketImageIndicator.visibility = View.VISIBLE
            itemView.basketImageCount.visibility = View.VISIBLE

            updateImageCount(1, post.images.size)
        }

        adapter.updateImaged(post)
        adapter.notifyDataSetChanged()

        itemView.basketImageIndicator.setItemCount(post.images.size)
        itemView.basketImageIndicator.setCurrentPosition(0)
        itemView.basketImageIndicator.forceUpdateItemCount()
    }


    override fun getPercentage(dy: Int, statusBarHeight: Int, toolbarHeight: Int): Float {

        val visibleHeight = itemView.basketImageRecyclerView.height - (statusBarHeight + toolbarHeight)

        return if (dy > visibleHeight) {
            1f
        } else {
            dy.toFloat() / visibleHeight
        }
    }
}
