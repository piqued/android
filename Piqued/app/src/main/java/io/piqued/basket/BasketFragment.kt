package io.piqued.basket

import android.animation.Animator
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.view.*
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.lottie.LottieAnimationView
import com.google.android.gms.maps.model.LatLng
import com.ncapdevi.fragnav.FragNavController
import io.piqued.R
import io.piqued.activities.HomeBaseActivity
import io.piqued.api.PiquedApiCallback
import io.piqued.basket.relatedcontent.RelatedContentItem
import io.piqued.basket.viewholders.NewBasketViewAdapter
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.model.piqued.Venue
import io.piqued.cloud.model.yelp.YelpBusiness
import io.piqued.cloud.repository.FourSquareRepository
import io.piqued.cloud.repository.YelpRepository
import io.piqued.cloud.util.CloudPreference
import io.piqued.database.post.CloudPost
import io.piqued.database.post.PostId
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.model.Comment
import io.piqued.utils.*
import io.piqued.utils.customview.CheckableImageButton
import io.piqued.utils.modelmanager.PiquedUserManager
import io.piqued.utils.postmanager.PostManager
import io.piqued.views.DoubleTapFragment
import kotlinx.android.synthetic.main.fragment_basket.*
import kotlinx.android.synthetic.main.toolbar_basket.*
import java.util.*
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 9/8/18.
 * Piqued Inc.
 *
 */
class BasketFragment: DoubleTapFragment(), BasketDataProvider {
    /**
     * Statics methods
     */
    companion object {
        private val TAG = BasketFragment::class.java.simpleName
        private const val KEY_POST_ID = "postId"
        private const val KEY_REPORT_RESULT_CODE = 303

        fun newInstance(postId: PostId): BasketFragment {
            return newInstance(postId.postId)
        }

        @JvmStatic
        fun newInstance(postId: Long): BasketFragment {

            val args = Bundle()

            args.putLong(KEY_POST_ID, postId)

            val fragment = BasketFragment()
            fragment.arguments = args

            return fragment
        }
    }

    @Inject
    internal lateinit var locationManager: PiquedLocationManager
    @Inject
    internal lateinit var postManager: PostManager
    @Inject
    internal lateinit var userManager: PiquedUserManager
    @Inject
    internal lateinit var yelpRepository: YelpRepository
    @Inject
    internal lateinit var fourSquareRepository: FourSquareRepository
    @Inject
    lateinit var cloudPreference: CloudPreference
    @Inject
    lateinit var preferences: Preferences

    private var postId: Long = 0
    private lateinit var viewModel: BasketViewModel
    private lateinit var adapter: NewBasketViewAdapter
    private var currentPost: CloudPost? = null
    private var mVenue: Venue? = null
    private var mComments: List<Comment>? = null
    private var mYelpBusiness: YelpBusiness? = null
    private var postStatsChanged = false
    private var animating = false

    /**
     * Life cycles
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        if (arguments != null) {
            postId = arguments!!.getLong(KEY_POST_ID)
        } else {
            throw IllegalArgumentException("Cannot open post without post id")
        }

        setHasOptionsMenu(true)

        viewModel = ViewModelProviders.of(this).get(BasketViewModel::class.java)
        viewModel.getPost().observe(this, postObserver)
        viewModel.getYelpVenueData().observe(this, yelpBusinessObserver)
        viewModel.getCommentData().observe(this, commentObserver)
        viewModel.getVenueData().observe(this, venueObserver)

        adapter = NewBasketViewAdapter(this, locationManager, onPercentageUpdateListener)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_basket, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        basketRecyclerView.layoutManager = LinearLayoutManager(view.context)

        basketRootView.setOnApplyWindowInsetsListener { _, windowInsets ->
            val param = basketBottomNavigationPlaceHolder.layoutParams as ConstraintLayout.LayoutParams
            param.setMargins(param.leftMargin, param.topMargin, param.rightMargin, windowInsets.systemWindowInsetBottom)
            basketBottomNavigationPlaceHolder.layoutParams = param

            val toolbarParam = basketToolbarWrapper.layoutParams as ConstraintLayout.LayoutParams
            toolbarParam.setMargins(toolbarParam.leftMargin, windowInsets.systemWindowInsetTop, toolbarParam.rightMargin, toolbarParam.bottomMargin)
            basketToolbarWrapper.layoutParams = toolbarParam

            windowInsets.consumeSystemWindowInsets()
        }

        basketRecyclerView.adapter = adapter
        basketRecyclerView.addOnScrollListener(adapter.scrollListener)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (currentPost != null && userManager.isAccountOwner(currentPost!!.userId)) {
            inflater.inflate(R.menu.basket_own_menu, menu)
        } else {
            inflater.inflate(R.menu.basket_other_menu, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.report_post -> {
                showReportPostPrompt()
                true
            }
            R.id.delete_post -> {
                showDeletePostPrompt()
                true
            }
            android.R.id.home -> {
                piquedActivity.onBackPressed()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        setSupportActionBar(basketToolbar, R.string.empty, true)

        viewModel.fetchVenue(postId, postManager)
        viewModel.fetchPost(postId, postManager)

        basketRecyclerView.adapter = adapter
        basketRecyclerView.addOnScrollListener(adapter.scrollListener)
    }

    /**
     * Post related actions
     */
    override val post: CloudPost?
        get() = currentPost

    override val venue: Venue?
        get() = mVenue

    override val comments: List<Comment>?
        get() = mComments

    override val isLiked: Boolean
        get() = post?.liked() ?: false

    override val isUserLoggedIn: Boolean
        get() = preferences.userLoggedIn()

    override fun onLoadMoreTips() {

        adapter.loadMoreTips()
        adapter.notifyDataSetChanged()
    }

    override fun onRelatedContentClicked(type: RelatedContentItem) {
        when (type.type) {
            BasketDataProvider.RelatedContentType.FOUR_SQUARE -> {
                openRelatedContentUrl(fourSquareRepository.getFourSquareUrl(type.contentURI))
            }
            BasketDataProvider.RelatedContentType.YELP -> {
                val yelpObj = mYelpBusiness
                if (yelpObj != null && yelpObj.yelpId == type.contentURI) {
                    openRelatedContentUrl(yelpObj.businessUrl)
                }
            }
            else -> return
        }
    }

    private fun openRelatedContentUrl(relatedContentUrl: String) {
        val openUrlIntent = Intent(Intent.ACTION_VIEW)
        openUrlIntent.data = Uri.parse(relatedContentUrl)

        startActivity(openUrlIntent)
    }

    override fun onPhoneNumberClicked(phoneNumber: String) {
        if (phoneNumber.isNotEmpty()) {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phoneNumber"))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    override fun onMapClicked() {

        val latLng: LatLng? = mVenue?.location ?: post?.getLatLng()
        val address: String = mVenue?.fullAddress ?: post?.address ?: ""

        if (latLng != null && !address.isEmpty()) {
            onAddressClicked(address, latLng)
        }
    }

    override fun onAddressClicked(address: String, latLng: LatLng) {
        val uriBuilder = StringBuilder()

        if (address.isNotEmpty()) {
            uriBuilder.append(String.format(Locale.getDefault(), "geo:0,0?q=%s", address))
        } else {
            uriBuilder.append(String.format(Locale.getDefault(), "geo:%f,%f", latLng.latitude, latLng.longitude))
        }

        val uri = uriBuilder.toString()

        if (uri.isNotEmpty()) {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            startActivity(intent)
        }
    }


    override fun onUserClicked(context: Context, userId: Long) {
        (activity as HomeBaseActivity).openUserProfile(userId)
    }

    override fun onOpenComment() {
        if (currentPost != null) {
            val verifyPostValid = postManager.getCloudPost(currentPost!!.postId)
            val currentComments = comments

            if (verifyPostValid != null && currentComments != null) {
                (activity as HomeBaseActivity).openCommentsFragment(PostId(verifyPostValid.postId), currentComments)
            } else {
                ViewUtil.presentSnackbar(piquedActivity, R.string.basket_comment_failed)
            }
        }
    }

    override fun onShare(post: CloudPost) {

        val myUserRecord = userManager.myUserRecord

        if (myUserRecord != null) {

            val sharePath = if (post.sharePath.isNullOrEmpty()) {
                val postIdEncoded = String(Base64.encode(post.postId.toString().toByteArray(), Base64.NO_WRAP))
                val userIdEncoded = String(Base64.encode(myUserRecord.userId.toString().toByteArray(), Base64.NO_PADDING))

                cloudPreference.getShareHostUrl() + "/shares/" + postIdEncoded + "?sb=" + userIdEncoded
            } else {
                val cloudSharePath = post.sharePath

                if (!cloudSharePath.isNullOrEmpty()) {
                    if (!cloudPreference.getShareHostUrl().endsWith("/") && !cloudSharePath.startsWith("/")) {
                        cloudPreference.getShareHostUrl() + "/" + cloudSharePath
                    } else if (cloudPreference.getShareHostUrl().endsWith("/") && cloudSharePath.startsWith("/")) {
                        cloudPreference.getShareHostUrl() + cloudSharePath.substring(1, cloudSharePath.length - 1)
                    } else {
                        cloudPreference.getShareHostUrl() + cloudSharePath
                    }
                } else{
                    ""
                }
            }

            if (sharePath.isNotEmpty()) {

                val sVenue = mVenue
                val yVenue = mYelpBusiness

                val venueName = if (!post.locationName.isNullOrEmpty()) {
                    post.locationName
                } else if (sVenue != null && !sVenue.venueName.isNullOrEmpty()) {
                    sVenue.venueName
                } else if (yVenue != null && !yVenue.businessName.isEmpty()) {
                    yVenue.businessName
                } else {
                    ""
                }

                val shareIntent = Intent(Intent.ACTION_SEND)
                        .setType("text/plain")
                        .putExtra(Intent.EXTRA_TEXT, (if (!venueName.isNullOrEmpty()) venueName + "\n\n" else "") + sharePath)


                startActivity(Intent.createChooser(shareIntent, getString(R.string.basket_share_with)))
            }
        }
    }

    private val animationListener = object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {}

        override fun onAnimationCancel(animation: Animator?) {}

        override fun onAnimationStart(animation: Animator?) {}

        override fun onAnimationEnd(animation: Animator?) {
            animating = false
            if (isUIActive && postStatsChanged) {
                adapter.updatePostStats()
            }
        }

    }

    override fun onLikePost(postId: Long, liked: Boolean, animationView: LottieAnimationView, imageButton: CheckableImageButton) {
        postManager.setPostReaction(
                postId,
                if (liked) CloudPost.Reaction.LIKE else CloudPost.Reaction.NONE,
                object : PiquedApiCallback<Long> {
                    override fun onSuccess(data: Long?) {
                        if (isUIActive) {
                            imageButton.isChecked = liked
                            if (liked) {
                                animationView.removeAllAnimatorListeners()
                                animationView.addAnimatorListener(animationListener)
                                animating = true
                                animationView.playAnimation()
                            }
                        }

                        currentPost?.mergeNewData(postManager.getCloudPost(postId))

                        if (!animating) {

                            adapter.updatePostStats()
                        } else {
                            postStatsChanged = true
                        }
                    }

                    override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                        PiquedLogger.e(TAG, errorCode, payload)
                        if (isUIActive) {
                            if (errorCode == PiquedErrorCode.POST_NOT_VALID) {
                                ViewUtil.presentSnackbar(piquedActivity, R.string.basket_like_failed)
                            }
                        }
                    }

                })
    }

    override fun onBookmark(postId: Long, bookmarked: Boolean) {
        postManager.bookmarkPost(postId, bookmarked, object : PiquedCloudCallback<Long> {
            override fun onSuccess(result: Long?) {
                if (!animating) {
                    adapter.updatePostStats()
                } else {
                    postStatsChanged = true
                }
            }

            override fun onFailure(error: Throwable?, message: String?) {
                PiquedLogger.e(TAG, PiquedErrorCode.NETWORK, message)
            }
        })
    }

    override fun onPostImageClicked(post: CloudPost) {
        // show image preview
    }

    override fun onTabDoubleTapped(backStackController: FragNavController) {
        if (isUIActive) {
            val layoutManager = basketRecyclerView.layoutManager as LinearLayoutManager
            if (layoutManager.findFirstVisibleItemPosition() == 0 && basketRecyclerView.computeVerticalScrollOffset() == 0) {
                backStackController.popFragments(backStackController.currentStack?.size ?: 1)
            } else {
                basketRecyclerView.smoothScrollToPosition(0)
            }
        }
    }

    /**
     * Helper methods
     */

    private fun showDeletePostPrompt() {

        if (currentPost != null) {
            createPromptMessage(R.string.basket_delete_title, R.string.basket_delete_prompt, DialogInterface.OnClickListener { _, _ ->

                val postId = currentPost!!.postId

                postManager.deletePost(currentPost!!, object : PiquedApiCallback<Void?> {
                    override fun onSuccess(data: Void?) {
                        PiquedLogger.i(TAG, "delete post successfully")

                        BroadcastUtil.sendPostDeleteBroadcast(context!!, postId)

                        piquedActivity.onBackPressed()
                    }

                    override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                        PiquedLogger.e(TAG, "failed to delete post", errorCode, payload)

                        if (isUIActive) {
                            if (errorCode == PiquedErrorCode.NETWORK_404) {

                                ViewUtil.presentSnackbar(piquedActivity, R.string.basket_delete_already_delete)

                                piquedActivity.onBackPressed()
                            }
                        }
                    }
                })
            }).create().show()
        }
    }

    private fun showReportPostPrompt() {
        runIfLoggedIn(preferences, object : LoginRequiredExecution {
            override fun runIfLoggedIn() {
                createPromptMessage(R.string.basket_report_title,
                        R.string.basket_report_prompt,
                        R.string.yes_button,
                        DialogInterface.OnClickListener { _, _ ->
                            openEmailForReport()
                        }).create().show()
            }
        })
    }

    private fun openEmailForReport() {

        val currentContext = context

        if (currentContext != null) {
            val emailSubject = currentContext.getString(R.string.report_email_subject, post!!.title, post!!.postId)
            val emailContent = currentContext.getString(R.string.report_content)

            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.parse(currentContext.getString(R.string.report_email_address)))
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, emailSubject)
            emailIntent.putExtra(Intent.EXTRA_TEXT, emailContent)

            startActivityForResult(emailIntent, KEY_REPORT_RESULT_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == KEY_REPORT_RESULT_CODE) {
            // user came back from report
            if (resultCode == Activity.RESULT_OK) {
                // show message
                val currentContext = context
                if (currentContext != null) {
                    Toast.makeText(context, R.string.report_success_message, Toast.LENGTH_LONG).show()
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun createPromptMessage(
            @StringRes titleRes: Int,
            @StringRes messageRes: Int,
            onClickListener: DialogInterface.OnClickListener
    ): AlertDialog.Builder {
        return createPromptMessage(titleRes, messageRes, R.string.ok_button, onClickListener)
    }
    private fun createPromptMessage(
            @StringRes titleRes: Int,
            @StringRes messageRes: Int,
            @StringRes confirmButtonTextRes: Int,
            onClickListener: DialogInterface.OnClickListener
    ): AlertDialog.Builder {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle(titleRes)
        builder.setMessage(messageRes)
        builder.setNeutralButton(R.string.cancel_button, null)
        builder.setPositiveButton(confirmButtonTextRes, onClickListener)

        return builder
    }

    override fun getStatusBarHeight(): Int {
        return (basketToolbarWrapper.layoutParams as ConstraintLayout.LayoutParams).topMargin
    }

    override fun getToolbarHeight(): Int {
        return basketToolbarWrapper.height
    }

    /**
     * Callbacks
     */
    private val onPercentageUpdateListener = object : NewBasketViewAdapter.OnPercentageUpdateListener {
        override fun onPercentageUpdate(percentage: Float) {
            if (isUIActive && context != null) {
                toolbarBackground.alpha = percentage
            }
        }
    }

    private fun refreshBasketUIData() {
        adapter.updateAdapterData()
        adapter.notifyDataSetChanged()
    }

    private val yelpBusinessObserver = Observer<YelpBusiness> { yelpBusiness ->
        mYelpBusiness = yelpBusiness
        if (yelpBusiness != null) {
            refreshBasketUIData()
        }
    }

    private val commentObserver = Observer<List<Comment>> {comments ->
        mComments = comments

        if (!mComments.isNullOrEmpty()) {
            refreshBasketUIData()
        }
    }

    private val venueObserver = Observer<Venue> {venue ->
        mVenue = venue

        if (mVenue != null) {
            refreshBasketUIData()
        }
    }

    private val postObserver = Observer<CloudPost> {post ->
        currentPost = post

        activity?.invalidateOptionsMenu()
        if (post != null) {
            //TODO: fix me
            // hide loading screen
            viewModel.fetchPostComments(post, postManager)
            if (!post.yelpId.isNullOrEmpty()) {
                viewModel.fetchYelpVenue(post, yelpRepository)
            }

            refreshBasketUIData()
        }
    }
}
