package io.piqued.basket.viewholders

import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.viewholders.viewitem.BasketViewItem
import kotlinx.android.synthetic.main.view_holder_basket_show_more.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/12/17.
 * Piqued Inc.
 */

class BasketCommentShowMoreViewHolder(
        parent: ViewGroup,
        dataProvider: BasketDataProvider
): BasketBaseViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_show_more, parent, false),
        dataProvider
) {
    init {
        itemView.loadMoreButton.setOnClickListener {
            dataProvider.onOpenComment()
        }
    }

    override fun onBindViewHolder(viewHolderItem: BasketViewItem) {
        // does nothing
    }
}
