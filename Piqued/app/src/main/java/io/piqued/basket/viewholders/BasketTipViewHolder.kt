package io.piqued.basket.viewholders

import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.viewholders.viewitem.BasketTipItem
import io.piqued.basket.viewholders.viewitem.BasketViewItem
import io.piqued.cloud.model.piqued.VenueTip
import io.piqued.module.GlideApp
import kotlinx.android.synthetic.main.view_holder_basket_tip.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/11/17.
 * Piqued Inc.
 */

class BasketTipViewHolder(
        parent: ViewGroup,
        dataProvider: BasketDataProvider
): BasketBaseViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_tip, parent, false),
        dataProvider
) {

    private val mUserImageSize: Int = parent.resources.getDimensionPixelSize(R.dimen.basket_tip_user_image_size)

    override fun onBindViewHolder(viewHolderItem: BasketViewItem) {
        if (viewHolderItem is BasketTipItem) {
            setVenueTip(viewHolderItem.tip)
        }
    }

    fun setVenueTip(tip: VenueTip) {

        itemView.tip_message.text = tip.message
        itemView.user_name.text = tip.userName

        if (tip.hasImageUrl()) {
            GlideApp.with(getContext())
                    .load(tip.getImageUrl(mUserImageSize))
                    .placeholder(R.drawable.blank_profile)
                    .centerCrop()
                    .circleCrop()
                    .into(itemView.user_image)
        } else {
            itemView.user_image.setImageResource(R.drawable.blank_profile)
        }
    }
}
