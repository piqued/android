package io.piqued.basket.viewholders

import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.R
import io.piqued.basket.comment.CommentDataHandler
import io.piqued.model.Comment
import io.piqued.utils.ImageDownloadUtil
import io.piqued.utils.StringUtil
import kotlinx.android.synthetic.main.view_holder_basket_flat_comment.view.*
import java.util.concurrent.TimeUnit

/**
 *
 * Created by Kenny M. Liou on 9/2/18.
 * Piqued Inc.
 *
 */

class BasketFlatCommentViewHolder(parent: ViewGroup): RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_flat_comment, parent, false)
) {

    init {
        itemView.userComment.maxLines = Int.MAX_VALUE
        itemView.userImage.setOnClickListener { it ->
            if (comment != null) {
                commentHandler?.onUserClicked(it.context, comment!!.user)
            }
        }
        itemView.userName.setOnClickListener { it ->
            if (comment != null) {
                commentHandler?.onUserClicked(it.context, comment!!.user)
            }
        }
    }

    private var comment: Comment? = null
    private val mainLoopHandler = Handler(Looper.getMainLooper())
    var commentHandler: CommentDataHandler? = null

    fun setComment(c: Comment) {

        comment = c

        updateViewHolder()
    }

    private fun updateViewHolder() {

        val currentComment = comment
        if (currentComment != null) {

            updateTimeText(currentComment.timeStamp)
            itemView.userComment.text = currentComment.message
            itemView.userName.text = currentComment.user.name

            ImageDownloadUtil.getDownloadUtilInstance(itemView.context)
                    .loadUserImage(currentComment.user, itemView.userImage)

            checkTimeUpdate()
        }
    }

    private fun checkTimeUpdate() {

        val currentComment = comment
        if (currentComment != null) {
            val diffInMilli = StringUtil.getDifferencesInMilli(currentComment.timeStamp)
            val diffInHours = TimeUnit.MILLISECONDS.toHours(diffInMilli)

            if (diffInHours > 0) {
                return // we don't care
            }

            val diffInMins = TimeUnit.MILLISECONDS.toMinutes(diffInMilli)

            if (diffInMins > 0) {
                mainLoopHandler.postDelayed(updateTimeRunnable, TimeUnit.MINUTES.toMillis(1))
            } else {
                mainLoopHandler.postDelayed(updateTimeRunnable, TimeUnit.SECONDS.toMillis(5))
            }
        }
    }

    private val updateTimeRunnable = Runnable {
        val currentComment = comment
        if (currentComment != null) {
            updateTimeText(currentComment.timeStamp)
            checkTimeUpdate()
        }
    }

    private fun updateTimeText(createdAt: Long) {
        itemView.createdTime.text = StringUtil.getTimeStringLong(itemView.context, createdAt)
    }
}