package io.piqued.basket.relatedcontent

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.piqued.R
import io.piqued.basket.BasketDataProvider
import kotlinx.android.synthetic.main.view_holder_related_content_button.view.*

/**
 *
 * Created by Kenny M. Liou on 8/26/18.
 * Piqued Inc.
 *
 */

class RelatedContentViewHolder(parent: ViewGroup)
    : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_related_content_button, parent, false)
) {

    fun setRelatedContentItem(contentItem: RelatedContentItem,
                            basketDataHandler: BasketDataProvider) {

        itemView.image_button.setImageResource(contentItem.type.imageRes)

        itemView.image_button.setOnClickListener {
            basketDataHandler.onRelatedContentClicked(contentItem)
        }
    }
}