package io.piqued.basket

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.piqued.api.PiquedApiCallback
import io.piqued.api.PiquedGetVenueCallback
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.model.piqued.Venue
import io.piqued.cloud.model.yelp.YelpBusiness
import io.piqued.cloud.repository.YelpRepository
import io.piqued.database.post.CloudPost
import io.piqued.model.Comment
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.utils.postmanager.PostManager

/**
 *
 * Created by Kenny M. Liou on 11/15/18.
 * Piqued Inc.
 *
 */

class BasketViewModel(): ViewModel() {

    companion object {
        private val TAG = BasketViewModel::class.java.simpleName
    }

    private val post = MutableLiveData<CloudPost>()
    private val commentData = MutableLiveData<List<Comment>>()
    private val yelpVenueData = MutableLiveData<YelpBusiness>()
    private val venueData = MutableLiveData<Venue>()

    fun getPost(): LiveData<CloudPost> {
       return post
    }

    fun getYelpVenueData(): LiveData<YelpBusiness> {
        return yelpVenueData
    }

    fun getCommentData(): LiveData<List<Comment>> {
        return commentData
    }

    fun getVenueData(): LiveData<Venue> {
        return venueData
    }

    fun fetchPost(postId: Long,
                  postManager: PostManager) {
        postManager.getPostAsync(postId, getPostCallback)
    }

    fun fetchVenue(postId: Long,
                  postManager: PostManager) {

        postManager.getVenuesFromPost(postId, getVenueCallback)
    }

    fun fetchPostComments(post: CloudPost, postManager: PostManager) {
        postManager.getPostComments(post, getCommentsCallback)
    }

    fun fetchYelpVenue(post: CloudPost, yelpRepository: YelpRepository) {
        if (!post.yelpId.isNullOrEmpty()) {
            yelpRepository.getBusinessById(post.yelpId!!, getYelpVenueCallback)
        }
    }

    private val getPostCallback = object: PiquedApiCallback<CloudPost> {
        override fun onSuccess(data: CloudPost?) {
            post.value = data
        }

        override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
            PiquedLogger.e(TAG, "Failed to fetch post data", errorCode, payload)
        }
    }

    private val getYelpVenueCallback = object : PiquedCloudCallback<YelpBusiness> {
        override fun onSuccess(result: YelpBusiness?) {
            yelpVenueData.value = result
        }

        override fun onFailure(error: Throwable?, message: String?) {
            PiquedLogger.e(TAG, "Failed to fetch yelp data")
        }
    }

    private val getCommentsCallback = object: PiquedApiCallback<List<Comment>> {
        override fun onSuccess(data: List<Comment>?) {
            commentData.value = data
        }

        override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
            PiquedLogger.e(TAG, "Failed to fetch post comment data")
            PiquedLogger.e(TAG, errorCode, payload)
        }
    }

    private val getVenueCallback = object : PiquedGetVenueCallback {
        override fun onSuccess(postId: Long, venue: Venue?) {
            venueData.value = venue
        }

        override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
            PiquedLogger.e(TAG, errorCode, payload)
        }

    }
}