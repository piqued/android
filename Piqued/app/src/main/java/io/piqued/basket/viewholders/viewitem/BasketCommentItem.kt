package io.piqued.basket.viewholders.viewitem

import io.piqued.model.Comment
import io.piqued.basket.BasketViewType

/**
 *
 * Created by Kenny M. Liou on 11/15/18.
 * Piqued Inc.
 *
 */
data class BasketCommentItem(
        val comment: Comment
): BasketViewItem(BasketViewType.COMMENT)