package io.piqued.basket.viewholders

import android.view.LayoutInflater
import android.view.ViewGroup

import io.piqued.R
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.viewholders.viewitem.BasketViewItem

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/11/17.
 * Piqued Inc.
 */

class BasketNoTipsViewHolder(parent: ViewGroup, dataProvider: BasketDataProvider)
    : BasketBaseViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_no_tip, parent, false),
        dataProvider
) {
    override fun onBindViewHolder(viewHolderItem: BasketViewItem) {
        // does nothing
    }
}
