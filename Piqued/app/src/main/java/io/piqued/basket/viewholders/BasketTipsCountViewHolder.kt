package io.piqued.basket.viewholders

import android.view.LayoutInflater
import android.view.ViewGroup
import io.piqued.R
import io.piqued.basket.BasketDataProvider
import io.piqued.basket.viewholders.viewitem.BasketViewItem
import kotlinx.android.synthetic.main.view_holder_basket_tips_count.view.*

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/11/17.
 * Piqued Inc.
 */

class BasketTipsCountViewHolder(
        parent: ViewGroup,
        dataProvider: BasketDataProvider
): BasketBaseViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_holder_basket_tips_count, parent, false),
        dataProvider
) {

    override fun onBindViewHolder(viewHolderItem: BasketViewItem) {
        val venue = dataProvider.venue

        if (venue != null) {
            val count = venue.tipCount

            val tipText: String
            if (count == 1) {
                tipText = getContext().getString(R.string.basket_tip, count)
            } else {
                tipText = getContext().getString(R.string.basket_tips, count)
            }

            itemView.tipCountText.text = tipText
        }
    }
}
