package io.piqued.model.imageupload;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 4/29/17.
 * Piqued Inc.
 */

public class PreSignObject {

    @SerializedName("url")
    public String url;
    @SerializedName("fields")
    public Map<String, String> fields;
}


