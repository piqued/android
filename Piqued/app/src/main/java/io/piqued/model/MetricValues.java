package io.piqued.model;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import io.piqued.R;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/11/17.
 * Piqued Inc.
 */

public class MetricValues {

    private final int mStatusBarHeight;
    private final int mNavigationBarHeight;
    private final int mHomeBottomNavigationBarHeight;
    private final int mSpace20;

    public MetricValues(Context context, WindowManager windowManager) {

        Resources resources = context.getResources();

        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            mStatusBarHeight = resources.getDimensionPixelSize(resourceId);
        } else {
            mStatusBarHeight = 0;
        }

        resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (hasSoftwareKeys(windowManager) && resourceId > 0) {
            mNavigationBarHeight = resources.getDimensionPixelSize(resourceId);
        } else {
            mNavigationBarHeight = 0;
        }

        mHomeBottomNavigationBarHeight = resources.getDimensionPixelSize(R.dimen.design_bottom_navigation_height);

        mSpace20 = resources.getDimensionPixelSize(R.dimen.space_20);
    }

    private boolean hasSoftwareKeys(WindowManager windowManager) {
        Display d = windowManager.getDefaultDisplay();

        DisplayMetrics realDisplayMetrics = new DisplayMetrics();

        d.getRealMetrics(realDisplayMetrics);

        int realHeight = realDisplayMetrics.heightPixels;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        d.getMetrics(displayMetrics);

        int displayHeight = displayMetrics.heightPixels;

        return (realHeight - displayHeight) > 0;
    }

    public int getStatusBarHeight() {
        return mStatusBarHeight;
    }

    public int getNavigationBarHeight() {
        return mNavigationBarHeight;
    }

    public int getHomeBottomNavigationBarHeight() {
        return mHomeBottomNavigationBarHeight;
    }

    public int getCameraButtonMarginBottom() {
        return mHomeBottomNavigationBarHeight + mSpace20;
    }

    public int getBothNavigationHeight() {
        return mHomeBottomNavigationBarHeight + mNavigationBarHeight;
    }
}
