package io.piqued.model.imageupload;

import org.json.JSONException;
import org.json.JSONObject;

import io.piqued.database.util.PiquedLogger;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 4/29/17.
 * Piqued Inc.
 */

public class ImageUploadResult {

    private static final String MIME_TYPE = "image/jpeg";

    private int mOriginalIndex;
    public String id;
    public String storage;
    public long sizeInBytes;

    public void setOriginalIndex(int index) {
        mOriginalIndex = index;
    }

    public int getOriginalIndex() {
        return mOriginalIndex;
    }

    public JSONObject getMetadata() {

        JSONObject object = new JSONObject();

        try {
            object.put("size", sizeInBytes);
            object.put("filename", "uploaded_via_android");
            object.put("mime_type", MIME_TYPE);
        } catch (JSONException e) {
            PiquedLogger.e(ImageUploadResult.class.getSimpleName(),"Failed to put data into json");
        }


        return object;
    }
}
