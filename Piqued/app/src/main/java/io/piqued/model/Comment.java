package io.piqued.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import io.piqued.database.post.CloudPost;
import io.piqued.database.user.User;
import io.piqued.database.util.PiquedLogger;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/11/17.
 * Piqued Inc.
 */

public class Comment implements Parcelable {

    private long id;
    private long postId;
    public String message;
    public User user;
    @SerializedName("createdAt")
    private String createdAtString;
    private long createdAtValue = -1;

    protected Comment(Parcel in) {
        id = in.readLong();
        postId = in.readLong();
        message = in.readString();
        createdAtValue = in.readLong();
        createdAtString = in.readString();
    }

    private Comment() {
        // DO NOT USE ME
    }

    public long getTimeStamp() {
        if (createdAtValue <= 0 && !TextUtils.isEmpty(createdAtString)) {
            if (createdAtString.contains("-")) {

                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
                    formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                    createdAtValue = TimeUnit.MILLISECONDS.toSeconds(formatter.parse(createdAtString).getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                    PiquedLogger.e(Comment.class.getSimpleName(), "Failed to parse time string: " + createdAtString);
                    throw new IllegalArgumentException("Failed to parse time string: " + createdAtString);
                }
            } else {
                createdAtValue = Long.valueOf(createdAtString);
            }
        }

        return createdAtValue;
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    public static Comment fromPost(@NonNull CloudPost post) {

        Comment comment = new Comment();

        comment.message = post.getDescription();
        comment.postId = post.getPostId();
        comment.createdAtValue = post.getCreatedAt();
        comment.user = new User(
                post.getUserId(),
                post.getUserName(),
                0,
                0,
                0,
                false,
                0,
                0,
                0,
                post.getUserImageUrl(),
                0,
                ""
        );

        return comment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeLong(postId);
        parcel.writeString(message);
        parcel.writeLong(createdAtValue);
        parcel.writeString(createdAtString);
    }
}
