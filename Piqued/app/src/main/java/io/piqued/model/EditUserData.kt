package io.piqued.model

import android.net.Uri
import android.text.TextUtils

/**
 * Created by Kenny M. Liou on 11/4/17.
 * Piqued Inc.
 */

class EditUserData {

    var name: String? = null
    var email: String? = null
    var password: String? = null
    var imageData: Uri? = null

    val imageUrl: String
        get() = imageData!!.toString()

    fun hasData(): Boolean {
        return !TextUtils.isEmpty(name) ||
                !TextUtils.isEmpty(email) ||
                !TextUtils.isEmpty(password) ||
                imageData != null
    }

    fun hasImageData(): Boolean {
        return imageData != null && !imageData!!.toString().isEmpty()
    }
}
