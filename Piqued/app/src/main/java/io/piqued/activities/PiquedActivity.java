package io.piqued.activities;

import android.Manifest;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.CrashManagerListener;
import net.hockeyapp.android.UpdateManager;

import io.piqued.AppComponent;
import io.piqued.BuildConfig;
import io.piqued.Piqued;
import io.piqued.PiquedFragment;
import io.piqued.R;
import io.piqued.model.MetricValues;
import io.piqued.utils.KeyboardEventListener;
import io.piqued.utils.PiquedDialogFragment;
import io.piqued.utils.PiquedOnBackHandler;

import static io.piqued.utils.PiquedCodes.REQUEST_LOCATION_CODE;

/**
 * Piqued
 * <p>
 * Created by Kenny M. Liou on 11/19/16.
 * <p>
 * Piqued Inc.
 */

public abstract class PiquedActivity extends AppCompatActivity {

    protected MetricValues mMetric;
    protected boolean isUiActive = false;
    private PiquedOnBackHandler mBackHandler;
    private KeyboardEventListener mKeyboardEventListener;
    private int mKeyboardHeight = 0;

    protected AppComponent getComponent() {
        return ((Piqued) getApplication()).getComponent();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMetric = new MetricValues(this, getWindowManager());

        onCreateSetupWindow();

        if (BuildConfig.DEBUG && !BuildConfig.VERSION_NAME.equals("1.0.1")) {
            checkForUpdates();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        isUiActive = true;

        if (getRootView() != null) {
            getRootView().getViewTreeObserver().removeOnGlobalLayoutListener(mGlobalLayoutListener); // make sure it doesn't exist
            getRootView().getViewTreeObserver().addOnGlobalLayoutListener(mGlobalLayoutListener);
        }

        checkForCrashes();
    }

    @Override
    protected void onPause() {

        if (getRootView() != null) {
            getRootView().getViewTreeObserver().removeOnGlobalLayoutListener(mGlobalLayoutListener);
        }

        isUiActive = false;

        super.onPause();

        if (BuildConfig.DEBUG && !BuildConfig.VERSION_NAME.equals("1.0.1")) {
            unregisterManagers();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (BuildConfig.DEBUG && !BuildConfig.VERSION_NAME.equals("1.0.1")) {
            unregisterManagers();
        }
    }

    public void setKeyboardEventListener(KeyboardEventListener keyboardEventListener) {
        mKeyboardEventListener = keyboardEventListener;
    }


    private void checkForCrashes() {

        CrashManager.register(this, new CrashManagerListener() {
            @Override
            public boolean shouldAutoUploadCrashes() {
                return true;
            }
        });
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

    public void pushFragmentAsRoot(PiquedFragment fragment, boolean shouldAnimate) {
        pushFragment(fragment, false, shouldAnimate);
    }

    public void pushFragment(PiquedFragment fragment, boolean addToBackStack, boolean shouldAnimate) {
        pushFragment(fragment, addToBackStack, shouldAnimate, true);
    }

    public void pushFragment(PiquedFragment fragment, boolean addToBackStack, boolean shouldAnimate, boolean rightToLeft) {

        pushFragment(fragment, fragment.getTagName(), addToBackStack, shouldAnimate, rightToLeft);
    }

    public void pushFragment(PiquedDialogFragment fragment) {
        pushFragment(fragment, fragment.getTagName(), true, true, false);
    }

    private void pushFragment(Fragment fragment, String tagName, boolean addToBackStack, boolean shouldAnimate, boolean rightToLeft) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.replace(getMainContainerId(), fragment, tagName);

        if (addToBackStack) ft.addToBackStack(tagName);

        ft.commit();
    }

    public void onCreateSetupWindow() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        );
        getWindow().setStatusBarColor(Color.BLACK);
    }

    public void setBackHandler(@Nullable PiquedOnBackHandler backHandler) {
        mBackHandler = backHandler;
    }

    @IdRes
    protected int getMainContainerId() {
        return R.id.main_container;
    }

    public View getRootView() {
        return findViewById(R.id.activity_root_view);
    }

    @Override
    public void onBackPressed() {

        if (mBackHandler != null && mBackHandler.onBackPressed()) {
            return;
        }

        super.onBackPressed();
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public int getNavigationBarHeight() {
        return getMetric().getNavigationBarHeight();
    }

    public int getBothNavigationHeight() {
        return getMetric().getBothNavigationHeight();
    }

    public int getPiquedNavigationBarHeight() {
        return getMetric().getHomeBottomNavigationBarHeight();
    }

    public int getStatusBarHeight() {
        return getMetric().getStatusBarHeight();
    }

    public abstract MetricValues getMetric();

    public void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            // TODO
            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

        } else {

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_CODE);
        }
    }

    public void showSnackBar(@StringRes int message) {
        // override me
    };

    public void onDialogPositiveClicked() {

    }

    public void onDialogNegativeClicked() {

    }

    protected void onKeyboardSizeChanged(boolean isHidden, int keyboardHeight) {
        // override me if activity need to know keyboard is present
    }

    private void onKeyboardChanged(int keyboardHeight) {
        if (mKeyboardEventListener != null) {
            mKeyboardEventListener.onKeyboardHidden((keyboardHeight == 0 || keyboardHeight == getNavigationBarHeight()), keyboardHeight);
        }

        onKeyboardSizeChanged(keyboardHeight == 0 || keyboardHeight == getNavigationBarHeight(), keyboardHeight);
    }

    private final ViewTreeObserver.OnGlobalLayoutListener mGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {

        @Override
        public void onGlobalLayout() {

            int screenHeight = getRootView().getHeight();
            Rect r = new Rect();
            View view = getWindow().getDecorView();
            view.getWindowVisibleDisplayFrame(r);

            int keyBoardHeight = Math.max(0, screenHeight - r.bottom);

            if (mKeyboardHeight != keyBoardHeight) {
                mKeyboardHeight = keyBoardHeight;
                onKeyboardChanged(mKeyboardHeight);
            }
        }
    };
}
