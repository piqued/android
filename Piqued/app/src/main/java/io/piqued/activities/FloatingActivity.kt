package io.piqued.activities

import android.app.AlertDialog
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.core.text.HtmlCompat
import io.piqued.R
import io.piqued.database.user.User
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.login.NewLoginActivity
import io.piqued.model.MetricValues
import io.piqued.utils.AccountManager
import io.piqued.utils.LoginCallback
import io.piqued.utils.Preferences
import io.piqued.utils.UIHelper
import io.piqued.utils.modelmanager.PiquedUserManager
import kotlinx.android.synthetic.main.activity_float_layout.*
import javax.inject.Inject

class FloatingActivity: FBLoginHandlerActivity() {

    companion object {
        private val TAG = FloatingActivity::class.java.simpleName
    }

    @Inject
    lateinit var accountManager: AccountManager
    @Inject
    lateinit var userManager: PiquedUserManager
    @Inject
    lateinit var preferences: Preferences

    private val showLoginListener = View.OnClickListener { showSignUpView() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)
        
        supportActionBar?.hide()

        setContentView(R.layout.activity_float_layout)

        login.setOnClickListener(showLoginListener)
        signup.setOnClickListener(showLoginListener)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            popup_disclaimer.text = Html.fromHtml(getString(R.string.login_disclaimer), HtmlCompat.FROM_HTML_MODE_COMPACT)
        } else {
            popup_disclaimer.text = Html.fromHtml(getString(R.string.login_disclaimer))
        }

        popup_disclaimer.movementMethod = LinkMovementMethod.getInstance()

        closeButton.setOnClickListener {
            finish()
        }

        setFinishOnTouchOutside(true)

        facebook_login_button.registerCallback(callbackManager, facebookLoginCallback)
    }

    override val facebookLoginResult: Callback
        get() = object : Callback {
            override fun onLoginSuccess(token: String) {
                if (token.isNotEmpty()) {
                    accountManager.loginWithOauth(
                            AccountManager.SupportedSite.FACEBOOK,
                            token,
                            piquedLoginCallback)
                }
            }

            override fun onCancelled() { /** do nothing */ }
        }

    override fun getMetric(): MetricValues {
        return mMetric
    }

    private fun showSignUpView() {
        UIHelper.startActivity(this, NewLoginActivity::class.java)
        finish()
    }

    val piquedLoginCallback = object: LoginCallback {
        override fun onLoginSuccess() {
            UIHelper.startActivity(this@FloatingActivity, WelcomeActivity::class.java)
        }

        override fun onLoginFailed(errorCode: PiquedErrorCode?, payload: String?) {
            PiquedLogger.e(TAG, "Failed to login", errorCode, payload)
            val builder = AlertDialog.Builder(this@FloatingActivity)
            builder.setMessage(R.string.facebook_login_failed)
            builder.setPositiveButton(R.string.ok_button) {
                _, _ ->
            }
            builder.setCancelable(false)
            builder.create().show()
        }

        override fun onFetchingUserInfoSuccess(user: User) {
            userManager.myUserRecord = user
        }

        override fun onFetchingUserInfoFailed(errorCode: PiquedErrorCode?, payload: String?) {}

        override fun onFetchingUserInfo() {}
    }
}