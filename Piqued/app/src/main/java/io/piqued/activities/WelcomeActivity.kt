package io.piqued.activities

import android.os.Bundle
import io.piqued.R
import io.piqued.model.MetricValues
import io.piqued.views.welcome.WelcomeFragment

/**
 *
 * Created by Kenny M. Liou on 1/20/18.
 * Piqued Inc.
 *
 */
class WelcomeActivity : PiquedActivity() {

    override fun getMetric(): MetricValues {
        return mMetric
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_welcome)

        pushFragmentAsRoot(WelcomeFragment.newInstance(), false)
    }
}