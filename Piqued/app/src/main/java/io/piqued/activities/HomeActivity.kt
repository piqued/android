package io.piqued.activities

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.transition.ChangeBounds
import android.transition.TransitionManager
import android.view.MenuItem
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.ncapdevi.fragnav.FragNavController
import io.piqued.Piqued
import io.piqued.PiquedFragment
import io.piqued.R
import io.piqued.api.PiquedApiCallback
import io.piqued.basket.BasketFragment
import io.piqued.basket.comment.BasketCommentFragment
import io.piqued.database.post.PostId
import io.piqued.database.user.User
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.model.Comment
import io.piqued.model.MetricValues
import io.piqued.service.imageupload.BroadcastCreateBasketStatus
import io.piqued.service.imageupload.ImageUploadStatus
import io.piqued.service.pushnotification.PiquedNotificationUtil
import io.piqued.utils.*
import io.piqued.utils.modelmanager.PiquedUserManager
import io.piqued.utils.postmanager.PostManager
import io.piqued.views.DoubleTapFragment
import io.piqued.views.HomeActivityFragment
import io.piqued.views.activities.ActivitiesFragment
import io.piqued.views.bookmark.BookmarkFragment
import io.piqued.views.home.HomeFragment
import io.piqued.views.profile.ProfileFragment
import kotlinx.android.synthetic.main.activity_new_home.*
import org.jetbrains.anko.contentView
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 11/17/18.
 * Piqued Inc.
 *
 */
class HomeActivity: HomeBaseActivity() {

    companion object {
        private val TAG = HomeActivity::class.java.simpleName
        private const val CURRENT_ITEM_INDEX = "current_index"
        private const val ACTION_OPEN_COMMENT = "open_comment"
        private const val ACTION_OPEN_PROFILE = "open_profile"
        private const val KEY_USER_ID = "user_id"
        private const val KEY_COMMENT_POST_ID = "comment_post_id"
        private const val KEY_COMMENT_LIST = "comment_list"
    }

    @Inject
    internal lateinit var userManager: PiquedUserManager
    @Inject
    internal lateinit var postManager: PostManager
    @Inject
    internal lateinit var locationManager: PiquedLocationManager
    @Inject
    internal lateinit var preferences: Preferences
    @Inject
    internal lateinit var accountManager: AccountManager
    @Inject
    internal lateinit var eventManager: EventManager

    override fun getMetric(): MetricValues {
        return mMetric
    }

    private var selectedTabIndex = HomeTab.HOME.ordinal
    private var reselectedItem: MenuItem? = null
    private var doubleTapTimestamp = 0L
    private var haveLocationPermission = false
    private var navBarHidden = false
    private lateinit var fragNavController: FragNavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_new_home)

        component.inject(this)

        bottomNavigation.setOnNavigationItemSelectedListener(bottomNavigationListener)
        bottomNavigation.setOnNavigationItemReselectedListener(
                bottomNavigationItemReselectedListener)

        val builder = FragNavController.newBuilder(savedInstanceState, supportFragmentManager,
                R.id.homeFragmentContainer)

        val fragments = ArrayList<Fragment>(arrayListOf(
                HomeFragment.newInstance(),
                BookmarkFragment.newInstance(),
                ActivitiesFragment.newInstance(),
                ProfileFragment.newInstance()))

        builder.rootFragments(fragments)
        builder.transactionListener(fragmentTransactionListener)

        fragNavController = builder.build()

        homeRootView.setOnApplyWindowInsetsListener { _, windowInsets ->
            updateStatusBarHeight()

            windowInsets
        }

        (application as Piqued).uploadStatusData.observe(this, Observer<Intent> { t ->
            if (t != null) {
                onUpdateImageUploadStatus(t)
            }
        })

        fetchFCMAndSubscribePushNotif()
        PiquedNotificationUtil.createNotificationChannel(this)
    }

    override fun onResume() {
        super.onResume()

        checkLocationPermission()

        if (haveLocationPermission()) {
            BroadcastUtil.registerReceiverWithMultipleAction(this, rootFragmentOperation,
                    ACTION_OPEN_COMMENT, ACTION_OPEN_PROFILE)

            // start location manager
            locationManager.onResume(this)
            bottomNavigation.menu.getItem(selectedTabIndex).isChecked = true
        }

        fetchActivityData()
    }

    override fun onPause() {

        LocalBroadcastManager.getInstance(baseContext).unregisterReceiver(rootFragmentOperation)

        // stop location manager
        locationManager.onPause()

        super.onPause()
    }

    override fun getRootView(): View {
        return homeRootView
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        selectedTabIndex = savedInstanceState.getInt(CURRENT_ITEM_INDEX)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(CURRENT_ITEM_INDEX, selectedTabIndex)
        fragNavController.onSaveInstanceState(outState)
    }

    override fun onBackPressed() {
        if (!fragNavController.isRootFragment) {
            fragNavController.popFragment()
            updateStatusBarHeight()
        } else {
            if (!(fragNavController.currentFrag as HomeActivityFragment).onBackPressed()) {
                super.onBackPressed()
            }
        }
    }
    /**
     * UI helper method
     */

    private fun setStatusBarVisible(visible: Boolean) {
        statusBarBackground.visibility = if (visible) View.VISIBLE else View.GONE
    }

    override fun setNavBarHidden(hidden: Boolean, animate: Boolean) {
        if (navBarHidden != hidden) {
            navBarHidden = hidden

            val constraintSet = ConstraintSet()
            constraintSet.clone(homeRootView)

            if (hidden) {
                constraintSet.clear(R.id.bottomNavigationContainer, ConstraintSet.BOTTOM)
                constraintSet.connect(R.id.bottomNavigationContainer, ConstraintSet.TOP,
                        R.id.homeRootView, ConstraintSet.BOTTOM)
                constraintSet.clear(R.id.navigationBarBackground, ConstraintSet.BOTTOM)
                constraintSet.connect(R.id.navigationBarBackground, ConstraintSet.TOP,
                        R.id.bottomNavigationContainer, ConstraintSet.BOTTOM)
            } else {
                constraintSet.clear(R.id.navigationBarBackground, ConstraintSet.TOP)
                constraintSet.connect(R.id.navigationBarBackground, ConstraintSet.BOTTOM,
                        R.id.homeRootView, ConstraintSet.BOTTOM)
                constraintSet.clear(R.id.bottomNavigationContainer, ConstraintSet.TOP)
                constraintSet.connect(R.id.bottomNavigationContainer, ConstraintSet.BOTTOM,
                        R.id.navigationBarBackground, ConstraintSet.TOP)
            }

            if (animate) {
                val transition = ChangeBounds()

                transition.interpolator = LinearInterpolator()
                transition.duration = Constants.NAVBAR_ANIMATION_DURATION

                TransitionManager.beginDelayedTransition(homeRootView, transition)
            }

            constraintSet.applyTo(homeRootView)
        }
    }

    private fun setProgressbarVisible(visible: Boolean, animate: Boolean)
    {
        val constraintSet = ConstraintSet()
        constraintSet.clone(homeRootView)

        if (visible) {
            constraintSet.clear(R.id.imageUploadProgressWrapper, ConstraintSet.TOP)
            constraintSet.connect(R.id.imageUploadProgressWrapper, ConstraintSet.BOTTOM,
                    R.id.bottomNavigationContainer, ConstraintSet.TOP)
        } else {
            constraintSet.clear(R.id.imageUploadProgressWrapper, ConstraintSet.BOTTOM)
            constraintSet.connect(R.id.imageUploadProgressWrapper, ConstraintSet.TOP,
                    R.id.bottomNavigationContainer, ConstraintSet.TOP)
        }

        if (animate) {
            val transition = ChangeBounds()

            transition.interpolator = LinearInterpolator()
            transition.duration = Constants.IMAGE_UPLOAD_PROGRESS_BAR_VISIBILITY_ANIMATION_DURATION

            TransitionManager.beginDelayedTransition(homeRootView, transition)
        }

        constraintSet.applyTo(homeRootView)
    }

    override fun getBottomNavigationHeight(): Int {
        return bottomNavigation.height
    }

    override fun pushSubFragment(fragment: PiquedFragment) {
        fragNavController.pushFragment(fragment)
    }

    override fun openUserProfile(userId: Long) {
        userManager.getUserInfo(userId, openUserProfileCallback)
    }

    override fun openCommentsFragment(postId: PostId, comments: List<Comment>) {

        val post = postManager.getCloudPost(postId)
        pushSubFragment(BasketCommentFragment.newInstance(post, comments))
    }

    private fun updateImageUploadProgress(intent: Intent) {

        imageUploadProgressWrapper.visibility = View.VISIBLE
        val status = BroadcastCreateBasketStatus.getUploadStatus(intent)
        val imageSent = BroadcastCreateBasketStatus.getImageSent(intent)
        val totalImageCount = BroadcastCreateBasketStatus.getTotalImageCount(intent)
        val byteSent = BroadcastCreateBasketStatus.getBytesSent(intent)
        val totalImageSize = BroadcastCreateBasketStatus.getContentLength(intent)

        val imageUrl = BroadcastCreateBasketStatus.getImageUrl(intent)

        if (imageUrl.isNotEmpty()) {
            Glide.with(this)
                    .load(imageUrl)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageUploadPreview)

            imageProgressBar.max = 100

            var animation: ProgressBarAnimation? = null

            if (totalImageCount > 0) {
                uploadStatusText.text = this.getString(R.string.uploading_count, imageSent,
                        totalImageCount)
                animation = ProgressBarAnimation(imageProgressBar,
                        (imageSent * 100 / totalImageCount).toFloat())
            }

            if (totalImageSize > 0) {
                uploadStatusText.setText(R.string.uploading)
                animation = ProgressBarAnimation(imageProgressBar,
                        (byteSent * 100 / totalImageSize).toFloat())
            }

            if (animation != null) {
                animation.duration = 200
                imageProgressBar.startAnimation(animation)
            }
        } else {
            if (status == ImageUploadStatus.STARTING) {
                uploadStatusText.setText(R.string.uploading)
                imageProgressBar.max = 100
                imageProgressBar.progress = 0
                imageUploadPreview.setImageDrawable(null)
            }
        }
    }

    /**
     * Permissions
     */

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {

        when (requestCode) {
            Constants.EXTERNAL_STORAGE_READ_REQUEST -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(
                        Constants.ACTION_SEND_PERMISSION_RESULT).putExtra(
                        Constants.KEY_PERMISSION_REQUEST, true))
            }
            Constants.FINE_LOCATION_PERMISSIONS_REQUEST -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(
                        Constants.ACTION_LOCATION_PERMISSION_RESULT).putExtra(
                        Constants.KEY_PERMISSION_REQUEST, true))
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun checkLocationPermission() {
        haveLocationPermission = haveLocationPermission()
    }

    override fun haveLocationPermission(): Boolean {
        return ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    override fun locationPermissionDenied(): Boolean {
        return ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED
    }

    override fun requestForLocationPermission() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                Constants.FINE_LOCATION_PERMISSIONS_REQUEST)
    }

    override fun showSnackBar(message: Int) {
        Snackbar.make(contentView!!, resources.getText(message), Snackbar.LENGTH_LONG)
                .setAction(R.string.error_okay) {
                    // do nothing
                }
                .setActionTextColor(ContextCompat.getColor(this, R.color.red1))
                .show()
    }

    /**
     * listeners
     */

    private val rootFragmentOperation = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            if (ACTION_OPEN_COMMENT == intent.getAction()) {

                val extras = intent.extras

                if (extras != null) {

                    val post = postManager.getCloudPost(extras.getLong(KEY_COMMENT_POST_ID))
                    val comments = extras.getParcelableArrayList<Comment>(KEY_COMMENT_LIST)

                    if (post != null) {

                        pushSubFragment(BasketCommentFragment.newInstance(post, comments))
                    }
                }
            } else if (ACTION_OPEN_PROFILE == intent.getAction()) {

                val extras = intent.getExtras()

                if (extras != null) {

                    val userId = extras.getLong(KEY_USER_ID)
                    userManager.getUserInfo(userId, object : PiquedApiCallback<User> {
                        override fun onSuccess(data: User?) {
                            if (data != null) {
                                pushSubFragment(ProfileFragment.newInstance(data.userId))
                            }
                        }

                        override fun onFailure(errorCode: PiquedErrorCode, payload: String) {
                            PiquedLogger.e(TAG, "Failed to get user, is the user id valid? $userId",
                                    errorCode, payload)
                        }
                    })
                }
            }
        }

    }

    private val openUserProfileCallback = object: PiquedApiCallback<User> {
        override fun onSuccess(data: User?) {
            if (data != null) {
                pushSubFragment(ProfileFragment.newInstance(data.userId))
            }
        }

        override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
            PiquedLogger.e(TAG, "Failed to get user, is the user id valid?", errorCode, payload)
        }
    }

    private val bottomNavigationListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_home -> {
                selectedTabIndex = HomeTab.HOME.ordinal
                fragNavController.switchTab(FragNavController.TAB1)
            }
            R.id.nav_bookmark -> {
                updateTabIfLoggedIn(HomeTab.BOOKMARKS, FragNavController.TAB2)
            }
            R.id.nav_activities -> {
                updateTabIfLoggedIn(HomeTab.ACTIVITIES, FragNavController.TAB3)
            }
            R.id.nav_profile -> {
                selectedTabIndex = HomeTab.PROFILE.ordinal
                fragNavController.switchTab(FragNavController.TAB4)
            }
        }

        reselectedItem = null

        true
    }

    private val bottomNavigationItemReselectedListener = BottomNavigationView.OnNavigationItemReselectedListener { item ->
        if (item !== reselectedItem) {
            reselectedItem = item
            doubleTapTimestamp = System.currentTimeMillis()
        } else {

            val currentTime = System.currentTimeMillis()

            if (TimeUnit.MILLISECONDS.toSeconds(currentTime - doubleTapTimestamp) < 2) {

                val fragment = fragNavController.currentFrag

                if (fragment is DoubleTapFragment) {
                    fragment.onTabDoubleTapped(fragNavController)
                }
            }

            reselectedItem = null
        }
    }

    private val fragmentTransactionListener = object: FragNavController.TransactionListener {
        override fun onFragmentTransaction(fragment: Fragment?,
                                           type: FragNavController.TransactionType?) {
            setStatusBarVisible(fragment !is BasketFragment)
        }

        override fun onTabTransaction(fragment: Fragment?, position: Int) {
            setStatusBarVisible(fragment !is BasketFragment)
        }
    }

    private fun updateTabIfLoggedIn(tab: HomeTab, tabIndex: Int) {
        IntentUtil.runIfUserLoggedIn(this, preferences.userLoggedIn(),
                object : LoginRequiredExecution {
                    override fun runIfLoggedIn() {
                        selectedTabIndex = tab.ordinal
                        fragNavController.switchTab(tabIndex)
                    }
                })
    }

    private fun onUpdateImageUploadStatus(intent: Intent)
    {

        when (BroadcastCreateBasketStatus.getUploadStatus(intent)) {
            ImageUploadStatus.STARTING -> {
                setProgressbarVisible(visible = true, animate = true)
                updateImageUploadProgress(intent)
            }
            ImageUploadStatus.UPLOADING -> updateImageUploadProgress(intent)
            ImageUploadStatus.FINISHING -> uploadStatusText.setText(R.string.finishing_up)
            ImageUploadStatus.FINISHED -> {
                showSnackBar(R.string.post_upload_succeeded)
                setProgressbarVisible(visible = false, animate = true)
            }
            ImageUploadStatus.ERROR -> {
                showSnackBar(R.string.post_upload_failed)
                setProgressbarVisible(visible = false, animate = true)
            }
        }
    }

    private fun updateStatusBarHeight() {
        if (fragNavController.currentFrag is BasketFragment) {
            val statusBarParams = statusHeight.layoutParams as ConstraintLayout.LayoutParams
            statusBarParams.guideBegin = 0
            statusHeight.layoutParams = statusBarParams
        } else {
            val statusBarParams = statusHeight.layoutParams as ConstraintLayout.LayoutParams
            statusBarParams.guideBegin = statusBarHeight
            statusHeight.layoutParams = statusBarParams
        }
    }

    private fun fetchFCMAndSubscribePushNotif() {
        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        PiquedLogger.e(task.exception, "getInstanceId failed")
                        return@OnCompleteListener
                    }

                    // Get new Instance ID token
                    val token = task.result?.token

                    if (!TextUtils.isEmpty(token)) {
                        preferences.setFCMToken(token!!)
                        pushNotificationSubscription()
                    }
                })
    }

    private fun fetchActivityData() {
        if (preferences.userLoggedIn()) {
            eventManager.fetchLatestActivity()
        }
    }

    private fun pushNotificationSubscription() {
        accountManager.registerDeviceForPushNotification(object : PiquedApiCallback<Boolean> {
            override fun onSuccess(data: Boolean?) {
                PiquedLogger.i(TAG, "push notification registration succeed")
            }

            override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
                PiquedLogger.i(TAG, "push notification registration FAILED")
            }
        })
    }
}