package io.piqued.activities

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 12/29/16.
 * Piqued Inc.
 */

enum class HomeTab {

    HOME,
    BOOKMARKS,
    ACTIVITIES,
    PROFILE;


    companion object {

        private val values = values()

        val count: Int
            get() = values.size

        fun fromInt(index: Int): HomeTab {

            if (index > -1 && index < values.size) {
                return values[index]
            }

            throw IllegalArgumentException("Invalid index value for enum HOMETAB: $index")
        }
    }
}
