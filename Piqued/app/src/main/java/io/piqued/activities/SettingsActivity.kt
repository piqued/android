package io.piqued.activities

import android.os.Bundle
import androidx.navigation.Navigation
import io.piqued.R
import io.piqued.model.MetricValues

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 2/4/17.
 * Piqued Inc.
 */

class SettingsActivity : PiquedActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        setContentView(R.layout.activity_settings)
    }

    override fun onCreateSetupWindow() {
        // do nothing
    }

    override fun getMetric(): MetricValues? {
        return mMetric
    }

    override fun onSupportNavigateUp(): Boolean {
        if (Navigation.findNavController(this, R.id.settingsHostFragment).navigateUp()) {
            return true
        }

        return super.onSupportNavigateUp()
    }
}
