package io.piqued.activities

import android.content.Intent
import android.widget.Toast
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import io.piqued.database.util.PiquedLogger

abstract class FBLoginHandlerActivity: PiquedActivity() {
    interface Callback {
        fun onLoginSuccess(token: String)
        fun onCancelled()
    }

    companion object {
        private val PERMISSIONS = listOf("public_profile", "email")
    }

    private val loginManager: LoginManager = LoginManager.getInstance()
    val callbackManager: CallbackManager = CallbackManager.Factory.create()
    abstract val facebookLoginResult: Callback
    val facebookLoginCallback = object: FacebookCallback<LoginResult> {
        override fun onSuccess(result: LoginResult?) {
            facebookLoginResult.onLoginSuccess(result?.accessToken?.token?: "")
        }

        override fun onCancel() {
            facebookLoginResult.onCancelled()
        }

        override fun onError(error: FacebookException?) {
            Toast.makeText(baseContext, "Cannot login to your facebook account", Toast.LENGTH_LONG).show()
            PiquedLogger.e("FACEBOOK", error)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    fun performManualLogin() {
        loginManager.registerCallback(callbackManager, facebookLoginCallback)
        loginManager.logInWithReadPermissions(this, PERMISSIONS)
    }
}