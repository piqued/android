package io.piqued.activities

import android.os.Bundle
import io.piqued.model.MetricValues
import io.piqued.utils.Preferences
import io.piqued.utils.UIHelper
import io.piqued.utils.modelmanager.PiquedUserManager
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 1/20/18.
 * Piqued Inc.
 *
 */
class SplashActivity : PiquedActivity() {

    @Inject
    lateinit var mPreferences : Preferences
    @Inject
    lateinit var mUserManager : PiquedUserManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)
            if (!mPreferences.welcomeScreenShown()) {
                UIHelper.startActivity(this, WelcomeActivity::class.java)
            } else {
                UIHelper.startActivity(this, HomeActivity::class.java)
            }
    }

    override fun getMetric(): MetricValues {
        return mMetric
    }
}