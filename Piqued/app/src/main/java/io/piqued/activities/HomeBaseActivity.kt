package io.piqued.activities

import io.piqued.PiquedFragment
import io.piqued.database.post.PostId
import io.piqued.model.Comment

/**
 *
 * Created by Kenny M. Liou on 11/17/18.
 * Piqued Inc.
 *
 */
abstract class HomeBaseActivity: PiquedActivity() {

    abstract fun getBottomNavigationHeight(): Int
    abstract fun setNavBarHidden(hidden: Boolean, animate: Boolean)
    abstract fun pushSubFragment(fragment: PiquedFragment)
    abstract fun openUserProfile(userId: Long)
    abstract fun openCommentsFragment(postId: PostId, comments: List<Comment>)
    abstract fun locationPermissionDenied(): Boolean
    abstract fun haveLocationPermission(): Boolean
    abstract fun requestForLocationPermission()
}