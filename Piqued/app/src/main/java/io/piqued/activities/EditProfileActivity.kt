package io.piqued.activities

import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.Navigation
import io.piqued.PiquedFragment
import io.piqued.R
import io.piqued.api.PiquedApiCallback
import io.piqued.database.user.User
import io.piqued.database.util.PiquedErrorCode
import io.piqued.database.util.PiquedLogger
import io.piqued.model.EditUserData
import io.piqued.model.MetricValues
import io.piqued.utils.Constants
import io.piqued.utils.PiquedDialogFragment
import io.piqued.utils.modelmanager.PiquedUserManager
import kotlinx.android.synthetic.main.activity_edit_profile.*
import java.io.File
import java.io.IOException
import javax.inject.Inject

/**
 *
 * Created by Kenny M. Liou on 10/6/18.
 * Piqued Inc.
 *
 */
class EditProfileActivity: PiquedActivity() {

    companion object {
        private val TAG = EditProfileActivity::class.java.simpleName

        fun editMyProfile(fragment: PiquedFragment) {
            val intent = Intent(fragment.context, EditProfileActivity::class.java)
            fragment.startActivityForResult(intent, Constants.EDIT_USER_REQUEST_CODE)
        }
    }

    @Inject
    lateinit var userManager: PiquedUserManager
    private val modifiedData = EditUserData()
    private var tempUserImageFile: File? = null

    override fun getMetric(): MetricValues {
        return mMetric
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        setContentView(R.layout.activity_edit_profile)

        loadingIndicator.setOnClickListener {
            // prevent user click
        }
    }

    override fun onSupportNavigateUp(): Boolean {

        if (Navigation.findNavController(this, R.id.editProfileHostFragment).navigateUp()) {
            return true
        }

        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {

        val navController = Navigation.findNavController(this, R.id.editProfileHostFragment)

        if (!navController.popBackStack()) {
            // check if we have changes
            if (hasChanges()) {
                showChangeNotSaveDialog()
                return
            } else {
                super.onBackPressed()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            Constants.EXTERNAL_STORAGE_READ_REQUEST -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(Constants.ACTION_SEND_PERMISSION_RESULT).putExtra(Constants.KEY_PERMISSION_REQUEST, true))
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    fun showChangeNotSaveDialog() {
        hideKeyboard()
        val fragment = PiquedDialogFragment.newInstance(R.string.edit_profile_unsaved_title, R.string.edit_profile_unsaved_message)
        fragment.show(supportFragmentManager, TAG)
    }

    override fun onDialogPositiveClicked() {

    }

    override fun onDialogNegativeClicked() {
        finish()
    }

    override fun onCreateSetupWindow() {

    }

    @Override
    override fun onDestroy() {
        cleanUpTempFile()

        super.onDestroy()
    }

    fun hasChanges(): Boolean {
        val originalUser = userManager.myUserRecord

        if (originalUser != null) {
            return (!modifiedData.name.isNullOrEmpty() ||
                    !modifiedData.email.isNullOrEmpty() ||
                    !modifiedData.password.isNullOrEmpty() ||
                    modifiedData.imageData != null)
        } else {
            return false
        }
    }

    fun saveAllChanges() {
        loadingIndicator.visibility = View.VISIBLE
        userManager.updateUserData(this, modifiedData, editUserCallback)
    }

    private fun cleanUpTempFile() {
        if (tempUserImageFile?.delete() == false) {
            tempUserImageFile?.deleteOnExit()
            tempUserImageFile = null
        }
    }

    fun createTempUserImageFile(): File? {

        cleanUpTempFile()

        try {
            tempUserImageFile = File.createTempFile("temporary_profile_image", ".jpeg")
        } catch (e: IOException) {
            PiquedLogger.e(TAG, "Failed to create temp file for user image")
        }

        return tempUserImageFile
    }

    fun showUploadResult(success: Boolean) {

        loadingIndicator.visibility = View.GONE
        val builder = AlertDialog.Builder(this)

        builder.setTitle(R.string.activity_edit_profile_account_update)
                .setMessage(if (success) R.string.activity_edit_profile_account_update_success else R.string.activity_edit_profile_account_update_failed)
                .setCancelable(false)
                .setPositiveButton(R.string.ok_button) { _, _ ->
                    exitWithResult(
                            if (success) Constants.EDIT_USER_SUCCESS else Constants.EDIT_USER_FAILED)
                }

        builder.create().show()
    }

    fun updateUserName(newUserName: String?) {
        modifiedData.name = newUserName
    }

    fun getModifiedUserData(): EditUserData {
        return modifiedData
    }

    fun getModifiedEmail(): String? {
        return modifiedData.email
    }

    fun getModifiedPassword(): String? {
        return modifiedData.password
    }

    fun setNewUserImageUri(newUserImageUri: Uri) {
        modifiedData.imageData = newUserImageUri
    }

    fun getNewUserImageUri(): Uri? {
        return modifiedData.imageData
    }

    fun setNewUserEmail(newUserEmail: String?) {
        modifiedData.email = newUserEmail
    }

    fun setNewUserPassword(newUserPassword: String?) {
        modifiedData.password = newUserPassword
    }

    private fun exitWithResult(result: Int) {
        setResult(result)
        finish()
    }

    private val editUserCallback = object: PiquedApiCallback<User> {
        override fun onSuccess(data: User?) {
            if (isUiActive) {
                showUploadResult(true)
            }
        }

        override fun onFailure(errorCode: PiquedErrorCode?, payload: String?) {
            PiquedLogger.e(TAG, "Failed to update user name", errorCode, payload)
            if (isUiActive) {
                showUploadResult(false)
            }
        }

    }
}
