package io.piqued.cloud.repository

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import io.piqued.cloud.PiquedCloud
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.PiquedRepository
import io.piqued.cloud.retrofit.ApiResponse
import io.piqued.cloud.retrofit.AppExecutors
import io.piqued.cloud.retrofit.NetworkBoundResource
import io.piqued.cloud.util.Resource
import io.piqued.database.event.Event
import io.piqued.database.event.EventDatabase
import io.piqued.database.util.PiquedLogger

/**
 *
 * Created by Kenny M. Liou on 11/11/18.
 * Piqued Inc.
 *
 */
class ActivityRepository(
        private val cloud: PiquedCloud,
        private val eventDatabase: EventDatabase,
        private val appExecutors: AppExecutors
): PiquedRepository {

    private var lastFetchSize = 0

    override fun reset() {
        eventDatabase.reset()
    }

    fun getLastFetchSize() : Int {
        return lastFetchSize
    }

    fun deletePost(postId: Long) {
        eventDatabase.postDeleted(postId)
    }

    /**
     * Get activities that are not viewed by user yet
     * The definition of not viewed yet is either user has not open the app and view activity page
     * and user has not swipe or clear all notification from the notification pull down
     */
    fun getNewActivitiesForPushNotification(callback: PiquedCloudCallback<List<Event>>) {
        val result = cloud.getEventsSync(1, 100)
        if (result != null && result.isNotEmpty()) {
            val newEvents = eventDatabase.getNewEvents(result)
            eventDatabase.insertEventsForNotification(newEvents)
            callback.onSuccess(eventDatabase.getUnreadEvents())
        }
    }

    fun getActivityLiveData(batchSize: Int): LiveData<Resource<List<Event>>> {
        return object: NetworkBoundResource<List<Event>, List<Event>>(appExecutors) {
            override fun saveCallResult(item: List<Event>) {
                lastFetchSize = item.size
                eventDatabase.insertEvents(item)
            }

            override fun shouldFetch(data: List<Event>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<Event>> {
                return eventDatabase.getEvents()
            }

            override fun createCall(): LiveData<ApiResponse<List<Event>>> {
                return cloud.getEventsAsLiveData(1, batchSize)
            }

        }.asLiveData()
    }

    fun fetchActivities(page: Int, batchSize: Int) {
        cloud.getEvents(page, batchSize, object : PiquedCloudCallback<List<Event>> {
            override fun onSuccess(result: List<Event>?) {
                if (result != null) {
                    AsyncTask.execute {
                        lastFetchSize = result.size
                        eventDatabase.insertEvents(result)
                    }
                }
            }

            override fun onFailure(error: Throwable?, message: String?) {
                PiquedLogger.e(error, message)
            }
        })
    }

    fun clearNotification() {
        eventDatabase.clearNotification()
    }
}