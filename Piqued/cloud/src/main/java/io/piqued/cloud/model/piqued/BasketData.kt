package io.piqued.cloud.model.piqued

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable

/**
 *
 * Created by Kenny M. Liou on 11/9/18.
 * Piqued Inc.
 *
 */

class BasketData(
        val imageUris: List<Uri>,
        val latitude: Double,
        val longitude: Double,
        val locationName: String,
        val title: String,
        val description: String,
        val fourSquareVenueId: String,
        val countryCode: String,
        val state: String,
        val city: String,
        val fullAddress: String,
        val yelpId: String,
        val primaryColor: String
) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(Uri.CREATOR) as List<Uri>,
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: ""
    )

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeTypedList(imageUris)
        dest.writeDouble(latitude)
        dest.writeDouble(longitude)
        dest.writeString(locationName)
        dest.writeString(title)
        dest.writeString(description)
        dest.writeString(fourSquareVenueId)
        dest.writeString(countryCode)
        dest.writeString(state)
        dest.writeString(city)
        dest.writeString(fullAddress)
        dest.writeString(yelpId)
        dest.writeString(primaryColor)
    }

    companion object CREATOR : Parcelable.Creator<BasketData> {
        override fun createFromParcel(parcel: Parcel): BasketData {
            return BasketData(parcel)
        }

        override fun newArray(size: Int): Array<BasketData?> {
            return arrayOfNulls(size)
        }
    }
}