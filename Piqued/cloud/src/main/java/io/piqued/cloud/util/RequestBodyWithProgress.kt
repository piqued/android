package io.piqued.cloud.util

import okhttp3.MediaType
import okhttp3.RequestBody
import okio.BufferedSink
import okio.Okio

class RequestBodyWithProgress(
        private val delegate: RequestBody,
        private val imageUrl: String,
        private val progressListener: ImageUploadProgressListener?
): RequestBody() {



    override fun contentType(): MediaType? {
        return delegate.contentType()
    }

    override fun contentLength(): Long {
        return delegate.contentLength()
    }

    override fun writeTo(sink: BufferedSink) {

        val countingSink = CountingSink(progressListener, imageUrl, contentLength(), sink)
        val bufferedSink = Okio.buffer(countingSink)

        delegate.writeTo(bufferedSink)

        bufferedSink.flush()
    }

}