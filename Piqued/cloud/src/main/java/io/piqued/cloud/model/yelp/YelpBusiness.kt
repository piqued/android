package io.piqued.cloud.model.yelp

import com.google.gson.annotations.SerializedName

/**
 *
 * Created by Kenny M. Liou on 8/27/18.
 * Piqued Inc.
 *
 */
class YelpBusiness(
        @SerializedName("id") val yelpId: String,
        @SerializedName("name") val businessName: String,
        @SerializedName("hours") val openHours: List<YelpOpenHour>,
        @SerializedName("coordinates") val coordinate: YelpCoordinate,
        @SerializedName("location") val location: YelpLocation,
        @SerializedName("url") val businessUrl: String,
        @SerializedName("phone") val phoneNumber: String,
        @SerializedName("price") val price: String
) {

}

class YelpLocation(
        @SerializedName("display_address") val displayAddress: List<String>,
        @SerializedName("city") val city: String,
        @SerializedName("state") val state: String,
        @SerializedName("country") val countryCode: String
)

class YelpCoordinate(
        @SerializedName("latitude") val latitude: Double,
        @SerializedName("longitude") val longitude: Double
)

class YelpOpenHour(
        @SerializedName("hours_type") val hourType: String,
        @SerializedName("open") val openDayInfo: List<YelpOpenDayInfo>,
        @SerializedName("is_open_now") val isOpenNow: Boolean
)

class YelpOpenDayInfo(
        @SerializedName("is_overnight") val overNight: Boolean,
        @SerializedName("start") val startTime: String,
        @SerializedName("end") val endTime: String,
        @SerializedName("day") val day: Int
)