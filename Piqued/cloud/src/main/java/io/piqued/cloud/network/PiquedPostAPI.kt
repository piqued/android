package io.piqued.cloud.network

import androidx.lifecycle.LiveData
import io.piqued.cloud.model.piqued.PiquedBookmarkResponse
import io.piqued.cloud.retrofit.ApiResponse
import io.piqued.database.post.GetPostsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface PiquedPostAPI {
    @POST("/posts/{post_id}/bookmark.json")
    fun bookmarkPost(@Path("post_id") postId: Long,
                     @Query("creds[token]") token: String,
                     @Query("creds[secret]") secret: String,
                     @Query("bookmark[bookmarked]") bookmarked: String
    ): Call<PiquedBookmarkResponse>

    @GET("/posts/region.json")
    fun fetchPostsBasedByRegionAsLiveData(@Query("creds[token]") token: String,
                                          @Query("creds[secret]") secret: String,
                                          @Query("page") page: Int?,
                                          @Query("size") size: Int?,
                                          @Query("region[outer_bounds][lat_min]") latMin: Double,
                                          @Query("region[outer_bounds][lat_max]") latMax: Double,
                                          @Query("region[outer_bounds][lng_min]") lngMin: Double,
                                          @Query("region[outer_bounds][lng_max]") lngMax: Double,
                                          @Query("region[inner_bounds][lat_min]") innerLatMin: Double?,
                                          @Query("region[inner_bounds][lat_max]") innerLatMax: Double?,
                                          @Query("region[inner_bounds][lng_min]") innerLngMin: Double?,
                                          @Query("region[inner_bounds][lng_max]") innerLngMax: Double?
    ): LiveData<ApiResponse<GetPostsResponse>>

    @GET("/posts/region.json")
    fun fetchPostsBasedByRegion(@Query("creds[token]") token: String,
                              @Query("creds[secret]") secret: String,
                              @Query("page") page: Int?,
                              @Query("size") size: Int?,
                              @Query("region[outer_bounds][lat_min]") latMin: Double,
                              @Query("region[outer_bounds][lat_max]") latMax: Double,
                              @Query("region[outer_bounds][lng_min]") lngMin: Double,
                              @Query("region[outer_bounds][lng_max]") lngMax: Double,
                              @Query("region[inner_bounds][lat_min]") innerLatMin: Double?,
                              @Query("region[inner_bounds][lat_max]") innerLatMax: Double?,
                              @Query("region[inner_bounds][lng_min]") innerLngMin: Double?,
                              @Query("region[inner_bounds][lng_max]") innerLngMax: Double?
    ): Call<GetPostsResponse>

    @GET("/posts.json")
    fun getAccountOwnerPostHistoryAsLiveData(
            @Query("creds[token]") token: String,
            @Query("creds[secret]") secret: String,
            @Query("page") page: Int?,
            @Query("size") size: Int?
    ): LiveData<ApiResponse<GetPostsResponse>>

    @GET("/posts.json")
    fun getAccountOwnerPostHistory(
            @Query("creds[token]") token: String,
            @Query("creds[secret]") secret: String,
            @Query("page") page: Int?,
            @Query("size") size: Int?
    ): Call<GetPostsResponse>

    @GET("/users/{user_id}/posts.json")
    fun getUserPostHistoryAsLiveData(
            @Path("user_id") userId: Long,
            @Query("creds[token]") token: String,
            @Query("creds[secret]") secret: String,
            @Query("page") page: Int?,
            @Query("size") size: Int?
    ): LiveData<ApiResponse<GetPostsResponse>>

    @GET("/users/{user_id}/posts.json")
    fun getUserPostHistory(
            @Path("user_id") userId: Long,
            @Query("creds[token]") token: String,
            @Query("creds[secret]") secret: String,
            @Query("page") page: Int?,
            @Query("size") size: Int?
    ): Call<GetPostsResponse>
}