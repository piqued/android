package io.piqued.cloud.model.twitter

import com.google.gson.annotations.SerializedName

/**
 *
 * Created by Kenny M. Liou on 8/26/18.
 * Piqued Inc.
 *
 */

class RawTwitterResponse (
        @SerializedName("hashtags") val hashtags: List<TwitterHashTag>
)