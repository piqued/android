package io.piqued.cloud.model.piqued

/**
 *
 * Created by Kenny M. Liou on 7/21/18.
 * Piqued Inc.
 *
 */
class Echo(
        val format: String,
        val controller: String,
        val action: String
)