package io.piqued.cloud.model.piqued

import com.google.gson.annotations.SerializedName

/**
 *
 * Created by Kenny M. Liou on 11/9/18.
 * Piqued Inc.
 *
 */

class UserToken(
        @SerializedName("user_id")
        val userId: Int,
        @SerializedName("site")
        val site: String,
        @SerializedName("token_id")
        val token: String,
        @SerializedName("secret")
        val secret: String
)
