package io.piqued.cloud.model.piqued

import com.google.gson.annotations.SerializedName

/**
 *
 * Created by Kenny M. Liou on 9/15/18.
 * Piqued Inc.
 *
 */
class PreSignResponse(
        @SerializedName("id") val preSignId: String,
        @SerializedName("storage") val storageType: String,
        @SerializedName("metadata") val mataData: PreSignMetaData
)
