package io.piqued.cloud.util

/**
 *
 * Created by Kenny M. Liou on 11/9/18.
 * Piqued Inc.
 *
 */

const val CRED_TOKEN = "creds[token]"
const val CRED_SECRET = "creds[secret]"

const val FIELD_CREDS = "creds"
const val FIELD_CRED_TOKEN = "token"
const val FIELD_CRED_SECRET = "secret"

const val FIELD_POST = "post"
const val FIELD_LOCATION_LAT = "lat"
const val FIELD_LOCATION_LNG = "lng"
const val FIELD_TITLE = "title"
const val FIELD_DESCRIPTION = "description"
const val FIELD_LOCATION_NAME = "location_name"
const val FIELD_ADDRESS = "address"
const val FIELD_FOURSQUARE_VENUE_ID = "foursquare_venue_id"
const val FIELD_FOURSQUARE_CC = "foursquare_cc"
const val FIELD_FOURSQUARE_CITY = "foursquare_city"
const val FIELD_FOURSQUARE_STATE = "foursquare_state"
const val FIELD_YELP_ID = "yelp_id"
const val FIELD_IMAGES = "images"
const val FIELD_PRIMARY_COLOR = "primaryColor"