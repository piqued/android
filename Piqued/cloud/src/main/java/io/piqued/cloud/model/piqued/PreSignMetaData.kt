package io.piqued.cloud.model.piqued

import com.google.gson.annotations.SerializedName

/**
 *
 * Created by Kenny M. Liou on 9/15/18.
 * Piqued Inc.
 *
 */
class PreSignMetaData(
        @SerializedName("filename") val fileName: String,
        @SerializedName("size") val sizeInByte: Long,
        @SerializedName("mime_type") val mimeType: String
)
