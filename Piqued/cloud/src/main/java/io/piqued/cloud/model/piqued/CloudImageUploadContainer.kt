package io.piqued.cloud.model.piqued

import com.google.gson.annotations.SerializedName
import io.piqued.cloud.util.ImageDetail

/**
 *
 * Created by Kenny M. Liou on 11/4/18.
 * Piqued Inc.
 *
 */
class CloudImageUploadContainer {
    @SerializedName("lg")
    val largeImageUrl = ImageDetail(ImageUploadType.LARGE)
    @SerializedName("sm")
    val smallImageUrl = ImageDetail(ImageUploadType.SMALL)
}