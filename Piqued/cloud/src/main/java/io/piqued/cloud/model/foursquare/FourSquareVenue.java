package io.piqued.cloud.model.foursquare;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/4/17.
 * Piqued Inc.
 */

public class FourSquareVenue {

    public class Contact {
        String phone;
        public String formattedPhone;
    }
    public class Location {
        public String address;
        public double lat;
        public double lng;
        String postalCode;
        public String cc;
        public String city;
        public String state;
        public String country;
        public String[] formattedAddress;
    }

    private class Category {
        String id;
        String name;
        String pluralName;
        String shortName;
    }

    public class Price {
        public int tier;
        String message;
        String currency;
    }

    public class Tips {
        public int count;
        public TipGroup[] groups;
    }

    public class TipGroup {
        String type;
        String name;
        int count;
        public Tip[] items;
    }

    public class Tip {
        String id;
        long createdAt;
        public String text;
        String type;
        public TipUser user;
    }

    public class TipUser {
        public String id;

        public String firstName;
        public String lastName;
        public TipUserPhoto photo;

        String gender;
        String photourl;
    }

    public class TipUserPhoto {
        public String prefix;
        public String suffix;
        // NOTE: prefix/widthxheight/suffix will return user photo
    }

    public class Hours {
        public String status;
        public boolean isOpen;
        public boolean isLocalHoliday;
        public TimeFrame[] timeframes;
    }

    public class TimeFrame {
        public String days;
        public boolean includesToday;
        public RenderedTime[] open;
    }

    public class RenderedTime {
        public String renderedTime;
    }

    public String id;
    public String name;
    public Contact contact;
    public Location location;
    public String canonicalUrl;
    public Category[] categories;
    public String url;
    public Price price;
    public Hours hours;
    public Hours popular;
    public Tips tips;
}
