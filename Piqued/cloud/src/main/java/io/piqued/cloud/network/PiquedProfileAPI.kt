package io.piqued.cloud.network

import androidx.lifecycle.LiveData
import io.piqued.cloud.retrofit.ApiResponse
import io.piqued.cloud.util.CRED_SECRET
import io.piqued.cloud.util.CRED_TOKEN
import io.piqued.database.event.Event
import io.piqued.database.post.CloudPost
import io.piqued.database.user.User
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface PiquedProfileAPI {
    @GET("/bookmarks.json")
    fun getBookmarksAsLiveData(@Query(CRED_TOKEN) token: String,
                               @Query(CRED_SECRET) secret: String
    ): LiveData<ApiResponse<List<CloudPost>>>

    @GET("/bookmarks.json")
    fun getBookmarks(@Query(CRED_TOKEN) token: String,
                     @Query(CRED_SECRET) secret: String
    ): Call<List<CloudPost>>

    @GET("/users/{accountOwnerId}/followings.json")
    fun getFollowingUserListAsLiveData(
            @Path("accountOwnerId") accountOwnerId: Long,
            @Query(CRED_TOKEN) token: String,
            @Query(CRED_SECRET) secret: String
    ): LiveData<ApiResponse<ResponseBody>>

    @GET("/users/{accountOwnerId}/followings.json")
    fun getFollowingUserList(
            @Path("accountOwnerId") accountOwnerId: Long,
            @Query(CRED_TOKEN) token: String,
            @Query(CRED_SECRET) secret: String
    ): Call<ResponseBody>

    @GET("/users/{userId}.json")
    fun getUserInfo(
            @Path("userId") userId: Long,
            @Query(CRED_TOKEN) token: String,
            @Query(CRED_SECRET) secret: String
    ): LiveData<ApiResponse<User>>

    @POST("/users/{email}/password_resets.json")
    fun resetPassword(
            @Path("email") email: String
    ): Call<ResponseBody>

    @GET("/events.json")
    fun getEventsAsLiveData(
            @Query(CRED_TOKEN) token: String,
            @Query(CRED_SECRET) secret: String,
            @Query("page") page: Int?,
            @Query("size") size: Int?
    ): LiveData<ApiResponse<List<Event>>>

    @GET("/events.json")
    fun getEvents(
            @Query(CRED_TOKEN) token: String,
            @Query(CRED_SECRET) secret: String,
            @Query("page") page: Int?,
            @Query("size") size: Int?
    ): Call<List<Event>>
}