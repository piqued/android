package io.piqued.cloud.repository

import io.piqued.cloud.PiquedCloud
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.PiquedRepository
import io.piqued.cloud.model.foursquare.FourSquareVenue

/**
 *
 * Created by Kenny M. Liou on 8/27/18.
 * Piqued Inc.
 *
 */
class FourSquareRepository(
        val clientId: String,
        val clientSecret: String,
        val cloud: PiquedCloud
) : PiquedRepository {

    override fun reset() {
        // do nothing
    }

    companion object {
        private const val FOUR_SQUARE_VERSION_NUMBER = "20170729" // 2017, JUL 29
    }

    fun getFourSquareVenue(fourSquareId: String, callback: PiquedCloudCallback<FourSquareVenue>) {

        // TODO: fix me
        // cloud.getFourSquareVenue(clientId, clientSecret, fourSquareId, FOUR_SQUARE_VERSION_NUMBER, callback)
    }

    fun getFourSquareUrl(fourSquareId: String): String {
        return "https://foursquare.com/v/$fourSquareId"
    }
}