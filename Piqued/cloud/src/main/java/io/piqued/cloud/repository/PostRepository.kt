package io.piqued.cloud.repository

import android.location.Location
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import io.piqued.cloud.PiquedCloud
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.PiquedCloudImpl
import io.piqued.cloud.PiquedRepository
import io.piqued.cloud.retrofit.ApiResponse
import io.piqued.cloud.retrofit.AppExecutors
import io.piqued.cloud.retrofit.NetworkBoundResource
import io.piqued.cloud.util.PostsCallback
import io.piqued.cloud.util.RegionBounds
import io.piqued.cloud.util.Resource
import io.piqued.database.UserId
import io.piqued.database.post.CloudPost
import io.piqued.database.post.GetPostsResponse
import io.piqued.database.post.HomeListPost
import io.piqued.database.post.PostDatabase
import io.piqued.database.util.PiquedLogger
import java.util.*
import kotlin.collections.ArrayList

class PostRepository(
        private val cloud: PiquedCloud,
        private val postDatabase: PostDatabase,
        private val appExecutors: AppExecutors
): PiquedRepository {

    companion object {
        const val DEFAULT_POST_BATCH_SIZE = 25
    }

    /**
     * Use this method to fetch more posts
     */
    fun getUserPostHistoryWithPage(userId: UserId, page: Int) {
        cloud.getUserPostHistory(userId, page, DEFAULT_POST_BATCH_SIZE, object : PiquedCloudCallback<List<CloudPost>> {
            override fun onSuccess(result: List<CloudPost>?) {
                if (result != null) {
                    AsyncTask.execute {
                        postDatabase.addPostsToCache(result)
                    }
                }
            }

            override fun onFailure(error: Throwable?, message: String?) {
                // TODO: implement log here
            }

        })
    }

    /**
     * Use this method to perform initial fetch
     */
    fun getUserPostHistory(userId: UserId): LiveData<Resource<List<CloudPost>>> {
        return object : NetworkBoundResource<List<CloudPost>, GetPostsResponse>(appExecutors) {
            override fun saveCallResult(item: GetPostsResponse) {
                postDatabase.addPostsToCache(item.posts)
            }

            override fun shouldFetch(data: List<CloudPost>?): Boolean {
                return cloud.isUserLoggedIn()
            }

            override fun loadFromDb(): LiveData<List<CloudPost>> {
                return postDatabase.getUserPostHistory(userId)
            }

            override fun createCall(): LiveData<ApiResponse<GetPostsResponse>> {
                return cloud.getUserPostHistory(userId, 1, DEFAULT_POST_BATCH_SIZE)
            }

        }.asLiveData()
    }

    fun getAccountOwnerPostHistory(accountOwnerUserId: UserId?): LiveData<Resource<List<CloudPost>>> {
        return object : NetworkBoundResource<List<CloudPost>, GetPostsResponse>(appExecutors) {
            override fun saveCallResult(item: GetPostsResponse) {
                postDatabase.addPostsToCache(item.posts)
            }

            override fun shouldFetch(data: List<CloudPost>?): Boolean {
                return cloud.isUserLoggedIn()
            }

            override fun loadFromDb(): LiveData<List<CloudPost>> {
                return postDatabase.getUserPostHistory(accountOwnerUserId!!)
            }

            override fun createCall(): LiveData<ApiResponse<GetPostsResponse>> {
                return cloud.getAccountOwnerPostHistory(1, DEFAULT_POST_BATCH_SIZE)
            }

        }.asLiveData()
    }

    fun getAccountOwnerPostHistory(page: Int) {
        cloud.getAccountOwnerPostHistory(page, DEFAULT_POST_BATCH_SIZE, object : PiquedCloudCallback<List<CloudPost>> {
            override fun onSuccess(result: List<CloudPost>?) {
                if (result != null) {
                    AsyncTask.execute {
                        postDatabase.addPostsToCache(result)
                    }
                }
            }

            override fun onFailure(error: Throwable?, message: String?) {
                PiquedLogger.e(error, message)
            }
        })
    }

    fun fetchPostsBasedOnLocation(page: Int, location: Location): LiveData<Resource<List<CloudPost>>> {

        val bounds = RegionBounds.fromLocation(location, PiquedCloudImpl.DEFAULT_SEARCH_DISTANCE_METER)

        return object : NetworkBoundResource<List<CloudPost>, GetPostsResponse>(appExecutors) {
            override fun saveCallResult(item: GetPostsResponse) {

                val postRecord = ArrayList<HomeListPost>()

                for (post: CloudPost in item.posts) {
                    postRecord.add(HomeListPost(post.postId, post.getLatLng().latitude, post.getLatLng().longitude))
                }

                if (page == 1) { // fetching whole new stuff
                    postDatabase.homeListPostDao.reset()
                }

                postDatabase.addPostsToCache(item.posts)
                postDatabase.homeListPostDao.insertPostRecords(postRecord)
            }

            override fun shouldFetch(data: List<CloudPost>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<CloudPost>> {
                return postDatabase.homeListPostDao.getAllPosts(bounds.latMin, bounds.latMax, bounds.lngMin, bounds.lngMax)
            }

            override fun createCall(): LiveData<ApiResponse<GetPostsResponse>> {
                return cloud.getPostsBasedOnLocation(bounds.latMin, bounds.latMax, bounds.lngMin, bounds.lngMax, page)
            }

        }.asLiveData()
    }

    fun fetchPostsBasedOnLocationAndUpdateDB(page: Int, location: Location) {

        val bounds = RegionBounds.fromLocation(location, PiquedCloudImpl.DEFAULT_SEARCH_DISTANCE_METER)

        cloud.getPostsBasedOnLocation(bounds.latMin, bounds.latMax, bounds.lngMin, bounds.lngMax, page, object : PiquedCloudCallback<List<CloudPost>> {
            override fun onSuccess(result: List<CloudPost>?) {

                if (result != null) {
                    AsyncTask.execute {
                        val postRecord = ArrayList<HomeListPost>()

                        for (post: CloudPost in result) {
                            postRecord.add(HomeListPost(post.postId, post.getLatLng().latitude, post.getLatLng().longitude))
                        }

                        postDatabase.addPostsToCache(result)
                        postDatabase.homeListPostDao.insertPostRecords(postRecord)
                    }
                }
            }

            override fun onFailure(error: Throwable?, message: String?) {}
        })
    }

    fun fetchPostsBasedOnMap(page: Int, region: RegionBounds, callback: PostsCallback) {

        cloud.getPostsBasedOnLocationOnMap(region, page, object : PiquedCloudCallback<List<CloudPost>> {
            override fun onSuccess(result: List<CloudPost>?) {
                if (result != null) {

                    AsyncTask.execute {
                        insertPosts(result)
                    }

                    callback.onPosts(page, result, result.size < 50)
                } else {
                    callback.onPosts(page, Collections.emptyList(), true)
                }
            }

            override fun onFailure(error: Throwable?, message: String?) {
                // TODO report me
                Log.d("PostRepository", "fetch post based on map failed", error)
            }

        })
    }

    private fun insertPosts(posts : List<CloudPost>) {
        if (posts.isNotEmpty()) {
            postDatabase.addPostsToCache(posts)
        }
    }

    fun deletePostFromLocal(post: CloudPost) {
        postDatabase.deletePost(post)
    }

    override fun reset() {
        postDatabase.reset()
    }
}