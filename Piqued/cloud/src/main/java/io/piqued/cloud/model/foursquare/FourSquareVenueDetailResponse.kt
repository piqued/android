package io.piqued.cloud.model.foursquare

import com.google.gson.annotations.SerializedName

/**
 *
 * Created by Kenny M. Liou on 8/27/18.
 * Piqued Inc.
 *
 */
class FourSquareVenueDetailResponse(
        @SerializedName("response") val venue: FourSquareVenue
)