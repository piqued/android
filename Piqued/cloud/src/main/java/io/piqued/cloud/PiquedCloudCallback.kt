package io.piqued.cloud

interface PiquedCloudCallback<T> {

    fun onSuccess(result: T?)

    fun onFailure(error: Throwable?, message: String?)
}