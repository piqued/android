package io.piqued.cloud.network

import androidx.annotation.Keep
import io.piqued.cloud.model.foursquare.FourSquareVenueDetailResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 *
 * Created by Kenny M. Liou on 8/27/18.
 * Piqued Inc.
 *
 */
@Keep
interface FourSquareV2API {

    @GET("v2/venues/{VENUE_ID}")
    fun getVenueById(
            @Path("VENUE_ID") fourSquareId: String,
            @Query("client_id") clientId: String,
            @Query("client_secret")clientSecret: String,
            @Query("v") version: String)
            : Call<FourSquareVenueDetailResponse>

    @GET("v2/venues/search")
    fun searchByLatLng(
        @Query("ll") latlng: String)
    : Call<FourSquareVenueDetailResponse>
}