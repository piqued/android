package io.piqued.cloud.network

import io.piqued.cloud.model.piqued.PreSignObject
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 *
 * Created by Kenny M. Liou on 11/4/18.
 * Piqued Inc.
 *
 */
interface PiquedUploadImageAPI {

    @GET("/photos/cache/presign")
    fun getPreSignObject(): Call<PreSignObject>

    @Headers("Content-Type: application/json")
    @POST("/posts.json")
    fun createNewBasket(
            @Body newBasketData: RequestBody
    ): Call<Void>
}