package io.piqued.cloud.network

import io.piqued.cloud.model.piqued.Echo
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface PiquedUtilAPI {
    @GET("/echo.json")
    fun getHealthCheck(): Call<Echo>
}