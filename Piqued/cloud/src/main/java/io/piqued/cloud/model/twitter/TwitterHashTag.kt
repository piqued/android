package io.piqued.cloud.model.twitter

import com.google.gson.annotations.SerializedName

/**
 *
 * Created by Kenny M. Liou on 8/26/18.
 * Piqued Inc.
 *
 */

class TwitterHashTag(
        @SerializedName("hashtag") val hashTag: String,
        @SerializedName("rounded_score") val score: Int,
        @SerializedName("social_proof") val socialProof: Int
)