package io.piqued.cloud.model.piqued;

import android.content.Context;
import android.text.TextUtils;

import java.util.Locale;

import io.piqued.cloud.R;
import io.piqued.cloud.model.foursquare.FourSquareVenue;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/11/17.
 * Piqued Inc.
 */

public class VenueTip {

    private final long mUserId;
    private final String mUserName;
    private final String mMessage;
    private final String mImageUrlPrefix;
    private final String mImageUrlSuffix;

    private static final String USER_IMAGE_SIZE = "20x20";

    private String mImageUrl;

    static VenueTip fromFourSquare(Context context, FourSquareVenue.Tip tip) {

        FourSquareVenue.TipUser user = tip.user;

        String name;
        if (user == null) {
            name = context.getString(R.string.tip_not_available);
        } else if (user.firstName != null && user.lastName != null) {
            name = String.format(Locale.getDefault(), context.getString(R.string.user_name), user.firstName, user.lastName);
        } else if (user.firstName == null && user.lastName == null) {
            name = context.getString(R.string.tip_not_available);
        } else if (user.firstName == null) {
            name = user.lastName;
        } else {
            name = user.firstName;
        }

        final String message = tip.text;

        return new VenueTip(
                Long.valueOf(tip.user.id),
                name,
                user != null ? user.photo.prefix : "",
                user != null ? user.photo.suffix : "",
                message);
    }

    private VenueTip(long userId, String userName, String imagePrefix, String imageSuffix, String message) {

        mUserId = userId;
        mUserName = userName;
        mImageUrlPrefix = imagePrefix;
        mImageUrlSuffix = imageSuffix;
        mMessage = message;
    }

    public boolean hasImageUrl() {
        return !TextUtils.isEmpty(mImageUrl) && TextUtils.isEmpty(mImageUrlPrefix) && TextUtils.isEmpty(mImageUrlSuffix);
    }

    public String getImageUrl(int mUserImageSize) {

        if (mImageUrl == null || !TextUtils.isEmpty(mImageUrl)) {
            mImageUrl = mImageUrlPrefix + String.format("%1$dx%2$d", mUserImageSize, mUserImageSize) + mImageUrlSuffix;
        }

        return mImageUrl;
    }

    public String getUserName() {
        return mUserName;
    }

    public String getMessage() {
        return mMessage;
    }
}
