package io.piqued.cloud.model.yelp

import com.google.gson.annotations.SerializedName

/**
 *
 * Created by Kenny M. Liou on 8/26/18.
 * Piqued Inc.
 *
 */
data class YelpMatchBusiness(
        @SerializedName("name") val venueName: String,
        @SerializedName("id") val yelpId: String

)