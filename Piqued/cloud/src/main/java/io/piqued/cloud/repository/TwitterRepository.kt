package io.piqued.cloud.repository

import io.piqued.cloud.PiquedCloud
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.PiquedRepository
import io.piqued.cloud.model.twitter.TwitterHashTag

/**
 *
 * Created by Kenny M. Liou on 8/26/18.
 * Piqued Inc.
 *
 */
class TwitterRepository(
        private val cloud: PiquedCloud
): PiquedRepository {
    override fun reset() {
        // do nothing
    }

    fun getTwitterTypeAHeadSuggestions(userInput: String, callback: PiquedCloudCallback<List<TwitterHashTag>>) {

        val inputList = userInput.split(" ")

        val input = inputList[inputList.size - 1]

        cloud.getTypeAheadHashTagSuggestion(input, callback)
    }
}