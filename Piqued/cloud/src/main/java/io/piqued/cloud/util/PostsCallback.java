package io.piqued.cloud.util;

import java.util.List;

import io.piqued.database.post.CloudPost;

/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/2/17.
 * Piqued Inc.
 */

public interface PostsCallback {

    void onPosts(int currentPage, List<CloudPost> posts, boolean isLast);
}
