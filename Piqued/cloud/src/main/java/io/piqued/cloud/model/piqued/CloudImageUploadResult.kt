package io.piqued.cloud.model.piqued

import com.google.gson.annotations.SerializedName

/**
 *
 * Created by Kenny M. Liou on 11/4/18.
 * Piqued Inc.
 *
 */
class CloudImageUploadResult(
        @SerializedName("id")
        val id: String,
        @SerializedName("size")
        val sizeInBytes: Long,
        @SerializedName("storage")
        val storage: String
) {
    companion object {
        private const val MIME_TYPE = "image/jpeg"
    }

    @SerializedName("filename")
    val fileName = "uploaded_via_android"

}