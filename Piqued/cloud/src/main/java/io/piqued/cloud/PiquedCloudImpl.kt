package io.piqued.cloud

import androidx.lifecycle.LiveData
import com.google.gson.Gson
import io.piqued.cloud.model.foursquare.FourSquareVenue
import io.piqued.cloud.model.foursquare.FourSquareVenueDetailResponse
import io.piqued.cloud.model.piqued.*
import io.piqued.cloud.model.twitter.RawTwitterResponse
import io.piqued.cloud.model.twitter.TwitterHashTag
import io.piqued.cloud.model.yelp.YelpBusiness
import io.piqued.cloud.model.yelp.YelpMatchBusiness
import io.piqued.cloud.model.yelp.YelpResultList
import io.piqued.cloud.network.*
import io.piqued.cloud.retrofit.ApiResponse
import io.piqued.cloud.retrofit.LiveDataCallAdapterFactory
import io.piqued.cloud.util.*
import io.piqued.database.UserId
import io.piqued.database.event.Event
import io.piqued.database.post.CloudPost
import io.piqued.database.post.GetPostsResponse
import io.piqued.database.user.User
import okhttp3.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 *
 * Created by Kenny M. Liou on 7/21/18.
 * Piqued Inc.
 *
 */
class PiquedCloudImpl(
        private val preference: CloudPreference
): PiquedCloud {

    companion object {
        const val DEFAULT_SEARCH_DISTANCE_METER = 2000000 // 20000KM
    }

    init {
        updateHostUrl(preference.getHostUrl())
        updateFourSquareHostUrl(preference.getFourSquareHostUrl())
    }

    private lateinit var awsHttpUploadClient: OkHttpClient
    private lateinit var httpClient: OkHttpClient
    private lateinit var retrofit : Retrofit
    private lateinit var utilAPI: PiquedUtilAPI
    private lateinit var profileAPI : PiquedProfileAPI
    private lateinit var postAPI : PiquedPostAPI
    private lateinit var uploadImageAPI: PiquedUploadImageAPI

    private val twitterRetrofit = Retrofit.Builder()
            .baseUrl("https://twitter.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    private val yelpRetrofit = Retrofit.Builder()
            .baseUrl("https://api.yelp.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    private val twitterAPI = twitterRetrofit.create(TwitterTypeAHeadAPI::class.java)!!
    private val yelpAPI = yelpRetrofit.create(YelpV3API::class.java)!!
    private lateinit var fourSquareAPI: FourSquareV2API

    override fun updateHostUrl(hostUrl: String) {
        awsHttpUploadClient = OkHttpClient.Builder().build()

        httpClient = OkHttpClient.Builder().addInterceptor { chain ->

            val newHost = HttpUrl.parse(hostUrl)
            val newUrl = chain.request().url().newBuilder()
                    .scheme(newHost!!.scheme())
                    .host(newHost.host())
                    .build()

            val request = chain.request().newBuilder().url(newUrl).build()

            chain.proceed(request)
        }.build()

        retrofit = Retrofit.Builder()
                .baseUrl(hostUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .client(httpClient)
                .build()

        utilAPI = retrofit.create(PiquedUtilAPI::class.java)!!
        profileAPI = retrofit.create(PiquedProfileAPI::class.java)
        postAPI = retrofit.create(PiquedPostAPI::class.java)
        uploadImageAPI = retrofit.create(PiquedUploadImageAPI::class.java)
    }

    override fun updateFourSquareHostUrl(hostUrl: String) {

        val baseUrl = if (hostUrl.endsWith("/")) hostUrl else hostUrl + "/"

        val fourSquareRetrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        fourSquareAPI = fourSquareRetrofit.create(FourSquareV2API::class.java)!!
    }

    override fun performHealthCheck(callback: PiquedCloudCallback<Echo?>) {

        val call = utilAPI.getHealthCheck()

        call.enqueue(object : Callback<Echo> {
            override fun onFailure(call: Call<Echo>?, t: Throwable?) {
                callback.onFailure(t, t?.toString())
            }

            override fun onResponse(call: Call<Echo>?, response: Response<Echo>?) {

                callback.onSuccess(response?.body())
            }
        })
    }

    override fun getBookmarksAsLiveData(): LiveData<ApiResponse<List<CloudPost>>> {

        val cred = preference.getToken()

        if (cred != null) {
            return profileAPI.getBookmarksAsLiveData(cred.token, cred.secret)
        }

        throw IllegalStateException("User should be logged in to use this feature")
    }

    override fun refreshBookmarks(callback: PiquedCloudCallback<List<CloudPost>>) {
        val cred = preference.getToken()

        if (cred != null) {
            val call = profileAPI.getBookmarks(cred.token, cred.secret)

            call.enqueue(object : Callback<List<CloudPost>> {
                override fun onFailure(call: Call<List<CloudPost>>?, t: Throwable?) {
                    callback.onFailure(t, t?.message)
                }

                override fun onResponse(call: Call<List<CloudPost>>?, response: Response<List<CloudPost>>?) {
                    callback.onSuccess(response?.body())
                }

            })

            return
        }

        throw IllegalStateException("User should be logged in to use this feature")
    }

    override fun bookmarkPost(postId: Long, bookmarked: Boolean, callback: PiquedCloudCallback<Long>) {

        val cred = preference.getToken()
        if (cred != null) {
            val call = postAPI.bookmarkPost(postId, cred.token, cred.secret, bookmarked.toString())

            call.enqueue(object : Callback<PiquedBookmarkResponse> {
                override fun onFailure(call: Call<PiquedBookmarkResponse>?, t: Throwable?) {
                    callback.onFailure(t, t?.message)
                }

                override fun onResponse(call: Call<PiquedBookmarkResponse>?, response: Response<PiquedBookmarkResponse>?) {
                    if (response != null) {
                        callback.onSuccess(postId)
                    }
                }

            })
        }
    }

    override fun getPostsBasedOnLocation(latMin: Double, latMax: Double, lngMin: Double, lngMax: Double, page: Int): LiveData<ApiResponse<GetPostsResponse>> {

        val cred = preference.getToken()
        return postAPI.fetchPostsBasedByRegionAsLiveData(
                cred?.token ?: "",
                cred?.secret ?: "",
                page,
                50,
                latMin,
                latMax,
                lngMin,
                lngMax,
                null,
                null,
                null,
                null
        )
    }

    override fun getPostsBasedOnLocation(latMin: Double, latMax: Double, lngMin: Double, lngMax: Double, page: Int, callback: PiquedCloudCallback<List<CloudPost>>) {
        val cred = preference.getToken()

        if (cred != null) {
            val call = postAPI.fetchPostsBasedByRegion(
                    cred.token,
                    cred.secret,
                    page,
                    50,
                    latMin,
                    latMax,
                    lngMin,
                    lngMax,
                    null,
                    null,
                    null,
                    null
            )

            call.enqueue(object : Callback<GetPostsResponse> {
                override fun onFailure(call: Call<GetPostsResponse>?, t: Throwable?) {
                    callback.onFailure(t, t?.message)
                }

                override fun onResponse(call: Call<GetPostsResponse>?, response: Response<GetPostsResponse>?) {

                    if (response?.body() != null) {
                        callback.onSuccess(response.body()!!.posts)
                    } else {
                        callback.onFailure(null, null)
                    }
                }

            })
        }
    }

    override fun getPostsBasedOnLocationOnMap(regionBounds: RegionBounds, page: Int, callback: PiquedCloudCallback<List<CloudPost>>) {
        val cred = preference.getToken()
        val call = postAPI.fetchPostsBasedByRegion(
                cred?.token ?: "",
                cred?.secret ?: "",
                page,
                50,
                regionBounds.latMin,
                regionBounds.latMax,
                regionBounds.lngMin,
                regionBounds.lngMax,
                null,
                null,
                null,
                null
        )

        call.enqueue(object : Callback<GetPostsResponse> {
            override fun onFailure(call: Call<GetPostsResponse>?, t: Throwable) {
                callback.onFailure(t, t.message)
            }

            override fun onResponse(call: Call<GetPostsResponse>, response: Response<GetPostsResponse>) {

                if (response.body() != null) {
                    callback.onSuccess(response.body()!!.posts)
                } else {
                    callback.onFailure(null, null)
                }
            }

        })
    }

    override fun getAccountOwnerPostHistory(page: Int, batchSize: Int): LiveData<ApiResponse<GetPostsResponse>> {
        val cred = preference.getToken()
        if (cred != null) {
            return postAPI.getAccountOwnerPostHistoryAsLiveData(
                    cred.token,
                    cred.secret,
                    page,
                    batchSize
            )
        }

        throw IllegalStateException("CREDENTIAL MISSING: User should be logged in to use getAccountOwnerPostHistory")
    }

    override fun getAccountOwnerPostHistory(page: Int, batchSize: Int, callback: PiquedCloudCallback<List<CloudPost>>) {
        val cred = preference.getToken()
        if (cred != null) {
            val call = postAPI.getAccountOwnerPostHistory(cred.token, cred.secret, page, batchSize)

            call.enqueue(object : Callback<GetPostsResponse> {
                override fun onFailure(call: Call<GetPostsResponse>, t: Throwable) {
                    callback.onFailure(t, t.message)
                }

                override fun onResponse(call: Call<GetPostsResponse>, response: Response<GetPostsResponse>) {
                    if (response.body() != null) {
                        callback.onSuccess(response.body()!!.posts)
                    } else {
                        callback.onFailure(null, null)
                    }
                }
            })
        }
    }

    override fun getFollowingUser(accountOwnerId: Long): LiveData<ApiResponse<ResponseBody>> {
        val cred = preference.getToken()
        if (cred != null) {
            return profileAPI.getFollowingUserListAsLiveData(accountOwnerId, cred.token, cred.secret)
        }

        throw java.lang.IllegalStateException("CREDENTIAL MISSING: User should be logged in to use getFollowingUser")
    }

    override fun getFollowingUser(accountOwnerId: Long, callback: PiquedCloudCallback<ResponseBody>) {
        val cred = preference.getToken()
        if (cred != null) {
            val call = profileAPI.getFollowingUserList(accountOwnerId, cred.token, cred.secret)

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    callback.onFailure(t, t.message)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        callback.onFailure(null, null)
                    }
                }

            })
        } else {
            throw java.lang.IllegalStateException("CREDENTIAL MISSING: User should be logged in to use getFollowingUser")
        }
    }

    override fun isUserLoggedIn(): Boolean {
        return preference.isUserLoggedIn()
    }

    override fun getUserInfo(userId: Long): LiveData<ApiResponse<User>> {

        val cred = preference.getToken()
        if (cred != null) {

            return profileAPI.getUserInfo(userId, cred.token, cred.secret)
        }

        throw java.lang.IllegalStateException("CREDENTIAL MISSING: User should be logged in to use getFollowingUser")
    }

    override fun resetPassword(email: String, callback: PiquedCloudCallback<ResetPasswordState>) {

        val call = profileAPI.resetPassword(email)

        call.enqueue(object: Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                callback.onFailure(t, t.message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.code() == 429) {
                    callback.onSuccess(ResetPasswordState.LIMIT_EXCEEDED)
                } else if (response.code() == 401) {
                    callback.onSuccess(ResetPasswordState.EMAIL_NOT_FOUND)
                } else if (response.code() == 204) {
                    callback.onSuccess(ResetPasswordState.EMAIL_SENT)
                } else {
                    callback.onSuccess(ResetPasswordState.ERROR)
                }
            }
        })
    }

    override fun getEventsAsLiveData(page: Int, batchSize: Int): LiveData<ApiResponse<List<Event>>> {
        val cred = preference.getToken()

        if (cred != null) {
            return profileAPI.getEventsAsLiveData(cred.token, cred.secret, page, batchSize)
        }

        throw java.lang.IllegalStateException("User should be logged in to use this feature")
    }

    override fun getEvents(page: Int, batchSize: Int, callback: PiquedCloudCallback<List<Event>>) {
        val cred = preference.getToken()
        if (cred != null) {
            val call = profileAPI.getEvents(cred.token, cred.secret, page, batchSize)
            call.enqueue(object : Callback<List<Event>> {
                override fun onFailure(call: Call<List<Event>>?, t: Throwable?) {
                    callback.onFailure(t, t?.message)
                }
                override fun onResponse(call: Call<List<Event>>?, response: Response<List<Event>>?) {
                    callback.onSuccess(response?.body())
                }
            })

            return
        }

        throw java.lang.IllegalStateException("User should be logged in to use this feature")
    }

    override fun getEventsSync(page: Int, batchSize: Int): List<Event>? {
        val cred = preference.getToken()
        if (cred != null) {
            val call = profileAPI.getEvents(cred.token, cred.secret, page, batchSize)
            return call.execute().body()
        }

        throw java.lang.IllegalStateException("User should be logged in to use this feature")
    }

    override fun getPreSign(): PreSignObject {
        val call = uploadImageAPI.getPreSignObject()

        val response = call.execute()

        if (response.isSuccessful) {
            val preSignObject = response.body()

            if (preSignObject != null) {
                return preSignObject
            }
        }

        throw IllegalStateException("Failed to get preSignObject")
    }

    override fun uploadImageToPreSign(preSign: PreSignObject, imageUrl: String, imageBase64: ImageContainer, listener: ImageUploadProgressListener?): CloudImageUploadResult? {

        val multiRequestBodyBuilder = MultipartBody.Builder()
                .setType(MultipartBody.FORM)

        for ((key, value) in preSign.fields) {
            multiRequestBodyBuilder.addFormDataPart(key, value)
        }

        multiRequestBodyBuilder
                .addFormDataPart(
                        "file",
                        "filename.jpg",
                        RequestBody.create(MediaType.parse("image/jpeg"), imageBase64.image))

        val multiRequestBody = multiRequestBodyBuilder.build()

        val request: Request =
                if (listener != null) {
                    val monitorBody = RequestBodyWithProgress(multiRequestBody, imageUrl, listener)

                    Request.Builder()
                            .url(preSign.url)
                            .post(monitorBody)
                            .build()
                } else {
                    Request.Builder()
                            .url(preSign.url)
                            .post(multiRequestBody)
                            .build()
                }

        val call = awsHttpUploadClient.newCall(request)

        val response = call.execute()

        if (response.isSuccessful) {
            val responseString = response.body()?.string() ?: ""

            if (responseString.isNotBlank()) {

                val preSignResponse = Gson().fromJson<PreSignResponse>(responseString, PreSignResponse::class.java)

                return CloudImageUploadResult(preSignResponse.preSignId, preSignResponse.mataData.sizeInByte, preSignResponse.storageType)
            } else {

                val keys = preSign.fields["key"]!!.split("/")

                return CloudImageUploadResult(keys[keys.size - 1], imageBase64.sizeInBytes, "cache")
            }

        } else {
            return null
        }
    }

    override fun createNewBasket(images: List<CloudImageUploadContainer>, basketData: BasketData, userToken: UserToken): Boolean {


        val jsonPayload = PayloadHelper.createNewBasketObject(images, basketData, userToken)

        val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonPayload.toString())

        val call = uploadImageAPI.createNewBasket(body)

        val response = call.execute()

        return response.isSuccessful
    }

    override fun getUserPostHistory(userId: UserId, page: Int, batchSize: Int): LiveData<ApiResponse<GetPostsResponse>> {
        val cred = preference.getToken()
        if (cred != null) {
            return postAPI.getUserPostHistoryAsLiveData(
                    userId.userId,
                    cred.token,
                    cred.secret,
                    page,
                    batchSize
            )
        }

        throw IllegalStateException("User should be logged in to use getUserPostHistoryAsLiveData")
    }

    override fun getUserPostHistory(userId: UserId, page: Int, batchSize: Int, callback: PiquedCloudCallback<List<CloudPost>>) {
        val cred = preference.getToken()
        if (cred != null) {
            val call = postAPI.getUserPostHistory(userId.userId, cred.token, cred.secret, page, batchSize)

            call.enqueue(object : Callback<GetPostsResponse> {
                override fun onFailure(call: Call<GetPostsResponse>, t: Throwable) {
                    callback.onFailure(t, t.message)
                }

                override fun onResponse(call: Call<GetPostsResponse>, response: Response<GetPostsResponse>) {
                    if (response.body() != null) {
                        callback.onSuccess(response.body()!!.posts)
                    } else {
                        callback.onFailure(null, null)
                    }
                }

            })
        }
    }

    override fun getTypeAheadHashTagSuggestion(input: String, callback: PiquedCloudCallback<List<TwitterHashTag>>) {

        val call = twitterAPI.getHashtagSuggestions(queryString = "%23$input")

        call.enqueue(object : Callback<RawTwitterResponse> {
            override fun onFailure(call: Call<RawTwitterResponse>, t: Throwable) {
                callback.onFailure(t, t.message)
            }

            override fun onResponse(call: Call<RawTwitterResponse>, response: Response<RawTwitterResponse>) {
                if (response.body() != null) {
                    callback.onSuccess(response.body()!!.hashtags)
                } else {
                    callback.onFailure(null, null)
                }
            }

        })
    }

    override fun matchYelpVenue(apiKey: String, venue: Venue, callback: PiquedCloudCallback<YelpMatchBusiness?>) {

        val call = yelpAPI.matchBusinessOnYelp(
                apiKey,
                cleanUpString(venue.venueName),
                cleanUpAddress(venue.address),
                cleanUpString(venue.city),
                venue.state ?: "",
                venue.countryShort ?: "",
                venue.location.latitude,
                venue.location.longitude)

        call.enqueue(object : Callback<YelpResultList> {
            override fun onFailure(call: Call<YelpResultList>, t: Throwable) {
                callback.onFailure(t, t.message)
            }

            override fun onResponse(call: Call<YelpResultList>, response: Response<YelpResultList>) {
                if (response.body() != null) {
                    val resultList = response.body()!!

                    if (resultList.businesses.isNotEmpty()) {

                        callback.onSuccess(selectVenue(venue, resultList.businesses))
                    } else {
                        callback.onSuccess(null)
                    }
                } else {
                    callback.onFailure(null, null)
                }
            }
        })
    }

    private fun selectVenue(target: Venue, options: List<YelpMatchBusiness>): YelpMatchBusiness? {

        val titleArray = cleanUpString(target.venueName).split(" ")
        val minCount = Math.min(titleArray.size / 2 + 1, 4);

        for (business in options) {
            var hitCount = 0;
            val cleanUpBusinessName = cleanUpString(business.venueName).split(" ")

            for (subString in titleArray) {
                if (cleanUpBusinessName.contains(subString)) {
                    hitCount ++
                }
            }

            if (hitCount >= minCount) {
                return business
            }
        }

        return null;
    }

    private fun cleanUpAddress(input: String?): String {
        return if (input.isNullOrEmpty()) {
            ""
        } else {
            input.replace("\\(.*\\)\\s?".toRegex(), "").toLowerCase()
        }
    }

    private fun cleanUpString(input: String?): String {

        return if (input.isNullOrEmpty()) {
            ""
        } else {
            input.replace("\\(.*\\)\\s?".toRegex(), "").replace("[^a-zA-Z0-9\\s]".toRegex(), "").toLowerCase()
        }
    }

    private fun cleanUpStringWithMax(input: String?, maxWords: Int): String {
        return if (input.isNullOrEmpty()) {
            ""
        } else {
            val newString = cleanUpString(input)
            val newStringSplit = newString.split(" ")
            if (newStringSplit.size <= maxWords) {
                newString
            } else {
                val sb = StringBuilder()
                for (i in 0 until maxWords) {
                    sb.append(newStringSplit[i])
                    sb.append(" ")
                }

                sb.toString()
            }
        }
    }

    override fun getYelpVenue(apiKey: String, yelpId: String, callback: PiquedCloudCallback<YelpBusiness>) {

        val call = yelpAPI.getYelpBusiness(apiKey, yelpId)

        call.enqueue(object : Callback<YelpBusiness> {
            override fun onFailure(call: Call<YelpBusiness>, t: Throwable) {
                callback.onFailure(t, t.message)
            }

            override fun onResponse(call: Call<YelpBusiness>, response: Response<YelpBusiness>) {
                callback.onSuccess(response.body())
            }

        })
    }

    override fun getFourSquareVenue(clientId: String, clientSecret: String, fourSquareId: String, version: String, callback: PiquedCloudCallback<FourSquareVenue>) {

        val call = fourSquareAPI.getVenueById(fourSquareId, clientId, clientSecret, version)

        call.enqueue(object : Callback<FourSquareVenueDetailResponse> {
            override fun onFailure(call: Call<FourSquareVenueDetailResponse>, t: Throwable) {
                callback.onFailure(t, t.message)
            }

            override fun onResponse(call: Call<FourSquareVenueDetailResponse>, response: Response<FourSquareVenueDetailResponse>) {
                callback.onSuccess(response.body()?.venue)
            }

        })
    }

    override fun searchFourSquareVenue(latitude: Double, longitude: Double, callback: PiquedCloudCallback<FourSquareVenue>) {

        val call = fourSquareAPI.searchByLatLng("$latitude,$longitude")

        call.enqueue(object : Callback<FourSquareVenueDetailResponse> {
            override fun onFailure(call: Call<FourSquareVenueDetailResponse>, t: Throwable) {
                callback.onFailure(t, t.message)
            }

            override fun onResponse(call: Call<FourSquareVenueDetailResponse>, response: Response<FourSquareVenueDetailResponse>) {
                callback.onSuccess(response.body()?.venue)
            }

        })
    }
}