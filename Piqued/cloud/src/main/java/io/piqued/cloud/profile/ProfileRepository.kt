package io.piqued.cloud.profile

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import io.piqued.cloud.PiquedCloud
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.retrofit.ApiResponse
import io.piqued.cloud.retrofit.AppExecutors
import io.piqued.cloud.retrofit.NetworkBoundResource
import io.piqued.cloud.util.Resource
import io.piqued.database.bookmark.Bookmark
import io.piqued.database.post.CloudPost
import io.piqued.database.post.PostDatabase

class ProfileRepository(
        private val cloud: PiquedCloud,
        private val postDatabase: PostDatabase,
        private val appExecutors: AppExecutors
) {
    fun getBookmarkPosts(): LiveData<Resource<List<CloudPost>>> {

        return object : NetworkBoundResource<List<CloudPost>, List<CloudPost>>(appExecutors) {
            override fun saveCallResult(item: List<CloudPost>) {

                updateBookmarks(item)
            }

            override fun shouldFetch(data: List<CloudPost>?): Boolean {
                return cloud.isUserLoggedIn()
            }

            override fun loadFromDb(): LiveData<List<CloudPost>> {
                return postDatabase.bookmarkDao.getAllBookmarksAsPost()
            }

            override fun createCall(): LiveData<ApiResponse<List<CloudPost>>> {
                return cloud.getBookmarksAsLiveData()
            }

        }.asLiveData()
    }

    fun getBookmarks(): LiveData<Resource<List<Bookmark>>> {

        return object : NetworkBoundResource<List<Bookmark>, List<CloudPost>>(appExecutors) {
            override fun saveCallResult(item: List<CloudPost>) {

                updateBookmarks(item)
            }

            override fun shouldFetch(data: List<Bookmark>?): Boolean {
                return cloud.isUserLoggedIn()
            }

            override fun loadFromDb(): LiveData<List<Bookmark>> {
                return postDatabase.bookmarkDao.getAllBookmarks()
            }

            override fun createCall(): LiveData<ApiResponse<List<CloudPost>>> {
                return cloud.getBookmarksAsLiveData()
            }

        }.asLiveData()
    }

    fun refreshBookmarks() {

        cloud.refreshBookmarks(object : PiquedCloudCallback<List<CloudPost>> {
            override fun onSuccess(result: List<CloudPost>?) {
                if (result != null) {

                    AsyncTask.execute {
                        updateBookmarks(result)
                    }
                }
            }

            override fun onFailure(error: Throwable?, message: String?) {
                // oh well...
            }

        })
    }

    private fun updateBookmarks(posts: List<CloudPost>) {

        postDatabase.addPostsToCache(posts)

        val bookmarks = ArrayList<Bookmark>()

        for (post: CloudPost in posts) {
            bookmarks.add(Bookmark(post.postId))
        }

        postDatabase.bookmarkDao.reset()
        postDatabase.bookmarkDao.insertBookmarks(bookmarks)
    }

    fun bookmarkPost(postId: Long, bookmarked: Boolean, callback: PiquedCloudCallback<Long>) {
        cloud.bookmarkPost(postId, bookmarked, object : PiquedCloudCallback<Long> {
            override fun onSuccess(result: Long?) {

                AsyncTask.execute {

                    val post = postDatabase.getPost(postId)
                    post.bookmarked = bookmarked

                    if (post.bookmarked) {
                        post.totalBookmarks += 1
                    } else {
                        post.totalBookmarks -= 1
                    }

                    postDatabase.addPostToCache(post)

                    if (bookmarked) {
                        postDatabase.bookmarkDao.insertBookmark(Bookmark(postId))
                    } else {
                        postDatabase.bookmarkDao.delete(Bookmark(postId))
                    }
                }


                callback.onSuccess(result)
            }

            override fun onFailure(error: Throwable?, message: String?) {
                callback.onFailure(error, message)
            }

        })
    }
}