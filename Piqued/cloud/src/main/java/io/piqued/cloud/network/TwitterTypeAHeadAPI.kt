package io.piqued.cloud.network

import io.piqued.cloud.model.twitter.RawTwitterResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *
 * Created by Kenny M. Liou on 8/26/18.
 * Piqued Inc.
 *
 */

interface TwitterTypeAHeadAPI {

    @GET("/i/search/typeahead.json")
    fun getHashtagSuggestions(@Query("count") count: Int = 10,
                              @Query("filters") filters: Boolean = true,
                              @Query("q") queryString: String,
                              @Query("result_type") resultType: String = "hashtags",
                              @Query("src") source: String = "COMPOSE"
    ): Call<RawTwitterResponse>

}