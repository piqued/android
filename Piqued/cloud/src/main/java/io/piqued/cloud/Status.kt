package io.piqued.cloud

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}