package io.piqued.cloud.model.piqued

/**
 *
 * Created by Kenny M. Liou on 11/4/18.
 * Piqued Inc.
 *
 */
class ImageContainer(
        val imageUrl: String,
        val image: ByteArray) {
    val sizeInBytes: Long = image.size.toLong()
}