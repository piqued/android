package io.piqued.cloud.model.piqued

import com.google.gson.annotations.SerializedName
import io.piqued.database.user.User

/**
 *
 * Created by Kenny M. Liou on 11/11/18.
 * Piqued Inc.
 *
 */
class CloudFollowingUser(
    @SerializedName("id")
    val userId: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("total_posts")
    val totalPostCount: Int,
    @SerializedName("followers")
    val followerCount: Int,
    @SerializedName("following")
    val followingCount: Int,
    @SerializedName("followed")
    var isFollowed: Boolean,
    @SerializedName("totalBookmarks")
    val bookmarkCount: Int,
    @SerializedName("totalReactions")
    val likeCount: Int,
    @SerializedName("totalComments")
    val commentCount: Int,
    @SerializedName("profileImage")
    val profileImageUrl: String?,
    @SerializedName("createdAt")
    val createdAt: Long
) {

    fun toUser(): User {

        return User(userId,
                name,
                totalPostCount,
                followerCount,
                followingCount,
                isFollowed,
                bookmarkCount,
                likeCount,
                commentCount,
                profileImageUrl,
                createdAt,
                "")
    }
}