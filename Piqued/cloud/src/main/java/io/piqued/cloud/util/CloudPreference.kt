package io.piqued.cloud.util

import android.content.Context
import android.os.Build
import android.text.TextUtils
import com.google.gson.Gson
import io.piqued.cloud.BuildConfig
import io.piqued.cloud.model.piqued.PiquedToken

class CloudPreference(context: Context) {

    companion object {
        private const val KEY_VERSION = "piqued_cloud_version"
        private const val KEY_PREFERENCE = "cloud_preference"
        private const val KEY_SESSION_TOKEN = "session_token"
        private const val KEY_HOST_URL = "host_url"
        private const val KEY_SHARE_URL = "share_url"
        private const val KEY_FOUR_SQUARE_HOST_URL = "fourSquare_host_url"
    }

    private val preference = context.getSharedPreferences(KEY_PREFERENCE, Context.MODE_PRIVATE)
    private var userToken: PiquedToken? = null

    init {
        val shouldUpdateCache = shouldUpdateCache(context)
        if (shouldUpdateCache) {
            setHostUrl(BuildConfig.PIQUED_HOST_URL)
        }
        if (shouldUpdateCache) {
            setShareHostUrl(BuildConfig.PIQUED_SHARE_HOST_URL)
        }
        if (shouldUpdateCache) {
            setFourSquareHostUrl(BuildConfig.DEFAULT_FOURSQUARE_HOST_URL)
        }
        if (shouldUpdateCache) {
            preference.edit().putLong(KEY_VERSION, Build.VERSION.SDK_INT.toLong()).apply()
        }
    }

    fun getHostUrl(): String {
        val hostUrl = preference.getString(KEY_HOST_URL, null)
        if (TextUtils.isEmpty(hostUrl)) {
            setHostUrl(BuildConfig.PIQUED_HOST_URL)
        }
        return preference.getString(KEY_HOST_URL, "") ?: ""
    }

    fun setHostUrl(hostUrl: String) {
        if (hostUrl.isNotEmpty()) {
            preference.edit()
                    .putString(KEY_HOST_URL, hostUrl)
                    .apply()
        }
    }

    fun getShareHostUrl(): String {
        val shareUrl = preference.getString(KEY_SHARE_URL, null)
        if (TextUtils.isEmpty(shareUrl)) {
            setShareHostUrl(BuildConfig.PIQUED_SHARE_HOST_URL)
        }
        return preference.getString(KEY_SHARE_URL, "") ?: ""
    }

    fun setShareHostUrl(hostUrl: String) {
        if (hostUrl.isNotEmpty()) {
            preference.edit()
                    .putString(KEY_SHARE_URL, hostUrl)
                    .apply()
        }
    }

    fun getFourSquareHostUrl(): String {
        val fourSquareUrl = preference.getString(KEY_FOUR_SQUARE_HOST_URL, null)
        if (TextUtils.isEmpty(fourSquareUrl)) {
            setFourSquareHostUrl(BuildConfig.DEFAULT_FOURSQUARE_HOST_URL)
        }
        return preference.getString(KEY_FOUR_SQUARE_HOST_URL, "") ?: ""
    }

    fun setFourSquareHostUrl(hostUrl: String) {
        if (hostUrl.isNotEmpty()) {
            preference.edit().putString(KEY_FOUR_SQUARE_HOST_URL, hostUrl).apply()
        }
    }

    fun setToken(token: PiquedToken) {
        preference
                .edit()
                .putString(KEY_SESSION_TOKEN, Gson().toJson(token))
                .apply()

        userToken = token
    }

    fun getToken(): PiquedToken? {
        if (!isUserLoggedIn()) return null

        if (userToken == null) {
            userToken = Gson().fromJson(preference.getString(KEY_SESSION_TOKEN, null), PiquedToken::class.java)
        }

        return userToken
    }

    fun isUserLoggedIn(): Boolean {
        return preference.getString(KEY_SESSION_TOKEN, null) != null
    }

    fun reset() {
        preference.edit().clear().apply()
    }

    private fun shouldUpdateCache(c: Context): Boolean {
        val packageInfo = c.packageManager.getPackageInfo(c.packageName, 0)
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            packageInfo.longVersionCode > preference.getLong(KEY_VERSION, -1)
        } else {
            packageInfo.versionCode > preference.getLong(KEY_VERSION, -1)
        }
    }
}