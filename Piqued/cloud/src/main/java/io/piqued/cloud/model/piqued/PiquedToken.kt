package io.piqued.cloud.model.piqued

import com.google.gson.annotations.SerializedName

data class PiquedToken(
        @SerializedName("user_id") val userId: Int,
        @SerializedName("site") val site: String,
        @SerializedName("token_id") val token: String,
        @SerializedName("secret") val secret: String
)