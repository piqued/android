package io.piqued.cloud.repository

import io.piqued.cloud.PiquedCloud
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.PiquedRepository
import io.piqued.cloud.model.piqued.Venue
import io.piqued.cloud.model.yelp.YelpBusiness
import io.piqued.cloud.model.yelp.YelpMatchBusiness

/**
 *
 * Created by Kenny M. Liou on 8/26/18.
 * Piqued Inc.
 *
 */

class YelpRepository(
        private val cloud: PiquedCloud,
        apiToken: String
): PiquedRepository {
    override fun reset() {
        // do nothing
    }

    private val authToken = "Bearer $apiToken"

    fun matchYelpBusiness(venue : Venue, callback: PiquedCloudCallback<YelpMatchBusiness?>) {

        cloud.matchYelpVenue(authToken, venue, callback)
    }

    fun getBusinessById(yelpId: String, callback: PiquedCloudCallback<YelpBusiness>) {

        cloud.getYelpVenue(authToken, yelpId, callback)
    }
}