package io.piqued.cloud.util

import io.piqued.cloud.model.piqued.BasketData
import io.piqued.cloud.model.piqued.CloudImageUploadContainer
import io.piqued.cloud.model.piqued.UserToken
import org.json.JSONArray
import org.json.JSONObject

/**
 *
 * Created by Kenny M. Liou on 11/9/18.
 * Piqued Inc.
 *
 */
class PayloadHelper {
    companion object {

        fun addCredObject(wrapperObject: JSONObject, userToken: UserToken) {
            val credObject = JSONObject()
            credObject.put(FIELD_CRED_TOKEN, userToken.token)
            credObject.put(FIELD_CRED_SECRET, userToken.secret)

            wrapperObject.put(FIELD_CREDS, credObject)
        }

        fun createImageObject(detail: ImageDetail): JSONObject {

            val imageDetails = JSONObject()
            val metadata = JSONObject()

            // setup metadata
            metadata.put("filename", detail.metaData.fileName)
            metadata.put("mime_type", detail.metaData.mimeType)
            metadata.put("size", detail.metaData.imageSize)
            imageDetails.put("id", detail.imageId)
            imageDetails.put("storage", detail.storage)
            imageDetails.put("metadata", metadata)

            return imageDetails
        }



        fun createNewBasketObject(
                imageList: List<CloudImageUploadContainer>,
                newBasket: BasketData,
                userToken: UserToken): JSONObject {

            val newBasketObject = JSONObject()
            addCredObject(newBasketObject, userToken)

            // create images object
            val images = JSONArray()

            for (container: CloudImageUploadContainer in imageList) {
                val image = JSONObject()
                image.put("lg", createImageObject(container.largeImageUrl))
                image.put("sm", createImageObject(container.smallImageUrl))
                images.put(image)
            }


            // create basket object
            val basketObject = JSONObject()
            basketObject.put(FIELD_LOCATION_LAT, newBasket.latitude)
            basketObject.put(FIELD_LOCATION_LNG, newBasket.longitude)
            basketObject.put(FIELD_TITLE, newBasket.title)
            basketObject.put(FIELD_LOCATION_NAME, newBasket.title)
            basketObject.put(FIELD_DESCRIPTION, newBasket.description)
            basketObject.put(FIELD_ADDRESS, newBasket.fullAddress)
            basketObject.put(FIELD_FOURSQUARE_VENUE_ID, newBasket.fourSquareVenueId)
            basketObject.put(FIELD_FOURSQUARE_CC, newBasket.countryCode)
            basketObject.put(FIELD_FOURSQUARE_CITY, newBasket.city)
            basketObject.put(FIELD_FOURSQUARE_STATE, newBasket.state)
            basketObject.put(FIELD_YELP_ID, newBasket.yelpId)
            basketObject.put(FIELD_IMAGES, images)
            // basketObject.put(FIELD_PRIMARY_COLOR, newBasket.primaryColor)

            newBasketObject.put(FIELD_POST, basketObject)

            return newBasketObject
        }
    }
}