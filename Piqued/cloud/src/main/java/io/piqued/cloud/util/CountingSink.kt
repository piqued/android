package io.piqued.cloud.util

import okio.Buffer
import okio.ForwardingSink
import okio.Sink

internal class CountingSink(
        private val listener: ImageUploadProgressListener?,
        private val imageUrl: String,
        private val contentLength: Long,
        delegate: Sink)
    : ForwardingSink(delegate) {

    private var bytesWritten = 0L

    override fun write(source: Buffer, byteCount: Long) {
        super.write(source, byteCount)

        bytesWritten += byteCount
        listener?.onRequestProgress(imageUrl, bytesWritten, contentLength)
    }
}