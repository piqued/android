package io.piqued.cloud.model.piqued;

import android.content.Context;
import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import io.piqued.cloud.model.foursquare.FourSquareVenue;
import io.piqued.cloud.model.yelp.YelpBusiness;


/**
 * Project: Piqued
 * Created by Kenny M. Liou on 3/6/17.
 * Piqued Inc.
 */

public class Venue {

    public static Venue fromFourSquare(Context context, FourSquareVenue fsVenue) {
        return new Venue(context, fsVenue);
    }

    public static Venue fromYelp(Context context, YelpBusiness yelpBusiness) {
        return new Venue(context, yelpBusiness);
    }

    private final String mVenueId;
    private final String mVenueName;
    private final String mAddress;
    private final String mFullAddress;
    private final LatLng mLocation;
    private final boolean mHasHourData;
    private final boolean mOpenNow;
    private final String mPhoneNumber;
    private final int mPriceTier;
    private final String mFoursquareUrl;
    private final String mVenueUrl;
    private final String mOpenStatus;
    private final String mCountry;
    private final String mCC;
    private final String mState;
    private final String mCity;
    private final List<VenueTip> mTips = new ArrayList<>();

    private Venue(Context context, YelpBusiness yelpBusiness) {
        mVenueId = yelpBusiness.getYelpId();
        mVenueName = yelpBusiness.getBusinessName();
        mFullAddress = TextUtils.join(", ", yelpBusiness.getLocation().getDisplayAddress());
        if (yelpBusiness.getLocation().getDisplayAddress().size() > 0) {
            mAddress = yelpBusiness.getLocation().getDisplayAddress().get(0);
        } else {
            mAddress = "";
        }
        mLocation = new LatLng(yelpBusiness.getCoordinate().getLatitude(), yelpBusiness.getCoordinate().getLongitude());
        if (yelpBusiness.getOpenHours().size() > 0) {
            mHasHourData = true;
            mOpenNow = yelpBusiness.getOpenHours().get(0).isOpenNow();
        } else {
            mHasHourData = false;
            mOpenNow = false;
        }

        mPhoneNumber = yelpBusiness.getPhoneNumber();
        mPriceTier = yelpBusiness.getPrice().length();
        mFoursquareUrl = "";
        mVenueUrl = yelpBusiness.getBusinessUrl();
        mOpenStatus = ""; //TODO fix me
        mCountry = "";
        mCC = yelpBusiness.getLocation().getCountryCode();
        mState = yelpBusiness.getLocation().getState();
        mCity = yelpBusiness.getLocation().getCity();
    }

    private Venue(Context context, FourSquareVenue fsVenue) {

        mVenueId = fsVenue.id;
        mVenueName = fsVenue.name;

        mLocation = new LatLng(fsVenue.location.lat, fsVenue.location.lng);

        mPhoneNumber = fsVenue.contact.formattedPhone;

        if (fsVenue.location.formattedAddress != null) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < fsVenue.location.formattedAddress.length; i++) {
                String address = fsVenue.location.formattedAddress[i];
                String[] addressSplit = address.split(",");
                if (i == 0) {
                    sb.append(addressSplit[0]);
                } else {
                    sb.append(address);
                }
                sb.append(", ");
            }

            mFullAddress = sb.toString();
        } else {
            mFullAddress = (fsVenue.location.address != null) ? fsVenue.location.address.split(",")[0] : "";
        }
        mAddress = (fsVenue.location.address != null) ? fsVenue.location.address.split(",")[0] : "";
        mCountry = fsVenue.location.country;
        mCC = fsVenue.location.cc;
        mState = fsVenue.location.state;
        mCity = fsVenue.location.city;

        if (fsVenue.price != null) {
            mPriceTier = fsVenue.price.tier;
        } else {
            mPriceTier = -1;
        }

        mFoursquareUrl = fsVenue.canonicalUrl;
        mVenueUrl = fsVenue.url;

        if (fsVenue.hours != null) {
            mHasHourData = true;
            mOpenNow = fsVenue.hours.isOpen;
            mOpenStatus = fsVenue.hours.status;
        } else if (fsVenue.popular != null) {
            mHasHourData = true;
            mOpenNow = fsVenue.popular.isOpen;
            mOpenStatus = getOpenStatus(fsVenue.popular.timeframes);
        } else {
            mHasHourData = false;
            mOpenNow = false;
            mOpenStatus = null;
        }

        if (fsVenue.tips != null && fsVenue.tips.count > 0) {
            for (FourSquareVenue.TipGroup group : fsVenue.tips.groups) {
                for (FourSquareVenue.Tip tip : group.items) {
                    if (tip.user != null) {
                        mTips.add(VenueTip.fromFourSquare(context, tip));
                    }
                }
            }
        }

    }

    private String getOpenStatus(FourSquareVenue.TimeFrame[] timeFrames) {

        for (FourSquareVenue.TimeFrame timeFrame : timeFrames) {
            if (timeFrame.includesToday) {
                StringBuilder sb = new StringBuilder(timeFrame.days);
                sb.append(": ");
                sb.append(timeFrame.open[0].renderedTime);

                return sb.toString();
            }
        }

        return null;
    }

    public String getVenueId() {
        return mVenueId;
    }

    public String getVenueName() {
        return mVenueName;
    }

    public String getFullAddress() {
        return mFullAddress;
    }

    public String getAddress() {
        return mAddress;
    }

    public LatLng getLocation() {
        return mLocation;
    }

    public boolean isOpenNow() {
        return mOpenNow;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public int getPriceTier() {
        return mPriceTier;
    }

    public boolean hasHourData() {
        return mHasHourData;
    }

    public String getFoursquareUrl() {
        return mFoursquareUrl;
    }

    public String getOpenStatus() {
        return mOpenStatus;
    }

    public int getTipCount() {
        return mTips.size();
    }

    public String getCountry() {
        return mCountry;
    }

    public String getCountryShort() {
        return mCC;
    }

    public String getCountryCode() {
        return mCC;
    }

    public String getState() {
        return mState;
    }

    public String getCity() {
        return mCity;
    }

    public VenueTip getTip(int index) {
        if (index > -1 && index < mTips.size()) {
            return mTips.get(index);
        }

        return null;
    }
}
