package io.piqued.cloud.util

import android.location.Location

import com.google.android.gms.maps.model.LatLng


/**
 * Project: Piqued
 * Created by Kenny M. Liou on 1/2/17.
 * Piqued Inc.
 */

class RegionBounds(val latMin: Double, val latMax: Double, val lngMin: Double, val lngMax: Double) {
    companion object {

        fun fromLocation(centerLocation: Location, distanceInMeter: Int): RegionBounds {

            val center = LatLng(centerLocation.latitude, centerLocation.longitude)

            val southwest = SphericalUtil.computeOffset(center, distanceInMeter * Math.sqrt(2.0), 225.0)
            val northeast = SphericalUtil.computeOffset(center, distanceInMeter * Math.sqrt(2.0), 45.0)

            return RegionBounds(southwest.latitude, northeast.latitude, southwest.longitude, northeast.longitude)
        }

        @JvmStatic
        fun createRegionBound(northEast: LatLng, southWest: LatLng): RegionBounds {

            return RegionBounds(southWest.latitude, northEast.latitude, southWest.longitude, northEast.longitude)
        }
    }
}
