package io.piqued.cloud

import androidx.lifecycle.LiveData
import io.piqued.cloud.model.foursquare.FourSquareVenue
import io.piqued.cloud.model.piqued.*
import io.piqued.cloud.model.twitter.TwitterHashTag
import io.piqued.cloud.model.yelp.YelpBusiness
import io.piqued.cloud.model.yelp.YelpMatchBusiness
import io.piqued.cloud.retrofit.ApiResponse
import io.piqued.cloud.util.ImageUploadProgressListener
import io.piqued.cloud.util.RegionBounds
import io.piqued.cloud.util.ResetPasswordState
import io.piqued.database.UserId
import io.piqued.database.event.Event
import io.piqued.database.post.CloudPost
import io.piqued.database.post.GetPostsResponse
import io.piqued.database.user.User
import okhttp3.ResponseBody

/**
 *
 * Created by Kenny M. Liou on 7/21/18.
 * Piqued Inc.
 *
 */
interface PiquedCloud {

    fun updateHostUrl(hostUrl: String)

    fun updateFourSquareHostUrl(hostUrl: String)

    fun performHealthCheck(callback: PiquedCloudCallback<Echo?>)

    fun getBookmarksAsLiveData(): LiveData<ApiResponse<List<CloudPost>>>

    fun refreshBookmarks(callback: PiquedCloudCallback<List<CloudPost>>)

    fun bookmarkPost(postId: Long, bookmarked: Boolean, callback: PiquedCloudCallback<Long>)

    fun getPostsBasedOnLocation(latMin: Double, latMax: Double, lngMin: Double, lngMax: Double, page: Int): LiveData<ApiResponse<GetPostsResponse>>

    fun getPostsBasedOnLocation(latMin: Double, latMax: Double, lngMin: Double, lngMax: Double, page: Int, callback: PiquedCloudCallback<List<CloudPost>>)

    fun getPostsBasedOnLocationOnMap(regionBounds: RegionBounds, page: Int, callback: PiquedCloudCallback<List<CloudPost>>)

    // Profile
    fun getUserPostHistory(userId: UserId, page: Int, batchSize: Int): LiveData<ApiResponse<GetPostsResponse>>

    fun getUserPostHistory(userId: UserId, page: Int, batchSize: Int, callback: PiquedCloudCallback<List<CloudPost>>)

    fun getAccountOwnerPostHistory(page: Int, batchSize: Int): LiveData<ApiResponse<GetPostsResponse>>

    fun getAccountOwnerPostHistory(page: Int, batchSize: Int, callback: PiquedCloudCallback<List<CloudPost>>)

    fun getFollowingUser(accountOwnerId: Long): LiveData<ApiResponse<ResponseBody>>

    fun getFollowingUser(accountOwnerId: Long, callback: PiquedCloudCallback<ResponseBody>)

    // User

    fun isUserLoggedIn(): Boolean

    fun getUserInfo(userId: Long): LiveData<ApiResponse<User>>

    fun resetPassword(email: String, callback: PiquedCloudCallback<ResetPasswordState>)

    fun getEventsAsLiveData(page: Int, batchSize: Int): LiveData<ApiResponse<List<Event>>>

    fun getEvents(page: Int, batchSize: Int, callback: PiquedCloudCallback<List<Event>>)

    fun getEventsSync(page: Int, batchSize: Int): List<Event>?

    // Upload Images

    fun getPreSign(): PreSignObject

    fun uploadImageToPreSign(preSign: PreSignObject,
                             imageUrl: String,
                             imageBase64: ImageContainer,
                             listener: ImageUploadProgressListener?): CloudImageUploadResult?

    fun createNewBasket(images: List<CloudImageUploadContainer>,
                        basketData: BasketData,
                        userToken: UserToken): Boolean

    // Twitter
    fun getTypeAheadHashTagSuggestion(input: String, callback: PiquedCloudCallback<List<TwitterHashTag>>)

    // Yelp
    fun matchYelpVenue(apiKey: String, venue: Venue, callback: PiquedCloudCallback<YelpMatchBusiness?>)

    fun getYelpVenue(apiKey: String, yelpId: String, callback: PiquedCloudCallback<YelpBusiness>)

    // Four Square
    fun getFourSquareVenue(clientId: String, clientSecret: String, fourSquareId: String, version: String, callback: PiquedCloudCallback<FourSquareVenue>)

    fun searchFourSquareVenue(latitude: Double, longitude: Double, callback: PiquedCloudCallback<FourSquareVenue>)
}