package io.piqued.cloud.util

import com.google.gson.annotations.SerializedName

/**
 *
 * Created by Kenny M. Liou on 11/9/18.
 * Piqued Inc.
 *
 */
class ImageMetaData {
    @SerializedName("filename")
    var fileName = ""
    @SerializedName("mime_type")
    val mimeType = "image/jpeg"
    @SerializedName("size")
    var imageSize = 0L
}