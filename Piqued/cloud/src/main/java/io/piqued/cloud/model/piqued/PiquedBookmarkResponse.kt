package io.piqued.cloud.model.piqued

class PiquedBookmarkResponse {

    var isSuccess: Boolean = false
        internal set
    internal var status: String? = null

    val isBookmarked: Boolean
        get() = "added" == status
}
