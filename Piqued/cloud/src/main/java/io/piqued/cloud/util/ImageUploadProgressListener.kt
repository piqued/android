package io.piqued.cloud.util

interface ImageUploadProgressListener {

    fun onRequestProgress(imageUrl: String, bytesWritten: Long, contentLength: Long)
}