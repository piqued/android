package io.piqued.cloud.network

import io.piqued.cloud.model.yelp.YelpBusiness
import io.piqued.cloud.model.yelp.YelpResultList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

/**
 *
 * Created by Kenny M. Liou on 8/26/18.
 * Piqued Inc.
 *
 */

interface YelpV3API {

    @GET("/v3/businesses/matches")
    fun matchBusinessOnYelp(
            @Header("Authorization") authAPIKey: String,
            @Query("name") venueName: String,
            @Query("address1") mainAddress: String,
            @Query("city") city: String,
            @Query("state") state: String,
            @Query("country") country: String,
            @Query("latitude") latitude: Double,
            @Query("longitude") longitude: Double
    ): Call<YelpResultList>

    @GET("/v3/businesses/{yelpId}")
    fun getYelpBusiness(
            @Header("Authorization") authAPIKey: String,
            @Path("yelpId") yelpId: String
    ): Call<YelpBusiness>
}