package io.piqued.cloud.model.piqued

import com.google.gson.annotations.SerializedName

/**
 *
 * Created by Kenny M. Liou on 11/4/18.
 * Piqued Inc.
 *
 */
class PreSignObject(
        @SerializedName("url")
        val url: String,
        @SerializedName("fields")
        val fields: Map<String, String>
)