package io.piqued.cloud.repository

import androidx.lifecycle.LiveData
import com.google.gson.Gson
import io.piqued.cloud.PiquedCloud
import io.piqued.cloud.PiquedCloudCallback
import io.piqued.cloud.PiquedRepository
import io.piqued.cloud.model.piqued.CloudFollowingUser
import io.piqued.cloud.retrofit.ApiResponse
import io.piqued.cloud.retrofit.AppExecutors
import io.piqued.cloud.retrofit.NetworkBoundResource
import io.piqued.cloud.util.ResetPasswordState
import io.piqued.cloud.util.Resource
import io.piqued.database.user.User
import io.piqued.database.user.UserDatabase
import okhttp3.ResponseBody
import org.json.JSONObject

/**
 *
 * Created by Kenny M. Liou on 9/8/18.
 * Piqued Inc.
 *
 */
class UserRepository(
        private val cloud: PiquedCloud,
        private val userDatabase: UserDatabase,
        private val appExecutors: AppExecutors
): PiquedRepository {
    override fun reset() {
        userDatabase.reset()
    }

    fun resetPassword(email: String, callback: PiquedCloudCallback<ResetPasswordState>) {
        cloud.resetPassword(email, callback)
    }

    fun getUserFollowing(userId: Long): LiveData<Resource<List<User>>> {

        return object: NetworkBoundResource<List<User>, ResponseBody>(appExecutors) {
            override fun saveCallResult(item: ResponseBody) {
               userDatabase.updateUserFollowing(responseBodyToFollowingUserList(item))
            }

            override fun shouldFetch(data: List<User>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<User>> {
                return userDatabase.getFollowingUsers()
            }

            override fun createCall(): LiveData<ApiResponse<ResponseBody>> {
                return cloud.getFollowingUser(userId)
            }

        }.asLiveData()
    }

    fun getUserFollowing(userId: Long, callback: PiquedCloudCallback<List<User>>) {

        cloud.getFollowingUser(userId, object : PiquedCloudCallback<ResponseBody> {
            override fun onSuccess(result: ResponseBody?) {
                if (result != null) {
                    callback.onSuccess(responseBodyToFollowingUserList(result))
                } else {
                    callback.onSuccess(null)
                }
            }

            override fun onFailure(error: Throwable?, message: String?) {
                callback.onFailure(error, message)
            }

        })
    }

    fun getUserInfoCachePriority(userId: Long): LiveData<Resource<User>> {

        return object : NetworkBoundResource<User, User>(appExecutors) {
            override fun saveCallResult(item: User) {
                return userDatabase.updateUser(item)
            }

            override fun shouldFetch(data: User?): Boolean {
                return data == null
            }

            override fun loadFromDb(): LiveData<User> {
                return userDatabase.getUser(userId)
            }

            override fun createCall(): LiveData<ApiResponse<User>> {
                return cloud.getUserInfo(userId)
            }

        }.asLiveData()
    }

    private fun responseBodyToFollowingUserList(responseBody: ResponseBody): List<User> {

        val result = ArrayList<User>()

        val jsonObject = JSONObject(responseBody.string())

        val userArray = jsonObject.getJSONArray("followings")

        if (userArray.length() > 0) {
            val gson = Gson()

            for (i in 0..(userArray.length() - 1)) {
                val userObject = userArray[i] as JSONObject

                val user = gson.fromJson<CloudFollowingUser>(userObject.getJSONObject("user").toString(), CloudFollowingUser::class.java)

                result.add(user.toUser())
            }
        }


        return result
    }
}