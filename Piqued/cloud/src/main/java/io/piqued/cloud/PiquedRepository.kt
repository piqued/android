package io.piqued.cloud

/**
 *
 * Created by Kenny M. Liou on 9/1/18.
 * Piqued Inc.
 *
 */
interface PiquedRepository {

    fun reset()
}