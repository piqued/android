package io.piqued.cloud.util

import com.google.gson.annotations.SerializedName
import io.piqued.cloud.model.piqued.CloudImageUploadResult
import io.piqued.cloud.model.piqued.ImageUploadType
import java.util.*

/**
 *
 * Created by Kenny M. Liou on 11/9/18.
 * Piqued Inc.
 *
 */
class ImageDetail(val type: ImageUploadType) {
    @SerializedName("id")
    internal var imageId = ""
    @SerializedName("storage")
    internal var storage = ""
    @SerializedName("metadata")
    internal val metaData = ImageMetaData()

    fun setImageUploadResult(imageUploadResult: CloudImageUploadResult) {

        metaData.fileName = "${type.name}_${Date().time}_${imageUploadResult.fileName}.jpg"
        metaData.imageSize = imageUploadResult.sizeInBytes
        imageId = imageUploadResult.id
        storage = imageUploadResult.storage
    }
}