package io.piqued.cloud.util

import androidx.annotation.StringRes
import io.piqued.cloud.R

/**
 *
 * Created by Kenny M. Liou on 2019-01-12.
 * Piqued Inc.
 *
 */

enum class ResetPasswordState(@StringRes val stringRes: Int) {

    LIMIT_EXCEEDED(R.string.request_exceeded_limits),
    EMAIL_NOT_FOUND(R.string.email_not_found),
    EMAIL_SENT(R.string.recovery_email_sent),
    ERROR(R.string.email_recovery_internet_error)
}
